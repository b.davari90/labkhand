import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'CounsalterFollowup2.dart';
import 'config.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';
import 'package:counsalter5/ModelBloc/CounsalterFollowModel.dart';
class CounsalterFollowUpProgramComponent extends StatefulWidget {
  bool isSelected = false;
  int index;
  CounsalterFollowModel model;
  CounsalterFollowUpProgramComponent(
      {this.isSelected,this.index,this.model});

  @override
  _CounsalterFollowUpProgramComponentState createState() =>
      _CounsalterFollowUpProgramComponentState();
}

class _CounsalterFollowUpProgramComponentState
    extends State<CounsalterFollowUpProgramComponent> {
  List colors = [Colors.greenAccent.withOpacity(0.5),Colors.purpleAccent.withOpacity(0.4), Colors.deepOrange.withOpacity(0.5),Colors.pink.withOpacity(0.2),Colors.yellow.withOpacity(0.5)];
  Random random = new Random();
  void changeIndex() {
    setState(() => widget.index = random.nextInt(4));
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        displayBottomSheet(context);
//        Navigator.push(context,
//            MaterialPageRoute(builder: (context) => CounsalterFollowup2()));
      },
      child: IntrinsicHeight(
        child: Padding(
          padding: EdgeInsets.only(top: Conf.p2h(2), bottom: Conf.p2h(1)),
          child: Container(width: Conf.p2w(100),
//            color: Colors.red,
            child: Row(children: <Widget>[
              Container(
                color: Colors.grey.withOpacity(0.01),
                child: Padding(
                  padding: EdgeInsets.only(left: Conf.p2w(1), right: Conf.p2w(1)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: IntrinsicHeight(
                          child: SizedBox(
                            child: VerticalDivider(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: Conf.p2w(4.1665)),
                        child: Text(
                          widget.model.time_at.substring(0,5),
                          style: TextStyle(
                              fontSize: Conf.p2t(15),
                              fontWeight: FontWeight.bold,
                              color: Colors.black87),
                        ),
                      ),
                      Expanded(
                        child: IntrinsicHeight(
                          child: SizedBox(
                            child: VerticalDivider(
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              IntrinsicHeight(
                child: Container(
                    width: Conf.p2w(78),
//                height: Conf.p2h(25),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.9),
                        border: Border.all(color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(Conf.p2w(3.125))),
                    child: Row(mainAxisAlignment:MainAxisAlignment.start,children: <Widget>[
                      Container(
                        width: Conf.p2w(2),
                        decoration: BoxDecoration(
                            color: colors[widget.index],
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(Conf.p2w(3)),
                                bottomRight: Radius.circular(Conf.p2w(3.125)))),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.only(top: Conf.p2h(1), right: Conf.p2w(2)),
                        child: Container(
//                        width: Conf.p2w(90),
//                        color: Colors.orange,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                  widget.model.doctor.name,
                                  style: TextStyle(
                                      color: Colors.grey[900],
                                      fontWeight: FontWeight.w600,
                                      fontSize: Conf.p2t(19))),
                              Text(
                                  widget.model.customer.name,
                                  style: TextStyle(
                                      color: Colors.grey[800],
                                      fontWeight: FontWeight.w600,
                                      fontSize: Conf.p2t(15))),
                              Padding(
                                  padding: EdgeInsets.only(
                                      top: Conf.p2h(2), bottom: Conf.p2h(2)),
                                  child: IntrinsicHeight(
                                    child: Container(
//                                    color: Colors.red,
                                      child: Text(
                                          widget.model.description,
                                          softWrap: true,
                                          maxLines: 5,
                                          overflow: TextOverflow.visible,
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontWeight: FontWeight.w600,
                                              fontSize: Conf.p2t(15))),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      )
                    ])),
              )
            ]),
          ),
        ),
      ),
    );
  }

  Widget displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: Colors.white,
        elevation: 10,
//      backgroundColor: Colors.yellow,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(32), topRight: Radius.circular(32))),
        isDismissible: true,
//        barrierColor: Colors.transparent,
        context: context,
        isScrollControlled: true,

        builder: (ctx) {
          return Container(
//            height: Conf.p2h(60),
            child: CounsalterFollowup2(),
          );
        });
  }
}
