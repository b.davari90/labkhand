import 'dart:ui';
import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'CommentPageComponent.dart';
import 'BottomNavigation.dart';
import 'CommentPageModel.dart';
import 'config.dart';

class CommentPage extends StatefulWidget {
  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  List<CommentPageModel> dataList;
  List<Widget> taskList;

  importlist(List<Widget> lists) {
    taskList = lists;
  }

  createCard() {
    dataList = [
      CommentPageModel(
        img: "assets/images/prof1.jpg",
        colorContainer: Conf.blluecontainerColor.withOpacity(0.15),
        colorContainerText: Conf.blluecontainerTextColor,
        comments:
            "نکته بعدی در مورد متن ساختگی لورم ایپسوم این است که بعضی از طراحان وبسایت و گرافیست کاران بعد از آنکه قالب و محتوای مورد نظرشون را ایجاد کردند از یاد می‌برند که متن لورم را از قسمتهای مختلف سایت حذف کنند و یا با متن دیگری جایگزین کنند. به همین دلیل اغلب اوقات ما با وبسایتهایی مواجه می‌شویم که در گوشه و کنار صفحات آنها متن لورم ایپسوم هنوز وجود دارد و حذف نشده است که این نشان دهنده بی توجهی طراحان است.",
        yearVertical: "99",
        monthVertical: "آبان",
        coustomerName: "سعید",
        coustomerFamily: "اسماعیلی",
        dayVertical: "16",
        day: "14",
        kind: "کودک",
        month: "01",
        year: "95",
      ),
      CommentPageModel(
        img: "assets/images/prof1.jpg",
        colorContainer: Conf.purplecontainerColor.withOpacity(0.15),
        colorContainerText: Conf.purplecontainerTextColor,
        comments:
            "گر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند. لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) متنی ساختگی و بدون معنی است که برای امتحان فونت و یا پر کردن فضا در یک طراحی گرافیکی و یا صنعت چاپ استفاده میشود. طراحان وب و گرافیک از این متن برای پرکردن صفحه و ارائه شکل کلی طرح استفاده می‌کنند.",
        yearVertical: "99",
        monthVertical: "مهر",
        coustomerName: "علی",
        coustomerFamily: "عابدی",
        dayVertical: "16",
        day: "21",
        kind: "کودک",
        month: "15",
        year: "87",
      ),
      CommentPageModel(
        img: "assets/images/prof1.jpg",
        colorContainer: Conf.greeencontainerColor.withOpacity(0.15),
        colorContainerText: Conf.greeencontainerTextColor,
        day: "08",
        kind: "کودک",
        month: "12",
        year: "92",
        comments:
            "گر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند. لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) متنی ساختگی و بدون معنی است که برای امتحان فونت و یا پر کردن فضا در یک طراحی گرافیکی و یا صنعت چاپ استفاده میشود. طراحان وب و گرافیک از این متن برای پرکردن صفحه و گر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند. لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) متنی ساختگی و بدون معنی است که برای امتحان فونت و یا پر کردن فضا در یک طراحی گرافیکی ارائه شکل کلی طرح استفاده می‌کنند.",
        yearVertical: "78",
        monthVertical: "اذر",
        coustomerName: "محمد",
        coustomerFamily: "رسولی",
        dayVertical: "16",
      ),
      CommentPageModel(
        img: "assets/images/prof1.jpg",
        colorContainer: Conf.greencontainerColor.withOpacity(0.15),
        colorContainerText: Conf.greencontainerTextColor,
        comments:
            "نکته بعدی در مورد متن ساختگی لورم ایپسوم این است که بعضی از طراحان وبسایت و گرافیست کاران بعد ازاغلب ا بی توجهی طراحان است.",
        yearVertical: "98",
        monthVertical: "بهمن",
        coustomerName: "سعید",
        coustomerFamily: "اسماعیلی",
        dayVertical: "10",
        day: "12",
        kind: "کودک",
        month: "06",
        year: "75",
      ),
      CommentPageModel(
        img: "assets/images/prof1.jpg",
        colorContainer: Conf.greeencontainerColor.withOpacity(0.15),
        colorContainerText: Conf.greeencontainerTextColor,
        comments:
            "گر شما یک طراح هستین و یا با طراحی های گرافیکی سروکار دارید به متن های برخورده اید که با نام لورم ایپسوم شناخته می‌شوند. لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) متنی ساختگی و بدون معنی است که برای امتحان فونت و یا پر کردن فضا در یک طراحی گرافیکی و یا صنعت چاپ استفاده میشود. طراحان وب و گرافیک از این متن برای پرکردن صفحه و ارائه شکل کلی طرح استفاده می‌کنند.",
        yearVertical: "99",
        monthVertical: "آبان",
        dayVertical: "16",
        coustomerName: "سعید",
        coustomerFamily: "اسماعیلی",
        day: "13",
        kind: "بالینی",
        month: "08",
        year: "92",
      ),
      CommentPageModel(
        img: "assets/images/prof1.jpg",
        colorContainer: Conf.greencontainerColor.withOpacity(0.15),
        colorContainerText: Conf.greencontainerTextColor,
        comments:
            "نکته بعدی در مورد متن ساختگی لورم ایپسوم این است که بعضی از طراحان وبسایت و گرافیست کاران بعد از آنکه قالب و محتوای مورد نظرشون را ایجاد کردند از یاد می‌برند که متن لورم را از قسمتهای مختلف سایت حذف کنند و یا با متن دیگری جایگزین کنند. به همین دلیل اغلب اوقات ما با وبسایتهایی مواجه می‌شویم که در گوشه و کنار صفحات آنها متن لورم ایپسوم هنوز وجود دارد و حذف نشده است که این نشان دهنده بی توجهی طراحان است.",
        yearVertical: "99",
        monthVertical: "آبان",
        dayVertical: "16",
        coustomerName: "علیرضا",
        coustomerFamily: "احمدی",
        day: "01",
        kind: "بالینی",
        month: "04",
        year: "90",
      ),
    ];
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
            padding: const EdgeInsets.only(left: 25, bottom: 10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: dataList.map((e) {
                  return CommentPageComponent(
                      isFirstItem: dataList.indexOf(e) < 1 ? true : false,
                      model: e,
                      onTap: (key) {
                        setState(() {});
                      });
//
                }).toList())));
  }

  @override
  void initState() {
    createCard();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: Conf.p2h(100),
              color: Conf.blue,
            ),
            Positioned(
              width: Conf.p2w(100),
              top: Conf.p2h(4),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("نظرات",
                      style: Conf.headline.copyWith(
                          color: Colors.white,
                          fontSize: Conf.p2t(25),
                          fontWeight: FontWeight.w500,
                          decoration: TextDecoration.none)),
                ],
              ),
            ),
            Positioned(
              top: Conf.p2h(4),
              left: Conf.p2w(6),
              child: Column(
                children: [
                  MasBackBotton()
//                ],)
//              ),
                ],
              ),
            ),
            Column(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: Conf.p2h(15)),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(Conf.p2w(6)),
                            topRight: Radius.circular(Conf.p2w(6)))),
                    child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(Conf.p2w(6)),
                            topRight: Radius.circular(Conf.p2w(6))),
                      child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: dataList.map((e) {
                                return CommentPageComponent(
                                    isFirstItem:
                                        dataList.indexOf(e) < 1 ? true : false,
                                    model: e,
                                    onTap: (key) {
                                      setState(() {});
                                    });
                              }).toList())),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigation(1),
      ),
    );
  }
}
