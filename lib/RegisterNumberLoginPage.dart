import 'dart:async';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/ForgotPasswordPage2.dart';
import 'package:counsalter5/HomePage.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/NumericKeyboard.dart';
import 'package:counsalter5/TimerBuilderClass.dart';
import 'package:counsalter5/htify.dart';
import 'package:flash/flash.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:timer_builder/timer_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
class RegisterNumberLoginPage extends StatefulWidget {
  _RegisterNumberLoginPageState createState() => _RegisterNumberLoginPageState();
}
class _RegisterNumberLoginPageState extends State<RegisterNumberLoginPage> {
  bool zero=false,one=false,two=false,three=false;
  String text = "";
  TextEditingController _controller1;
  TextStyle txttStyleBlack = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w800,
    fontSize: 23,
    decoration: TextDecoration.none,
  );
  TextStyle txttStyleGray = TextStyle(
    color: Colors.black45,
    fontWeight: FontWeight.w800,
    fontSize: 20,
    decoration: TextDecoration.none,
  );
  bool loading;
  ForgotPasswordPage2 forgotpas2;
  DateTime alert;
  SharedPreferences sharedPreferences;
  @override
  void initState() {
    super.initState();
    alert = DateTime.now().add(Duration(seconds: 10));
    forgotpas2 = ForgotPasswordPage2();
    _controller1 = TextEditingController();
  }
  void dispose() {
    _controller1.dispose();
    super.dispose();
  }
  DoctorBloc get blocDoctor => context.bloc<DoctorBloc>();
  Future sendDataToServer() async {
    setState(() {
      loading = false;
    });
    try {
      Map data = await Htify.create("auth/verify", {
        "token_sms": text,
        "id": Conf.idLogin
      });
      sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString("token", data["access_token"]);
      sharedPreferences.setInt("customerType", 1);
      Conf.token = data["access_token"];
      print("_____Conf.token RegisterNumberLoginPage===> ${ Conf.token }");
//      Conf.loginType=1;
      print("_____Conf.loginType RegisterNumberLoginPage===> ${ Conf.loginType }");

      Navigator.pushReplacement(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: HomePage()));
    } catch (e) {
      print("sendDataToServer CATCH ERROR  ===> ${e.toString()}");
      _showTopFlash();
    } finally {
      setState(() {
        loading = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                width: double.infinity,
                height: double.infinity,
                color: Conf.blue,
                child: Padding(
                  padding: EdgeInsets.only(left: Conf.xxlSize,right: Conf.xxlSize),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TimerBuilder.scheduled([alert],
                          builder: (context) {
                            var now = DateTime.now();
                            var reached = now.compareTo(alert) > 0;
                            final textStyle = TextStyle(fontSize: 14,color: Colors.white);
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                !reached ?
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xxlSize),
                                  child: TimerBuilder.periodic(Duration(seconds: 2),
                                      alignment: Duration(seconds: 1),
                                      builder: (context) {
                                        DateTime now = DateTime.now();
                                        print("now ===> ${ now.toString() }");
                                        var remaining = alert.difference(now);
                                        return Text("کد در حال ارسال میباشد..."+"      "+formatDuration(remaining),style: textStyle,);
                                      }
                                  ),
                                ): Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: RaisedButton(  shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                                    child: Text(reached ?"ارسال مجدد کد؟":"", style: textStyle),
                                    onPressed: () {
                                      setState(() {
                                        alert = DateTime.now().add(Duration(seconds: 60));
                                      });
                                    },
                                  ),
                                ),
                              ],
                            );
                          }
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: Conf.xlSize * 1.2),
                          child: MasBackBotton()
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: Conf.p2w(0),
                top: Conf.p2w(0),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child: Container(
                    height: double.infinity,
                    margin: Conf.smEdgeTop2 * 5.787,
//              margin: EdgeInsets.only(top: 150),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          FittedBox(
                            fit: BoxFit.cover,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2h(3), right: Conf.p2w(6)),
                              child: Text("شماره خود را تایید کنید",
                                  style: Conf.body1.copyWith(
                                    color: Colors.black87.withOpacity(0.7),
                                    fontWeight: FontWeight.w700,
                                    fontSize: Conf.p2t(25),
                                    decoration: TextDecoration.none,
                                  )),
                            ),
                          ),
                          FittedBox(
                            fit: BoxFit.cover,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2h(0.5), right: Conf.p2w(6)),
                              child: Text(
                                  "کد 4 رقمی ارسال شده از طریق پیامک را وارد کنید",
                                  style: Conf.body1.copyWith(
                                    color: Colors.black45.withOpacity(0.52),
                                    fontWeight: FontWeight.w700,
                                    fontSize: Conf.p2t(15),
                                    decoration: TextDecoration.none,
                                  )
//                                  txttStyleGray
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(7), right: Conf.p2w(11)),
                            child: Container(
                              width: Conf.p2w(80),
                              child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    containerRegisterNumLoginPage(14.5, 5, 3),
                                    containerRegisterNumLoginPage(14.5, 5, 2),
                                    containerRegisterNumLoginPage(14.5, 5, 1),
                                    containerRegisterNumLoginPage(14.5, 5, 0),
                                  ]),
                            ),
                          ),
                        ])),
              ),
              Positioned(
                bottom: Conf.p2w(0),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                height: Conf.p2h(45),
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: Conf.xlSize * 2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: Material(
                            color: Colors.transparent,
                            type: MaterialType.button,
                            child: NumericKeyboard(
                              onKeyboardTap: _onKeyboardTap,
                              textColor: Colors.white,
                              rightButtonFn: () {
                                setState(() {
                                  if (text.length <= 0) {
                                    return null;
                                  }
                                  text = text.substring(0, text.length - 1);
                                  _controller1.text = text;
                                  print("onkeybord ===> ${_controller1.text}");
                                });
                              },
                              rightIcon: Icon(
                                Icons.backspace,
                                color: Colors.white,
                              ),
                              leftButtonFn: () {
                                print('left button clicked');
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: Conf.p2w(1.042), //5,
                        blurRadius: Conf.p2w(1.459), //7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(42),
                        topRight: Radius.circular(42)),
                    color: Conf.blue,
                  ),
                ),
              ),
              Positioned(
                bottom: Conf.p2h(2.5),
                left: Conf.p2w(7),
                right: Conf.p2w(7),
                child: SizedBox(
                    width: Conf.p2w(88),
                    height: Conf.p2w(16),
                    child: MasRaisedButton(
                      margin: EdgeInsets.only(
                          right: Conf.p2w(0),
                          left: Conf.p2w(1),
                          top: Conf.p2w(3),
                          bottom: Conf.p2w(1.5)),
                      borderSideColor: Conf.lightColor,
                      text: "ادامه",
                      color: Conf.blue,
                      textColor: Colors.white,
                      onPress: () {
                        sendDataToServer();
                      },
                    )
                ),
              )
            ],
          ),
        ));
  }

  Widget otpNumberWidget(int position) {
    try {
      return Center(
        child: Text(
          text[position],
          style: TextStyle(height: 1, color: Colors.black87, fontSize: 20
          ),
        ),
      );
    } catch (e) {
      return Center(
        child: Icon(
          Icons.circle,
          color: Colors.white,
          size: 11,
        ),
      );
    }
  }

  _onKeyboardTap(String value) {
    setState(() {
      if (text.length >= 0 && text.length < 4) {
        text = text + value;
        text = text;
        zero=true;
        print("text ===> ${text}");
      } else {
        _controller1.clear();
        print("else ===> ${_controller1.text}");
      }
    });
  }
  Widget containerRegisterNumLoginPage(double w, border, int positionNumber) {

    return
      Container(
        width: Conf.p2w(w),
        height: Conf.p2w(w),
        decoration:
        BoxDecoration(
          color: Colors.white,
          border:zero==true? Border.all(color: Conf.orang)
      :Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(Conf.p2w(border)), //23
        ), //23
        child: Center(child: otpNumberWidget(positionNumber)),
      );
  }
  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context:context,
      duration: const Duration(seconds:6),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Icon(FeatherIcons.alertOctagon,color: Colors.red,size: 50,),
            title: Text('خطا',style: TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w400,color: Colors.red),),
            message: Text('کد ناممتبر است...',style: TextStyle(fontSize:Conf.p2t(12),fontWeight: FontWeight.w200,color: Colors.black),),
            showProgressIndicator: true,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
