import 'dart:ui';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/TransActionListPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'config.dart';

class AboutUs extends StatelessWidget {
//  TextStyle txtStyletitle = Conf.subtitle.copyWith(
//      fontSize: Conf.p2t(20),
//      fontWeight: FontWeight.w700,
//      color: Colors.black87);
  TextStyle txtStyleSubtitle = Conf.subtitle.copyWith(
      fontSize: Conf.p2t(20),
      fontWeight: FontWeight.w500,
      color: Colors.black87);
//  TextStyle txtStyleRate =
//  TextStyle(fontWeight: FontWeight.bold, fontSize: Conf.p2t(16));

  String aboutTxt="این برنامه نسخه اولیه لبخند بود و مشکلات و ایرادات و اشکالات آن در نسخه های نهایی رفع میگردد";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Stack(children: [
          Positioned(
          top: Conf.p2w(0),
          right: Conf.p2w(0),
          left: Conf.p2w(0),
          child: Container(
              width: Conf.p2w(100),
              height: Conf.p2h(35),
              child: Image.asset("assets/images/aboutImag.png" ??
                  "https://th.bing.com/th/id/OIP.MVPo7ij6L0D4_7tmS53kKAHaE7?pid=Api&rs=1")),
        ),
        Positioned(
          top: Conf.p2h(4.167),
          left: Conf.p2w(4.167),
          child: MasBackBotton(),
        ),
        Positioned(
          right: Conf.p2w(0),
          left: Conf.p2w(0),
          bottom: Conf.p2w(0),
          child: Container(
            height: Conf.p2h(70),
            decoration: BoxDecoration(
                color: Colors.white10.withOpacity(0.95),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    spreadRadius: 1,
                    blurRadius:10,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(Conf.p2w(6.25)),
                    topLeft: Radius.circular(Conf.p2w(6.25))
                )),
          ),
        )
          ,Positioned(
              right: Conf.p2w(5),
              left: Conf.p2w(5),
              bottom: Conf.p2w(10),
              child: InkWell(  onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.leftToRight,
                        duration: Duration(milliseconds: 500),
                        alignment: Alignment.centerLeft,
                        curve: Curves.easeOutCirc,
                        child: TransActionListPage()
                    ));
              },
                child: Container(
                  height: Conf.p2w(100),
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: Conf.p2w(15),
                        right: Conf.p2w(5),left:Conf.p2w(5)),
                    child: Text(aboutTxt,
                    style:txtStyleSubtitle),
                  ),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius:10,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30))),),
              ),
            )])
    )
    );
  }
}
