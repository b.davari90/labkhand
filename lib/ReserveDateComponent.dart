import 'package:flutter/material.dart';

import 'config.dart';
class ReserveDateComponent extends StatelessWidget {

  DateModel model;
  bool isActive =false;
  @override
  Widget build(BuildContext context) {
    return Container(width: Conf.p2w(5), height: Conf.p2h(5),
      decoration: BoxDecoration(color: isActive ? Colors.white : Colors.grey,
          border: Border.all(color: Colors.grey),
          boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 10)],
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Text(model.date,
        style: TextStyle(color: isActive ? Colors.black87 : Colors.grey),),);
  }

  ReserveDateComponent({this.model, this.isActive});
}

class DateModel {
  String date;
//  bool color;

  DateModel({this.date});
//  DateModel(this.date,{this.color=false});
}

