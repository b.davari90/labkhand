import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'ContainerModel.dart';
import 'config.dart';

class CustomContainerComponent extends StatelessWidget {

 ContainerModel homePageListViewModel;
 Function onTap;
 TextStyle txtTitleStyle=TextStyle(fontSize: 12,fontWeight: FontWeight.w900);
 TextStyle txtTitle2Style=TextStyle(fontSize: 12,color: Colors.black45,fontWeight: FontWeight.w900);

 CustomContainerComponent({this.homePageListViewModel, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
      if(onTap is Function){
      onTap();
    }},
      child: Padding(
        padding: Conf.smEdgeRightLeft,
        child: Padding(
          padding:Conf.smEdgeTop2,
          child: Container(
            decoration: BoxDecoration(color:Colors.white,borderRadius: BorderRadius.circular(9),
              boxShadow: [
                BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: Conf.p2w(1.042),//5,
                blurRadius: Conf.p2w(1.459),//7,
                offset: Offset(0,3), // changes position of shadow
              ),],
              ),
            width: Conf.p2w(28),
            height: Conf.p2h(16),
            child: Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                Positioned(
                  top: Conf.p2w(-5.209),//-25,
                  right: Conf.p2w(6.25),//30,
                child:Column(children: [],)
              ),
                Positioned(
                  bottom: Conf.p2w(2.5),//12,
                  right: Conf.p2w(0),//0,
                  left:  Conf.p2w(0),
                  child: Center(
                    child: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:8),
                        child: FittedBox(fit: BoxFit.cover,
                          child: Text(homePageListViewModel?.title1??"مشاور ",
                            style:Conf.subtitle.copyWith(fontSize: 12,fontWeight: FontWeight.w900) ,
//                          txtTitleStyle,
                            softWrap: true,
                            overflow: TextOverflow.fade,),
                        ),
                      ),
                      FittedBox(fit: BoxFit.cover,
                        child: Text(homePageListViewModel?.title2??"متخصص کودک ",
                          style:Conf.subtitle.copyWith(fontSize: 12,color: Colors.black45,fontWeight: FontWeight.w900)
//                          txtTitle2Style
                          ,softWrap: true,
                          overflow: TextOverflow.fade,),
                      ),
                      Padding(
                        padding: Conf.smEdgeTop1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                          Padding(
                            padding: Conf.smEdgeBottomTop0,
                            child: FittedBox(
                              fit: BoxFit.cover,
                              child: Text(homePageListViewModel?.rate.toString()??"15", softWrap: true,
                                  overflow: TextOverflow.fade,
                                  style:Conf.subtitle.copyWith(fontSize: 12,color: Colors.black45,fontWeight: FontWeight.w900)
//                                  txtTitle2Style
                              ),
                            ),
                          ),
                          Padding(
                            padding:Conf.smEdge,
                            child: Icon(Icons.star,color: Colors.amberAccent,
                              size: Conf.p2w(3.75),//18
                            ),
                          )],),
                      )],),
                  ),
                ),
            ],),
          ),
        ),
      ),
    );
  }
}
