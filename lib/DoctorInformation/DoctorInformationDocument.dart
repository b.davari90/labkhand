import 'package:counsalter5/ModelBloc/DocumentModel.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:counsalter5/DoctorInformation/DoctorInformationPage.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class DoctorInformationDocument extends StatefulWidget {
  int documentNumber;

  final _formKeyDoctorDocument = GlobalKey<FormBuilderState>();
  TextStyle txtStyle = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w600,
      fontSize: Conf.p2t(17));
  TextStyle txtWhiteStyle = TextStyle(
      color: Colors.white, fontWeight: FontWeight.w600, fontSize: Conf.p2t(15));
  TextStyle txtGreyStyle = TextStyle(
      color: Colors.black45,
      fontWeight: FontWeight.w400,
      fontSize: Conf.p2t(12));
  TextStyle txt2Style = TextStyle(
      color: Colors.black45,
      fontWeight: FontWeight.w400,
      fontSize: Conf.p2t(13));
  bool loading = true;
  String educationPic = "بارگذاری تصویر عکس مدرک تحصیلی",
      nezamPic = "بارگذاری پروانه نظام روانشناسی",
      profilePic = "بارگذاری تصویر پروفایل",
      melicodePic = "بارگذاری تصویر کارت ملی",
      deleteFile1 = "حذف فایل",
      deleteFile2 = "حذف فایل",
      deleteFile3 = "حذف فایل",
      deleteFile4 = "حذف فایل",
      createFile = "ایجاد فایل";
  DoctorRepository doctorRepository;
  SharedPreferences sharedPreferences;
  int i;
  List<DoctorModel> contents = [];

  @override
  _DoctorInformationDocumentState createState() =>
      _DoctorInformationDocumentState();
}

class _DoctorInformationDocumentState extends State<DoctorInformationDocument> {
  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();
  List<DoctorModel> getTotalDataList = [];
  DocumentModelBloc get documentBloc => context.bloc<DocumentModelBloc>();
  bool load = true;
  String imagePath1 = "", imagePath2 = "", imagePath3 = "", imagePath4 = "";
  File _image;
  bool isSelectedImg = false;
  int file1IdServer = 0, file2IdServer = 0, file3IdServer = 0;
  DoctorModel doctorRegister;

  DoctorBloc get bloc => context.bloc<DoctorBloc>();
  List<SubDataDocument> subDataDocumentList = [];
  SubDataDocument s1, s2, s3;

  Future getDoctor() async {
    try {
      setState(() {
        load = true;
      });
      var data = await Htify.get('dr/doctor');
      print("data ===> ${data}");
      setState(() {
        doctorRegister = DoctorModel.fromJson(data);

        if(doctorRegister.avatar != null ){
          print("doctorRegister.avatar ===> ${ doctorRegister.avatar }");
          imagePath4 =doctorRegister.avatar.split("/").last;
        }
        if(doctorRegister.avatar == null){imagePath4 = "";}

        doctorRegister.document.forEach((element) {
          if (element.type_doc == 1) {
            imagePath1 = element.title;
            file1IdServer = element.file_id;
            doctorBloc.setImageNezam(imagePath1);
            doctorBloc.setImageNezamID(file1IdServer);
            s1.file_id = file1IdServer;
            s1.type_doc = 1;
            print("getDoctor imagePath1 ===> ${doctorBloc.state.imagePath1}");
            print("getDoctor file1IdServer ===> ${doctorBloc.state.file1IdServer}");
//            if (s1.file_id == null || s1.type_doc == null) {

//            }
          }

          if (element.type_doc == 2) {
            imagePath2 = element.title;
            file2IdServer = element.file_id;
            doctorBloc.setImageMeli(imagePath2);
            doctorBloc.setImageMeliID(file2IdServer);
            s2.file_id = file2IdServer;
            s2.type_doc = 2;
            print("getDoctor imagePath2 ===> ${doctorBloc.state.imagePath2}");
            print("getDoctor file2IdServer ===> ${doctorBloc.state.file2IdServer}");
//            if (s2.file_id == null || s2.type_doc == null) {

//            }
          }

          if (element.type_doc == 3) {
            imagePath3 = element.title;
            file3IdServer = element.file_id;
            doctorBloc.setImageEducation(imagePath3);
            doctorBloc.setImageEducationID(file3IdServer);
            print("getDoctor imagePath3 ===> ${doctorBloc.state.imagePath3}");
            print("getDoctor file3IdServer ===> ${doctorBloc.state.file3IdServer}");
//            if (s3.file_id == null || s3.type_doc == null) {
              s3.file_id = file3IdServer;
              s3.type_doc = 3;
//            }
          }

        });
      });
    } catch (e) {
      print("CATCH ERROR getDoctor===> ${e.toString()}");
    } finally {
      setState(() {
        load = false;
      });
    }
  }

  void _showPicker(context, i) {
    widget.i = i;
    showModalBottomSheet(
        barrierColor: Conf.transparentColor,
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: Conf.p2w(40),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.8),
                    spreadRadius: 1,
                    blurRadius: 10,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                color: Conf.blue,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
            child: Padding(
              padding: EdgeInsets.only(top: Conf.p2w(5), right: Conf.p2w(5)),
              child: Wrap(
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      _imgFromGallery(i);
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: Conf.xlSize),
                          child: Icon(
                            FeatherIcons.folder,
                            size: Conf.p2w(4),
                            color: Colors.white,
                          ),
                        ),
                        Text('انتخاب از گالری', style: widget.txtWhiteStyle),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: Conf.xlSize, bottom: Conf.xlSize),
                    child: Divider(
                      color: Colors.white70,
                      thickness: 0.5,
                      endIndent: 20,
                      indent: 10,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _imgFromCamera(i);
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: Conf.xlSize),
                          child: Icon(FeatherIcons.camera,
                              color: Colors.white, size: Conf.p2w(4)),
                        ),
                        Text(
                          'دوربین',
                          style: widget.txtWhiteStyle,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  _imgFromCamera(i) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      _image = image;
      switch (i) {
        case 1:
          imagePath1 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath1,
              thumb: 0,
              type: 3,
              typeDoc: 1,
              avatar: "",
              waterMark: 0);
          break;
        case 2:
          imagePath2 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath2,
              thumb: 0,
              type: 3,
              typeDoc: 2,
              avatar: "",
              waterMark: 0);
          break;
        case 3:
          imagePath4 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath4,
              thumb: 0,
              type: 1,
              avatar: imagePath4,
              typeDoc: 4,
              waterMark: 0);
          break;
        case 4:
          imagePath3 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath3,
              thumb: 0,
              type: 3,
              avatar: "",
              typeDoc: 3,
              waterMark: 0);
          break;
      }
    });
  }

  _imgFromGallery(i) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      _image = image;
      switch (i) {
        case 1:
          imagePath1 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath1,
              thumb: 0,
              type: 3,
              avatar: "",
              typeDoc: 1,
              waterMark: 0);
          break;
        case 2:
          imagePath2 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath2,
              thumb: 0,
              type: 3,
              avatar: "",
              typeDoc: 2,
              waterMark: 0);
          break;
        case 4:
          imagePath4 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath4,
              avatar: imagePath4,
              thumb: 0,
              type: 1,
              typeDoc: 4,
              waterMark: 0);
          break;
        case 3:
          imagePath3 = _image.path.split('/').last;
          sendDataDocument(
              imagePath: _image.path,
              title: imagePath3,
              thumb: 0,
              typeDoc: Conf.typeDoc.entries
                  .firstWhere((element) => element.key == 3)
                  .key,
              avatar: "",
              type: 3,
              waterMark: 0);
          break;
      }
    });
  }

  Future sendDataDocument(
      {int fileStatue,
      String title,
      String avatar,
      String imagePath,
      int waterMark,
      int thumb,
      int type,
      int typeDoc}) async {
    try {
      print("#####     imagePath ===> ${imagePath}");
      print("type ===> ${type}");
      FormData formData = FormData.fromMap({
        "title": title,
        "has_watermark": waterMark,
        "has_thumb": thumb,
        "type": type,
        "file": await MultipartFile.fromFile(imagePath, filename: title),
      });
      var response = await Htify.sendFile("dr/file", formData);
      print("response ===> ${response}");
      if (typeDoc == 1) {
        s1.file_id = response['id'];
        s1.type_doc = 1;
        print("sendDataDocument s1.file_id ===> ${s1.file_id}");
      }
      if (typeDoc == 2) {
        s2.file_id = response['id'];
        s2.type_doc = 2;
        print("sendDataDocument s2.file_id ===> ${s2.file_id}");
      }
      if (typeDoc == 3) {
        s3.file_id = response['id'];
        s3.type_doc = 3;
        print("sendDataDocument s4.file_id ===> ${s3.file_id}");
      }
    } catch (e) {
      print("@@@@@ sendDataDocument   e ===> ${e.toString()}");
    }
  }

  Future deleteDocumentServer(int fileId, int kind) async {
    print("fileId Document===> ${fileId}");
    setState(() {
      widget.loading = false;
    });
    try {
      var x = await Htify.delete("dr/file", fileId);
      print("x ===> ${x}");
      if (kind == 1) {
        s1 = SubDataDocument();
        imagePath1 = "";
        print("deleteDocumentServer 1===> ${deleteDocumentServer}");
      }
      if (kind == 2) {
        s2 = SubDataDocument();
        imagePath2 = "";
        print("deleteDocumentServer 2===> ${deleteDocumentServer}");
      }
      if (kind == 3) {
        s3 = SubDataDocument();
        imagePath3 = "";
        print("deleteDocumentServer 3===> ${deleteDocumentServer}");
      }
//      getDoctor();
    } catch (e) {
      print("deleteDocumentServer CATCH ERROR  ===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  Future sendDataToServer() async {

    print("subDataDocumentList.ma ===> ${ subDataDocumentList.map((e) => e.file_id).toList() }");
    setState(() {
      widget.loading = false;
    });
    try {
      Map data = await Htify.create("dr/doctor", {
        "data": {
          "avatar": imagePath4 ?? "no Avatar",
          "doc": subDataDocumentList.map((e) => e.toJson()).toList()
        },
        "op": "doc"
      });
      doctorBloc.setAvatar(imagePath4);
      print("___doctorBloc.state.doctor.avatar ===> ${ doctorBloc.state.doctor.avatar }");
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: DoctorInformationPage()));
    } catch (e) {
      _showTopFlash(title1: "خطا",title2: 'امکان ارسال داده به سرور وجود ندارد',
          color: Colors.red);
      print("sendDataToServer CATCH ERROR  ===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  @override
  void initState() {
    doctorBloc.state.imagePath1="";
    doctorBloc.state.imagePath2="";
    doctorBloc.state.imagePath3="";
    s1 = SubDataDocument();
    s2 = SubDataDocument();
    s3 = SubDataDocument();
    doctorRegister = DoctorModel();
    getDoctor();
    print("imagePath1 ===> ${ imagePath1 }");
    print("doctorBloc.state.imagePath1 ===> ${ doctorBloc.state.imagePath1 }");
    if(doctorBloc.state.imagePath1==""||doctorBloc.state.imagePath1==null){imagePath1="";}else{imagePath1=doctorBloc.state.imagePath1;}
    if(doctorBloc.state.imagePath2==""||doctorBloc.state.imagePath2==null){imagePath2="";}else{imagePath2=doctorBloc.state.imagePath2;}
    if(doctorBloc.state.imagePath3==""||doctorBloc.state.imagePath3==null){imagePath3="";}else{imagePath3=doctorBloc.state.imagePath3;}
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SafeArea(
          child: Scaffold(
              bottomNavigationBar: Container(
                width: Conf.p2w(100),
                color: Colors.grey.shade100,
                child: Container(
                    width: Conf.p2w(100),
                    height: Conf.p2h(12),
                    margin: EdgeInsets.only(
                        top: Conf.p2h(1),
                        bottom: Conf.p2h(1),
                        left: Conf.p2w(3.8),
                        right: Conf.p2w(3.8)),
                    child: MasRaisedButton(
                      margin: EdgeInsets.only(
                          right: Conf.p2w(0),
                          left: Conf.p2w(1),
                          top: Conf.p2w(1.5),
                          bottom: Conf.p2w(3)),
                      borderSideColor: Conf.orang,
                      text: "تایید",
                      color: Conf.orang,
                      textColor: Colors.white,
                      onPress: () {
                        if (imagePath1 != "" &&
                            imagePath2 != "" &&
                            imagePath3 != "" &&
                            imagePath4 != "") {
                          subDataDocumentList = [];
                          print("imagePath1 تایید ===> ${ imagePath1 }");
                          print("imagePath2 تایید===> ${ imagePath2 }");
                          print("imagePath3 تایید===> ${ imagePath3 }");
                          print("imagePath4 تایید===> ${ imagePath4 }");
                          print("s1.file_id تایید===> ${ s1.file_id }");
                          print("s2.file_id تایید===> ${ s2.file_id }");
                          print("s3.file_id تایید===> ${ s3.file_id }");
                          subDataDocumentList.add(s1);
                          subDataDocumentList.add(s2);
                          subDataDocumentList.add(s3);
                          print( "subDataDocumentList.length ===> ${subDataDocumentList.length}");
                          sendDataToServer();
                        } else {
                          _showTopFlash(title1: "خطا",title2: 'لطفا تمام تصاویر خواسته شده را از پوشه تصویر خود انتخاب کنید',
                          color: Colors.red);
                          throw ("ERROR...Cant push");
                        }
                      },
                    )),
              ),
              body: Stack(children: [
                BlocBuilder<DocumentModelBloc, DocumentModelRepository>(
                    builder: (context, state) {
                  return FormBuilder(
                    key: widget._formKeyDoctorDocument,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Container(
                      margin: EdgeInsets.only(top: Conf.p2h(8)),
                      child: Column(children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: Conf.p2h(4)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    color: Colors.red,
                                    width: Conf.p2w(80),
                                  ),
                                  Text("مدارک",
                                      style: Conf.headline.copyWith(
                                          color: Colors.black87,
                                          fontSize: Conf.p2t(23),
                                          fontWeight: FontWeight.w500,
                                          decoration: TextDecoration.none)),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [MasBackBotton()],
                              ),
                            ],
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              height: Conf.p2h(64.3),
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade100,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.4),
                                      spreadRadius: 1,
                                      blurRadius: 10,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(35),
                                      topLeft: Radius.circular(35))),
                              child: SingleChildScrollView(
                                child: Padding(
                                  padding: EdgeInsets.only(right: Conf.p2w(5)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(4)),
                                        child: Text(widget.nezamPic,
                                            style: widget.txtStyle),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: Conf.p2w(6)),
                                        padding: EdgeInsets.only(
                                            left: Conf.p2w(7),
                                            right: Conf.p2w(7)),
                                        width: Conf.p2w(100),
                                        height: Conf.p2h(9),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            border:
                                                Border.all(color: Colors.grey)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            FlatButton(
                                                child: Text(
                                                  imagePath1 != ""
                                                      ? "حذف فایل"
                                                      : "انتخاب فایل",
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  side: BorderSide(
                                                      color: Colors.black54),
                                                ),
                                                onPressed: () async {
                                                  print("imagePath1 onPressed===> ${ imagePath1 }");
                                                  if (
                                                      imagePath1 == "" ||
                                                          imagePath1 == null) {
                                                    _showPicker(context, 1);
                                                  } else {
                                                    if (s1.file_id != null) {
                                                      print("____IF s1.file_id ===> ${ s1.file_id }");
                                                      deleteDocumentServer(
                                                          s1.file_id, 1);
                                                    } else {
                                                      deleteDocumentServer(
                                                          s1.file_id, 1);
                                                      print("delete file_id");
                                                    }
                                                  }
                                                }),
                                            Row(children: [
                                              Icon(
                                                imagePath1 != ""
                                                    ? FeatherIcons.check
                                                    : null,
                                                size: 16,
                                                color: Conf.blue,
                                              ),
                                              Text(
                                                imagePath1 != ""
                                                    ? "یک فایل انتخاب شد"
                                                    : "بدون انتخاب...",
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: Conf.p2t(12)),
                                              ),
                                            ]),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(1.5)),
                                        child: Text(
                                            "حداکثر فایل بارگذاری ا مگابات با پسوند .jpeg",
                                            style: widget.txtGreyStyle),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(2)),
                                        child: Text(
                                          widget.melicodePic,
                                          style: widget.txtStyle,
                                        ),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: Conf.p2w(6)),
                                        padding: EdgeInsets.only(
                                            left: Conf.p2w(7),
                                            right: Conf.p2w(7)),
                                        width: Conf.p2w(100),
                                        height: Conf.p2h(9),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            border:
                                                Border.all(color: Colors.grey)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            FlatButton(
                                                child: Text(
                                                    imagePath2 != ""
                                                        ? "حذف فایل"
                                                        : "انتخاب فایل"),
                                                shape: RoundedRectangleBorder(
                                                    side: BorderSide(
                                                        color: Colors.black54)),
                                                onPressed: () async {
                                                  if (
                                                      imagePath2 == "" ||
                                                          imagePath2 == null) {
                                                    _showPicker(context, 2);
                                                  } else {
                                                    print(
                                                        "s2.file_id ELSE===> ${s2.file_id}");
                                                    if (s2.file_id != null) {
                                                      deleteDocumentServer(
                                                          s2.file_id, 2);
                                                    } else {
                                                      deleteDocumentServer(
                                                          s2.file_id, 2);
                                                      print("delete file_id");
                                                    }
                                                  }
                                                }),
                                            Row(
                                              children: [
                                                Icon(
                                                  imagePath2 != ""
                                                      ? FeatherIcons.check
                                                      : null,
                                                  size: 16,
                                                  color: Conf.blue,
                                                ),
                                                Text(
                                                  imagePath2 != ""
                                                      ? "یک فایل انتخاب شد"
                                                      : "بدون انتخاب...",
                                                  style: TextStyle(
                                                      color: Colors.black87,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: Conf.p2t(12)),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(2)),
                                        child: Text(
                                          widget.profilePic,
                                          style: widget.txtStyle,
                                        ),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: Conf.p2w(6)),
                                        padding: EdgeInsets.only(
                                            left: Conf.p2w(7),
                                            right: Conf.p2w(7)),
                                        width: Conf.p2w(100),
                                        height: Conf.p2h(9),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            border:
                                                Border.all(color: Colors.grey)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            FlatButton(
                                                child: Text(imagePath4 != ""
                                                    ? "حذف فایل"
                                                    : "انتخاب فایل"),
                                                shape: RoundedRectangleBorder(
                                                    side: BorderSide(
                                                        color: Colors.black54)),
                                                onPressed: () async {
                                                  if (imagePath4 == "" ||
                                                      imagePath4 == null) {
                                                    _showPicker(context, 4);
                                                  } else {
                                                    doctorBloc.state.doctor
                                                        .avatar = "";
                                                    imagePath4 = doctorBloc
                                                        .state.doctor.avatar;
                                                    _showTopFlash(color: Colors.lightGreen,title2: "حذف با موفقیت انجام شد،لطفا تصویر دلخواه خود را مجدد انتخاب کنبد"
                                                    ,title1: " موفق...");
//                                                    if (file4IdServer != null ||
//                                                        file4IdServer == 0) {
//                                                      deleteDocumentServer(
//                                                          file4IdServer, 4);
//                                                    } else {
//                                                      deleteDocumentServer(
//                                                          s4.file_id, 4);
//                                                      print("delete file_id");
//                                                    }
                                                  }
                                                }),
                                            Row(
                                              children: [
                                                Icon(
                                                  imagePath4 != ""
                                                      ? FeatherIcons.check
                                                      : null,
                                                  size: 16,
                                                  color: Conf.blue,
                                                ),
                                                Text(
                                                  imagePath4 != ""
                                                      ? "یک فایل انتخاب شد"
                                                      : "بدون انتخاب...",
                                                  style: TextStyle(
                                                      color: Colors.black87,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: Conf.p2t(12)),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(1.5)),
                                        child: Text(
                                            "حداکثر فایل بارگذاری یک مگابات با پسوند .jpeg",
                                            style: widget.txtGreyStyle),
                                      ),
                                      Text(
                                          "عکس سلفی بدون حجاب و با آرایش غیر عرف مجاز نمیباشد",
                                          style: widget.txtGreyStyle),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(2)),
                                        child: Text(
                                          widget.educationPic,
                                          style: widget.txtStyle,
                                        ),
                                      ),
                                      Container(
                                        margin:
                                            EdgeInsets.only(left: Conf.p2w(6)),
                                        padding: EdgeInsets.only(
                                            left: Conf.p2w(7),
                                            right: Conf.p2w(7)),
                                        width: Conf.p2w(100),
                                        height: Conf.p2h(9),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            border:
                                                Border.all(color: Colors.grey)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            FlatButton(
                                                child: Text(
                                                  imagePath3 != ""
                                                      ? "حذف فایل"
                                                      : "انتخاب فایل",
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  side: BorderSide(
                                                      color: Colors.black54),
                                                ),
                                                onPressed: () async {
                                                  print(
                                                      "imagePath3 ===> ${imagePath3}");
                                                  if (imagePath3 == "") {
                                                    _showPicker(context, 3);
                                                  } else {
//                                                    print("tahsiliId ELSE===> ${tahsiliId}");
                                                    if (imagePath3 != "") {
                                                      deleteDocumentServer(
                                                          s3.file_id, 3);
                                                    } else {
                                                      deleteDocumentServer(
                                                          s3.file_id, 3);
                                                      print("delete file_id");
                                                    }
                                                  }
//                                                      else {
//                                                        print("s4.file_id ELSE===> ${ s4.file_id }");
//                                                        deleteDocumentServer(s4.file_id,4);
//                                                        print("delete 4");
//                                                      }
                                                }),
                                            Row(
                                              children: [
                                                Icon(
                                                  imagePath3 != ""
                                                      ? FeatherIcons.check
                                                      : null,
                                                  size: 16,
                                                  color: Conf.blue,
                                                ),
                                                Text(
                                                  imagePath3 != ""
                                                      ? "یک فایل انتخاب شد"
                                                      : "بدون انتخاب...",
                                                  style: TextStyle(
                                                      color: Colors.black87,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: Conf.p2t(12)),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(1.5)),
                                        child: Text(
                                            "حداکثر فایل بارگذاری سه مگابایت و با پسوند های .png .jpeg .pdf",
                                            style: widget.txtGreyStyle),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2w(4)),
                                        child: Text("حداکثر تعداد سه فایل",
                                            style: widget.txtGreyStyle),
                                      ),
                                      imagePath1 != ""
                                          ? Text("1." + "   " + imagePath1,
                                              style: widget.txt2Style)
                                          : Text("1." + "   "),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: Conf.p2w(1),
                                            bottom: Conf.p2w(1)),
                                        child: imagePath2 != ""
                                            ? Text("2." + "   " + imagePath2,
                                                style: widget.txt2Style)
                                            : Text("2." + "   "),
                                      ),
                                      Text(
                                          imagePath4 != ""
                                              ? "3." + "   " + imagePath4
                                              : "3." + "   ",
                                          style: widget.txt2Style),
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: Conf.p2w(1),
                                            bottom: Conf.p2w(1)),
                                        margin: EdgeInsets.only(
                                            bottom: Conf.xxlSize * 4),
                                        child: imagePath3 != ""
                                            ? Text("4." + "   " + imagePath3,
                                                style: widget.txt2Style)
                                            : Text("4." + "   "),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
//                      widget.loading == false ? Conf.circularProgressIndicator() : Container()
                          ],
                        ),
                      ]),
//              ),
                    ),
                  );
                }),
                widget.loading == false
                    ? Conf.circularProgressIndicator()
                    : Container()
              ])),
//
        ),
        load == true ? Conf.circularProgressIndicator() : Container()
      ],
    );
//    );
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating,String title1,title2,Color color}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: color,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
             title1,
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              title2,
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
