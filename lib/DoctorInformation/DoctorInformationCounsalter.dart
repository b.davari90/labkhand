import 'package:counsalter5/Collections.dart'; ///////////////////وقتی دکمه تایید در هر صفحه ای خورده شد، تمام اطلاعات در بلاک ذخیره میشوند و پروگرس ابر و تیک مقدارشون براساس بلاک پر میشه مثلا 10درصد از داده ها
import 'package:counsalter5/DoctorInformation/DoctorInformationPage.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../config.dart';
import 'DoctorInformationPage.dart';
import 'package:counsalter5/ModelBloc/DoctorPriceModel.dart';

class DoctorInformationCounsalter extends StatefulWidget {
  bool isColorgrey = false, loading = true, isShowContainer = false;
  final _formKeyDoctorCounsalter = GlobalKey<FormBuilderState>();
  DoctorModel doctorInformationCounsalter;
  SharedPreferences sharedPreferences;
  String doctorItemTypeId = "";
  TextStyle title2 = TextStyle(
      color: Colors.black, fontSize: Conf.p2t(14), fontWeight: FontWeight.w500);

  @override
  _DoctorInformationCounsalterState createState() =>
      _DoctorInformationCounsalterState();
}

class _DoctorInformationCounsalterState
    extends State<DoctorInformationCounsalter> {
  TextStyle title2 = TextStyle(
      color: Colors.black, fontSize: Conf.p2t(16), fontWeight: FontWeight.w500);
  TextStyle titleGrey = TextStyle(
      color: Colors.grey, fontSize: Conf.p2t(15), fontWeight: FontWeight.w500);
  String checkBoxValu, textPrice, videoPrice, callPrice, chatPrice;
  int pChat, pCall, PVideoCall, pText;
  bool iscall = false,
      isVideo = false,
      isText = false,
      isChat = false,
      loading1 = true;
  int txtCallVal, txtChatval, txtVideoVal, txtTextval;
  String chat = "چت", call = "تلفنی", video = "تصویری", text = "متنی";
  List<Collections> collectionList = [];
  List<Collections> addCollectionList = [];
  List addCollectionList2 = [], addTypeList2 = [];
  List<DoctorPriceModel> priceList = [];
  DoctorModel doctorCounsalter;
  Collections collections;

  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  Future getDoctorType() async {
    try {
      setState(() {
        loading1 = true;
      });
      var data = await Htify.get('dr/doctor');
      setState(() {
        doctorCounsalter = DoctorModel.fromJson(data);
//        doctorBloc.state.iscall=false;
//        doctorBloc.state.istext=false;
//        doctorBloc.state.ischat=false;
//        doctorBloc.state.isvideo=false;
//        doctorBloc.state.iscall=true;
//        doctorBloc.state.istext=true;
//        doctorBloc.state.ischat=true;
//        doctorBloc.state.isvideo=true;
        doctorCounsalter.price.forEach((element) {
          if (element.type == 1) {
            textPrice = element.price.toString();
            doctorBloc.setTextPrice(textPrice);
            doctorBloc.setisText(true);
            print(" ___doctorBloc.state.istext===> ${ doctorBloc.state.istext }");
            print("textPrice ===> ${doctorBloc.state.priceText}");
          }
          if (element.type == 2) {
            videoPrice = element.price.toString();
            doctorBloc.setVideoPrice(videoPrice);
            doctorBloc.setisVideo(true);
            print("__doctorBloc.state.isvideo ===> ${ doctorBloc.state.isvideo  }");
            print("priceVideo ===> ${doctorBloc.state.priceVideo}");
          }
          if (element.type == 3) {
            callPrice = element.price.toString();
            doctorBloc.setCallPrice(callPrice);
            doctorBloc.setisCall(true);
            print("___doctorBloc.state.iscall===> ${ doctorBloc.state.iscall }");
            print("priceCall ===> ${doctorBloc.state.priceCall}");
          }
          if (element.type == 4) {
            chatPrice = element.price.toString();
            doctorBloc.setChatPrice(chatPrice);
            doctorBloc.setisChat(true);
            print("___doctorBloc.state.ischat ===> ${ doctorBloc.state.ischat }");
            print("priceChat ===> ${doctorBloc.state.priceChat}");
          }
        });
        doctorCounsalter.doctor_collections.forEach((element) {
          addCollectionList2.add(element.collection.id.toString());
        });
      });
      print("addCollectionList2.ma ===> ${addCollectionList2.map((e) => e)}");
      doctorBloc.setDoctor(doctorCounsalter);
      print("&&  addTypeList2.m ===> ${addTypeList2.map((e) => e)}");
    } catch (e) {
      print("CATCH ERROR getDoctor===> ${e.toString()}");
    } finally {
      setState(() {
        loading1 = false;
      });
    }
  }

  Future getListDataCollection() async {
    print("222222 ===> ${222222}");
    try {
      setState(() {
        widget.loading = false;
      });
      print("11111 ===> ${11111}");
      var data = await Htify.get('dr/collection');
      print(data);
      List contetntList = data;
      print("__contetntList ===> ${contetntList}");
      collectionList =
          contetntList.map((item) => Collections.fromJson(item)).toList();
      print(collectionList);
    } catch (e) {
      print("e.toString() ===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  Future setDoctorInformCounsalter() async {
    setState(() {
      widget.loading = false;
    });
    try {
      print( "222  bloc.state.doctor.specialty ===> ${doctorBloc.state.doctor.specialty}");
      print(" 222  a===> ${doctorBloc.state.doctor.description}");
      print("0000 addTypeList2 ===> ${addTypeList2.length}");
      print("000 addTypeList2 ===> ${addTypeList2.map((e) => e)}");
      print("22 addCollectionList2.length ===> ${addCollectionList2.length}");
      print(
          "11 addCollectionList2.length ===> ${addCollectionList2.map((e) => e)}");
      print("222111 ===> ${priceList.map((e) => e.type)}");
      Map data = await Htify.create("dr/doctor", {
        "data": {
          "description": doctorBloc.state.doctor.description,
          "collections": addCollectionList2,
          "specialty": doctorBloc.state.doctor.specialty,
          "types": addTypeList2,
          "price": priceList.map((e) => e.toJson()).toList()
        },
        "op": "collection"
      });
      print(
          "@@@  specialty ===> ${widget.doctorInformationCounsalter.specialty}");
      print("@@@  Conf.access_token ===> ${Conf.token}");
      Navigator.pushReplacement(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: DoctorInformationPage()));
    } catch (e) {
      print("CATCH ERROR  ===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  @override
  void initState() {
    collections = Collections();
    doctorCounsalter = DoctorModel();
    widget.doctorInformationCounsalter = DoctorModel();
    doctorBloc.state.priceCall="";
    doctorBloc.state.priceChat="";
    doctorBloc.state.priceVideo="";
    doctorBloc.state.priceText="";
    getListDataCollection();
    getDoctorType();
    if(doctorBloc.state.ischat==null||doctorBloc.state.ischat==""){isChat=isChat;}else{isChat=doctorBloc.state.ischat;}
    if(doctorBloc.state.isvideo==null||doctorBloc.state.isvideo==""){isVideo=isVideo;}else{isVideo=doctorBloc.state.isvideo;}
    if(doctorBloc.state.iscall==null||doctorBloc.state.iscall==""){iscall=iscall;}else{iscall=doctorBloc.state.iscall;}
    if(doctorBloc.state.istext==null||doctorBloc.state.istext==""){isText=isText;}else{isText=doctorBloc.state.istext;}
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Conf.token ===> ${Conf.token}");
    return SafeArea(child:
        BlocBuilder<DoctorBloc, DoctorRepository>(builder: (context, state) {
      return Scaffold(
          resizeToAvoidBottomInset: true,
          resizeToAvoidBottomPadding: false,
          bottomNavigationBar: Container(
              height: Conf.p2h(12),
              margin: EdgeInsets.only(
                  top: Conf.p2h(0),
                  bottom: Conf.p2h(1),
                  left: Conf.p2w(1.5),
                  right: Conf.p2w(4)),
              child:
              MasRaisedButton(
                  margin: EdgeInsets.only(
                      right: Conf.p2w(1),
                      left: Conf.p2w(4),
                      top: Conf.p2w(1.5),
                      bottom: Conf.p2w(3)),
                  borderSideColor: Conf.orang,
                  text: "تایید",
                  color: Conf.orang,
                  textColor: Colors.white,
                  onPress: () async {
                    widget._formKeyDoctorCounsalter.currentState.save();
                    bool validate =
                        widget._formKeyDoctorCounsalter.currentState.validate();
                    print("validate  is===> ${validate}");
                    if (validate) {
                      var result =
                          widget._formKeyDoctorCounsalter.currentState.value;
                      DoctorModel t = doctorBloc.state.doctor;
                      print("t is....===> ${t}");
                      print(" 778===> ${result["specialty"]}");
                      print("99===> ${result["description"]}");
                      t.specialty = result["specialty"];
                      t.description = result["description"];
                      print("result 44444 ===> ${result}");
                      print("44444444 ===> ${result["description"]}");
                      print("t.description 44444===> ${t.description}");
                      t.doctor_collections = result["doctor_collections"];
                      t.types = result["types"];
                      doctorBloc.setDoctor(t);
                      print(
                          "**bloc.state.doctor.description ===> ${doctorBloc.state.doctor.description}");
                      checkFunc();
                      await setDoctorInformCounsalter();
                    } else {
                      print("validation failed");
                      _showTopFlash();
                    }
                  })),
          body: Stack(children: [
            Positioned(
              top: Conf.p2w(6),
              right: 0,
              left: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        color: Colors.red,
                        width: Conf.p2w(80),
                      ),
                      Text("نوع مشاوره",
                          style: Conf.headline.copyWith(
                              color: Colors.black87,
                              fontSize: Conf.p2t(23),
                              fontWeight: FontWeight.w500,
                              decoration: TextDecoration.none)),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [MasBackBotton()],
                  ),
                ],
              ),
            ),
            Positioned(
                top: Conf.p2w(10),
                right: 0,
                left: 0,
                child: Container(
                  margin: EdgeInsets.only(top: Conf.p2h(8)),
                  height: Conf.p2h(70),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.4),
                          spreadRadius: 1,
                          blurRadius: 10,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(35),
                          topLeft: Radius.circular(35))),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(35),
                        topLeft: Radius.circular(35)),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(
                            right: Conf.xlSize + 6, top: Conf.xlSize),
                        child: FormBuilder(
                          key: widget._formKeyDoctorCounsalter,
                          autovalidateMode: AutovalidateMode.always,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("نوع مشاوره", style: title2),
                              IntrinsicHeight(
                                child: Row(
                                  children: [
                                    Container(
                                        width: Conf.p2w(30),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            FormBuilderCheckbox(
                                                leadingInput: true,
                                                contentPadding: EdgeInsets.zero,
                                                activeColor: Conf.blue,
                                                tristate: false,
                                                decoration: InputDecoration(
                                                    border: InputBorder.none),
                                                initialValue:
                                                doctorBloc.state.istext,
//                                                    ==true?isText=true:isText=false,
                                                onChanged: (value) {
                                                  setState(() {
                                                    if (isText == false) {
                                                      isText = true;
//                                                      print("isText if===> ${isText}");
                                                      pText = 1;
                                                      addTypeList2.add(pText);
                                                      print("addTypeList2.length if===> ${addTypeList2.length}");
                                                      print("addTypeList2.map if===> ${addTypeList2.map((e) => e)}");
                                                    } else {
                                                      isText = false;
                                                      print("isText else===> ${isText}");
                                                      addTypeList2
                                                          .remove(pText);
                                                      print(
                                                          "addTypeList2.length else===> ${addTypeList2.length}");
                                                      print(
                                                          "addTypeList2.map else===> ${addTypeList2.map((e) => e)}");
                                                    }
                                                  });
                                                },
                                                label: Text(text,
                                                    style: widget.title2),
                                                validators: []),
                                            FormBuilderCheckbox(
                                                leadingInput: true,
                                                contentPadding: EdgeInsets.zero,
                                                activeColor: Conf.blue,
                                                tristate: false,
                                                decoration: InputDecoration(
                                                    border: InputBorder.none),
                                                initialValue: doctorBloc.state.isvideo,
//                                                    ==true?isVideo=true:isVideo=false,
                                                onChanged: (value) {
                                                  setState(() {
                                                    if (isVideo == false) {
                                                      isVideo = true;
                                                      print("isvideo if===> ${isVideo}");
                                                      PVideoCall = 2;
                                                      addTypeList2.add(PVideoCall);
                                                      print("addTypeList2.length if video===> ${addTypeList2.length}");
                                                      print("addTypeList2.map if video===> ${addTypeList2.map((e) => e)}");
                                                    } else {
                                                      isVideo = false;
                                                      print("isText else===> ${isVideo}");
//                                                          pText=0;
                                                      addTypeList2.remove(PVideoCall);
                                                      print(
                                                          "addTypeList2.length else video===> ${addTypeList2.length}");
                                                      print(
                                                          "addTypeList2.map else video===> ${addTypeList2.map((e) => e)}");
                                                    }
                                                  });
                                                },
                                                label: Text(video,
                                                    style: widget.title2),
                                                validators: []),
                                            FormBuilderCheckbox(
                                                leadingInput: true,
                                                contentPadding: EdgeInsets.zero,
                                                activeColor: Conf.blue,
                                                tristate: false,
                                                decoration: InputDecoration(
                                                    border: InputBorder.none),
                                                initialValue:
                                                    doctorBloc.state.iscall,
//                                                        ==true?iscall=true:iscall=false,
                                                onChanged: (value) {
                                                  setState(() {
                                                    if (iscall == false) {
                                                      iscall = true;
                                                      print(
                                                          "iscall if===> ${iscall}");
                                                      pCall = 3;
                                                      addTypeList2.add(pCall);
                                                      print(
                                                          "addTypeList2.length if iscall===> ${addTypeList2.length}");
                                                      print(
                                                          "addTypeList2.map if iscall===> ${addTypeList2.map((e) => e)}");
                                                    } else {
                                                      iscall = false;
                                                      print(
                                                          "iscall else===> ${iscall}");
//                                                          pText=0;
                                                      addTypeList2
                                                          .remove(pCall);
                                                      print(
                                                          "addTypeList2.length else iscall===> ${addTypeList2.length}");
                                                      print(
                                                          "addTypeList2.map else iscall===> ${addTypeList2.map((e) => e)}");
                                                    }
                                                  });
                                                },
                                                label: Text(call,
                                                    style: widget.title2),
                                                validators: []),
                                            FormBuilderCheckbox(
                                                leadingInput: true,
                                                contentPadding: EdgeInsets.zero,
                                                activeColor: Conf.blue,
                                                tristate: false,
                                                decoration: InputDecoration(
                                                    border: InputBorder.none),
                                                initialValue:
                                                doctorBloc.state.ischat,
//                                                    ==true?isChat=true:isChat=false,
                                                onChanged: (value) {
                                                  setState(() {
                                                    if (isChat == false) {
                                                      isChat = true;
                                                      print("isChat if===> ${isChat}");
                                                      pChat = 3;
                                                      addTypeList2.add(pChat);
                                                      print(
                                                          "addTypeList2.length if isChat===> ${addTypeList2.length}");
                                                      print(
                                                          "addTypeList2.map if isChat===> ${addTypeList2.map((e) => e)}");
                                                    } else {
                                                      isChat = false;
                                                      print(
                                                          "isChat else===> ${isChat}");
//                                                          pText=0;
                                                      addTypeList2
                                                          .remove(pChat);
                                                      print(
                                                          "addTypeList2.length else isChat===> ${addTypeList2.length}");
                                                      print(
                                                          "addTypeList2.map else isChat===> ${addTypeList2.map((e) => e)}");
                                                    }
                                                  });
                                                },
                                                label: Text(chat,
                                                    style: widget.title2),
                                                validators: []),
                                          ],
                                        )),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          width: Conf.p2w(52),
                                          height: Conf.p2w(16),
                                          child: Conf.textFieldFormBuilder2(
                                              () {},
                                              [],
                                              TextInputType.number,
                                              false,
                                              lable:doctorBloc.state.priceText?? " هزینه مشاوره متنی...",
                                              hasPadding: true,
                                              isActive:
//                                              true,
                                                  isText == true ? true : false,
                                              initialVal:textPrice,
                                              onChange: (value) {
                                            print("776676767 ===> ${776676767}");
                                            print("value ===> ${value}");
                                            print("txtTextval ===> ${txtTextval}");
//                                                setState(() {
                                            txtTextval = int.parse(value);
                                            print("666===> ${txtTextval}");
                                          }),
                                        ),
                                        Container(
                                          width: Conf.p2w(52),
                                          height: Conf.p2w(16),
                                          child: Conf.textFieldFormBuilder2(
//                                                  pChat.toString(),
                                              () {},
                                              [],
                                              TextInputType.number,
                                              true,
                                              lable:doctorBloc.state.priceVideo??" هزینه مشاوره تصویری...",
                                              hasPadding: true,
                                              isActive:
//                                              true,
                                              isVideo == true
                                                  ? true
                                                  : false,
                                              initialVal:videoPrice,
                                              onChange: (value) {
                                            print("value ===> ${value}");
                                            print(
                                                "txtVideoVal ===> ${txtVideoVal}");
//                                                setState(() {
                                            txtVideoVal = int.parse(value);
                                            print("666===> ${txtVideoVal}");
                                          }),
                                        ),
                                        Container(
                                          width: Conf.p2w(52),
                                          height: Conf.p2w(16),
                                          child: Conf.textFieldFormBuilder2(
//                                                  PVideoCall.toString(),
                                              () {},
                                              [],
                                              TextInputType.number,
                                              true,
                                              lable: doctorBloc.state.priceCall??" هزینه مشاوره تلفنی...",
                                              hasPadding: true,
                                              isActive:
//                                              true,
                                                  iscall == true ? true : false,
                                              initialVal:callPrice,
                                              onChange: (value) {
                                            print("value ===> ${value}");
                                            print(
                                                "txtCallVal===> ${txtCallVal}");
//                                                setState(() {
                                            txtCallVal = int.parse(value);
                                            print("1151===> ${txtCallVal}");
                                          }),
                                        ),
                                        Container(
                                          width: Conf.p2w(52),
                                          height: Conf.p2w(16),
                                          child: Conf.textFieldFormBuilder2(
//                                                  pText.toString(),
                                              () {},
                                              [],
                                              TextInputType.number,
                                              true,
                                              lable:doctorBloc.state.priceChat?? " هزینه مشاوره چت...",
                                              hasPadding: true,
                                              isActive:
//                                              true,
                                                  isChat == true ? true : false,
                                              initialVal:chatPrice,
                                              onChange: (value) {
                                            print("value ===> ${value}");
                                            print(
                                                "txtChatval ===> ${txtChatval}");
                                            txtChatval = int.parse(value);
                                            print("1100===> ${txtChatval}");
                                          }),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    bottom: Conf.p2w(3), top: Conf.p2w(1.5)),
                                child: Text(
                                  "امکان انتخاب چند گزینه وجود دارد",
                                  style: titleGrey,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: Conf.p2h(3)),
                                child: Text(
                                  "در چه حوزه ای قصد ارائه مشاوره دارید؟",
                                  style: title2,
                                ),
                              ),
                              FormBuilderCheckboxGroup(
                                  orientation:
                                      GroupedCheckboxOrientation.vertical,
                                  tristate: false,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  attribute: "languages",
                                  initialValue: addCollectionList2,
                                  onChanged: (val) {
                                    if (!addCollectionList2.contains(val)) {
                                      addCollectionList2.add(val);
                                    }
                                    addCollectionList2.remove(val);
                                    print(
                                        "++  addCollectionList2.length ===> ${addCollectionList2.length}");
                                    print(
                                        "++  addCollectionList2.ma ===> ${addCollectionList2.map((e) => e.toString())}");
                                  },
//                                      ['1'],
//                                      enabled: (titles!=null)?false:true,
//                                      initialValue: bloc.state.doctor.doctorCollections.map((e) => e.collection.id).toList(),
                                  options: collectionList
                                      .map((e) => FormBuilderFieldOption(
                                            value: e.id.toString(),
                                            child: Text(e.title),
                                          ))
                                      .toList()),
                              Padding(
                                padding: EdgeInsets.only(
                                    right: Conf.p2w(3),
                                    top: Conf.p2w(1.5),
                                    bottom: Conf.p2w(3)),
                                child: Text(
                                  "میتوانید حداکثر 4 مورد را انتخاب کنید",
                                  style: titleGrey,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: Conf.p2h(3)),
                                child: Text(
                                  "لطفا تخصص خود را وارد کنید",
                                  style: title2,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: Conf.p2w(6)),
                                child: Conf.textFieldFormBuilder2(
                                  () {},
                                  [
                                    FormBuilderValidators.required(
                                        errorText: ""),
                                  ],
                                  TextInputType.text,
                                  true,
                                  lable: doctorBloc.state.doctor.specialty??'تخصص....',
                                  hasPadding: true,
                                  atrribute: "specialty",
//                                  initialVal: doctorBloc.state.doctor.specialty,
                                  isActive: (state.doctor.specialty != null)
                                      ? false
                                      : true,
                                ),
                              ),
                              Text(
                                "بیوگرافی شغلی :",
                                style: title2,
                              ),
                              Container(
//                                  color: Colors.red,height: Conf.p2w(80),
                                padding: EdgeInsets.only(
                                    left: Conf.p2w(6), bottom: Conf.p2w(3)),
                                margin: EdgeInsets.only(
                                    bottom: Conf.p2w(47), top: Conf.p2w(2)),
                                child: Conf.textFieldFormBuilder2(
                                    () {},
                                    [
                                      FormBuilderValidators.required(
                                          errorText: ""),
                                    ],
                                    TextInputType.text,
                                    true,
                                    lable: doctorBloc.state.doctor.description??'بیوگرافی شغلی....',
                                    hasPadding: true,
                                    maxLines: 7,
                                    atrribute: "description",
//                                    initialVal: ,
                                    expand: true,
                                    isActive: (state.doctor.description != null)
                                        ? false
                                        : true),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
            widget.loading == false
                ? Conf.circularProgressIndicator()
                : Container()
          ]));
    }));
  }

  checkFunc() {
    print("isVideo ===> ${isVideo.toString()}");
    print("isCall ===> ${iscall.toString()}");
    print("isChat ===> ${isChat.toString()}");
    print("isVoice ===> ${isText.toString()}");
    if (isText) {
      DoctorPriceModel d = DoctorPriceModel(price: txtTextval, type: pText);
//      DoctorTypeModel d = DoctorTypeModel(type_str: pText.toString(), type: pText);
      if (!priceList.contains(d)) {
        priceList.add(d);
        print("priceList.length isText===> ${priceList.length}");
      } else {
        priceList.remove(d);
        print("priceList.length isText===> ${priceList.length}");
      }
    }
    if (isChat) {
      DoctorPriceModel dd = DoctorPriceModel(price: txtChatval, type: pChat);
      if (!priceList.contains(dd)) {
        priceList.add(dd);
        print("priceList.length isChat===> ${priceList.length}");
      } else {
        priceList.remove(dd);
        print("priceList.length isChat===> ${priceList.length}");
      }
    }
    if (isVideo) {
      DoctorPriceModel ddd =
          DoctorPriceModel(price: txtVideoVal, type: PVideoCall);
      if (!priceList.contains(ddd)) {
        priceList.add(ddd);
        print("priceList.length isVideo===> ${priceList.length}");
      } else {
        priceList.remove(ddd);
        print("priceList.length isVideo===> ${priceList.length}");
      }
    }
    if (iscall) {
      DoctorPriceModel dddd = DoctorPriceModel(price: txtCallVal, type: pCall);
      if (!priceList.contains(dddd)) {
        priceList.add(dddd);
        print("priceList.length iscall===> ${priceList.length}");
      } else {
        priceList.remove(dddd);
        print("priceList.length iscall===> ${priceList.length}");
      }
    }
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              'اطلاعات را به طور کامل وارد کنید',
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
