import 'package:counsalter5/DoctorInformation/DoctorInformationPage.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorInformationBank extends StatefulWidget {
  final _formKeyDoctorRegisterBank = GlobalKey<FormBuilderState>();
  TextEditingController controllerShabaNumber, controllerAccount;
  DoctorModel doctorRegisterBank;
  SharedPreferences sharedPreferences;
  bool loading = true,load=true;

  @override
  _DoctorInformationBankState createState() => _DoctorInformationBankState();
}

class _DoctorInformationBankState extends State<DoctorInformationBank> {
  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();
  Future getDoctorBaseInfo() async {
    try {
      setState(() {
        widget.load = true;
      });
      Map data = await Htify.get('dr/doctor');
      widget.doctorRegisterBank.fname = data["fname"];
      widget.doctorRegisterBank.lname = data["lname"];
      widget.doctorRegisterBank.name = data["name"];
      widget.doctorRegisterBank.city_code = data["city_code"];
      widget.doctorRegisterBank.n_code= data["n_code"];
      widget.doctorRegisterBank.n_doctor= data["n_doctor"];
      widget.doctorRegisterBank.phone = data["phone"];
      widget.doctorRegisterBank.email = data["email"] ;
      widget.doctorRegisterBank.address = data["address"] ;
      widget.doctorRegisterBank.birth_day = data["birth_day"];
      widget.doctorRegisterBank.city_id = data["city_id"] ;
      widget.doctorRegisterBank.mobile = data["mobile"] ;
      widget.doctorRegisterBank.inf_personal = data["inf_personal"] ;
      widget.doctorRegisterBank.inf_bank = data["inf_bank"] ;
      widget.doctorRegisterBank.inf_doc = data["inf_doc"] ;
      widget.doctorRegisterBank.inf_collection = data["inf_collection"] ;
      widget.doctorRegisterBank.isSelelectedTimeSlot = data["isSelelectedTimeSlot"] ;
      widget.doctorRegisterBank.active = data["active"] ;
      widget.doctorRegisterBank.shaba_number = data["shaba_number"] ;
      widget.doctorRegisterBank.account_number = data["account_number"] ;
      widget.doctorRegisterBank.specialty=data["specialty"];
      widget.doctorRegisterBank.description=data["describtion"];
      widget.doctorRegisterBank.account_number=data["account_number"];
      widget.doctorRegisterBank.shaba_number=data["shaba_number"];

//      widget.doctorInformationCounsalter.doctor_collections = data["doctor_collections"];
//      print("77777===> ${ data["doctor_collections"] }");
//      print("***d.doctorCollections ===> ${ widget.doctorInformationCounsalter.doctor_collections }");
//      titles.add(d.doctor_collections.map((e) => e.collection.id).toList());
      print("d.account_number ===> ${ widget.doctorRegisterBank.account_number }");
      print("d.shaba_number ===> ${ widget.doctorRegisterBank.shaba_number}");
      doctorBloc.setDoctor(widget.doctorRegisterBank);
      print("____d.fname  ===> ${ widget.doctorRegisterBank.fname}");
    } catch (e) {
      print("CATCH ERROR getDoctor===> ${e.toString()}");
    } finally {
      setState(() {
        widget.load = false;
      });
    }
  }
  Future setDoctorRegisterBank() async {
    setState(() {
      widget.loading = false;
    });
    try {
      print("doctorRegisterBank.shaba_number ===> ${widget.doctorRegisterBank.shaba_number}");
      print("doctorRegisterBank.account_number ===> ${widget.doctorRegisterBank.account_number}");

      var data = await Htify.create("dr/doctor", {
        "data": {
          "account_number": doctorBloc.state.doctor.account_number,
          "shaba_number":  doctorBloc.state.doctor.shaba_number,
        },
        "op": "bank"
      });
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: DoctorInformationPage()));
    } catch (e) {
      print("e ===> ${e}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  @override
  void initState() {
    widget.doctorRegisterBank = DoctorModel();
    widget.controllerShabaNumber = TextEditingController();
    widget.controllerAccount = TextEditingController();
    getDoctorBaseInfo();
    super.initState();
  }

  @override
  void dispose() {
    widget.controllerShabaNumber.dispose();
    widget.controllerAccount.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            bottomNavigationBar: Container(
              width: Conf.p2w(100),
//              height: Conf.p2h(20),
              color: Colors.grey.shade100,
              child: Container(
                  width: Conf.p2w(100),
                  height: Conf.p2h(12),
                  margin: EdgeInsets.only(
                      top: Conf.p2h(1),
                      bottom: Conf.p2h(4),
                      right: Conf.p2w(4),
                      left: Conf.p2w(4)),
                  child: MasRaisedButton(
                      margin: EdgeInsets.only(
                          right: Conf.p2w(1.5),
                          left: Conf.p2w(1),
                          top: Conf.p2w(1.5),
                          bottom: Conf.p2w(3)),
                      borderSideColor: Conf.orang,
                      text: "تایید",
                      color: Conf.orang,
                      textColor: Colors.white,
                      onPress: () async {
                        widget._formKeyDoctorRegisterBank.currentState.save();
                        print( "***********&&&****_formKey.currentState.value.save() ===> ${widget._formKeyDoctorRegisterBank.currentState.value}");
                        bool validate =widget._formKeyDoctorRegisterBank.currentState.validate();
                        if (validate) {
                          var result = widget._formKeyDoctorRegisterBank.currentState.value;
                          widget.doctorRegisterBank.account_number = result["account_number"];
                          widget.doctorRegisterBank.shaba_number = result["shaba_number"];

                          DoctorModel t  = doctorBloc.state.doctor;
                          t.account_number =result["account_number"];
                          t.shaba_number =result["shaba_number"];
                          print("t.account_number ===> ${ t.account_number }");
                          print(" ===> ${t.shaba_number  }");
                          doctorBloc.setDoctor(t);
                          await setDoctorRegisterBank();
                        } else {
                          print("validation failed");
                          _showTopFlash();
                        }
                      })),
            ),
            body: BlocBuilder<DoctorBloc, DoctorRepository>(builder: (context, state){
              return
                Stack(
                children: [
                  Positioned(
                    left: 0,
                    right: 0,
                    top: Conf.p2w(6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            Container(
                              color: Colors.red,
                              width: Conf.p2w(80),
                            ),
                            Text("اطلاعات بانکی",
                                style: Conf.headline.copyWith(
                                    color: Colors.black87,
                                    fontSize: Conf.p2t(23),
                                    fontWeight: FontWeight.w500,
                                    decoration: TextDecoration.none)),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [MasBackBotton()],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    top: Conf.p2w(26),
                    child: Container(
                        height: Conf.p2h(100),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade100,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.4),
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset:
                                Offset(0, 3), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(35),
                                topLeft: Radius.circular(35))),
                        child: Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(6), right: Conf.p2w(5)),
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  topLeft: Radius.circular(10)),
                              child: Flex(
                                direction: Axis.vertical,
                                children: [
                                  Expanded(
                                      child: SingleChildScrollView(
                                        child: FormBuilder(
                                            key: widget._formKeyDoctorRegisterBank,
                                            autovalidateMode: AutovalidateMode.onUserInteraction,
                                            child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: Conf.xlSize,top:Conf.xlSize ),
                                                    child:
                                                    Conf.textFieldFormBuilder2(
                                                            () {},
                                                        [
                                                          FormBuilderValidators
                                                              .minLength(10,
                                                              errorText:
                                                              "*حداقل 10 رقم")
                                                        ],
                                                        TextInputType.number,
                                                        true,
                                                        lable: 'شماره شبا',
                                                        atrribute:  "shaba_number",
                                                        initialVal:state.doctor.shaba_number,
                                                        isActive: (state.doctor.shaba_number==""||state.doctor.shaba_number==null)?true:false,
                                                        hasPadding: true),

                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        top: Conf.p2h(0.5),
                                                        bottom: Conf.p2h(3),
                                                        right: Conf.p2w(1)),
                                                    child: Text(
                                                      "شماره شبا باید متعلق به فرد ثبت نام کننده باشد",
                                                      style: TextStyle(
                                                          color: Colors.black87
                                                              .withOpacity(0.4),
                                                          fontSize: Conf.p2t(15),
                                                          fontWeight:
                                                          FontWeight.w800),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: Conf.xlSize),
                                                    child: Conf.textFieldFormBuilder2(
                                                            () {},
                                                        [
                                                          FormBuilderValidators
                                                              .minLength(11,
                                                              errorText:
                                                              "*حداقل 10 رقم")
                                                        ],
                                                        TextInputType.number,
                                                        true,
                                                        lable:'شماره حساب',
                                                        atrribute: "account_number",
                                                        initialVal:state.doctor.account_number,
                                                        isActive: (state.doctor.account_number==""||state.doctor.account_number==null)?true:false,
                                                        hasPadding: true),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        right: Conf.p2w(1),
                                                        top: Conf.p2w(1)),
                                                    child: Text(
                                                      "شماره حساب باید متعلق به فرد ثبت نام کننده باشد",
                                                      style: TextStyle(
                                                          color: Colors.black87
                                                              .withOpacity(0.4),
                                                          fontSize: Conf.p2t(15),
                                                          fontWeight:
                                                          FontWeight.w800),
                                                    ),
                                                  ),
                                                ])),
                                      ))
                                ],
                              ),
                            )
                        )),
                  ),widget.load == true ? Conf.circularProgressIndicator() : Container()
                ],
              );
            }
            )));
  }
  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Padding(
            padding:EdgeInsets.only(right: Conf.p2w(2)),
            child: Icon(FeatherIcons.alertOctagon,color: Colors.white,size: 40,),
          ),
            title: Text('خطا',style: TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w600,color: Colors.white),),
            message: Text('اطلاعات به درستی وارد نشده است',style: TextStyle(fontSize:Conf.p2t(12),fontWeight: FontWeight.w600,color: Colors.white),),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}


//Widget fillRow(
//    String hint, TextInputType keybordType, TextEditingController controller) {
//  return Padding(
//    padding: EdgeInsets.only(top: Conf.p2h(1),left: Conf.p2w(5)),
//    child: Container(
//        height: Conf.p2h(7.5),
//        width: Conf.p2w(90),
////      margin: EdgeInsets.only(top: Conf.p2h(0.5), bottom: Conf.p2h(0.5)),
//        decoration: BoxDecoration(
////              color: Colors.purpleAccent,
//            borderRadius: BorderRadius.all(Radius.circular(10))
////        border: Border.all(width: 2,color:Colors.orange)
//            ),
//        child: TextField(
////        onChanged: (String value){if (value.isEmpty||value!=null){
////
////        }},
//          controller: controller,
//          keyboardType: keybordType ?? null,
//          decoration: InputDecoration(
//            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//            hintText: hint,
//            hintStyle: TextStyle(
//                color: Colors.black87.withOpacity(0.7),
//                fontSize: Conf.p2t(16),
//                fontWeight: FontWeight.bold),
//            border: OutlineInputBorder(
//                borderSide: BorderSide(color: Colors.black45, width: 2),
//                borderRadius: const BorderRadius.all(
//                  const Radius.circular(13.0),
//                )),
//          ),
//        )),
//  );
//}
