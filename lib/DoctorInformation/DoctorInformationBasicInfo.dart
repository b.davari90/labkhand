import 'package:counsalter5/DoctorInformation/DoctorInformationPage.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/SearchDelegateCityTown/city.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc2.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/htify.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:counsalter5/SearchDelegateCityTown/province_city_page2.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc.dart';

class DoctorInformationBasicInfo extends StatefulWidget {
  TextStyle txtStyle = TextStyle(
      height: 0.5,
      fontSize: Conf.p2t(15),
      color: Colors.black54.withOpacity(0.4),
      fontWeight: FontWeight.w900);
  DoctorModel doctorRegister;
  Map<String, dynamic> cityTownList;
  var contetntList;
  String dpValTown, dpValueCity, nameCity = "", codeCity;

  String telValue,
      nameValue,
      fnameValue,
      birthdayValue,
      nezamValue,
      meliValue,
      emailValue,
      adressValue;
  int cityId;
  var a, b;
  List<String> c = [];
  List<City> cityList = [];
  bool loading = false, loading1 = false;
  SharedPreferences sharedPreferences;
  List<DoctorModel> doctorList = [];

  DoctorInformationBasicInfo(
      {this.nameCity,
      this.emailValue,
      this.adressValue,
      this.nezamValue,
      this.meliValue,
      this.codeCity,
      this.cityId,
      this.birthdayValue,
      this.nameValue,
      this.fnameValue,
      this.telValue});

  @override
  _DoctorInformationBasicInfoState createState() =>
      _DoctorInformationBasicInfoState();
}

class _DoctorInformationBasicInfoState extends State<DoctorInformationBasicInfo>
    with AutomaticKeepAliveClientMixin {
  DoctorBloc get bloc => context.bloc<DoctorBloc>();
  final _formKeyDoctorRegister = GlobalKey<FormBuilderState>();
  final TextEditingController textEditingController =
      TextEditingController(text: "");
  PersianDatePickerWidget persianDatePicker;

  CityProvinceCubit2 get cityBloc2 => context.bloc<CityProvinceCubit2>();

  Future getCityList() async {
    try {
      setState(() {
        widget.loading = false;
      });
      var data = await Htify.get('site/city');
      print("data ===> ${data}");
      List<City> t = <City>[];
      data.forEach((item) => t.add(City.fromJson(item)));
      context.bloc<CityProvinceCubit>().setProvinceCity((t));
    } catch (e) {
      print("e.toString() ===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  Future getDoctorBaseInfo() async {
    try {
      setState(() {
        widget.loading1 = true;
      });
      Map data = await Htify.get('dr/doctor');
      print(data);

      widget.doctorRegister.fname = data["fname"];
      widget.doctorRegister.lname = data["lname"];
      widget.doctorRegister.name = data["name"];
      widget.doctorRegister.city_code = data["city_code"];
      widget.doctorRegister.n_code = data["n_code"];
      widget.doctorRegister.n_doctor = data["n_doctor"];
      widget.doctorRegister.phone = data["phone"];
      widget.doctorRegister.email = data["email"];
      widget.doctorRegister.address = data["address"];
      widget.doctorRegister.birth_day = data["birth_day"];
      widget.doctorRegister.city_id = data["city_id"];
      widget.doctorRegister.city = data["city"];
      widget.doctorRegister.mobile = data["mobile"];
      widget.doctorRegister.inf_personal = data["inf_personal"];
      widget.doctorRegister.inf_bank = data["inf_bank"];
      widget.doctorRegister.inf_doc = data["inf_doc"];
      widget.doctorRegister.inf_collection = data["inf_collection"];
      widget.doctorRegister.isSelelectedTimeSlot = data["isSelelectedTimeSlot"];
      widget.doctorRegister.active = data["active"];
      widget.doctorRegister.mobile = data["mobile"];
      bloc.setDoctor(widget.doctorRegister);
      widget.birthdayValue = widget.doctorRegister.birth_day;
      print("___bloc.state.doctor.city  ===> ${bloc.state.doctor.city}");
      print("____widget.nameCity**** ===> ${widget.nameCity}");
    } catch (e) {
      print("CATCH ERROR getDoctorBaseInfo===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading1 = false;
      });
    }
  }

  Future setDoctorRegister() async {
    setState(() {
      widget.loading = false;
    });
    try {
      var data = await Htify.create("dr/doctor", {
        "data": {
          "fname": bloc.state.doctor.fname,
          "lname": bloc.state.doctor.lname,
          "name": bloc.state.doctor.fname + " " + bloc.state.doctor.lname,
          "n_code": bloc.state.doctor.n_code,
          "n_doctor": bloc.state.doctor.n_doctor,
          "avatar": "",
          "birth_day": bloc.state.doctor.birth_day,
          "phone": bloc.state.doctor.phone,
          "email": bloc.state.doctor.email,
          "address": bloc.state.doctor.address,
          "city_id": widget.cityId,
          "city_code": widget.codeCity,
          "degree": 2
        },
        "op": "personally"
      });
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: DoctorInformationPage()));
    } catch (e) {
      _showTopFlash();
      print("e  SETDOCTORREGISTER===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  @override
  void initState() {
    print("widget.birthdayValue ===> ${widget.birthdayValue}");
    print("widget.nameValue ===> ${widget.nameValue}");
    print("widget.fnameValue ===> ${widget.fnameValue}");
    print("widget.telValue ===> ${widget.telValue}");
    print("___widget.nameCity ===> ${widget.nameCity}");
    print("widget.codeCity ===> ${widget.codeCity}");
    print("widget.cityId ===> ${widget.cityId}");
    print("widget.emailValue ===> ${widget.emailValue}");
    print("widget.nezamValue ===> ${widget.nezamValue}");
    print("widget.adressValue ===> ${widget.adressValue}");
    print("widget.meliValue ===> ${widget.meliValue}");

    persianDatePicker = PersianDatePicker(
      controller: textEditingController,
    ).init();

    widget.doctorRegister = DoctorModel();
    getCityList();
    getDoctorBaseInfo();
    if (bloc.state.doctor.city == null ||bloc.state.doctor.city == "") {
      widget.nameCity = widget.nameCity;
      print("initState widget.nameCity IF===> ${ widget.nameCity }");
    } else {
      widget.nameCity = bloc.state.doctor.city;
      print("initState widget.nameCity ELSE===> ${ widget.nameCity }");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      String token = Conf.token;
      print(" ===> ${token.toString()}");
    });
    return Stack(
      children: [
        SafeArea(
            child: Scaffold(
          resizeToAvoidBottomPadding: true,
          resizeToAvoidBottomInset: false,
          bottomSheet: Container(
            width: Conf.p2w(100),
            color: Colors.grey.shade200,
            child: Container(
                width: Conf.p2w(100),
                height: Conf.p2h(12),
                child: MasRaisedButton(
                  margin: EdgeInsets.only(
                      right: Conf.p2w(4.2),
                      left: Conf.p2w(3.5),
                      top: Conf.p2w(1.5),
                      bottom: Conf.p2w(2)),
                  borderSideColor: Conf.orang,
                  text: "تایید",
                  color: Conf.orang,
                  textColor: Colors.white,
                  onPress: () async {
                    _formKeyDoctorRegister.currentState.save();
                    print(
                        "***********&&&****_formKey.currentState.value.save() ===> ${_formKeyDoctorRegister.currentState.value}");
                    bool validate =
                        _formKeyDoctorRegister.currentState.validate();
                    print("validate ===> ${validate}");
                    if (validate) {
                      var result = _formKeyDoctorRegister.currentState.value;
                      print("result ===> ${result}");
                      DoctorModel t = bloc.state.doctor;
                      print("t is....===> ${t}");
                      t.fname = result["fname"];
                      print("t.fname ===> ${t.fname}");
                      t.email = result["email"];
                      t.lname = result["lname"];
                      t.name = result["fname"] + result["lname"];
                      t.n_code = result["n_code"];
                      t.n_doctor = result["n_doctor"];
                      t.phone = result["phone"];
                      t.address = result["address"];
                      t.birth_day = result["birth_day"];
                      bloc.setDoctor(t);
                      print("5656565 ===> ${t.toString()}");
                      await setDoctorRegister();
                    } else
                      print("validation is not complete");
                  },
                )),
          ),
          body: BlocBuilder<DoctorBloc, DoctorRepository>(
            builder: (context, state) {
              print(
                  "bloc.state.doctor.n_doctor ===> ${bloc.state.doctor.n_doctor}");
              return Column(children: [
                Container(
                  margin: EdgeInsets.only(top: Conf.p2w(8)),
                  width: Conf.p2w(100),
                  height: Conf.p2w(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Container(
                            color: Colors.grey,
                            width: Conf.p2w(80),
                          ),
                          Text("اطلاعات اولیه",
                              style: Conf.headline.copyWith(
                                  color: Colors.black87,
                                  fontSize: Conf.p2t(21),
                                  fontWeight: FontWeight.w600,
                                  decoration: TextDecoration.none)),
                        ],
                      ),
                      MasBackBotton(),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: Conf.p2w(6)),
                  width: Conf.p2w(100),
                  height: Conf.p2h(100),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.4),
                          spreadRadius: 1,
                          blurRadius: 10,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30))),
                  child: Padding(
                    padding: EdgeInsets.only(top: Conf.p2h(0.1), right: 0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      child: SingleChildScrollView(
                        child: FormBuilder(
                          key: _formKeyDoctorRegister,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: Padding(
                            padding: EdgeInsets.only(
                                right: Conf.xlSize, left: Conf.xlSize),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.required(
                                            errorText: ""),
                                        FormBuilderValidators.minLength(3,
                                            errorText: "حداقل 3 حرف")
                                      ],
                                      TextInputType.text,
                                      true,
                                      lable: 'نام',
                                      atrribute: "fname",
                                      initialVal: bloc.state.doctor.fname ??
                                          widget.nameValue,
                                      isActive: (bloc.state.doctor.fname ==
                                                  "" ||
                                              bloc.state.doctor.fname == null)
                                          ? true
                                          : false,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.nameValue = val;
                                      print(
                                          "widget.telValue ===> ${widget.telValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "")
                                      ],
                                      TextInputType.text,
                                      true,
                                      lable: 'نام و نام خانوادگی',
                                      atrribute: "lname",
                                      isActive: (bloc.state.doctor.lname ==
                                                  "" ||
                                              bloc.state.doctor.lname == null)
                                          ? true
                                          : false,
                                      initialVal: state.doctor.lname ??
                                          widget.fnameValue,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.fnameValue = val;
                                      print(
                                          "widget.fnameValue ===> ${widget.fnameValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.minLength(10,
                                            errorText: "حداقل 10 عدد*"),
                                        FormBuilderValidators.maxLength(10,
                                            errorText:
                                                "تعداد اعداد وارد شده بیش از حد مجاز میباشد")
                                      ],
                                      TextInputType.number,
                                      true,
                                      atrribute: "n_code",
                                      lable: 'کد ملی ',
                                      isActive: (bloc.state.doctor.n_code ==
                                                  "" ||
                                              bloc.state.doctor.n_code == null)
                                          ? true
                                          : false,
                                      initialVal: state.doctor.n_code ??
                                          widget.meliValue,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.meliValue = val;
                                      print(
                                          "widget.meliValue ===> ${widget.meliValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.minLength(10,
                                            errorText: "10 عدد"),
                                        FormBuilderValidators.maxLength(10,
                                            errorText:
                                                "تعداد اعداد وارد شده بیش از حد مجاز میباشد")
                                      ],
                                      TextInputType.number,
                                      true,
                                      lable: 'شماره نظام پزشکی/روانشناسی ',
                                      initialVal: bloc.state.doctor.n_doctor ??
                                          widget.nezamValue,
                                      atrribute: "n_doctor",
                                      isActive:
                                          (bloc.state.doctor.n_doctor == "" ||
                                                  bloc.state.doctor.n_doctor ==
                                                      null)
                                              ? true
                                              : false,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.nezamValue = val;
                                      print(
                                          "widget.nezam ===> ${widget.nezamValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                    () {},
                                    [
                                      FormBuilderValidators.required(
                                          errorText:
                                              " مطابق الگوی 1399/04/01 وارد کنید")
                                    ],
                                    null,
                                    true,
                                    lable: " تاریخ تولد",
//                                    initialVal:"",
                                    atrribute: "birth_day",
                                    isActive: (bloc.state.doctor.n_doctor ==
                                                "" ||
                                            bloc.state.doctor.n_doctor == null)
                                        ? true
                                        : false,
                                    hasPadding: true,
                                    controller: textEditingController
                                      ..text = widget.birthdayValue,
                                    onTap: () {
                                      FocusScope.of(context).requestFocus(
                                          FocusNode()); // to prevent opening default keyboard
                                      showModalBottomSheet(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return persianDatePicker;
                                          });
//                                      setState(() {
//                                        widget.birthdayValue =textEditingController.text;
//                                        print( "widget.birthday ===> ${widget.birthdayValue}");
//                                      });
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.minLength(8,
                                            errorText: "*8 الی 11 عدد"),
                                        FormBuilderValidators.maxLength(11,
                                            errorText:
                                                "*تعداد بیش از حد مجاز میباشد")
                                      ],
                                      TextInputType.number,
                                      true,
                                      lable: 'تلفن ثابت ',
                                      atrribute: "phone",
                                      initialVal:
                                          state.doctor.phone ?? widget.telValue,
                                      isActive: (bloc.state.doctor.phone ==
                                                  "" ||
                                              bloc.state.doctor.phone == null)
                                          ? true
                                          : false,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.telValue = val;
                                      print(
                                          "widget.telValue ===> ${widget.telValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "x@gmail.com-*"),
                                        FormBuilderValidators.minLength(11,
                                            errorText:
                                                "حداقل یازده کارکتر باید باشد")
                                      ],
                                      TextInputType.text,
                                      true,
                                      lable: 'ایمیل',
                                      initialVal: state.doctor.email ??
                                          widget.emailValue,
                                      atrribute: "email",
                                      isActive: (bloc.state.doctor.email ==
                                                  "" ||
                                              bloc.state.doctor.email == null)
                                          ? true
                                          : false,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.emailValue = val;
                                      print(
                                          "widget.email ===> ${widget.emailValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Conf.textFieldFormBuilder2(
                                      () {},
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "")
                                      ],
                                      TextInputType.text,
                                      true,
                                      lable: 'آدرس کامل',
                                      atrribute: "address",
                                      initialVal: state.doctor.address ??
                                          widget.adressValue,
                                      isActive: (bloc.state.doctor.address ==
                                                  "" ||
                                              bloc.state.doctor.address == null)
                                          ? true
                                          : false,
                                      hasPadding: true, onChange: (val) {
                                    setState(() {
                                      widget.adressValue = val;
                                      print(
                                          "widget.adres ===> ${widget.adressValue}");
                                    });
                                  }),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.p2h(1)),
                                  child: Container(
                                    margin: EdgeInsets.only(
                                      bottom: Conf.xxlSize * 12,
                                    ),
                                    height: Conf.p2h(10.5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(13)),
                                        border: Border.all(
                                            color:
                                                Colors.grey.withOpacity(0.4))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                            padding: EdgeInsets.only(
                                                right: Conf.p2w(2.5)),
//                                                  margin:
                                            child: Text(" استان / شهر",
                                                style: widget.txtStyle)),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: Conf.p2w(2)),
                                            child: OutlineButton(
                                              child: Text(
                                                  widget.nameCity == null ||
                                                          widget.nameCity == ""
                                                      ? "انتخاب کنید"
                                                      : widget.nameCity,
                                                  style: widget.txtStyle),
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    PageTransition(
                                                        type: PageTransitionType
                                                            .leftToRight,
                                                        duration: Duration(
                                                            milliseconds: 500),
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        curve:
                                                            Curves.easeOutCirc,
                                                        child:
                                                            ProvinceCityPage2(
                                                          adressValue: widget
                                                              .adressValue,
                                                          emailValue:
                                                              widget.emailValue,
                                                          meliValue:
                                                              widget.meliValue,
                                                          nezamValue:
                                                              widget.nezamValue,
                                                          birthday:
                                                              textEditingController
                                                                  .text,
                                                          name:
                                                              widget.nameValue,
                                                          fname:
                                                              widget.fnameValue,
                                                          tel: widget.telValue,
                                                        )));
                                              },
                                              shape: RoundedRectangleBorder(
                                                  side: BorderSide(
//                                                     style: BorderStyle.solid
                                                      ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                            ))
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ]);
            },
          ),
        )),
        widget.loading1 == true ? Conf.circularProgressIndicator() : Container()
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              'لطفا تمام فیلدها را پر کنید...',
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
