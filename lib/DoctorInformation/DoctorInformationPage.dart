import 'package:counsalter5/AboutUs.dart';
import "package:counsalter5/Doctor/HomePageDoctor.dart";
import 'package:counsalter5/DoctorInformation/DoctorInformationBank.dart';
import 'package:counsalter5/DoctorInformation/DoctorInformationBasicInfo.dart';
import 'package:counsalter5/DoctorInformation/DoctorInformationCounsalter.dart';
import 'package:counsalter5/DoctorInformation/DoctorInformationDocument.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc2.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/htify.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:counsalter5/LoginPage0.dart';
class DoctorInformationPage extends StatefulWidget {
  double percent,TotalPercent = 0.0;
bool loading=true,loading1=true;
  DoctorModel doctorInformationCounsalter;
  TextStyle titleAlertStyle = Conf.body1.copyWith(
      color: Colors.black, fontSize: Conf.p2t(16), fontWeight: FontWeight.w600);
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(19),
      fontWeight: FontWeight.w500,
      color: Colors.black87);
  TextStyle alertBtnStyle = Conf.body1.copyWith(
      color: Colors.white, fontSize: Conf.p2t(18), fontWeight: FontWeight.w600);
  bool isNull = false;
  SharedPreferences sharedPreferences;
  List<DoctorModel> getTotalDataList = [];

  double calculateLinearPercentage() {
    percent=0.0;

    if (doctorInformationCounsalter.inf_bank == true) {
      percent += 0.25;
      print("____doctor.inf_bank percentage ===> ${{percent}}");
    }

    if (doctorInformationCounsalter.inf_collection == true) {
      percent += 0.25;
      print(
          "____doctor.inf_collection percentage ===> ${{percent.toString()}}");
    }

    if (doctorInformationCounsalter.inf_doc == true) {
      percent += 0.25;
      print("___doctor.inf_doc percentage ===> ${{percent}}");
//      0.6000000000000001
    }

    if (doctorInformationCounsalter.inf_personal == true) {
      percent += 0.25;
      print("___doctor.inf_personal percentage ===> ${{percent}}");
    }
    return percent;
  }
  showAlertDialog(BuildContext cntx) {
    AlertDialog alert = AlertDialog(
      content: Icon(FeatherIcons.alertTriangle, color: Colors.red, size: 70),
      shape:
      new RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      title: Text(
        "آیا میخواهید از حساب کاربری خود خارج شوید؟",
        style: titleAlertStyle,
      ),
//      actionsOverflowButtonSpacing: 5,
      actions: [
        Center(
          child: Container(
//            color: Colors.red,
            width: Conf.p2w(70), height: Conf.p2h(11),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: Conf.p2w(1.5), right: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.blue,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      color: Conf.orang,
                      onPressed: () {
                        exitDoctor(cntx);
                      },
                      child: Text("بله", style: alertBtnStyle),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      right: Conf.p2w(1.5), left: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.purple,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      color: Conf.orang,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      onPressed: () {
                        Navigator.pop(cntx);
//                        Navigator.pushReplacement(
//                            cntx,
//                            PageTransition(
//                                type: PageTransitionType.leftToRight,
//                                duration: Duration(milliseconds: 500),
//                                alignment: Alignment.centerLeft,
//                                curve: Curves.easeOutCirc,
//                                child: DoctorInformationPage()));
                      },
                      child: Text("خیر", style: alertBtnStyle),
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
    showDialog(
      context: cntx,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  Future exitDoctor(BuildContext cnx) async {
    sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.remove('token');
    print("********## Now Doctor is Log Out ===> ${sharedPreferences}");
    CircularProgressIndicator();
    Navigator.push(
        cnx,
        PageTransition(
            type: PageTransitionType.leftToRight,
            duration: Duration(milliseconds: 500),
            alignment: Alignment.centerLeft,
            curve: Curves.easeOutCirc,
            child: LoginPage0()));
  }
  @override
  _DoctorInformationPageState createState() => _DoctorInformationPageState();
}

class _DoctorInformationPageState extends State<DoctorInformationPage> {
  DoctorBloc get bloc => context.bloc<DoctorBloc>();

  Future getListDataCollection() async {
    try {
      setState(() {
        widget.loading=false;
      });
      var data = await Htify.get('dr/doctor');
      print(data);
      Map<String, dynamic> contetntList = data;
      contetntList.forEach((key, value) {
        widget.getTotalDataList.add(DoctorModel());
      });
      print("__widget.getTotalDataList ===> ${widget.getTotalDataList}");

      widget.doctorInformationCounsalter.inf_collection = data['inf_collection'];
      widget.doctorInformationCounsalter.inf_doc = data['inf_doc'];
      widget.doctorInformationCounsalter.inf_personal = data['inf_personal'];
      widget.doctorInformationCounsalter.inf_bank = data['inf_bank'];

      doctorBloc.setDoctor(widget.doctorInformationCounsalter);
    } catch (e) {
      print("CATCH ERROR_e.toString() ===> ${e.toString()}");
    }finally{
      setState(() {
        widget.loading=true;
      });
    }
  }
  Future getDoctorBaseInfo() async {
    try {
      setState(() {
        widget.loading1 = true;
      });
      Map data = await Htify.get('dr/doctor');
      widget.doctorInformationCounsalter.fname = data["fname"];
      widget.doctorInformationCounsalter.avatar = data["avatar"];
      widget.doctorInformationCounsalter.lname = data["lname"];
      widget.doctorInformationCounsalter.name = data["name"];
      widget.doctorInformationCounsalter.city_code = data["city_code"];
      widget.doctorInformationCounsalter.n_code= data["n_code"];
      widget.doctorInformationCounsalter.n_doctor= data["n_doctor"];
      widget.doctorInformationCounsalter.phone = data["phone"];
      widget.doctorInformationCounsalter.email = data["email"] ;
      widget.doctorInformationCounsalter.address = data["address"] ;
      widget.doctorInformationCounsalter.birth_day = data["birth_day"];
      widget.doctorInformationCounsalter.city_id = data["city_id"] ;
      widget.doctorInformationCounsalter.mobile = data["mobile"] ;
      widget.doctorInformationCounsalter.inf_personal = data["inf_personal"] ;
      widget.doctorInformationCounsalter.inf_bank = data["inf_bank"] ;
      widget.doctorInformationCounsalter.inf_doc = data["inf_doc"] ;
      widget.doctorInformationCounsalter.city = data['city'];
      widget.doctorInformationCounsalter.inf_collection = data["inf_collection"] ;
      widget.doctorInformationCounsalter.isSelelectedTimeSlot = data["isSelelectedTimeSlot"] ;
      widget.doctorInformationCounsalter.shaba_number = data["shaba_number"] ;
      widget.doctorInformationCounsalter.account_number = data["account_number"] ;
      widget.doctorInformationCounsalter.active = data["active"] ;
      widget.doctorInformationCounsalter.specialty=data["specialty"];
      widget.doctorInformationCounsalter.description=data["description"];

      print("d.specialty ===> ${ widget.doctorInformationCounsalter.specialty }");
      print("d.describtion ===> ${ widget.doctorInformationCounsalter.description}");
      bloc.setDoctor(widget.doctorInformationCounsalter);
      print("____d.city  ===> ${ widget.doctorInformationCounsalter.city}");
      print("____d.speciality  ===> ${ widget.doctorInformationCounsalter.specialty}");
    } catch (e) {
      print("CATCH ERROR getDoctor===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading1 = false;
      });
    }
  }
  CityProvinceCubit2 get cityBloc2 => context.bloc<CityProvinceCubit2>();

  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  @override
  void initState() {
    widget.doctorInformationCounsalter = DoctorModel();
    getDoctorBaseInfo();
    getListDataCollection();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
        BlocBuilder<DoctorBloc, DoctorRepository>(builder: (context, state) {
      print("_state.doctor.inf_bank ===> ${state.doctor.inf_bank}");
      print("_state.doctor.inf_collection ===> ${state.doctor.inf_collection}");
      print("_state.doctor.inf_doc ===> ${state.doctor.inf_doc}");
      print("_state.doctor.inf_personal ===> ${state.doctor.inf_personal}");
        print("___state.doctor.fname ===> ${ doctorBloc.state.doctor.fname }");
      return Stack(
        children: [
          Container(
              margin: EdgeInsets.only(top: Conf.p2h(3)),
              child: Column(children: [
                Padding(
                  padding: EdgeInsets.only(top: Conf.p2h(5)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.leftToRight,
                                      duration: Duration(milliseconds: 500),
                                      alignment: Alignment.centerLeft,
                                      curve: Curves.easeOutCirc,
                                      child: HomePageDoctor()));
                            },
                            child:(doctorBloc.state.doctor.inf_personal==null) || doctorBloc?.state?.doctor?.inf_personal?
                            Text("مشاور "+doctorBloc.state.doctor.name.toString(),
                                style: Conf.headline.copyWith(
                                    color: Colors.black87,
                                    fontSize: Conf.p2t(22),
                                    fontWeight: FontWeight.w500,
                                    decoration: TextDecoration.none)):Text("مشاور     ",
                                style: Conf.headline.copyWith(
                                    color: Colors.black87,
                                    fontSize: Conf.p2t(22),
                                    fontWeight: FontWeight.w500,
                                    decoration: TextDecoration.none))

                          ),
                          Padding(
                            padding: EdgeInsets.only(top: Conf.p2h(2)),
                            child: Row(
//                      mainAxisAlignment: MainAxisAlignment.start,crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "مشخصات کامل ",
                                    style: Conf.body2.copyWith(
                                      color: Conf.orang,
                                      fontWeight: FontWeight.w700,
                                      fontSize: Conf.p2t(16),
                                      decoration: TextDecoration.none,
                                    ),
                                    softWrap: true,
                                    overflow: TextOverflow.fade,
                                  ),
                                  Padding(
                                    padding:EdgeInsets.only(top: Conf.p2w(0.2),right: Conf.p2w(2)),
                                    child: Text(
                                      "%",
                                      style: Conf.body2.copyWith(
                                        color: Conf.orang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 11,
                                        decoration: TextDecoration.none,
                                      ),
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: Conf.p2w(0.5)),
                                    child: Text(
                                      (widget.calculateLinearPercentage()*100).toString().split(".").first,
                                      style: Conf.body2.copyWith(
                                        color: Conf.orang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: Conf.p2t(18),
                                        decoration: TextDecoration.none,
                                      ),
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                  )
                                ]),
                          ),
                          LinearPercentIndicator(
                            width: 190.0,
                            animation: true,
                            backgroundColor: Colors.grey.shade200,
                            animationDuration: 700,
                            lineHeight: 12.0,
                            percent:
                            widget.calculateLinearPercentage(),
                            isRTL: true,
                            progressColor: Conf.orang,
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: Conf.p2w(7)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100.0),
                          child: Container(
                            color: Colors.grey.withOpacity(0.2),
                            width: Conf.p2w(29),
                            height: Conf.p2w(29),
                            child:Conf.image(bloc.state.doctor.avatar)?? Conf.image(null)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: Conf.p2w(115),
                  width: Conf.p2w(double.infinity),
                  margin: EdgeInsets.only(top: Conf.p2h(5)),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 10,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(35),
                          topLeft: Radius.circular(35))),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(35),
                        topLeft: Radius.circular(35)),
                    child: Flex(direction: Axis.vertical, children: [
                      Expanded(
                        child: SingleChildScrollView(
                          padding: EdgeInsets.only(
                              left: Conf.xlSize, right: Conf.xlSize),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: DoctorInformationBasicInfo()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [Padding(
                                  padding: EdgeInsets.only(bottom: Conf.p2h(1), top: Conf.p2h(3)),
                                    child: Text("اطلاعات اولیه", style: widget.txtStyle),
                                  ),
                                    state.doctor.inf_personal??widget.isNull?Padding(
                                      padding:EdgeInsets.only(left: Conf.p2w(3)),
                                      child: Icon(Icons.check,color: Conf.orang,size: 20),
                                    ):Container()
                                  ],
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
          GestureDetector(onTap: (){
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.leftToRight,
                    duration: Duration(milliseconds: 500),
                    alignment: Alignment.centerLeft,
                    curve: Curves.easeOutCirc,
                    child: DoctorInformationCounsalter()));
          },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                    padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                    child: Text("نوع مشاوره",style:widget.txtStyle ,),
                                  ),
                                    state.doctor.inf_collection??widget.isNull?Padding(
                                      padding:EdgeInsets.only(left: Conf.p2w(3)),
                                      child: Icon(Icons.check,color: Conf.orang,size: 20),
                                    ):Container()
                                  ],
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: DoctorInformationBank()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                      child: Text("اطلاعات بانکی",style: widget.txtStyle),
                                    ),
                                    state.doctor.inf_bank??widget.isNull?Padding(
                                      padding:EdgeInsets.only(left: Conf.p2w(3)),
                                      child: Icon(Icons.check,color: Conf.orang,size: 20),
                                    ):Container()
                                  ],
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: DoctorInformationDocument()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                      child: Text("مدارک",style: widget.txtStyle),
                                    ),
                                    state.doctor.inf_doc??widget.isNull?Padding(
                                      padding:EdgeInsets.only(left: Conf.p2w(3)),
                                      child: Icon(Icons.check,color: Conf.orang,size: 20),
                                    ):Container(),
                                  ],
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: AboutUs()));
                                },
                                child: Container(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                    child: Text("سوالات متداول",style: widget.txtStyle),
                                  ),
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: AboutUs()));
                                },
                                child: Container(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                    child: Text("پشتیبانی",style: widget.txtStyle),
                                  ),
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: AboutUs()));
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                  child: Text("درباره ما",style: widget.txtStyle),
                                ),
                              ),
                              Container(
                                child: Divider(),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: Conf.p2h(1)),
                                child: GestureDetector(onTap: (){
                                  widget.showAlertDialog(context);
                                },child: Text("خروج از حساب کاربری",style: widget.txtStyle)),
                              ),
                              Container(
                                child: Divider(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ),
                ),
               ]
              )
          ),
          widget.loading == false ? Conf.circularProgressIndicator() : Container()      ],

      );
    })
    );

  }

  Widget txtRow({String txt, Widget naviagteTo, bool isFirst}) {
    isFirst = false;
    return Padding(
      padding: isFirst
          ? EdgeInsets.only(bottom: Conf.p2h(1), top: Conf.p2h(3))
          : EdgeInsets.symmetric(vertical: Conf.p2h(1)),
      child: Container(
        child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.leftToRight,
                      duration: Duration(milliseconds: 500),
                      alignment: Alignment.centerLeft,
                      curve: Curves.easeOutCirc,
                      child: naviagteTo));
            },
            child: Text(
              txt,
              style: widget.txtStyle,
            )),
      ),
    );
  }
}
