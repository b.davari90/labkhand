import 'package:counsalter5/CustomerModel.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/IncreaseMoney.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/htify.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'Collections.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'CustomerPageBottomNavComponent.dart';
import 'CustomerPageBottomNavModel.dart';
import 'raised_button_ui.dart';
import 'config.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/AddCoustomer.dart';
import 'ModelBloc/CustomerChilderenModel.dart';
import "dart:convert";
import 'package:counsalter5/Collections.dart'; ///////////////////وقتی دکمه تایید در هر صفحه ای خورده شد، تمام اطلاعات در بلاک ذخیره میشوند و پروگرس ابر و تیک مقدارشون براساس بلاک پر میشه مثلا 10درصد از داده ها
import 'package:counsalter5/raised_button_ui.dart';

class CoustomerPageForm extends StatefulWidget {
  int id, idDr, price, type, selectedDay,index;
  List<int> priceLst;
  List<int> typeLst;
  bool isPrivate;
  int colection_Id;
  String gerogarianDate,description="",
      selectedTime,
      selectedMonth,
      subject,
      shamsiDate,
      finalDay;

  CoustomerPageForm(
      {this.id,
      this.price,
      this.type,
      this.idDr,
      this.selectedDay,
      this.finalDay,
      this.shamsiDate,
      this.subject,
      this.colection_Id,
      this.selectedMonth,
      this.selectedTime,
      this.priceLst,
      this.typeLst,
      this.isPrivate,
      this.gerogarianDate});

  @override
  _CoustomerPageFormState createState() => _CoustomerPageFormState();
}

class _CoustomerPageFormState extends State<CoustomerPageForm> {
  bool loading = true;
  bool loading1 = false,activee;
  String dpValue;
  List<Collections> doctorCollection;

  List doctorCollectionTitles;
  TextStyle titleStyle = Conf.title.copyWith(
      fontSize: Conf.p2t(25), fontWeight: FontWeight.w700, color: Colors.black);
  TextStyle subTitleStyle = Conf.title.copyWith(
      fontSize: Conf.p2t(14),
      fontWeight: FontWeight.w700,
      color: Colors.black87);
  TextStyle timeDateStyle = Conf.body1.copyWith(
      color: Colors.black, fontWeight: FontWeight.w500, fontSize: Conf.p2t(15));

//  List<Widget> taskList;
  List<CustomerPageBottomNavModel> dataList;
  final _formKey = GlobalKey<FormBuilderState>();
  bool keybordShow = false;
  Function onTap;
  CustomerChilderenModel customerChilderenModel;
  CustomerModel customerModel;

  CoustomerBloc get coustomerBloc => context.bloc<CoustomerBloc>();

  CollectionsBloc get doctorCollectionBloc => context.bloc<CollectionsBloc>();

  CoustomerChilderenBloc get customerChilderenBloc =>
      context.bloc<CoustomerChilderenBloc>();

  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  Future getCustomer() async {
    try {
      setState(() {
        loading1 = true;
      });
      var data = await Htify.get('cu/customer');
      setState(() {
        customerModel = CustomerModel.fromJson(data);
//      print("childeren ===> ${ customerModel.childeren.map((e) => e.fname).toList() }");
        coustomerBloc.setUser(customerModel);
        print("___coustomerBloc NAME===> ${coustomerBloc.state.user.name}");
      });
    } catch (e) {
      print("___CATCH ERROR getCustomer===> ${e.toString()}");
    } finally {
      setState(() {
        loading1 = false;
      });
    }
  }

  Future getDoctorCollectionList() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.get('cu/collection');
      print(data);
      List collectionList = data;
      print("collectionList ===> ${collectionList.toString()}");
      doctorCollection =
          collectionList.map((item) => Collections.fromJson(item)).toList();
      doctorCollectionTitles.addAll(doctorCollection.map((e) => e.title));
      print(
          "_____doctorCollectionTitles ===> ${doctorCollectionTitles.toString()}");
      print("____getListDataCollection ===> ${collectionList.toString()}");
    } catch (e) {
      print("CATCH ERROR getDoctorCollectionList===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    customerModel = CustomerModel();
    DoctorModel doctorModel = DoctorModel();
    doctorCollection = [];
    doctorCollectionTitles = [];
    customerChilderenModel = CustomerChilderenModel();
    print("Conf.token ===> ${Conf.token}");
    print("widget.shamsiDate ===> ${widget.shamsiDate}");
    print("widget.id ===> ${widget.id}");
    print("widget.idDr ===> ${widget.idDr}");
    print("widget.selectedDay ===> ${widget.selectedDay}");
    print("widget.selectedMonth ===> ${widget.selectedMonth}");
    print("widget.selectedTime ===> ${widget.selectedTime}");
    print("widget.priceLst ===> ${widget.priceLst}");
    print("widget.typeLst ===> ${widget.typeLst}");
    print("widget.price ===> ${widget.price}");
    print("widget.type ===> ${widget.type}");
    print("isPrivate ===> ${widget.isPrivate}");
    print("widget.gerogarianDate ===> ${widget.gerogarianDate}");
//    finalDay ="شمنبه";
    widget.finalDay =
        widget.shamsiDate.substring(0, widget.shamsiDate.indexOf("ه")) + "ه";
//    doctorBloc.setDoctorDate(widget.finalDay);
    print("---finalDay ===> ${widget.finalDay}");
    getDoctorCollectionList();
    KeyboardVisibility.onChange.listen((bool visible) {
      if (this.mounted) {
        setState(() {
          keybordShow = visible;
        });
      }
    });
    getCustomer();
    if (customerModel.childeren == null) {
      customerModel.childeren = [];
    }
    super.initState();
  }

  fetch() async {
    Future.delayed(Duration(seconds: 5), () {
      setState(() {
        dataList = [];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Stack(
        children: [
          BlocBuilder<DoctorBloc, DoctorRepository>(builder: (context, state) {
            return Scaffold(
                bottomNavigationBar: Container(
                  width: Conf.p2w(100),
                  height: Conf.p2h(30),
                  decoration: BoxDecoration(
                      color: Conf.blue,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: Conf.p2w(2.2)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      right: Conf.p2w(2.5),
                                      left: Conf.p2w(2.0839),
                                      top: Conf.p2w(1)),
                                  child: InkWell(
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.white),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100))),
                                        padding: EdgeInsets.all(5),
                                        width: Conf.p2w(13),
                                        height: Conf.p2w(13),
                                        child: Icon(
                                          FeatherIcons.plus,
                                          color: Colors.white,
                                        )),
                                    onTap: () {
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AddCoustomer(
                                                    finalDay: widget.finalDay,
                                                    price: widget.price,
                                                    id: widget.id,
                                                    shamsiDate:
                                                        widget.shamsiDate,
                                                    gerogarianDate:
                                                        widget.gerogarianDate,
                                                    selectedTime:
                                                        widget.selectedTime,
                                                    selectedMonth:
                                                        widget.selectedMonth,
                                                    selectedDay:
                                                        widget.selectedDay,
                                                    isPrivate: widget.isPrivate,
                                                    priceLst: widget.priceLst,
                                                    idDr: widget.idDr,
                                                    colection_Id:
                                                        widget.colection_Id,
                                                    subject: widget.subject,
                                                    type: widget.type,
                                                    typeLst: widget.typeLst,
                                                  )));
                                    },
                                  ),
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(top: Conf.p2h(0.418)),
                                  child: FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text(
                                      "اضافه کردن",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: Conf.p2t(12),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                              width: Conf.p2w(2),
                              height: Conf.p2h(8),
                              child: VerticalDivider(
                                color: Colors.white.withOpacity(0.7),
                              )),
                          BlocBuilder<CoustomerBloc, CoustomerRepository>(
                              builder: (context, state) {
                            return Container(
                              margin: EdgeInsets.only(
                                  top: Conf.p2w(3), bottom: Conf.p2w(1.5)),
                              width: Conf.p2w(75.5),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Padding(
                                  padding:
                                      EdgeInsets.only(bottom: Conf.p2w(2.0839)),
                                  child: Row(
                                      children: customerModel.childeren
                                          .map((e) =>
                                          CustomerPageBottomNavComponent(
                                            model: e,
                                            onTap: () {
                                              setState(() {
                                                widget.index =
                                                    customerModel.childeren
                                                        .indexOf(e);});
                                            },active: customerModel.childeren
                                              .indexOf(e) ==
                                              widget.index
                                              ? true
                                              : false,
                                          ))
                                          .toList()),
                                ),
                              ),
                            );
                          }),
                        ],
                      ),
//                      loading==true?Conf.circularProgressIndicator():Container()
                      Container(
                          width: double.infinity,
                          height: Conf.p2w(18),
                          margin: EdgeInsets.only(
                              top: Conf.p2w(1.5), bottom: Conf.p2w(3)),
                          child: MasRaisedButton(
                            margin: EdgeInsets.only(
                                right: Conf.p2w(6),
                                left: Conf.p2w(6),
                                top: Conf.p2w(1.5),
                                bottom: Conf.p2w(3)),
                            borderSideColor: Conf.orang,
                            text: "مشاوره آنلاین و تعیین وقت قبلی",
                            color: Conf.orang,
                            textColor: Colors.white,
                            onPress: () {
                              if(( widget.colection_Id==0||widget.colection_Id ==null||widget.subject==null||widget.subject=="")&&widget.description==""){
                                _showTopFlash();
                              }
                              else{save();}
                            },
                          )),
                    ],
                  ),
                ),
                resizeToAvoidBottomInset: true,
                resizeToAvoidBottomPadding: true,
                body: Stack(children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      padding:
                          EdgeInsets.only(top: Conf.xlSize, right: Conf.xlSize),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(top: Conf.p2w(2.0839)),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: Conf.p2w(16),
                                    height: Conf.p2w(16),
//                        height: Conf.p2w(9.55),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(43.0),
                                      child: FadeInImage.assetNetwork(
                                        placeholder: "assets/images/prof1.jpg",
                                        image:
                                            "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          right: Conf.p2w(2.0839)),
//                    padding: const EdgeInsets.only(right: 10),
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Text(state.doctor.name ?? "",
                                                    softWrap: true,
                                                    overflow: TextOverflow.fade,
                                                    style: titleStyle
//                                        txtStyleName,
                                                    ),
//                                              Text(" کلایی",
//                                                  softWrap: true,
//                                                  overflow: TextOverflow.fade,
//                                                  style: titleStyle),
                                              ],
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Text("متخصص",
                                                    softWrap: true,
                                                    overflow: TextOverflow.fade,
                                                    style: subTitleStyle
//                                txtStyleAboutDoctor,
                                                    ),
                                                Text(" - ",
                                                    softWrap: true,
                                                    overflow:
                                                        TextOverflow.fade),
                                                Text(
                                                    state.doctor.specialty ??
                                                        "",
                                                    softWrap: true,
                                                    overflow: TextOverflow.fade,
                                                    style: subTitleStyle)
                                              ],
                                            ),
                                          ])),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2w(5), left: Conf.p2w(5)),
                              child: MasBackBotton()),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                      left: 0,
                      right: 0,
                      top: Conf.p2w(26),
                      bottom: 0,
                      child: Container(
//                  margin: EdgeInsets.only(top: Conf.xlSize),
//                  height: Conf.p2h(52),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white10.withOpacity(0.5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.15),
                                  spreadRadius: 2,
                                  blurRadius: 10,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30),
                                  topLeft: Radius.circular(30))),
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30)),
                            child: Flex(direction: Axis.vertical, children: [
                              Expanded(
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: Conf.p2w(5.2),
                                            right: Conf.p2w(6.25)),
//                          top: Conf.p2w(6.25), right: Conf.p2w(6.25)),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: Conf.blue,
                                              size: Conf.p2t(25),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  right: Conf.p2w(1.042),
                                                  top: Conf.p2h(0.25)),
                                              child: Flex(
                                                  direction: Axis.vertical,
                                                  children: [
                                                    RichText(
                                                      text: TextSpan(children: [
                                                        TextSpan(
                                                          text: widget.finalDay
                                                                          .length <=
                                                                      2 &&
                                                                  widget
                                                                      .finalDay
                                                                      .startsWith(
                                                                          "چ")
                                                              ? " چهارشنبه "
                                                              : widget.finalDay
                                                                              .length <=
                                                                          2 &&
                                                                      widget
                                                                          .finalDay
                                                                          .startsWith(
                                                                              "س")
                                                                  ? " سه شنبه "
                                                                  : widget
                                                                      .finalDay,
//                                                              text:state.date.length<=2&& state.date.startsWith("چ")?" چهارشنبه "
//                                                                  :state.date.length<=2&& state.date.startsWith("س")?" سه شنبه ":state.date,
                                                          style: timeDateStyle,
                                                        ),
                                                        TextSpan(
                                                            text: widget
                                                                .selectedDay
                                                                .toString(),
                                                            style:
                                                                timeDateStyle),
                                                        TextSpan(
                                                            text: widget
                                                                .selectedMonth,
                                                            style:
                                                                timeDateStyle),
                                                        TextSpan(
                                                            text: " 1400 ",
                                                            style:
                                                                timeDateStyle),
                                                        TextSpan(
                                                            text: "،ساعت: ",
                                                            style:
                                                                timeDateStyle),
                                                        TextSpan(
                                                            text: widget
                                                                .selectedTime,
                                                            style:
                                                                timeDateStyle),
                                                        TextSpan(
                                                            text: "دقیقه",
                                                            style:
                                                                timeDateStyle),
                                                      ]),
                                                    ),
                                                  ]),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: Conf.p2h(0.6),
//                          top: Conf.p2h(1),
                                            right: Conf.p2w(6.25),
                                            bottom: Conf.p2w(2.0839)),
//                  const EdgeInsets.only(top: 20, right: 30, bottom: 10),
                                        child: Text(
                                          "موضوع مشاوره",
                                          style: timeDateStyle,
                                        ),
                                      ),
                                      BlocBuilder<CollectionsBloc,
                                              CollectionsRepository>(
                                          builder: (context, state) {
                                        return FormBuilder(
                                          key: _formKey,
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                right: Conf.xlSize,
                                                left: Conf.xlSize),
                                            margin: EdgeInsets.symmetric(
                                                horizontal: Conf.xlSize + 5),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.black54),
                                                borderRadius:
                                                    Conf.primaryBorderRadius),
                                            child: FormBuilderDropdown(
                                              attribute: 'collection',
                                              allowClear: true,
                                                decoration: InputDecoration(
                                                  enabledBorder: InputBorder.none,
                                                   ),
                                              underline: SizedBox(),
                                              hint: Text('انتخاب کنید'),
//                                              validators: [
//                                                FormBuilderValidators.required(
//                                                    errorText: "")
//                                              ],
                                              onChanged: (val) {
                                                setState(() {
                                                  widget.colection_Id =
                                                      doctorCollectionTitles
                                                          .indexOf(val);
                                                  widget.subject = val;
                                                  print(
                                                      "widget.colection_Id ===> ${widget.colection_Id}");
                                                });
                                              },
                                              items: doctorCollectionTitles
                                                  .map((e) => DropdownMenuItem(
                                                        value: e,
                                                        child: Text('$e'),
                                                      ))
                                                  .toList(),
                                            ),
                                          ),
                                        );
                                      }),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: Conf.p2w(1),
                                            right: Conf.p2w(6.25),
                                            bottom: Conf.p2w(2.0839)),
//                  const EdgeInsets.only(top: 20, right: 30, bottom: 10),
                                        child: Text(
                                          "شرح حال",
                                          style: timeDateStyle,
                                        ),
                                      ),
//
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: Conf.xlSize + 5),
                                        width: Conf.p2w(90),
                                        height: Conf.p2h(15),
//                      height: Conf.p2h(20),
                                        child: TextField(
                                          onChanged: (val){
                                            widget.description=val;
                                          },
                                            keyboardType: TextInputType.text,
                                            expands: true,
                                            textAlignVertical:
                                                TextAlignVertical.top,
                                            maxLines: null,
                                            decoration: InputDecoration(
                                              alignLabelWithHint: false,
                                              errorMaxLines: 10,
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(10),
                                                ),
                                              ),
                                              filled: true,
                                              hintStyle: new TextStyle(
                                                fontSize: Conf.p2t(14),
                                                color: Colors.black12
                                                    .withOpacity(0.6),
                                              ),
                                              hintText:
                                                  "شرح حال خود را بنویسید",
                                              fillColor:
                                                  Colors.grey.withOpacity(0.01),
                                            )),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: Conf.p2w(5.5),
                                            top: Conf.p2w(4)
//                              top: Conf.p2w(2.5)
//                        bottom: Conf.p2w(2.0839),
//                          top: Conf.p2w(10)
                                            ),
                                        child: Text(
                                          "انتخاب مشاوره گیرنده",
                                          style: timeDateStyle,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ]),
                          ))),
                ]));
          }),
          loading == true && loading1 == true
              ? Conf.circularProgressIndicator()
              : Container()
        ],
      ),
    );
//    ]
//    )));
  }

  Widget displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      barrierColor: Colors.transparent,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (ctx) {
        return Container(
          height: keybordShow ? double.infinity : Conf.p2h(65),
          child: Scaffold(
            resizeToAvoidBottomPadding: true,
            resizeToAvoidBottomInset: true,
            body: Container(
                child: Padding(
              padding: keybordShow?EdgeInsets.only(top:Conf.xxlSize):EdgeInsets.only(top:0),
              child: Increasemoney(
                price: widget.price,
                selectedTime: widget.selectedTime,
                gerogarianDate: widget.gerogarianDate,
                subject: widget.subject,
              ),
            )),
          ),
        );
      },
      isScrollControlled: true,
    );
  }

  void save() {
    _formKey.currentState.save();
    bool validate = _formKey.currentState.validate();
    print("validate  is===> ${validate}");
    if (validate) {
      var result = _formKey.currentState.value;
      CustomerChilderenModel t = customerChilderenBloc.state.userChild;
      customerChilderenBloc.setUser(t);
      sendDataVisit();
      displayBottomSheet(context);
    }
  }
  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context:context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Icon(FeatherIcons.alertOctagon,color: Colors.red,size: 50,),
            title: Text('خطا',style: TextStyle(fontSize:Conf.p2t(16),fontWeight: FontWeight.w400,color: Colors.red),),
            message: Text('لطفا موضوع مشاوره و شرح حال خود را وارد کنید...',style: TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w400,color: Colors.black),),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
  Future sendDataVisit() async {
    setState(() {
      loading = false;
    });
    try {
      var t = jsonEncode({
        "data.owner_id": widget.id,
        "data.doctor_id": widget.idDr,
        "data.type": widget.type,
        "data.price": widget.price,
        "data.date_at": widget.gerogarianDate + widget.selectedTime,
        "data.private": widget.isPrivate,
        "data.collection_id": widget.colection_Id
      });
      print("t ===> ${t}");
      var data = await Htify.create("cu/visit", {
        "data.owner_id": widget.id,
        "data.doctor_id": widget.idDr,
        "data.type": widget.type,
        "data.price": widget.price,
        "data.date_at": widget.gerogarianDate + widget.selectedTime,
        "data.private": widget.isPrivate,
        "data.collection_id": widget.colection_Id
      });
    } catch (e) {
      print("Error Catch sendDataVisit ===> ${e.toString()}");
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
}
