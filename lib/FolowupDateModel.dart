class FolowupDateModel {
    bool actived,active,isChoose,isDefault;
    int id;
    String text;

    FolowupDateModel({this.active, this.actived, this.id, this.isChoose, this.isDefault, this.text});

    factory FolowupDateModel.fromJson(Map<String, dynamic> json) {
        return FolowupDateModel(
            active: json['active'],
            actived: json['actived'], 
            id: json['id'], 
            isChoose: json['isChoose'], 
            isDefault: json['isDefault'], 
            text: json['text'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['active'] = this.active;
        data['actived'] = this.actived;
        data['id'] = this.id;
        data['isChoose'] = this.isChoose;
        data['isDefault'] = this.isDefault;
        data['text'] = this.text;

        return data;
    }
}