import 'package:counsalter5/DoctorListPage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/CoustomerPage.dart';
import 'package:counsalter5/VisitPage.dart';
import 'package:counsalter5/DoctorDocument.dart';
import 'package:counsalter5/IncreaseMoneyEdited.dart';
import 'package:counsalter5/VisitListPage.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:counsalter5/htify.dart';
import "LoginPage.dart";
import 'config.dart';
import 'VisitListPage.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'CounsalterFollowup.dart';
import 'AboutUs.dart';
import 'package:counsalter5/CounsalterAbout.dart';
import 'package:counsalter5/ModelBloc/WalletModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'HomePageCollectionDrComponent.dart';
import 'StatusReserverCoustomerPage.dart';
import 'Collections.dart';
import 'DoctorModel.dart';
import 'package:counsalter5/CustomerModel.dart';
import 'BottomNavigation.dart';
import 'DoctorCardComponent.dart';

class HomePage extends StatefulWidget {
  int rowId;
  TextEditingController controller = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextStyle titleDrawer = Conf.caption.copyWith(
      fontSize: Conf.p2t(19), color: Colors.white, fontWeight: FontWeight.w700);
  TextStyle menuStyle = Conf.caption.copyWith(
      fontSize: Conf.p2t(16), color: Colors.white, fontWeight: FontWeight.w600);
  ScrollController _scrollController;
  bool hasChangeColor = true;
  bool loading = false, loading1 = false;
  TextStyle alertBtnStyle = Conf.body1.copyWith(
      color: Colors.white, fontSize: Conf.p2t(18), fontWeight: FontWeight.w600);
  TextStyle titleAlertStyle = Conf.body1.copyWith(
      color: Colors.black, fontSize: Conf.p2t(16), fontWeight: FontWeight.w600);
  List<Collections> doctorCollection, dataList = [];
  SharedPreferences sharedPreferences;
  List<DoctorModel> dataList2, doctorModelList;
  CustomerModel customerModel;
  Collections collection;

  Future exitUser(BuildContext cnx) async {
    sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.remove('token');
    print("********## Now User is Log Out ===> ${sharedPreferences}");

    CircularProgressIndicator();
    Navigator.push(
        cnx,
        PageTransition(
            type: PageTransitionType.leftToRight,
            duration: Duration(milliseconds: 500),
            alignment: Alignment.centerLeft,
            curve: Curves.easeOutCirc,
            child: LoginPage()));
  }

  Widget alertBtn(String title, BuildContext ctx, bool isFunction) {
    isFunction = false;
    return Container(
      width: Conf.p2w(30),
      margin: EdgeInsets.only(right: Conf.p2w(2), left: Conf.p2w(2)),
      decoration: BoxDecoration(
          color: Conf.orang,
          borderRadius: BorderRadius.circular(13),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.6),
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(-2, 2))
          ]),
      child: FlatButton(
        height: Conf.p2h(5),
        padding: EdgeInsets.only(left: Conf.p2w(7), right: Conf.p2w(5)),
        shape: new RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(13.0)),
        child: Center(child: Text(title, style: alertBtnStyle)),
        onPressed: () {
          isFunction ? exitUser(ctx) : Navigator.pop(ctx);
        },
      ),
    );
  } //  Color co

  showAlertDialog(BuildContext cntx) {
    AlertDialog alert = AlertDialog(
      content: Icon(FeatherIcons.alertTriangle, color: Colors.red, size: 70),
      shape:
          new RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      title: Text(
        "آیا میخواهید از حساب کاربری خود خارج شوید؟",
        style: titleAlertStyle,
      ),
//      actionsOverflowButtonSpacing: 5,
      actions: [
        Center(
          child: Container(
//            color: Colors.red,
            width: Conf.p2w(70), height: Conf.p2h(11),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: Conf.p2w(1.5), right: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.blue,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      color: Conf.orang,
                      onPressed: () {
                        exitUser(cntx);
//                        Navigator.pushReplacement(
//                            cntx,
//                            PageTransition(
//                                type: PageTransitionType.leftToRight,
//                                duration: Duration(milliseconds: 500),
//                                alignment: Alignment.centerLeft,
//                                curve: Curves.easeOutCirc,
//                                child: LoginPage()));
                      },
                      child: Text("بله", style: alertBtnStyle),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      right: Conf.p2w(1.5), left: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.purple,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      color: Conf.orang,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      onPressed: () {
                        Navigator.pushReplacement(
                            cntx,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                duration: Duration(milliseconds: 500),
                                alignment: Alignment.centerLeft,
                                curve: Curves.easeOutCirc,
                                child: HomePage()));
                      },
                      child: Text("خیر", style: alertBtnStyle),
                    ),
                  ),
                )
//            cancelButton,
//            continueButton,
              ],
            ),
          ),
        )
      ],
    );
    showDialog(
      context: cntx,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CollectionsBloc get doctorCollectionBloc => context.bloc<CollectionsBloc>();

  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  CoustomerBloc get coustomerBloc => context.bloc<CoustomerBloc>();

  Future getCustomer() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get('cu/customer');
      print(data);
      widget.customerModel = CustomerModel.fromJson(data);
      coustomerBloc.setUser(widget.customerModel);
      print("*coustomerBloc NAME===> ${coustomerBloc.state.user.name}");
    } catch (e) {
      print("CATCH ERROR getCustomer===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  Future getDoctorList() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get('cu/doctor');
      print(data);
      List<dynamic> doctorList = data;
      print("____doctorList ===> ${doctorList.toString()}");
      print("____doctorList222 ===> ${doctorList is List<dynamic>}");
      widget.doctorModelList = doctorList.map((item) => DoctorModel.fromJson(item)).toList();
      print("____widget.doctorModelList ===> ${widget.doctorModelList}");
      print("___widget ===> ${widget.doctorModelList.map((e) => e.fname)}");
      widget.doctorModelList.map((e) => e.fname);
      widget.doctorModelList.map((e) => e.lname);
      widget.doctorModelList.map((e) => e.avatar ?? "");
      doctorBloc.setDoctorList(widget.doctorModelList);
    } catch (e) {
      print("CATCH ERROR getDoctorList===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  Future getDoctorCollectionList() async {
    try {
      setState(() {
        widget.loading1 = true;
      });
      var data = await Htify.get('cu/collection');
      print(data);
      List collectionList = data;
      print("collectionList 525252===> ${collectionList.toString()}");
      widget.doctorCollection = collectionList.map((item) => Collections.fromJson(item)).toList();
      print("4000044 ===> ${ 4400004 }");

//      widget.collection.title = data["title"];
//      widget.collection.thumb = data["thumb"] ?? "";
      print("____thumb5050 ===> ${ widget.doctorCollection.map((e) => e.thumb)}");
      doctorCollectionBloc.setDoctorCollection(widget.collection);
      print("____getListDataCollection ===> ${collectionList.toString()}");
    } catch (e) {
      print("CATCH ERROR getDoctorCollectionList===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading1 = false;
      });
    }
  }

  @override
  void initState() {
    print("coustomerBloc.state.user.fname ===> ${coustomerBloc.state.user.fname}");
    print("coustomerBloc.state.user.lname ===> ${coustomerBloc.state.user.lname}");
    print("coustomerBloc.state.user.mobile ===> ${coustomerBloc.state.user.mobile}");
    print("coustomerBloc.state.user.password ===> ${coustomerBloc.state.user.password}");
//    print("token ===> ${Conf.token}");
//    Conf.loginType=1;
    widget.doctorModelList = [];
    widget.doctorCollection = [];
    widget.collection=Collections();
    widget.dataList2 = [];
    widget.dataList = [];
    getDoctorList();
    getDoctorCollectionList();
    widget._scrollController = ScrollController();
    widget._scrollController.addListener(() {
      if (widget._scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (widget.hasChangeColor == true) {
          setState(() {
            widget.hasChangeColor = false;
            widget.hasChangeColor = false;
          });
        }
      } else {
        if (widget._scrollController.position.userScrollDirection ==
            ScrollDirection.forward) {
          if (widget.hasChangeColor == false) {
            setState(() {
              widget.hasChangeColor = true;
            });
          }
        }
      }
      getCustomer();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(Conf.p2w(27.25));
    Conf.calcWH(context);
    Conf.textStyle(context);
    return Stack(
      children: [
        SafeArea(
            bottom: true,
            top: true,
            child: Theme(
                data: ThemeData(
                    primaryIconTheme: IconThemeData(
                        color: widget.hasChangeColor ? Conf.blue : Colors.white,
                        size: Conf.p2w(7))),
                child: Scaffold(
                    key: widget._scaffoldKey,
                    drawer: Drawer(
                      child: Container(
                        color: Conf.blue,
                        child: Padding(
                          padding: EdgeInsets.only(right: Conf.p2w(1.5)),
                          child: ListView(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(4.295)),
                                child: Row(
                                  children: [
                                    Column(
                                      children: [
                                        Container(
                                          width: Conf.p2w(11.8),
                                          height: Conf.p2w(11.5),
                                          child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(50.0),
                                              child: coustomerBloc
                                                          .state.user.avatar !=
                                                      null
                                                  ? ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100.0),
                                                      child: Container(
                                                        color: Colors.grey
                                                            .withOpacity(0.2),
                                                        width: Conf.p2w(12),
                                                        height: Conf.p2w(12),
                                                        child: Conf.image(
                                                            coustomerBloc.state
                                                                .user.avatar),
//                              Image.asset(state.doctor.avatar)
//                              Conf.image(state.doctor.avatar),
                                                      ),
                                                    )
                                                  : Conf.image(null)
//                                            FadeInImage.assetNetwork(
//                                              placeholder:
//                                                  "assets/images/prof1.jpg",
//                                              image:
//                                                  "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
//                                              fit: BoxFit.cover,
//                                              width: Conf.p2t(100),
//                                            ),
                                              ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(right: Conf.xlSize),
                                      child: BlocBuilder<CoustomerBloc,
                                          CoustomerRepository>(
                                        builder: (context, state) {
                                          return Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      top: Conf.p2h(1.042)),
                                                  child: Text(
                                                      state.user.name ?? "",
                                                      style:
                                                          widget.titleDrawer)),
    BlocBuilder<WalletBloc, WalletRepository>(
    builder: (context, state) {
    return
                                                Row(
                                                  children: [
                                                    Text("اعتبار من : ",
                                                        style: widget.menuStyle),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: Conf.p2h(0.2)),
                                                      child: Text(
                                                       state.wallet.amount==0||state.wallet.amount==null?"0":state.wallet.amount.toString(),
                                                        style: TextStyle(
                                                            fontSize: Conf.p2t(
                                                                Conf.p2w(3.50)),
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            color: Colors.white),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          top: Conf.p2h(0.7)),
                                                      child: Text("   تومان",
                                                          style: Conf.caption
                                                              .copyWith(
                                                                  fontSize:
                                                                      Conf.p2t(
                                                                          12),
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700)),
                                                    )
                                                  ],
                                                );},
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(2.0839)),
                                child: Container(
                                  child: Row(
                                    children: [
                                      listTileItem(
                                          FeatherIcons.home, "خانه", context,
                                          onTap: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType
                                                    .leftToRight,
                                                duration:
                                                    Duration(milliseconds: 500),
                                                alignment: Alignment.centerLeft,
                                                curve: Curves.easeOutCirc,
                                                child: HomePage()));
                                      })
                                    ],
                                  ),
                                ),
                              ),
//                    https://www.youtube.com/watch?v=E3-WdYBrEDc
                              listTileItem(
                                  FeatherIcons.user, "پروفایل", context,
                                  onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: CoustomerPage()));
                              }),
                              listTileItem(
                                  FeatherIcons.repeat, "پیگیری ها", context,
                                  onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: CounsalterFollowUp()));
                              }),
                              listTileItem(
                                  FeatherIcons.clipboard, "ویزیت ها", context,
                                  onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: VisitListPage()));
                              }),
                              listTileItem(
                                  FeatherIcons.settings, "تنظیمات ", context,
                                  onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: VisitPage()));
                              }),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: Conf.p2h(1), bottom: Conf.p2h(1)),
                                child: Row(children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(right: Conf.p2w(1.459)),
                                    child: Icon(FeatherIcons.dollarSign,
                                        size: Conf.p2w(3.8),
                                        color: Colors.white),
                                  ),
                                  InkWell(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            right: Conf.p2w(5),
                                            top: Conf.p2h(1.5)),
                                        child: Text("افزایش موجودی ",
                                            style: widget.menuStyle),
                                      ),
                                      onTap: () {
//                                displayBottomSheet(context: context);
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType
                                                    .leftToRight,
                                                duration:
                                                    Duration(milliseconds: 500),
                                                alignment: Alignment.centerLeft,
                                                curve: Curves.easeOutCirc,
                                                child: IncreaseMoneyEdited()));
//              Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) =>
//                                  displayBottomSheet(context: context)));
                                      })
                                ]),
                              ),
//                    listTileItem(FeatherIcons.dollarSign, "افزایش موجودی ",
//                        context, displayBottomSheet(context: context)),
//                    listTileItem(FeatherIcons.dollarSign, "افزایش موجودی ",
//                        context, Increasemoney()),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: Conf.p2h(1.042),
                                    bottom: Conf.p2w(2.0839)),
//                      padding: const EdgeInsets.only(top: 5,bottom: 10),
                                child: Divider(
                                  thickness: 0.5,
                                  color: Colors.white.withOpacity(0.8),
                                  indent: 13,
                                  endIndent: 90,
                                ),
                              ),
                              listTileItem(
                                  FeatherIcons.home, "خلاصه مشاوره ها", context,
                                  onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: DoctorDocument()));
                              }),
                              listTileItem(FeatherIcons.bookmark,
                                  "لیست علاقه مندی ها", context, onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: CoustomerPage()));
                              }),
                              listTileItem(
                                  FeatherIcons.file, "درباره ما", context,
                                  onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: AboutUs()));
                              }),
                              listTileItem(FeatherIcons.x,
                                  "خروج از حساب کاربری", context,
//                          VisitListPage()
//                          widget.showAlertDialog(context),
                                  onTap: () {
                                widget.showAlertDialog(context);
//                        Navigator.push(
//                            context,
//                            PageTransition(
//                                type: PageTransitionType.rightToLeft, child: navigateTo));
                              }),
                            ],
                          ),
                        ),
                      ),
                    ),
                    body: Container(
                      child: NestedScrollView(
                          controller: widget._scrollController,
                          scrollDirection: Axis.vertical,
                          headerSliverBuilder:
                              (BuildContext context, bool innerBoxIsScrolled) {
                            return [
                              SliverAppBar(
                                expandedHeight: Conf.p2h(25),
                                actions: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: Conf.p2h(1), left: Conf.p2w(1.5)),
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.notifications,
                                        color: widget.hasChangeColor
                                            ? Conf.blue
                                            : Colors.white,
                                        size: Conf.p2w(7),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType
                                                    .leftToRight,
                                                duration:
                                                    Duration(milliseconds: 500),
                                                alignment: Alignment.centerLeft,
                                                curve: Curves.easeOutCirc,
                                                child:
                                                    StatusReserveCoustomerPage()));
                                      },
                                    ),
                                  )
                                ],
                                floating: false,
                                pinned: true,
                                flexibleSpace: FlexibleSpaceBar(
                                  title: Opacity(
                                      opacity:
                                          widget.hasChangeColor ? 0.0 : 0.9,
//                          child: Text("")),
                                      child: Text(
                                        "لبخند",
                                        style: Conf.caption.copyWith(
                                            fontSize: Conf.p2t(16),
                                            color: widget.hasChangeColor
                                                ? Colors.transparent
                                                : Colors.white,
                                            fontWeight: FontWeight.w700),
                                      )),
                                  background: Container(
                                    width: Conf.p2w(100),
                                    height: Conf.p2h(30),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
//                              colorFilter: ColorFilter.linearToSrgbGamma(),
                                        image: AssetImage(
                                          "assets/images/doctors.png",
//                                "assets/images/Doctor.jpg",
                                        ),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            blurRadius: 8)
                                      ],
//                          borderRadius: BorderRadius.only(
//                              bottomLeft: Radius.circular(30),
//                              bottomRight: Radius.circular(30))
                                    ),
                                  ),
                                ),
                              ),
                            ];
                          },
                          body: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin:
                                        EdgeInsets.all(16).copyWith(bottom: 0),
                                    width: Conf.p2w(100),
                                    height: Conf.p2w(15),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(13),
                                        border: Border.all(
                                            color:
                                                Colors.grey.withOpacity(0.2)),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.2),
//            blurRadius: 10,spreadRadius: 1,
                                              offset: Offset(-1, 2.7))
                                        ]),
                                    child: IntrinsicWidth(
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(
                                                right: Conf.p2w(3.5)),
                                            child: Icon(
                                              FeatherIcons.search,
                                              size: Conf.p2w(3.5),
                                              color:
                                                  Colors.grey.withOpacity(0.9),
                                            ),
                                          ),
                                          Expanded(
                                            child: TextField(
                                              expands: true,
                                              minLines: null,
                                              maxLines: null,
                                              onTap: () {
                                                FocusScopeNode currentFocus =
                                                    FocusScope.of(context);
                                                if (!currentFocus
                                                    .hasPrimaryFocus) {
                                                  currentFocus.unfocus();
                                                }
                                                Navigator.push(
                                                    context,
                                                    PageTransition(
                                                        type: PageTransitionType
                                                            .leftToRight,
                                                        duration: Duration(
                                                            milliseconds: 500),
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        curve:
                                                            Curves.easeOutCirc,
//                                                        child: SearchDoctor()
                                                        child:
                                                            DoctorListPage()));
                                              },
                                              textDirection: TextDirection.rtl,
                                              style: TextStyle(
                                                color: Colors.black87,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              controller: widget.controller,
                                              decoration: InputDecoration(
                                                hintStyle: TextStyle(
                                                    height: 2.45,
                                                    color: Colors.black45,
                                                    fontWeight: FontWeight.w600,
                                                    fontFamily: 'ybakh',
                                                    fontSize: Conf.p2t(15)),
                                                isDense: true,
                                                border: InputBorder.none,
                                                contentPadding: EdgeInsets.only(
                                                    top: Conf.p2w(2.5),
                                                    right: Conf.p2w(2.0839)),
                                                hintText: 'جست و جوی مشاور',
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: Conf.p2h(0.5),
//                            bottom: Conf.p2h(0.834),
//                                  bottom: Conf.p2w(0.834),
                                            right: Conf.p2w(4)),
                                        child: FittedBox(
                                          fit: BoxFit.cover,
                                          child: Text("دسته بندی",
                                              softWrap: true,
                                              overflow: TextOverflow.fade,
                                              style: Conf.body1.copyWith(
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: Conf.p2t(16),
                                                  color: Colors.black87,
                                                  decoration:
                                                      TextDecoration.none)
//                              txtStyle2AboveListViewContainer,
                                              ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {},
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            FittedBox(
                                              fit: BoxFit.cover,
                                              child: Padding(
                                                padding: EdgeInsets.only(
                                                    top: Conf.p2h(0.418)),
                                                child: Text("بیشتر",
                                                    softWrap: true,
                                                    overflow: TextOverflow.fade,
                                                    style: Conf.body1.copyWith(
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      fontSize: Conf.p2t(13),
                                                      color: Conf.orang,
                                                      decoration:
                                                          TextDecoration.none,
                                                    )),
                                              ),
                                            ),
                                            IconButton(
                                              icon: Icon(
                                                  FeatherIcons.chevronLeft),
                                              color: Conf.orang,
                                              iconSize: Conf.p2t(20),
                                              alignment: Alignment.centerRight,
                                              onPressed: () {},
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  BlocBuilder<DoctorBloc, DoctorRepository>(
                                      builder: (context, state) {
                                    return Container(
                                            color: Colors.transparent,
                                            height: Conf.p2h(23),
                                            child: SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                  children: widget
                                                      .doctorCollection
                                                      .map((itemType) =>
                                                          HomePageCollectionDrComponent(
                                                            model: itemType))
                                                      .toList()),
                                            ));
                                  }),
//                                        widget.loading1==false?Conf.circularProgressIndicator():Container(),
//                              Container(
//                                color: Colors.transparent,
////                      width: Conf.p2w(77),
//                                height: Conf.p2h(23),
//                                child: SingleChildScrollView(
//                                  scrollDirection: Axis.horizontal,
//                                  child: Row(
//                                      children: widget.dataList
//                                          .map((e) =>
//                                              HomePageCollectionDrComponent(
//                                                model: e,
//                                              ))
//                                          .toList()),
//                                ),
//                              ),Padding(
//                                padding: EdgeInsets.only(top: Conf.p2h(2)),
//                                child:BlocBuilder<DoctorBloc, DoctorRepository>(
//                                        builder: (context, state) {
//                                  return SingleChildScrollView(
//                                    scrollDirection: Axis.vertical,
//                                    child: Column(
//                                        crossAxisAlignment:
//                                            CrossAxisAlignment.start,
//                                        children: widget.doctorModelList
//                                            .map((item) => DoctorCardComponent(
//                                                  model:state.doctor,
//                                                  isOnline: true,
//                                                  hasLightShadow: true,
//                                                  hour: "",
//                                                  minute: "",
//                                                  msgNumber: "",
//                                                  hasTime: false,
//                                                  hasShadow: true,
//                                                  hasUnSeenMsg: false,
//                                                ))
//                                            .toList()),
//                                  );
//                                }),
//                              )
//                                        widget.loading==false?Conf.circularProgressIndicator():
                                  Padding(
                                      padding:
                                          EdgeInsets.only(top: Conf.p2h(2)),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: widget.doctorModelList
                                                .map((item) =>
                                                    DoctorCardComponent(
                                                      model: item,
                                                      onTap: () {
                                                        widget.rowId = item.id;
                                                        DoctorModel d =
                                                            DoctorModel();
                                                        d.name = item.name;
                                                        d.id = item.id;
                                                        d.description =
                                                            item.description;
                                                        print(
                                                            " ____d.id===> ${d.id}");
                                                        print(
                                                            "___e.describtion ===> ${item.description}");
                                                        print(
                                                            "item.name ===> ${item.name}");
                                                        Navigator.push(
                                                            context,
                                                            PageTransition(
                                                                type: PageTransitionType
                                                                    .leftToRight,
                                                                duration: Duration(
                                                                    milliseconds:
                                                                        500),
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                curve: Curves
                                                                    .easeOutCirc,
                                                                child:
                                                                    CounsalterAbout(
                                                                  id: widget
                                                                      .rowId,
                                                                )));
                                                      },
                                                      isOnline: true,
                                                      hasLightShadow: true,
                                                      hour: "",
                                                      minute: "",
                                                      msgNumber: "",
                                                      hasTime: false,
                                                      hasShadow: true,
                                                      hasUnSeenMsg: false,
                                                    ))
                                                .toList()),
                                      )),
//                                        widget.loading==false?Conf.circularProgressIndicator():Container(),
//                                        (widget.loading==true && widget.loading1==true)?Conf.circularProgressIndicator():Container()
                                ],
                              ))
//                      widget.loading == true ? Conf.circularProgressIndicator() : Container()
//                      (widget.loading==true && widget.loading1==true)?Conf.circularProgressIndicator():Container()
                          ),
                    )
//                    Stack(
//                        children: [

//                        ]),
                    ,
                    bottomNavigationBar: BottomNavigation(3)))
//        (widget.loading == true && widget.loading1==true)? Conf.circularProgressIndicator() : Container(),
            ),
        (widget.loading == true && widget.loading1 == true)
            ? Conf.circularProgressIndicator()
            : Container()
      ],
    );
  }

  Widget listTileItem(IconData icon, String title, BuildContext context,
      {Function onTap}) {
    return Padding(
      padding: EdgeInsets.only(top: Conf.p2h(1), bottom: Conf.p2h(1)),
      child: Row(children: [
        Padding(
          padding: EdgeInsets.only(right: Conf.p2w(1.459)),
          child: Icon(icon, size: Conf.p2w(3.8), color: Colors.white),
        ),
        InkWell(
            child: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(5), top: Conf.p2h(1.5)),
              child: Text(title, style: widget.menuStyle),
            ),
            onTap: () {
              onTap();
            })
      ]),
    );
  }
}
