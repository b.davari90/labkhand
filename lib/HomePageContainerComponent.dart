import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'config.dart';

class HomePageContainerComponent extends StatelessWidget {
  IconData picIconIconData;
  String title1, title2, title2Count, title3;

  Color backColor;

  bool isColor = false;
  bool isChoose = false;
  bool haveBackgroundColor = false;

  TextStyle txtStyleGrey = TextStyle(
    color: Colors.black45,
    fontSize: 15,
    fontWeight: FontWeight.w900,
  );
  TextStyle txtStyleDarkGrey = TextStyle(
      color: Colors.black87.withOpacity(0.7),
      fontSize: 15,
      fontWeight: FontWeight.w900);
  TextStyle txtStyleWhite =
      TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.w900);

  @override
  Widget build(BuildContext context) {
    return Container(
//      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          spreadRadius: Conf.p2w(0.4167), //2,
          blurRadius: Conf.p2w(1.042), //5,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ], borderRadius: Conf.fullBorderRadius / 3.0, color: backColor),
//      width: Conf.p2w(25),
//      height: Conf.p2h(15),
      child: Padding(
        padding: Conf.mdEdge * 2.0,
        child: IntrinsicWidth(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Icon(picIconIconData,
                    color: isChoose ? Colors.white : Conf.iconColor,
                    size: 30),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Expanded(
                  flex: 2,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: SizedBox(
                      child: Text(
                        title1,
                        softWrap: true,
                        style: isChoose ? txtStyleWhite : txtStyleGrey,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: Conf.smEdgeTop00,
                      child: Text(title2,
                          softWrap: true,
                          style: isChoose ? txtStyleWhite : txtStyleDarkGrey),
                    ),
                    FittedBox(fit: BoxFit.cover, child: Text("  ")),
                    Padding(
                      padding: EdgeInsets.only(top: 2),
                      child: Text(
                        title2Count,
                        softWrap: true,
                        style: isChoose ? txtStyleWhite : txtStyleDarkGrey,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      decoration: haveBackgroundColor
                          ? BoxDecoration(
                          color: Conf.orang,
                              borderRadius: Conf.primaryBorderRadius)
                          : null,
                      child: Padding(
                        padding: EdgeInsets.only(top: 5),
//                        padding: Conf.smEdgeTop0 * 2.5,
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: Text(title3,
                              softWrap: true,
                              style: Conf.body1.copyWith(
                                color: Conf.orang,
                                fontWeight: FontWeight.w900,
                                fontSize: 13,
                                decoration:
                                    isColor ? TextDecoration.underline : null,
                              )
//                      TextStyle(color: Colors.orange,fontWeight: FontWeight.w900,
//                       fontSize: 13, decoration: isColor ? TextDecoration.underline : null,
//                      ),
                              ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  HomePageContainerComponent(
      {this.picIconIconData,
      this.isChoose,
      this.backColor,
      this.title1,
      this.isColor,
      this.haveBackgroundColor,
      this.title2,
      this.title3,
      this.title2Count});
}
