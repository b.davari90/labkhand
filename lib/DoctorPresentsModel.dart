import 'package:bloc/bloc.dart';
import 'package:counsalter5/Doctor/DoctorTimeSliceModel.dart';

class DoctorPresentsModel {
  String date, type, data_shamsi, created_at, updated_at, dj,time, time_start, time_end;
  bool isSelectCounsalterKind;
  int delay, minutes, id, doctor_id;
  List times_filled, times_free,types,times;
  DoctorTimeSliceModel doctorTimeSliceModel;

  DoctorPresentsModel(
      {this.date,
      this.doctorTimeSliceModel,
      this.delay,
      this.types,
      this.time_start,
      this.time_end,
      this.isSelectCounsalterKind,
      this.doctor_id,
      this.times,
      this.updated_at,
      this.created_at,
      this.data_shamsi,
      this.dj,
      this.times_free,
      this.id,
      this.minutes,
      this.time,
      this.times_filled,
        this.type
      });

  factory DoctorPresentsModel.fromJson(Map<String, dynamic> json) {
//    DoctorTimeSliceModel timeSliceModel;
//      if (json['timeSliceModel'] != null) {
//        timeSliceModel = DoctorTimeSliceModel.fromJson(json['transaction']);
//      } else {
//        timeSliceModel = null;
//      }
    return DoctorPresentsModel(
      date: json['date'],
      times_filled: json['times_filled'],
      updated_at: json['updated_at'],
      created_at: json['created_at'],
      types: json['type'],
      times:json["times"],
      dj: json['dj'],
      times_free: json['times_free'],
      delay: json['delay'],
      doctor_id: json['doctor_id'],
      id: json['id'],
      time_start: json['time_start'],
      time_end: json['time_end'],
      minutes: json['minutes'],
//      type: json['type'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['type'] = this.types;
    data['updated_at'] = this.updated_at;
    data['created_at'] = this.created_at;
    data['dj'] = this.dj;
    data['times'] = this.times;
    data['times_free'] = this.times_free;
    data['delay'] = this.delay;
    data['data_shamsi'] = this.data_shamsi;
    data['doctor_id'] = this.doctor_id;
    data['time_start'] = this.time_start;
    data['time_end'] = this.time_end;
    data['id'] = this.id;
    data['isSelectCounsalterKind'] = this.isSelectCounsalterKind;
    data['minutes'] = this.minutes;
    data['time'] = this.time;
    data['times_filled'] = this.times_filled;
    return data;
  }

  DoctorPresentsModel copy2() {
    return DoctorPresentsModel(
        date: "",
        created_at: "",
        data_shamsi: "",
        dj: "",
        types:[],
        doctorTimeSliceModel: DoctorTimeSliceModel(),
        type: "",
        times: [],
        times_free: [],
        updated_at: "",
        delay: 0,
        time_start: "زمان شروع",
        time_end: "زمان پایان",
        doctor_id: 0,
        id: 0,
        minutes: 0,
        isSelectCounsalterKind: false,
        time: "",
        times_filled: []);
  }
}

class DoctorPresentsRepository {
  DoctorPresentsModel doctorpresent;
  List<DoctorPresentsModel> doctorPresentsModelList = [];
  List<DoctorTimeSliceModel> doctorTimeSliceList = [];
  List<String> typeList = [];

  DoctorPresentsRepository(
      {this.doctorpresent, this.doctorPresentsModelList, this.typeList}) {
    if (this.doctorpresent == null) {
      this.doctorpresent = new DoctorPresentsModel();
    }
  }

  DoctorPresentsRepository copy() {
    return DoctorPresentsRepository(
        typeList: typeList,
        doctorpresent: doctorpresent,
        doctorPresentsModelList: doctorPresentsModelList);
  }

  DoctorPresentsRepository clearDocumentRepository() {
    return DoctorPresentsRepository(
        doctorpresent: doctorpresent,
        doctorPresentsModelList: doctorPresentsModelList);
  }
}

class DoctorPresentsBloc extends Cubit<DoctorPresentsRepository> {
  DoctorPresentsBloc(DoctorPresentsRepository state) : super(state);

  void setDoctorPresent(DoctorPresentsModel doctorPresent) {
    state.doctorpresent = doctorPresent;
    refresh();
  }

  setTimeSliceListPresent(List<DoctorTimeSliceModel> list) {
    if (state.doctorTimeSliceList == null) {
      state.doctorTimeSliceList = [];
    }
    print("state.doctorTimeSliceList  bloc===> ${state.doctorTimeSliceList}");
    print("list bloc===> ${list}");
    state.doctorTimeSliceList.addAll(list);
    refresh();
  }

  removeTypeListPresent(String types) {
    if (state.typeList == null) {
      state.typeList = [];
    }
    if (state.typeList.contains(types)) {
      print("111111 ===> ${111111}");
      state.typeList.remove(types);
      print(" bloc remove===> ${state.typeList.length}");
      refresh();
    }
  }

  setTypeListPresent(String types) {
    if (state.typeList == null) {
      state.typeList = [];
    }
    print("setTypeListPresent contain ===> ${state.typeList.contains(types)}");
    if (state.typeList.contains(types)) {
      state.typeList.remove(types);
    }
    print("types ===> ${types}");
    state.typeList.add(types);
    print("bloc  Add===> ${state.typeList.length}");
    refresh();
//  }
  }

  setDelayPresent(int delay) {
    state.doctorpresent.delay = delay;
    refresh();
  }

  setType(String type) {
    state.doctorpresent.type = type;
    refresh();
  }
  setTypeS(List typeLst) {
    state.doctorpresent.types = typeLst;
    refresh();
  }
  setFreeTimes(List timesFreeLst) {
    state.doctorpresent.times_free = timesFreeLst;
    refresh();
  }
  setMinutePresent(int minute) {
    state.doctorpresent.minutes = minute;
    refresh();
  }

  set_DateGerogarian_Present(String date) {
    state.doctorpresent.date = date;
    print(" set_DateGerogarian_Present===> ${state.doctorpresent.date}");
    refresh();
  }

  set_DateShamsi_Present(String date) {
    state.doctorpresent.data_shamsi = date;
    print("set_DateShamsi_Present ===> ${state.doctorpresent.data_shamsi}");
    refresh();
  }

  void refresh() {
    emit(state.copy());
  }
}
