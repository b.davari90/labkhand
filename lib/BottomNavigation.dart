import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:counsalter5/VisitListPage.dart';
import 'package:counsalter5/visitPage.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:page_transition/page_transition.dart';
import 'CoustomerPage.dart';
import 'HomePage.dart';
import 'config.dart';
import 'package:counsalter5/htify.dart';
import 'CustomerModel.dart';
import 'package:flutter/widgets.dart';

class BottomNavigation extends StatefulWidget {
  int currentIndex = 0;

  BottomNavigation(this.currentIndex);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
//  bool loading = false,loading1=false;
//  CustomerModel customerModel;
//  Future getCustomer() async {
//    try {
//      setState(() {
//        loading1 = true;
//      });
//      var data = await Htify.get('cu/customer');
//      print("909090 ===> ${ 909090 }");
//      setState(() {
//        customerModel=CustomerModel.fromJson(data);
//        print("____77777 ===> ${ 77777 }");
////        coustomerBloc.setUser(customerModel);
////        print("___coustomerBloc NAME===> ${ coustomerBloc.state.user.name }");
//      });
//    } catch (e) {
//      print("___CATCH ERROR getCustomer===> ${e.toString()}");
//    } finally {
//      setState(() {
//        loading1 = false;
//      });
//    }
//  }
  @override
  void initState() {
    super.initState();
  }
  void changePage(int index) {
    if (index == widget.currentIndex) return;
    switch (index) {
      case 0:
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 0),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: CoustomerPage()
            ));
        break;
      case 1:
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: VisitPage()
            ));
        break;
      case 2:
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: VisitListPage()
            ));
        break;
      case 3:
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: HomePage()
            ));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Conf.p2h(11),
      color: Colors.white12,
      child: BubbleBottomBar(
        backgroundColor: Conf.blue,
        opacity: 0.999,
        currentIndex: widget.currentIndex,
        onTap: changePage,
        borderRadius: BorderRadius.vertical(top: Radius.circular(13)),
//        elevation: 30,
//            inkColor: Colors.black12,
        items: <BubbleBottomBarItem>[
          BubbleBottomBarItem(
              backgroundColor: Conf.orang,
              icon: Icon(
                Icons.person_outline,
                color: Colors.white,
              ),
              activeIcon: Icon(
                Icons.person_outline,
                color: Colors.white,
              ),
              title: FittedBox(
                fit: BoxFit.cover,
                child: Text("پروفایل من", softWrap: true,
                    overflow: TextOverflow.fade,style:
                    Conf.body2.copyWith(color: Colors.white)
//                  TextStyle(color: Colors.white)
                ),
              )),
          BubbleBottomBarItem(
              backgroundColor: Conf.orang,
              icon: Icon(
                FeatherIcons.messageSquare,
                color: Colors.white,
              ),
              activeIcon: Icon(
                FeatherIcons.messageSquare,
                color: Colors.white,
              ),
              title: Text("پیام ها", softWrap: true,
                  overflow: TextOverflow.fade,
                  style:Conf.body2.copyWith(color: Colors.white)
//                  TextStyle(color: Colors.white)
              )),
          BubbleBottomBarItem(
              backgroundColor: Conf.orang,
              icon: Icon(
                FeatherIcons.clipboard,
                color: Colors.white,
              ),
              activeIcon: Icon(
                FeatherIcons.clipboard,
                color: Colors.white,
              ),
              title: FittedBox(
                fit: BoxFit.cover,
                child: Text("ویزیت ها", softWrap: true,
                    overflow: TextOverflow.fade,style:
                Conf.body2.copyWith(color:Colors.white)
//                  TextStyle(color: Colors.white)
                ),
              )),
          BubbleBottomBarItem(
              backgroundColor:  Conf.orang,
//              backgroundColor: Colors.white,
              icon: Icon(
                FeatherIcons.home,
                color: Colors.white,
              ),
              activeIcon: Icon(
                FeatherIcons.home,
                color: Colors.white,
              ),
              title: FittedBox(
                fit: BoxFit.cover,
                child: Text("خانه", softWrap: true,
                  overflow: TextOverflow.fade,style:
                Conf.body2.copyWith(color: Colors.white)
//                TextStyle(color: Colors.white),
                ),
              ))
        ],
      ),
    );
  }
}

@override
Widget build(BuildContext context) {
  // TODO: implement build
  throw UnimplementedError();
}

//

