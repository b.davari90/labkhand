import 'package:counsalter5/CustomerModel.dart';
import 'DoctorModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import 'ChatPage.dart';
import 'config.dart';

class CounsalterFollowup2 extends StatefulWidget {
  @override
  _CounsalterFollowUpState createState() => _CounsalterFollowUpState();
}

class _CounsalterFollowUpState extends State<CounsalterFollowup2> {
  TextStyle nameStyle = TextStyle(
      fontSize: Conf.p2t(16),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w600,
      color: Colors.black87);
  TextStyle name2Style = TextStyle(
      fontSize: Conf.p2t(16),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w600,
      color: Colors.black87);
  TextStyle describtionStyle = TextStyle(
      fontSize: Conf.p2t(16),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w500,
      color: Colors.black87);
  TextStyle titleStyle = TextStyle(
      fontSize: Conf.p2t(20),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w500,
      color: Colors.black87);
  TextStyle progressBarStyleTxt = TextStyle(
      fontSize: Conf.p2t(16),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w600,
      color: Colors.black87);
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DoctorBloc, DoctorRepository>(
    builder: (context, state) {
    return SingleChildScrollView(
        child: Column(mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(right: Conf.p2w(5), top: Conf.p2h(2)),
              child: Text(
                 state.doctor.name??"",
                style: nameStyle,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: Conf.p2w(5)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  BlocBuilder<CoustomerBloc, CoustomerRepository>(
    builder: (context, state) {
      return Text(
        state.user.name??"",
        style: name2Style,
      );}),
                ],
              ),
            ),
            IntrinsicHeight(
              child: Center(
                child: Container(width: Conf.p2w(80),height:Conf.p2h(30),padding: EdgeInsets.all(5),
                  child: Text(""),margin: EdgeInsets.only(top: Conf.p2w(2),bottom:  Conf.p2w(5)),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      color: Colors.grey.shade100,
                      border: Border.all(width: 1, color: Colors.grey)),
                ),
              ),
            )
          ],
        ),
    );
    },
    );
  }
}
