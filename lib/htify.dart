import 'dart:convert';
import 'package:counsalter5/config.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Htify {
  static String TOKEN;
  static Map<String, String> HEADER ={
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };

  static Future specify(String method, String url,
      {Map<String, String> payload, Map<String,String> params,
      Map<String, String> header}) {
    return Htify._send(method, url, header);
  }

  static Future one(String url, int id, {Map<String, String> header,Map<String,dynamic> params}) {
    return Htify._send('get', url + '/$id', header);
  }

  static Future get(String url, {Map<String,String> query ,Map<String, String> header,Map<String,dynamic> params}) {
    return Htify._send('get', url, header,payload: params);
  }

  static Future update(String url, int id, Map<String, dynamic> payload,
      {Map<String, String> header}) {
    return Htify._send('put', url + '/$id', header,payload: payload);
  }

  static Future create(String url, Map<String, dynamic> payload,
      {Map<String, String> header}) {
    
    return Htify._send('post', url, header,payload: payload);
  }


  static Future sendFile(String url,FormData formData,
      {Map<String, String> header}) {
    return Htify._send('sendFile', url, header,formData: formData);
  }

  static Future delete(String url, int id, {Map<String, String> header,Map<String, dynamic> payload,}) {
    return Htify._send('delete', url + '/$id', header,payload: payload);
  }

  static Future download(String url,{Map<String, String> header}) {
    return Htify._send('download', url , header);
  }

  static Future _send(String method, String url, Map<String, String> header,{Map<String,dynamic> payload,FormData formData}) async{
    // final login = new FlutterSecureStorage();
    final SharedPreferences login = await SharedPreferences.getInstance();
    // HEADER['Authorization'] = 'Bearer ' + await login.read(key: 'token');
    if(login.getString('token') is String){
      HEADER['Authorization'] = 'Bearer ' + login.getString('token');
    }
    var response;
    Dio dio = new Dio();
    var _header = header??Htify.HEADER;
    var _url = Conf.BASE_URL+url;
    try{
    switch(method){
      case 'delete':
        response = await dio.delete(_url,data:payload ,options: RequestOptions(headers: _header,));
        break;
      case 'post':
        response = await dio.post(_url,data: payload,options: RequestOptions(headers: _header));
        // print('789999 ===> ${789999}');
        // response = await http.post( _url,
        //     headers: _header, body: json.encode(payload) ?? {});
        // print('response ===> ${response}');
        break;
      case 'sendFile':
        Map<String,String> h = {'Accept': 'application/json'};
        // h['Authorization'] = 'Bearer '+ await login.read(key: 'token');
        if(login.getString('token') is String){
          h['Authorization'] = 'Bearer ' + login.getString('token');
        }
        response = await dio.post(_url,data: formData,options: RequestOptions(headers:h,responseType: ResponseType.json,contentType:'multipart/form-data'));
        break;
      case 'put':
        response = await dio.put(_url,data: payload,options: RequestOptions(headers: _header));
        break;
      case 'get':
        response = await dio.get(_url,options: RequestOptions(headers: _header,),queryParameters: payload);
        print("___getData_htify===> ${ response.toString() }");
        break;
      case 'download':
        response = await dio.download(_url, "/pdf.pdf",options:RequestOptions(headers: _header,) );
        break;
    }
//    print('response?.statusCode ===> ${response?.statusCode}');
      print('response.statusCode ===> ${response.statusCode}');
//      print('___response ===> ${response}');
      return response?.data;
    }on DioError catch(e){
      switch(e.response?.statusCode){
        case 500:
          print('response.body ===> ${e.response.data}');
          throw ServerError500(e.response?.data,'');
          break;
        case 404:
          print('response.body ===> ${e.response.data}');
          throw ServerError404(e.response?.data,'');
          break;
        case 422:
          print('response.body ===> ${e.response.data}');
          var t = json.decode(e?.response?.data);
          throw ServerError422(e.response?.data, t['message']);
          break;
        case 401:
          print('response.body ===> ${e.response.data}');
          throw ServerError401(e.response?.data,'');
          break;
        case 403:
          print('response.body ===> ${e.response.data}');
          throw ServerError403(e.response?.data, '');
          break;
        default:
          print('response.body ===> ${e}');
          throw ServerError(e.response?.data, e.response?.statusCode, '');
      }
    } catch(e){
      print('e e e===> ${e}');
      throw e;
    }
  }

}

class ServerError {
  final data;
  final int code;
  final String message;

  ServerError(this.data, this.code, this.message);
}
class ServerError403 extends ServerError {
  ServerError403(data, String message) : super(data, 403, message);
}

class ServerError401 extends ServerError {
  ServerError401(data, String message) : super(data, 401, message){
    Future removeToken() async {
      // final storage = new FlutterSecureStorage();
      // await storage.delete(key: 'token');
      final SharedPreferences ln = await SharedPreferences.getInstance();
      ln.remove('token');
      Conf.token = null;
    }
  }
}

class ServerError500 extends ServerError {
  ServerError500(data, String message) : super(data, 500, message);
}

class ServerError404 extends ServerError {
  ServerError404(data, String message) : super(data, 404, message);
}

class ServerError422 extends ServerError {
  ServerError422(data, String message) : super(data, 422, message);
}

