import 'package:counsalter5/config.dart';
import 'package:counsalter5/CounsalterReserveDateRowModel.dart';
import 'package:flutter/material.dart';
class CounsalterReserveDateRowComponent extends StatefulWidget {
  CounsalterReserveDateRowModel model;
  Function onTap;
  bool isSelectedRow,isActiveDay;
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);

  CounsalterReserveDateRowComponent({this.model,this.onTap,this.isSelectedRow=false,this.isActiveDay=false});

  @override
  _CounsalterReserveDateComponentState createState() =>
      _CounsalterReserveDateComponentState();
  }
class _CounsalterReserveDateComponentState extends State<CounsalterReserveDateRowComponent> {

  @override
  Widget build(BuildContext context) {
    return
      GestureDetector(
        onTap: () {
          widget.onTap();
          setState(() {
            if (widget.onTap is Function) {
              widget.onTap();
              if (widget.isSelectedRow == false) {
                print("widget.isSelectedRow===> ${ widget.isSelectedRow }");
                widget.isSelectedRow = true;
              } else
                widget.isSelectedRow = false;
              print("widget.isSelectedRow ===> ${ widget.isSelectedRow}");
            }
          });
        },
      child: Container(
          child: Center(
              child: Text(
                widget.model.numberDate.toString(),
                style: TextStyle(
                    fontSize: Conf.p2t(20),
                    fontWeight: FontWeight.w700,
                    color: widget.isSelectedRow? Colors.white : Colors.black87
                ),
              )),
          margin: EdgeInsets.only(
              top: Conf.p2h(0.25),
              left: Conf.p2w(2.918),
              right: Conf.p2w(2.918)),
          width: Conf.p2w(11.5),
          height: Conf.p2w(11.5),
          decoration: BoxDecoration(
              color: widget.isSelectedRow ? Conf.blue : Conf.lightColor,
              border: Border.all(
                color: widget.isSelectedRow ?  Conf.blue : Colors.grey.withOpacity(0.3)),
              boxShadow: [
                BoxShadow(color: Colors.grey.withOpacity(0.2),offset: Offset(-1, 2))],
              borderRadius: BorderRadius.all(Radius.circular(Conf.p2w(2.71))))),
    );
  }
}
