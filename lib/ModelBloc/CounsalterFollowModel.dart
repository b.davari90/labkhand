import 'package:counsalter5/CustomerModel.dart';
import 'package:counsalter5/DoctorModel.dart';

class CounsalterFollowModel {
    String created_at,subject,updated_at,date_at,description,time_at;
    int customer_id,doctor_id,folder_id,id;
    CustomerModel customer;
    DoctorModel doctor;


    CounsalterFollowModel({this.created_at, this.customer, this.customer_id, this.date_at, this.description, this.doctor, this.doctor_id, this.folder_id, this.id, this.subject, this.time_at, this.updated_at});

    factory CounsalterFollowModel.fromJson(Map<String, dynamic> json) {
        CustomerModel customer;
        if (json['customer'] != null && json.containsKey('customer')) {
            customer = CustomerModel.fromJson(json['customer']);
        }
        DoctorModel doctor;
        if (json['doctor'] != null && json.containsKey('doctor')) {
            doctor = DoctorModel.fromJson(json['doctor']);
        }
        return CounsalterFollowModel(
            created_at: json['created_at'], 
            customer:customer,
            customer_id: json['customer_id'], 
            date_at: json['date_at'], 
            description: json['description'], 
            doctor: doctor,
            doctor_id: json['doctor_id'], 
            folder_id: json['folder_id'], 
            id: json['id'], 
            subject: json['subject'], 
            time_at: json['time_at'], 
            updated_at: json['updated_at'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['created_at'] = this.created_at;
        data['customer'] = this.customer;
        data['customer_id'] = this.customer_id;
        data['date_at'] = this.date_at;
        data['description'] = this.description;
        data['doctor'] = this.doctor;
        data['doctor_id'] = this.doctor_id;
        data['folder_id'] = this.folder_id;
        data['id'] = this.id;
        data['subject'] = this.subject;
        data['time_at'] = this.time_at;
        data['updated_at'] = this.updated_at;
        return data;
    }
}