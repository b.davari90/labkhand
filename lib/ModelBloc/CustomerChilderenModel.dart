import 'package:bloc/bloc.dart';
import 'package:counsalter5/ModelBloc/DocumentModel.dart';

class CustomerChilderenModel {
  String email,
      fname,
      lname,
      name,
      last_seen_at,
      mobile,
      password,
      avatar,
      married_str,
      gender_str,
      sts_str,
      token_sms,
      token_mobile;
  int id, gender, married, wallet, sts;
  bool active;
  DocumentModel document;
  CustomerChilderenModel(
      {this.email,
      this.fname,
      this.lname,
      this.name,
      this.last_seen_at,
      this.mobile,
      this.password,
      this.avatar,
      this.married_str,
      this.gender_str,
      this.sts_str,
      this.token_sms,
      this.token_mobile,
      this.id,
      this.gender,
      this.married,
      this.wallet,
      this.sts,
      this.active,
      this.document});

  CustomerChilderenModel copy() {
    return new CustomerChilderenModel(
        name: name,
        gender_str: gender_str,
        married_str: married_str,
        sts: sts,
        sts_str: sts_str,
        token_mobile: token_mobile,
        token_sms: token_sms,
        document: document,
        wallet: wallet,
        email: email,
        mobile: mobile,
        password: password,
        active: active,
        id: id,
        gender: gender,
        avatar: avatar,
        fname: fname,
        last_seen_at: last_seen_at,
        lname: lname,
        married: married);
  }

  factory CustomerChilderenModel.fromJson(Map<String, dynamic> json) {
    try {
      DocumentModel document;
      if (json['document'] != null && json.containsKey('document')) {
        document = DocumentModel.fromJson(json['document']);
      }


      return CustomerChilderenModel(
        name: json['name'] ?? null,
        mobile: json['mobile'] ?? null,
        email: json['email'] ?? null,
        document: document ?? null,
        password: json['password'] ?? null,
        wallet: json['wallet'] ?? null,
        token_sms: json['token_sms'] ?? null,
        token_mobile: json['token_mobile'] ?? null,
        sts: json['sts'] ?? null,
        gender_str: json['gender_str'] ?? null,
        sts_str: json['sts_str'] ?? null,
        married_str: json['married_str'] ?? null,
        active: json['active'] ?? null,
        id: json['id'] ?? null,
        gender: json['gender'] ?? null,
        avatar: json['avatar'] ?? null,
        fname: json['fname'] ?? null,
        last_seen_at: json['last_seen_at'] ?? null,
        lname: json['lname'] ?? null,
        married: json['married'] ?? null,
      );
    } catch (e) {
      throw 'user model not valid model=> ${e}';
    }
  }

  Map<String, dynamic> toJson(String provider) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mobile'] = this.mobile;
    data['name'] = this.name;
    data['gender_str'] = this.gender_str;
    data['married_str'] = this.married_str;
    data['wallet'] = this.wallet;
    data['sts'] = this.sts;
    data['sts_str'] = this.sts_str;
    data['token_sms'] = this.token_sms;
    data['token_mobile'] = this.token_mobile;
    data['email'] = this.email;
    data['password'] = this.password;
    data['document'] = this.document;
    data['active'] = this.active;
    data['id'] = this.id;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['fname'] = this.fname;
    data['last_seen_at'] = this.last_seen_at;
    data['lname'] = this.lname;
    data['married'] = this.married;
    data["provider"] = provider;
    return data;
  }
}

class CustomerChilderenRepository {
  CustomerChilderenModel userChild;
  CustomerChilderenRepository({this.userChild}) {
    if (this.userChild == null) {
      this.userChild = new CustomerChilderenModel();
    }
  }
  CustomerChilderenRepository copy() {
    return CustomerChilderenRepository(userChild: userChild);
  }
  CustomerChilderenRepository clearDocumentRepository() {
    return CustomerChilderenRepository(userChild: userChild);
  }
}
class CoustomerChilderenBloc extends Cubit<CustomerChilderenRepository> {
  CoustomerChilderenBloc(CustomerChilderenRepository state) : super(state);

void setUser(CustomerChilderenModel user) {
  state.userChild = user;
  print("state.doctor customer MODEL===> ${state.userChild}");
  refresh();
}
setAvatar(String avatar){
  state.userChild.avatar=avatar;
  print("setAvatar bloc  ===> ${ state.userChild.avatar}");
  refresh();
}
void refresh() {
  emit(state.copy());
}
}
