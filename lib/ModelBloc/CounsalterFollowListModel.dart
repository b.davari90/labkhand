import 'package:counsalter5/ModelBloc/CounsalterFollowModel.dart';
class CounsalterFollowListModel {
    int count,limit,page;
    String date_at;
    List<CounsalterFollowModel> rows;

    CounsalterFollowListModel({this.count,this.date_at, this.limit, this.page, this.rows});

    factory CounsalterFollowListModel.fromJson(Map<String, dynamic> json) {
        List<CounsalterFollowModel> rows =[];
        if (json.containsKey('rows') && json['rows'] != null ) {
            List dList = json['rows'];
            rows = dList.map((e) => CounsalterFollowModel.fromJson(e)).toList();
        }
        return CounsalterFollowListModel(
            count: json['count'], 
            limit: json['limit'],
            date_at: json['date_at'],
            page: json['page'], 
            rows: rows,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['date_at'] = this.date_at;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}