import 'package:flutter_bloc/flutter_bloc.dart';


class DocumentModel {
  int id, doctor_id, type, type_doc, folder_id, file_id, sts;
  String title, updated_at, src, created_at,type_doc_str;
  SubDataDocument subdata;
  DocumentModel(
      {this.created_at,
      this.doctor_id,
        this.subdata,
      this.file_id,
      this.folder_id,
        this.type_doc_str,
      this.id,
      this.src,
      this.sts,
      this.title,
      this.type,
      this.type_doc,
      this.updated_at});

  factory DocumentModel.fromJson(Map<String, dynamic> json) {
    SubDataDocument subdata;
    if (json['subdata'] != null) {
      subdata = SubDataDocument.fromJson(json['subdata']);
    } else {
      subdata = null;
    }
    return DocumentModel(
      subdata: subdata ?? null,
      created_at: json['created_at'],
      doctor_id: json['doctor_id'],
      file_id: json['file_id'],
      folder_id: json['folder_id'],
      id: json['id'],
      type_doc_str:json['type_doc_str'] ,
      src: json['src'],
      sts: json['sts'],
      title: json['title'],
      type: json['type'],
      type_doc: json['type_doc'],
      updated_at: json['updated_at'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.created_at;
    data['doctor_id'] = this.doctor_id;
    data['file_id'] = this.file_id;
    data['folder_id'] = this.folder_id;
    data['id'] = this.id;
    data['subdata'] = this.subdata;
    data['src'] = this.src;
    data['sts'] = this.sts;
    data['type_doc_str'] = this.type_doc_str;
    data['title'] = this.title;
    data['type'] = this.type;
    data['type_doc'] = this.type_doc;
    data['updated_at'] = this.updated_at;
    return data;
  }
  DocumentModel clearDocument() {
    return DocumentModel(
      type_doc: 1,updated_at: "",src: "",file_id: 0,created_at: "",id: 0,title: "",doctor_id: 0,sts: 0,folder_id: 0,type: 0
     );
  }
  DocumentModel copy() {
    return DocumentModel(
        type: type,
        folder_id: folder_id,
        sts: sts,
        subdata: subdata,
        doctor_id: doctor_id,
        title: title,
        id: id,
        created_at: created_at,
        file_id: file_id,
        src: src,
        type_doc: type_doc,
        updated_at: updated_at);
  }
}
class SubDataDocument{
  int file_id,type_doc;
String avatar;
  SubDataDocument({this.avatar,this.file_id, this.type_doc});
  factory SubDataDocument.fromJson(Map<String, dynamic> json) {
    SubDataDocument subdata;
    if (json['subdata'] != null) {
      subdata = SubDataDocument.fromJson(json['subdata']);
    } else {
      subdata = null;
    }
    return SubDataDocument(
      file_id: json['file_id'],
      avatar: json['avatar'],
      type_doc: json['type_doc'],
    );
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['file_id'] = this.file_id;
    data['avatar'] = this.avatar;
    data['type_doc'] = this.type_doc;

    return data;
  }
}
class DocumentModelRepository {
    DocumentModel documentModel;
    DocumentModelRepository({this.documentModel
    }) {
        if (this.documentModel == null) {
            this.documentModel = new DocumentModel();
        }
    }
    DocumentModelRepository cleanDocumentRepository(){return DocumentModelRepository(documentModel: documentModel,
    );}

    DocumentModelRepository copy() {
        return DocumentModelRepository(documentModel: documentModel
        );
    }
}
class DocumentModelBloc extends Cubit<DocumentModelRepository> {
    DocumentModelBloc(DocumentModelRepository state) : super(state);

    void refresh() {
        emit(state.copy());
    }
}