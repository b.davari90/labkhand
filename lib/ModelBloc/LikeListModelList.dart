import 'package:counsalter5/ModelBloc/LikeListModel.dart';
class LikeListModelList {
    int count, limit,page;
    List<LikeListModel> rows;

    LikeListModelList({this.count, this.limit, this.page, this.rows});

    factory LikeListModelList.fromJson(Map<String, dynamic> json) {
        List<LikeListModel> rows;
        if (json['rows'] != null && json.containsKey('rows')) {
            List dList = json['rows'];
            rows = dList.map((e) => LikeListModel.fromJson(e)).toList();
        }
        return LikeListModelList(
            count: json['count'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: rows,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}