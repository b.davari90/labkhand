import 'package:counsalter5/DoctorPresentsModel.dart';
class DoctorPresentListModel {
    int count,limit, page;
    List<DoctorPresentsModel> rows;
    String date;
    DoctorPresentListModel({this.count, this.date, this.limit, this.page, this.rows});

    factory DoctorPresentListModel.fromJson(Map<String, dynamic> json) {

        List<DoctorPresentsModel> row =[];
        if (json.containsKey('rows') && json['rows'] != null ) {
            List dList = json['rows'];
            row = dList.map((e) => DoctorPresentsModel.fromJson(e)).toList();
        }

        return DoctorPresentListModel(
            count: json['count'], 
            date: json['date'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: row,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['date'] = this.date;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}