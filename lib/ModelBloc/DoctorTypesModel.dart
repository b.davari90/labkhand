class DoctorTypesModel {
    String doctor_id;
    String id;
    String type;
    String type_str;

    DoctorTypesModel({this.doctor_id, this.id, this.type, this.type_str});

    factory DoctorTypesModel.fromJson(Map<String, dynamic> json) {
        return DoctorTypesModel(
            doctor_id: json['doctor_id'], 
            id: json['id'], 
            type: json['type'], 
            type_str: json['type_str'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['doctor_id'] = this.doctor_id;
        data['id'] = this.id;
        data['type'] = this.type;
        data['type_str'] = this.type_str;
        return data;
    }
}