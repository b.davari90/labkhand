import 'CustomerFolderListModel.dart';
import 'package:counsalter5/ModelBloc/CustomerFolderModel.dart';
class CustomerFolderListModel {
  int count, limit, page;
  List<CustomerFolderModel> rows;

  CustomerFolderListModel({this.count, this.limit, this.page, this.rows});

  factory CustomerFolderListModel.fromJson(Map<String, dynamic> json) {
    List<CustomerFolderModel> rows =[];
    if (json.containsKey('rows') && json['rows'] != null ) {
      List dList = json['rows'];
      rows = dList.map((e) => CustomerFolderModel.fromJson(e)).toList();
    }
    return CustomerFolderListModel(
      count: json['count'],
      limit: json['limit'],
      page: json['page'],
      rows: rows,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    data['limit'] = this.limit;
    data['page'] = this.page;
    data['rows'] = this.rows;
    return data;
  }
}