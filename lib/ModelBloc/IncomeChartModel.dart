class IncomeChartModel {
    String date, month, month_fa;
    int total;

    IncomeChartModel({this.date, this.month, this.month_fa, this.total});

    factory IncomeChartModel.fromJson(Map<String, dynamic> json) {
        return IncomeChartModel(
            date: json['date'], 
            month: json['month'], 
            month_fa: json['month_fa'], 
            total: json['total'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['date'] = this.date;
        data['month'] = this.month;
        data['month_fa'] = this.month_fa;
        data['total'] = this.total;
        return data;
    }
}