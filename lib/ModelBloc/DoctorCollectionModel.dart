import 'package:counsalter5/Collections.dart';
import 'package:bloc/bloc.dart';
import 'package:counsalter5/DoctorTypeModel.dart';
import 'package:counsalter5/ModelBloc/DoctorMediaModel.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
class DoctorCollectionModel {
    Collections collection;
    int collection_id,doctor_id,id;
    DoctorMediaModel media;
    DoctorPresentsModel peresent;
    DoctorTypeModel type;

    DoctorCollectionModel({this.collection, this.collection_id, this.doctor_id,this.type,this.id,this.media,this.peresent});

    factory DoctorCollectionModel.fromJson(Map<String, dynamic> json) {
        Collections collection;
        if (json['collection'] != null && json.containsKey('collection')) {
            collection = Collections.fromJson(json['collection']);
        }
        DoctorMediaModel media;
        if (json['media'] != null) {
            media = DoctorMediaModel.fromJson(json['media']);
        } else {
            media = null;
        }
//        DoctorPresentsModel peresent;
//        if (json['peresent'] != null) {
//            peresent = DoctorPresentsModel.fromJson(json['peresent']);
//        } else {
//            peresent = null;
//        }
        DoctorTypeModel type;
        if (json['type'] != null) {
            type = DoctorTypeModel.fromJson(json['type']);
        } else {
            type = null;
        }
        return DoctorCollectionModel(
            collection: collection ?? null,
            media: media ?? null,
//            peresent: peresent ?? null,
            type: type ?? null,
            collection_id: json['collection_id'],
            doctor_id: json['doctor_id'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['collection'] = this.collection;
        data['collection_id'] = this.collection_id;
        data['doctor_id'] = this.doctor_id;
        data['media'] = this.media;
        data['peresent'] = this.peresent;
        data['type'] = this.type;
        return data;
    }
}
class DoctorCollectionModelRepository {

    DoctorCollectionModel doctorCollectionModel;
    List<Collections> listCollections=[];
    DoctorCollectionModelRepository({this.doctorCollectionModel,this.listCollections});

    DoctorCollectionModelRepository copy() {
        return DoctorCollectionModelRepository(doctorCollectionModel: doctorCollectionModel);
    }
}

class DoctorCollectionModelCubit extends Cubit<DoctorCollectionModelRepository> {
    DoctorCollectionModelCubit(DoctorCollectionModelRepository state) : super(state);

    void refresh() {
        emit(state.copy());
    }

    void setCollectionId(int collection_id) {
        state.doctorCollectionModel.collection_id = collection_id;
        refresh();
    }

    void setDoctorId(int doctor_id) {
        state.doctorCollectionModel.doctor_id = doctor_id;
        refresh();
    }

//  void setCollection(Collections collection) {
//    state.doctorCollectionModel.collection = collection;
//    refresh();
//  }
    void setCollection(List<Collections> collection) {
        print("(((**   collection ===> ${ collection }");
        if(state.listCollections == null){
            state.listCollections = [];
        }
        state.listCollections.addAll(collection);
        refresh();
    }
}