import 'package:counsalter5/CustomerModel.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/Collections.dart';
class CustomerFolderModel {
  String body, caj,created_at,deleted_at,name,meta,sts_str,updated_at;
  int collection_id,id,doctor_id,owner_id,sts;
  CustomerModel owner;
  DoctorModel doctor;
  Collections collection;

  CustomerFolderModel({this.body, this.caj, this.collection_id,this.collection,this.doctor, this.created_at, this.deleted_at, this.doctor_id, this.id, this.meta, this.name, this.owner, this.owner_id, this.sts, this.sts_str, this.updated_at});

  factory CustomerFolderModel.fromJson(Map<String, dynamic> json) {
    CustomerModel owner;
    if (json['owner'] != null && json.containsKey('owner')) {
      owner = CustomerModel.fromJson(json['owner']);
    }
    DoctorModel doctor;
    if (json['doctor'] != null && json.containsKey('doctor')) {
      doctor = DoctorModel.fromJson(json['doctor']);
    }
    Collections collection;
    if (json['collection'] != null && json.containsKey('collection')) {
      collection = Collections.fromJson(json['collection']);
    }

    return CustomerFolderModel(
      body: json['body'],
      caj: json['caj'],
      collection_id: json['collection_id'],
      created_at: json['created_at'],
      deleted_at: json['deleted_at'],
      doctor_id: json['doctor_id'],
      id: json['id'],
      meta: json['meta'],
      name: json['name'],
      owner:owner,
      doctor: doctor,
      collection: collection,
      owner_id: json['owner_id'],
      sts: json['sts'],
      sts_str: json['sts_str'],
      updated_at: json['updated_at'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    data['caj'] = this.caj;
    data['collection_id'] = this.collection_id;
    data['created_at'] = this.created_at;
    data['deleted_at'] = this.deleted_at;
    data['doctor_id'] = this.doctor_id;
    data['id'] = this.id;
    data['meta'] = this.meta;
    data['name'] = this.name;
    data['owner'] = this.owner;
    data['owner_id'] = this.owner_id;
    data['sts'] = this.sts;
    data['sts_str'] = this.sts_str;
    data['updated_at'] = this.updated_at;
    return data;
  }
}