import 'package:counsalter5/DoctorModel.dart';
class LikeListModel {
    String created_at,updated_at;
    int customer_id,doctor_id,id;
    DoctorModel doctor;

    LikeListModel({this.created_at, this.customer_id, this.doctor, this.doctor_id, this.id, this.updated_at});

    factory LikeListModel.fromJson(Map<String, dynamic> json) {
        DoctorModel doctor;
        if (json['doctor'] != null && json.containsKey('doctor')) {
            doctor = DoctorModel.fromJson(json['doctor']);
        }
        return LikeListModel(
            created_at: json['created_at'], 
            customer_id: json['customer_id'], 
            doctor: doctor,
            doctor_id: json['doctor_id'],
            id: json['id'],
            updated_at: json['updated_at'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['created_at'] = this.created_at;
        data['customer_id'] = this.customer_id;
        data['doctor'] = this.doctor;
        data['doctor_id'] = this.doctor_id;
        data['id'] = this.id;
        data['updated_at'] = this.updated_at;
        return data;
    }
}