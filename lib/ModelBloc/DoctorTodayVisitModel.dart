class DoctorTodayVisitModel {
    int count,limit,page;
    String date ;
    List rows;

    DoctorTodayVisitModel({this.count, this.date, this.limit, this.page, this.rows});

    factory DoctorTodayVisitModel.fromJson(Map<String, dynamic> json) {
        return DoctorTodayVisitModel(
            count: json['count'], 
            date: json['date'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: json['rows'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['date'] = this.date;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}