
class DoctorPriceModel {
    String icon,created_at,updated_at;
    int fee,id,price,total,type,doctor_id;

    DoctorPriceModel({this.created_at, this.doctor_id, this.fee, this.icon, this.id, this.price, this.total, this.type, this.updated_at});

    factory DoctorPriceModel.fromJson(Map<String, dynamic> json) {
        return DoctorPriceModel(
            created_at: json['created_at'],
            doctor_id: json['doctor_id'],
            fee: json['fee'],
            icon: json['icon'],
            id: json['id'],
            price: json['price'],
            total: json['total'],
            type: json['type'],
            updated_at: json['updated_at'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['created_at'] = this.created_at;
        data['doctor_id'] = this.doctor_id;
        data['fee'] = this.fee;
        data['icon'] = this.icon;
        data['id'] = this.id;
        data['price'] = this.price;
        data['total'] = this.total;
        data['type'] = this.type;
        data['updated_at'] = this.updated_at;
        return data;
    }
}