class DoctorMediaModel {
    int type,folder_id,type_doc,sts,doctor_id,file_id;
    String created_at,title,src,updated_at;

    DoctorMediaModel({this.created_at, this.doctor_id, this.file_id, this.folder_id, this.src, this.sts, this.title, this.type, this.type_doc, this.updated_at});

    factory DoctorMediaModel.fromJson(Map<String, dynamic> json) {
        return DoctorMediaModel(
            created_at: json['created_at'], 
            doctor_id: json['doctor_id'], 
            file_id: json['file_id'], 
            folder_id: json['folder_id'], 
            src: json['src'], 
            sts: json['sts'], 
            title: json['title'], 
            type: json['type'], 
            type_doc: json['type_doc'], 
            updated_at: json['updated_at'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['created_at'] = this.created_at;
        data['doctor_id'] = this.doctor_id;
        data['file_id'] = this.file_id;
        data['folder_id'] = this.folder_id;
        data['src'] = this.src;
        data['sts'] = this.sts;
        data['title'] = this.title;
        data['type'] = this.type;
        data['type_doc'] = this.type_doc;
        data['updated_at'] = this.updated_at;
        return data;
    }
}