import 'package:counsalter5/DoctorModel.dart';
class MyCounsalterListModel {
    String caj,created_at,updated_at;
    int customer_id,id,owner_id,folder_id,doctor_id;
    DoctorModel doctor;

    MyCounsalterListModel({this.caj, this.created_at, this.customer_id, this.doctor, this.doctor_id, this.folder_id, this.id, this.owner_id, this.updated_at});

    factory MyCounsalterListModel.fromJson(Map<String, dynamic> json) {
        DoctorModel doctor;
        if (json['doctor'] != null && json.containsKey('doctor')) {
            doctor = DoctorModel.fromJson(json['doctor']);
        }
        return MyCounsalterListModel(
            caj: json['caj'],
            created_at: json['created_at'],
            customer_id: json['customer_id'],
            doctor:doctor,
            doctor_id: json['doctor_id'],
            folder_id: json['folder_id'],
            id: json['id'],
            owner_id: json['owner_id'],
            updated_at: json['updated_at'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['caj'] = this.caj;
        data['created_at'] = this.created_at;
        data['customer_id'] = this.customer_id;
        data['doctor'] = this.doctor;
        data['doctor_id'] = this.doctor_id;
        data['folder_id'] = this.folder_id;
        data['id'] = this.id;
        data['owner_id'] = this.owner_id;
        data['updated_at'] = this.updated_at;
        return data;
    }
}