class TransactionModel {
    int amount,customer_id,doctor_id,id,sts,transaction_id,visit_id;
    String caj,created_at,note,sts_str,updated_at;

    TransactionModel({this.amount, this.caj, this.created_at, this.customer_id, this.doctor_id, this.id, this.note, this.sts, this.sts_str, this.transaction_id, this.updated_at, this.visit_id});

    factory TransactionModel.fromJson(Map<String, dynamic> json) {
        return TransactionModel(
            amount: json['amount'], 
            caj: json['caj'], 
            created_at: json['created_at'], 
            customer_id: json['customer_id'], 
            doctor_id: json['doctor_id'], 
            id: json['id'], 
            note: json['note'], 
            sts: json['sts'], 
            sts_str: json['sts_str'], 
            transaction_id: json['transaction_id'], 
            updated_at: json['updated_at'], 
            visit_id: json['visit_id'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['amount'] = this.amount;
        data['caj'] = this.caj;
        data['created_at'] = this.created_at;
        data['customer_id'] = this.customer_id;
        data['doctor_id'] = this.doctor_id;
        data['id'] = this.id;
        data['note'] = this.note;
        data['sts'] = this.sts;
        data['sts_str'] = this.sts_str;
        data['transaction_id'] = this.transaction_id;
        data['updated_at'] = this.updated_at;
        data['visit_id'] = this.visit_id;
        return data;
    }
}