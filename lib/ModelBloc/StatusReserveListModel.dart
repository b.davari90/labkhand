import 'package:counsalter5/ModelBloc/StatusReserveModel.dart';
class StatusReserveListModel {
    int count, limit, page;
    List<StatusReserveModel> rows;

    StatusReserveListModel({this.count, this.limit, this.page, this.rows});

    factory StatusReserveListModel.fromJson(Map<String, dynamic> json) {
        List<StatusReserveModel> row =[];
        if (json.containsKey('rows') && json['rows'] != null ) {
            List dList = json['rows'];
            row = dList.map((e) => StatusReserveModel.fromJson(e)).toList();
        }
        return StatusReserveListModel(
            count: json['count'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: row,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}