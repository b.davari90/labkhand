
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/ModelBloc/DocumentModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DoctorBlocDoctorInformationPage extends Cubit<DoctorRepository> {
  DoctorBlocDoctorInformationPage(DoctorRepository state) : super(state);

  void setDoctor(DoctorModel doctor) {
    state.doctor = doctor;
    refresh();
  }

  void removeAvatar(DoctorModel doctor) {
    state.doctorAvatarList.removeWhere((item) => item.id == doctor.id);
    refresh();
  }

  setMobile(String mobile) {
    state.doctor.mobile = mobile;
    refresh();
  }
  setId(int id) {
    state.doctor.id = id;
    refresh();
  }
//  void setDocument(DocumentModel document) {
//    state.doctor.document = document;
//    refresh();
//  }

  void setAvatar(String avatar) {
    state.doctor.avatar = avatar;
    refresh();
  }

  void refresh() {
    emit(state.copy());
  }
}