//import 'package:flutter/cupertino.dart';
//
//class MyModel {
//  List<OrderItem> items;
//  String mobile;
//  int active = 0;
//
//  num get price {
//    num p = 0;
//    items.map((e) => e.finalPrice).forEach((element) {
//      p += element;
//    });
//    return p;
//  }
//
//  Color get color => items[active].color;
//
//  int get index => items[active].index;
//
//  Color get cl => color;
//
//  MyModel({this.items, this.mobile, this.active});
//
//  OrderItem get activeItem => items[active];
//
//  MyModel copy() {
//    return new MyModel(items: items, mobile: mobile, active: active);
//  }
//
//  MyModel.init() {
//    this.items = [OrderItem()];
//  }
//}
//
//class OrderItem {
//  Color color;
//  int price, qty, index, size1, size2;
//  String name, description;
//
//  num get finalPrice => price * qty;
//
//  String get sizeStr => 'L';
//
//  OrderItem({
//    this.color = const Color.fromRGBO(100, 38, 243, 1.0),
//    this.price = 1000,
//    this.qty = 1,
//    this.index = 5,
//    this.size1 = 35,
//    this.size2 = 35,
//    this.name = 'تی شرت',
//    this.description =
//        'طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده است می نماید ، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد',
//  });
//}
