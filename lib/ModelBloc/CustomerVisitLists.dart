import 'package:bloc/bloc.dart';
class CustomerVisitListsModel {
    int count,sts, limit,page;
    List rows;

    CustomerVisitListsModel({this.count, this.limit, this.page, this.rows, this.sts});

    factory CustomerVisitListsModel.fromJson(Map<String, dynamic> json) {
        return CustomerVisitListsModel(
            count: json['count'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: json['rows'], 
            sts: json['sts'], 
        );
    }
    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        data['sts'] = this.sts;
        return data;
    }
}

class CustomerVisitListsRepository {
    CustomerVisitListsModel customerVisitListsModel;
    CustomerVisitListsRepository({this.customerVisitListsModel}) {
        if (this.customerVisitListsModel == null) {
            this.customerVisitListsModel = new CustomerVisitListsModel();
        }
    }
    CustomerVisitListsRepository copy() {
        return CustomerVisitListsRepository(customerVisitListsModel:customerVisitListsModel);
    }
    CustomerVisitListsRepository clearDocumentRepository() {
        return CustomerVisitListsRepository(customerVisitListsModel: customerVisitListsModel);
    }
}
class CustomerVisitListsBloc extends Cubit<CustomerVisitListsRepository> {
    CustomerVisitListsBloc(CustomerVisitListsRepository state) : super(state);

    void setCustomerVisitLists(CustomerVisitListsModel user) {
        state.customerVisitListsModel = user;
        print("setCustomerVisitLists MODEL===> ${state.customerVisitListsModel}");
        refresh();
    }
    void refresh() {
        emit(state.copy());
    }
}