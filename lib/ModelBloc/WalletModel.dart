import 'package:bloc/bloc.dart';
class WalletModel {
    int amount,sts,created_at,customer_id,doctor_id,id,visit_id;
    String note;
    String transaction_id,updated_at;

    WalletModel({this.amount, this.created_at, this.customer_id, this.doctor_id, this.id, this.note, this.sts, this.transaction_id, this.updated_at, this.visit_id});

    factory WalletModel.fromJson(Map<String, dynamic> json) {
        return WalletModel(
            amount: json['amount'], 
            created_at: json['created_at'], 
            customer_id: json['customer_id'], 
            doctor_id: json['doctor_id'], 
            id: json['id'], 
            note: json['note'], 
            sts: json['sts'], 
            transaction_id: json['transaction_id'], 
            updated_at: json['updated_at'], 
            visit_id: json['visit_id'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['amount'] = this.amount;
        data['created_at'] = this.created_at;
        data['customer_id'] = this.customer_id;
        data['doctor_id'] = this.doctor_id;
        data['id'] = this.id;
        data['note'] = this.note;
        data['sts'] = this.sts;
        data['transaction_id'] = this.transaction_id;
        data['updated_at'] = this.updated_at;
        data['visit_id'] = this.visit_id;
        return data;
    }
}
class WalletRepository {
    WalletModel wallet;
    WalletRepository({this.wallet}) {
        if (this.wallet == null) {
            this.wallet= new WalletModel();
        }
    }
    WalletRepository copy() {
        return WalletRepository(wallet: wallet);
    }
    WalletRepository clearDocumentRepository() {
        return WalletRepository(wallet: wallet);
    }
}
class WalletBloc extends Cubit<WalletRepository> {
    WalletBloc(WalletRepository state) : super(state);

    setWallet(WalletModel wallet) {
        state.wallet = wallet;
        refresh();
    }
    setAmount(int amount) {
        state.wallet.amount = amount;
        refresh();
    }
    void refresh() {
        emit(state.copy());
    }
}