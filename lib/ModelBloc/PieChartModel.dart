
class PieChartModel {
    int count,type;
    String type_str;

    PieChartModel({this.count, this.type, this.type_str});

    factory PieChartModel.fromJson(Map<String, dynamic> json) {
        return PieChartModel(
            count: json['count'], 
            type: json['type'], 
            type_str: json['type_str'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['type'] = this.type;
        data['type_str'] = this.type_str;
        return data;
    }
}