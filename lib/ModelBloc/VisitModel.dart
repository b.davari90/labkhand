import 'package:counsalter5/DoctorModel.dart';
import 'package:bloc/bloc.dart';
import 'package:counsalter5/Collections.dart';
class VisitModel {
    int id,collection_id,price_doctor,discount_amount,type,sts,price,customer_id,fin_doc_id,present_id,doctor_id,owner_id,folder_id,discount_id;
    bool private;
    String created_at,updated_at,sts_str,date_at,description,dtj;
    DoctorModel doctor;
    Collections collection;
    String price_with_discount;

    VisitModel({this.private, this.collection_id,
        this.created_at, this.customer_id,this.collection,
        this.date_at, this.description,
        this.discount_amount, this.discount_id,
        this.doctor, this.doctor_id, this.dtj,
        this.fin_doc_id, this.folder_id, this.id,
        this.owner_id, this.present_id, this.price, this.price_doctor,
        this.price_with_discount, this.sts,
        this.sts_str, this.type, this.updated_at});

    factory VisitModel.fromJson(Map<String, dynamic> json) {
        DoctorModel doctor;
        if (json['doctor'] != null && json.containsKey('doctor')) {
            doctor = DoctorModel.fromJson(json['doctor']);
        }
        Collections collection;
        if (json['collection'] != null && json.containsKey('collection')) {
            collection = Collections.fromJson(json['collection']);
        }
        return VisitModel(
            private: json['private'],
            collection_id: json['collection_id'],
            created_at: json['created_at'],
            customer_id: json['customer_id'],
            date_at: json['date_at'],
            description: json['description'],
            discount_amount: json['discount_amount'],
            discount_id: json['discount_id'],
            doctor: doctor,
            collection: collection,
            doctor_id: json['doctor_id'],
            dtj: json['dtj'],
            fin_doc_id: json['fin_doc_id'], 
            folder_id: json['folder_id'], 
            id: json['id'], 
            owner_id: json['owner_id'], 
            present_id: json['present_id'], 
            price: json['price'], 
            price_doctor: json['price_doctor'], 
            price_with_discount: json['price_with_discount'], 
            sts: json['sts'], 
            sts_str: json['sts_str'], 
            type: json['type'], 
            updated_at: json['updated_at'], 
        );
    }
    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['private'] = this.private;
        data['collection_id'] = this.collection_id;
        data['created_at'] = this.created_at;
        data['customer_id'] = this.customer_id;
        data['date_at'] = this.date_at;
        data['description'] = this.description;
        data['discount_amount'] = this.discount_amount;
        data['discount_id'] = this.discount_id;
        data['doctor'] = this.doctor;
        data['collection'] = this.collection;
        data['doctor_id'] = this.doctor_id;
        data['dtj'] = this.dtj;
        data['fin_doc_id'] = this.fin_doc_id;
        data['folder_id'] = this.folder_id;
        data['id'] = this.id;
        data['owner_id'] = this.owner_id;
        data['present_id'] = this.present_id;
        data['price'] = this.price;
        data['price_doctor'] = this.price_doctor;
        data['price_with_discount'] = this.price_with_discount;
        data['sts'] = this.sts;
        data['sts_str'] = this.sts_str;
        data['type'] = this.type;
        data['updated_at'] = this.updated_at;
        return data;
    }
}
class VisitModelRepository {
    VisitModel visitModel;
    List<VisitModel> doctorVisitList = [];
    VisitModelRepository(
        {this.visitModel, this.doctorVisitList}) {
        if (this.visitModel == null) {
            this.visitModel = new VisitModel();
        }
    }
    VisitModelRepository copy() {
        return VisitModelRepository(
            doctorVisitList: doctorVisitList,visitModel: visitModel);
    }
    VisitModelRepository clearVisitModelRepository() {
        return VisitModelRepository(
            visitModel: visitModel,doctorVisitList: doctorVisitList);
    }
}
class VisitModelBloc extends Cubit<VisitModelRepository> {
    VisitModelBloc(VisitModelRepository state) : super(state);
    void setVisitModel(VisitModel visitModel) {
        state.visitModel = visitModel;
        refresh();
    }
    void refresh() {
        emit(state.copy());
    }
}
