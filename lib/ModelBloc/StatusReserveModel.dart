
class StatusReserveModel {
    String caj, created_at,title,updated_at,payload,sts_str;
    int customer_id, user_id,doctor_id, type,sts,id;

    StatusReserveModel({this.caj, this.created_at, this.customer_id, this.doctor_id, this.id, this.payload, this.sts, this.sts_str, this.title, this.type, this.updated_at, this.user_id});

    factory StatusReserveModel.fromJson(Map<String, dynamic> json) {
        return StatusReserveModel(
            caj: json['caj'], 
            created_at: json['created_at'], 
            customer_id: json['customer_id'], 
            doctor_id: json['doctor_id'], 
            id: json['id'], 
            payload: json['payload'],
            sts: json['sts'], 
            sts_str: json['sts_str'], 
            title: json['title'], 
            type: json['type'], 
            updated_at: json['updated_at'], 
            user_id: json['user_id'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['caj'] = this.caj;
        data['created_at'] = this.created_at;
        data['customer_id'] = this.customer_id;
        data['doctor_id'] = this.doctor_id;
        data['id'] = this.id;
        data['payload'] = this.payload;
        data['sts'] = this.sts;
        data['sts_str'] = this.sts_str;
        data['title'] = this.title;
        data['type'] = this.type;
        data['updated_at'] = this.updated_at;
        data['user_id'] = this.user_id;
        return data;
    }
}