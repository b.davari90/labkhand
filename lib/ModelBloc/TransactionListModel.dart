import 'package:counsalter5/ModelBloc/TransactionModel.dart';
class TransactionListModel {
    int count,limit,page;
    List<TransactionModel> rows;

    TransactionListModel({this.count, this.limit, this.page, this.rows});

    factory TransactionListModel.fromJson(Map<String, dynamic> json) {
        List<TransactionModel> rows =[];
        if (json.containsKey('rows') && json['rows'] != null ) {
            List dList = json['rows'];
            rows = dList.map((e) => TransactionModel.fromJson(e)).toList();
        }
        return TransactionListModel(
            count: json['count'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: rows,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}