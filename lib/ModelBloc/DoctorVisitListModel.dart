import 'package:counsalter5/ModelBloc/VisitModel.dart';
class DoctorVisitListModel {
    int count,limit, page;
    List<VisitModel> rows;
    int sts;

    DoctorVisitListModel({this.count, this.limit, this.page, this.rows, this.sts});

    factory DoctorVisitListModel.fromJson(Map<String, dynamic> json) {
        List<VisitModel> row =[];
        if (json.containsKey('rows') && json['rows'] != null ) {
            List dList = json['rows'];
            row = dList.map((e) => VisitModel.fromJson(e)).toList();
        }

        return DoctorVisitListModel(
            count: json['count'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: row,
            sts: json['sts'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        data['sts'] = this.sts;
        return data;
    }
}