import 'DoctorFolderModel.dart';
class DoctorFolderListModel {
    int count, limit, page;
    List<DoctorFolderModel> rows;

    DoctorFolderListModel({this.count, this.limit, this.page, this.rows});

    factory DoctorFolderListModel.fromJson(Map<String, dynamic> json) {
        List<DoctorFolderModel> rows =[];
        if (json.containsKey('rows') && json['rows'] != null ) {
            List dList = json['rows'];
            rows = dList.map((e) => DoctorFolderModel.fromJson(e)).toList();
        }
        return DoctorFolderListModel(
            count: json['count'], 
            limit: json['limit'], 
            page: json['page'], 
            rows: rows,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['count'] = this.count;
        data['limit'] = this.limit;
        data['page'] = this.page;
        data['rows'] = this.rows;
        return data;
    }
}