import 'package:bloc/bloc.dart';
import 'package:counsalter5/ModelBloc/DocumentModel.dart';
import 'package:counsalter5/ModelBloc/CustomerChilderenModel.dart';

class CustomerModel {
  String email,
      fname,
      lname,
      name,
      last_seen_at,
      mobile,
      password,
      avatar,
      married_str,
      gender_str,
      sts_str,
      token_sms,
      token_mobile;
  int id, gender, married, wallet, sts,parent_id;
  bool active;
  List<CustomerChilderenModel>childeren;
//  Map<int, String> gender = {1: "مرد", 2: "زن"};
//  Map<int, String> married = {1: "مجرد", 2: "متاهل", 3: "نامشخص"};

  DocumentModel document;

  CustomerModel(
      {this.email,
      this.fname,
      this.lname,
        this.childeren,
      this.name,
      this.last_seen_at,
      this.mobile,
        this.parent_id,
      this.password,
      this.avatar,
      this.married_str,
      this.gender_str,
      this.sts_str,
      this.token_sms,
      this.token_mobile,
      this.id,
      this.gender,
      this.married,
      this.wallet,
      this.sts,
      this.active,
      this.document});

  CustomerModel copy() {
    return new CustomerModel(
        name: name,
        gender_str: gender_str,
        married_str: married_str,
        sts: sts,
        parent_id: parent_id,
        childeren: childeren,
        sts_str: sts_str,
        token_mobile: token_mobile,
        token_sms: token_sms,
        document: document,
        wallet: wallet,
        email: email,
        mobile: mobile,
        password: password,
        active: active,
        id: id,
        gender: gender,
        avatar: avatar,
        fname: fname,
        last_seen_at: last_seen_at,
        lname: lname,
        married: married);
  }

  factory CustomerModel.fromJson(Map<String, dynamic> json) {
    try {
      DocumentModel document;
      if (json['document'] != null && json.containsKey('document')) {
        document = DocumentModel.fromJson(json['document']);
      }
      List<CustomerChilderenModel> childerenList ;
      if(childerenList==null){childerenList=[];}
      if (json['children'] != null &&json.containsKey('children')) {
        var childList = json['children'] as List;
        childerenList = childList.map((f) => CustomerChilderenModel.fromJson(f)).toList();
      }
      return CustomerModel(
        name: json['name'] ?? null,
        mobile: json['mobile'] ?? null,
        email: json['email'] ?? null,
        parent_id: json['parent_id'] ?? null,
//        childeren: json['children'] ?? null,
        document: document ?? null,
        childeren: childerenList ?? [],
        password: json['password'] ?? null,
        wallet: json['wallet'] ?? null,
        token_sms: json['token_sms'] ?? null,
        token_mobile: json['token_mobile'] ?? null,
        sts: json['sts'] ?? null,
        gender_str: json['gender_str'] ?? null,
        sts_str: json['sts_str'] ?? null,
        married_str: json['married_str'] ?? null,
        active: json['active'] ?? null,
        id: json['id'] ?? null,
        gender: json['gender'] ?? null,
        avatar: json['avatar'] ?? null,
        fname: json['fname'] ?? null,
        last_seen_at: json['last_seen_at'] ?? null,
        lname: json['lname'] ?? null,
        married: json['married'] ?? null,
      );
    } catch (e) {
      throw 'user model not valid model=> ${e}';
    }
  }

  Map<String, dynamic> toJson(String provider) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mobile'] = this.mobile;
    data['name'] = this.name;
    data['gender_str'] = this.gender_str;
    data['married_str'] = this.married_str;
    data['wallet'] = this.wallet;
    data['sts'] = this.sts;
    data['parent_id'] = this.parent_id;
    data['sts_str'] = this.sts_str;
    data['children'] = this.childeren;
    data['token_sms'] = this.token_sms;
    data['token_mobile'] = this.token_mobile;
    data['email'] = this.email;
    data['password'] = this.password;
    data['document'] = this.document;
    data['active'] = this.active;
    data['id'] = this.id;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['fname'] = this.fname;
    data['last_seen_at'] = this.last_seen_at;
    data['lname'] = this.lname;
    data['married'] = this.married;
    data["provider"] = provider;
    return data;
  }
}

class CoustomerRepository {
  CustomerModel user;
  CoustomerRepository({this.user}) {
    if (this.user == null) {
      this.user = new CustomerModel();
    }
    if (this.user.childeren == null) {
      this.user.childeren =[];
    }
  }
  CoustomerRepository copy() {
    return CoustomerRepository(user: user);
  }
  CoustomerRepository clearDocumentRepository() {
    return CoustomerRepository(user: user);
  }
}
class CoustomerBloc extends Cubit<CoustomerRepository> {
  CoustomerBloc(CoustomerRepository state) : super(state);

void setUser(CustomerModel user) {
  state.user = user;
  print("state.doctor customer MODEL===> ${state.user}");
  refresh();
}
void refresh() {
  emit(state.copy());
}
}
