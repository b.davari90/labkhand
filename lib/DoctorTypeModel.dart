import 'package:flutter_bloc/flutter_bloc.dart';

class DoctorTypeModel {
    int doctor_id, type,id;
    String type_str;
    DoctorTypeModel({this.type, this.doctor_id,this.type_str,this.id});

    factory DoctorTypeModel.fromJson(Map<String, dynamic> json) {
//    factory DoctorTypeModel.fromJson(Map<String, dynamic> json) {
        try{ return DoctorTypeModel(
            type: json['type'],
            id: json['id'],
            doctor_id: json['doctor_id'],
            type_str: json['type_str'],
        );}catch(e){throw "@@@@ $e";}
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['id'] = this.id;
        data['type_str'] = this.type_str;
        data['type'] = this.type;
        data['doctor_id'] = this.doctor_id;
        return data;

    }
    DoctorTypeModel copy(){
        return DoctorTypeModel(id: id,doctor_id:doctor_id,type_str: type_str);
    }
}
class DoctorTypeRepository {

    DoctorTypeModel doctorTypeModel;

    DoctorTypeRepository({this.doctorTypeModel});

    DoctorTypeRepository copy() {
        return DoctorTypeRepository(doctorTypeModel: doctorTypeModel);
    }
}

class DoctorTypeCubit extends Cubit<DoctorTypeRepository> {
    DoctorTypeCubit(DoctorTypeRepository state) : super(state);
    void refresh() {
        emit(state.copy());
    }
    void setType(String id) {
        state.doctorTypeModel.type_str =id;
        refresh();
    }void setId(int id) {
        state.doctorTypeModel.id =id;
        refresh();
    }
    void setDoctor_id(int id) {
        state.doctorTypeModel.doctor_id =id;
        refresh();
    }
    void setTitle(int title) {
        state.doctorTypeModel.type =title;
        refresh();
    }
}