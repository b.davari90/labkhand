//import 'dart:ffi';
//import 'package:salare_jade/model/Comment.dart';
//import 'package:salare_jade/model/adv_attribute.dart';
//import 'package:salare_jade/model/adv_image.dart';
//import 'package:salare_jade/model/adv_ip.dart';
//import 'package:salare_jade/model/attribute.dart';
//import 'package:salare_jade/model/cnt.dart';
//import 'package:salare_jade/model/collection.dart';
//import 'package:salare_jade/model/file.dart';
//import 'package:salare_jade/model/price_rule.dart';
//import 'package:salare_jade/model/store.dart';
//import 'package:salare_jade/model/transaction.dart';
//import 'package:salare_jade/model/user.dart';
//import 'package:intl/intl.dart';
//import 'package:salare_jade/str.dart';
//import '../config.dart';
//import 'ip.dart';
//import 'ipc.dart';
//
//class Adv {
//  List<PriceRule> priceList;
//  List<int> allPinAdvId, imageIds;
//  List<String> phones;
//  Transaction transaction;
//  Ipc ipc;
//  int typePrice,
//      id,
//      storeId,
//      expiredAtCollection,
//      collectionId,
//      ownerId,
//      app,
//      acceptorId,
//      version,
//      marketerAccess,
//      marketerId,
//      w,
//      cityId,
//      level,
//      type,
//      sts,
//      totalImg;
//  String title,
//      body,
//      browserIp,
//      provinceName,
//      cityName,
//      note,
//      token,
//      phone,
//      email,
//      thumb,
//      thumbTiny,
//      map,
//      typeStr,
//      rejectReason,
//      stsStr,
//      lastViewAt,
//      createdAt,
//      visionAt,
//      expiredAt,
//      updatedAt;
//  String tags, typePriceStr,website;
//  num price, ipPrice;
//  List<AdvAttribute> attributes, attributesSwap, attr;
//  Collection collection;
//  List<Collection> subCollection;
//  AdvIp advIp;
//  Float tax;
//  User acceptor;
//  List<AdvImage> images;
//  List<Comment> comment;
//  List<FileServer> imageFile;
//  List<Ip> ipList;
//  List<Ip> webIpList;
//  List<String> dateList;
//  User marketer;
//  bool lock, expiredLabel, activeChat, forOther, isSwap, forStore, linkSite;
//  Store store;
//  Map attributesValues, exchangeAttributesValues;
//
//  Adv(
//      {this.id,
//        this.webIpList,
//        this.dateList,
//        this.priceList,
//        this.website,
//        this.transaction,
//      this.typePrice,
//      this.typePriceStr,
//      this.comment,
//      this.imageFile,
//      this.ipPrice,
//      this.ipc,
//      this.linkSite,
//      this.ipList,
//      this.phones,
//      this.storeId,
//      this.attributesSwap,
//      this.expiredAtCollection,
//      this.exchangeAttributesValues,
//      this.attributesValues,
//      this.provinceName,
//      this.cityName,
//      this.subCollection,
//      this.forOther,
//      this.forStore,
//      this.isSwap,
//      this.activeChat,
//      this.imageIds,
//      this.allPinAdvId,
//      this.advIp,
//      this.expiredLabel,
//      this.totalImg,
//      this.collectionId,
//      this.ownerId,
//      this.app,
//      this.acceptorId,
//      this.version,
//      this.price,
//      this.marketerAccess,
//      this.marketerId,
//      this.w,
//      this.cityId,
//      this.level,
//      this.type,
//      this.sts,
//      this.title,
//      this.body,
//      this.browserIp,
//      this.note,
//      this.token,
//      this.phone,
//      this.email,
//      this.thumb,
//      this.thumbTiny,
//      this.tags,
//      this.map,
//      this.typeStr,
//      this.rejectReason,
//      this.stsStr,
//      this.attributes,
//      this.collection,
//      this.tax,
//      this.acceptor,
//      this.images,
//      this.marketer,
//      this.lastViewAt,
//      this.visionAt,
//      this.createdAt,
//      this.expiredAt,
//      this.updatedAt,
//      this.lock,
//      this.store,
//      this.attr});
//
//  factory Adv.fromJson(Map<String, dynamic> json) {
//    try {
//
//      Transaction transaction;
//      if (json['transaction'] != null) {
//        transaction = Transaction.fromJson(json['transaction']);
//      } else {
//        transaction = null;
//      }
//
//      Collection collection;
//      if (json['collection'] != null && json.containsKey('collection')) {
//        collection = Collection.fromJson(json['collection']);
//      }
//
//      AdvIp advIp;
//      if (json['adv_ip'] != null && json.containsKey('adv_ip')) {
//        advIp = AdvIp.fromJson(json['adv_ip']);
//      }
//
//      Store store;
//      if (json.containsKey('store') && json['store'] != null) {
//        store = Store.fromJson(json['store']);
//      }
//
//      User marketer, acceptor;
//      if (json['maraketer'] != null && json.containsKey('maraketer')) {
//        marketer = User.fromJson(json['maraketer']);
//      }
//
//      if (json['acceptor'] != null && json.containsKey('acceptor')) {
//        acceptor = User.fromJson(json['acceptor']);
//      }
//
//      List<AdvImage> images = [];
//      if (json.containsKey('images')) {
//        var listImg = json['images'] as List;
//        images = listImg.map((f) => AdvImage.fromJson(f)).toList();
//      } else {
//        images = [];
//      }
//
//      List<Comment> comment = [];
//      if (json.containsKey('comment')) {
//        var listComment = json['comment'] as List;
//        comment = listComment.map((f) => Comment.fromJson(f)).toList();
//      } else {
//        comment = [];
//      }
//
//      List<AdvAttribute> attributes = [];
//      if (json.containsKey('attributes')) {
//        var listAttributes = json['attributes'] as List;
//        attributes = listAttributes.map((f) => AdvAttribute.fromJson(f)).toList();
//      } else {
//        attributes = [];
//      }
//
//      List<AdvAttribute> attributesSwap = [];
//      if (json.containsKey('attributes_swap')) {
//        var listAttributes = json['attributes_swap'] as List;
//        attributesSwap = listAttributes.map((f) => AdvAttribute.fromJson(f)).toList();
//      } else {
//        attributesSwap = [];
//      }
//
////       attr = [];
////      if(json.containsKey('attr')){
////        var listAttr = json['attr'] as List;
////        attr = listAttr.map((f)=>AdvAttribute.fromJson(f)).toList();
////      }else{
////        attr = [];
////      }
//
//      return Adv(
//          id: json['id'] ?? null,
//          website: json['website'] ?? null,
//          transaction: transaction ?? null,
//          advIp: advIp ?? null,
//          isSwap: json['is_swap'] ?? null,
//          typePrice: json['type_price'] ?? null,
//          typePriceStr: json['type_price_str'] ?? null,
////          forOther: json['for_other'] ?? null,
//          activeChat: json['active_chat'] ?? null,
//          expiredLabel: json['expired_lable'] ?? false,
//          totalImg: json['total_img'] ?? null,
//          title: json['title'] ?? null,
//          attributes: attributes ?? null,
//          comment: comment,
//          attributesSwap: attributesSwap ?? null,
////          attr: attr ?? null,
//          body: json['body'] ?? null,
//          collectionId: json['collection_id'] ?? null,
//          collection: collection ?? null,
////          tax: json['tax'] ?? null,
//          ownerId: json['owner_id'] ?? null,
//          browserIp: json['browser_ip'] ?? null,
//          app: json['app'] ?? null,
//          note: json['note'] ?? null,
//          token: json['token'] ?? null,
//          phone: json['phone'] ?? null,
//          email: json['email'] ?? null,
//          cityName: json['city_name'] ?? null,
//          provinceName: json['province_name'] ?? null,
//          acceptorId: json['acceptor_id'] ?? null,
//          acceptor: acceptor ?? null,
//          version: json['version'] ?? null,
//          thumb: json['thumb'] ?? null,
//          thumbTiny: json['thumb_tiny'] ?? null,
//          images: images ?? null,
//          price: json['price'] ?? null,
//          marketerAccess: json['marketer_access'] ?? null,
//          marketerId: json['maraketer_id'] ?? null,
//          marketer: marketer ?? null,
//          tags: json['tags'] ?? null,
//          w: json['w'] ?? null,
//          cityId: json['city_id'] ?? null,
//          map: json['map'] ?? null,
//          level: json['level'] ?? null,
//          type: json['type'] ?? null,
//          typeStr: json['type_str'] ?? null,
//          lastViewAt: json['last_view_at'] ?? null,
//          lock: json['lock'] ?? null,
//          rejectReason: json['reject_reason'] ?? null,
//          storeId: json['store_id'] ?? null,
//          store: store ?? null,
//          visionAt: json['vision_at'] ?? null,
//          sts: json['sts'] ?? null,
//          stsStr: json['sts_str'] ?? null,
//          createdAt: json['created_at'] ?? null,
//          expiredAt: json['expired_at'] ?? null,
//          updatedAt: json['updated_at'] ?? null);
//    } catch (e) {
//      throw 'Adv not valid model=> ${e}';
//    }
//  }
//
//  setFieldValue(key, value) {
//    try {
//      if (this.attributesValues is! Map) {
//        this.attributesValues = Map();
//      }
//      this.attributesValues[key] = value;
//      print('advAttributesValues ===> ${attributesValues}');
//    } catch (e) {}
//  }
//
//  setExchangeFieldValue(key, value) {
//    try {
//      if (this.exchangeAttributesValues is! Map) {
//        this.exchangeAttributesValues = Map();
//      }
//      this.exchangeAttributesValues[key] = value;
//      print('advAttributesValuesExchange ===> ${exchangeAttributesValues}');
//    } catch (e) {}
//  }
//
//  Map<String, dynamic> toMap() {
//    try {
//      return {
//        "title": title ?? null,
//        "attributes": attributesValues ?? null,
//        "website": website ?? null,
//        "attributes_swap": exchangeAttributesValues ?? null,
////      "attr": attr.map((AdvAttribute advAttr) => advAttr.toMap()).toList() ?? null,
//        "body": body ?? null,
//        "collection_id": collectionId,
////      "collection": collection.toMap() ?? null,
//        "type_price": typePrice,
//        "app": app ?? null,
//        "note": note ?? null,
//        "phone": phone ?? null,
//        "email": email ?? null,
//        "thumb": thumb ?? null,
//        "thumb_tiny": thumbTiny ?? null,
//        "images": imageIds ?? null,
//        "price": price ?? null,
//        "tags": tags ?? null,
//        "w": w,
//        "city_id": cityId ?? null,
//        "map": map ?? null,
//        "level": level ?? null,
//        "store_id": storeId ?? null,
//        "expired_at": int.parse(expiredAt),
//      };
//    } catch (e) {
//      print('e ===> ${e}');
//    }
//  }
//
//  static copyFromBloc(Adv adv) {
//    return Adv(allPinAdvId: adv.allPinAdvId);
//  }
//
//  static copyFrom(Adv adv) {
//    return Adv(
//        id: adv.id,
//        dateList: adv.dateList,
//        priceList: adv.priceList,
//        advIp: adv.advIp,
//        website: adv.website,
//        typePrice: adv.typePrice,
//        activeChat: adv.activeChat ?? false,
//        imageFile: adv.imageFile ?? [],
//        ipc: adv.ipc,
//        ipPrice: adv.ipPrice,
//        title: adv.title,
//        ipList: adv.ipList,
//        webIpList: adv.webIpList,
//        phones: adv.phones,
//        linkSite: adv.linkSite,
//        expiredAt: adv.expiredAt,
//        exchangeAttributesValues: adv.exchangeAttributesValues ?? {},
//        isSwap: adv.isSwap ?? false,
//        forOther: adv.forOther ?? false,
//        attributesSwap: adv.attributesSwap ?? [],
//        forStore: adv.forStore,
//        provinceName: adv.provinceName,
//        cityName: adv.cityName,
//        subCollection: adv.subCollection,
//        collection: adv.collection,
//        totalImg: adv.totalImg,
//        attributes: adv.attributes ?? [],
//        body: adv.body,
//        price: adv.price,
//        note: adv.note,
//        phone: adv.phone,
//        email: adv.email,
//        thumb: adv.thumb,
//        images: adv.images,
//        imageIds: adv.imageIds,
//        tags: adv.tags,
//        cityId: adv.cityId,
//        map: adv.map,
//        type: adv.type,
//        storeId: adv.storeId,
//        attributesValues: adv.attributesValues);
//  }
//
//  static clear(Adv adv) {
//    return Adv(
//      priceList: null,
//        dateList: null,
//        id: adv?.id ?? null,
//        advIp: null,
//        ipList: null,
//        webIpList: null,
//        website: null,
//        typePrice: null,
//        title: null,
//        totalImg: null,
//        attributes: [],
//        phones: [],
//        body: null,
//        collectionId: null,
//        note: null,
//        phone: null,
//        email: null,
//        thumb: null,
//        images: [],
//        tags: null,
//        cityId: null,
//        map: null,
//        type: null,
//        storeId: null,
//        attributesValues: {},
//        exchangeAttributesValues: {},
//        provinceName: null,
//        cityName: null,
//        price: null,
//        collection: Collection(),
//        activeChat: null,
//        attributesSwap: [],
//        isSwap: null);
//  }
//
//  Iterable<AdvAttribute> get atr => attributes == null ? [] : attributes?.where((test) => test.attribute.hasInView == true);
//
//  int get numberImage => images?.length ?? 0;
//
//  AdvAttribute get atr1 => atr.length > 0 ? atr?.first : null;
//
//  AdvAttribute get atr2 => atr.length > 1 ? atr?.elementAt(1) : null;
//
//  AdvAttribute get atr3 => atr.length > 2 ? atr?.elementAt(2) : null;
//
//  String get priceFormat => NumberFormat.currency(symbol: '', decimalDigits: 0).format(price ?? 0);
//
//  List<String> get titlesOfGroup => attributes.map((item) => item.attribute.group.title).toSet().toList();
//
//  List<Cnt> get groups => body == null || body.isEmpty
//      ? attributes.map((item) => item.attribute.group).toSet().toList()
//      : attributes.map((item) => item.attribute.group).toSet().toList()
//    ..insert(0, Cnt(title: Str.ADV_DETAIL_PAGE_DESCRIPTION));
//}
