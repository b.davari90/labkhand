import 'package:counsalter5/config.dart';
import 'package:flutter/material.dart';

class MasBackBotton extends StatelessWidget {
  Color color,colorBackground;
  IconData icon;
  Function onTap;
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: () {
//        onTap();
        Navigator.pop(context);
      },
      child: Container(
        width: Conf.p2w(10),
        height: Conf.p2w(10),
//        width: Conf.p2w(8.3),
//        height: Conf.p2h(4.5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: colorBackground ?? Conf.orang),
        child: Icon(icon??
          Icons.navigate_next,
          size: Conf.p2w(5.417), //26,
          color:color?? Colors.white,
        ),
      ),
    );
  }

  MasBackBotton({this.onTap,this.color, this.icon,this.colorBackground});
}
