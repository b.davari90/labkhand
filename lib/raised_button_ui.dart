import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'config.dart';

class MasRaisedButton extends StatelessWidget {

  Function onPress;
  String text;
  Color color,textColor,borderSideColor;
  EdgeInsets margin;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.maxFinite,
      margin: margin ?? Conf.xlEdge,
      child: RaisedButton(
        color: color??Conf.orang,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(13.0),
            side: BorderSide(
              color: borderSideColor??Conf.orang,
            )),
        onPressed: () {
        onPress();
        },
        child: FittedBox(
          fit: BoxFit.cover,
          child: Text(
            text??'',
            softWrap: false,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: Conf.p2t(19),
                color: textColor,
                fontWeight: FontWeight.w400),
          ),
        ),
      ),
    );
  }

  MasRaisedButton({this.onPress, this.text, this.color, this.textColor,this.borderSideColor,this.margin});
}
