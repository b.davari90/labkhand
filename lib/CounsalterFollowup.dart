import 'package:counsalter5/CounsalterReserveDateRowComponent.dart';
import 'package:counsalter5/CounsalterReserveDateRowModel.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/ModelBloc/CounsalterFollowModel.dart';
import 'config.dart';
import 'package:flutter/cupertino.dart';
import 'package:counsalter5/ModelBloc/CounsalterFollowListModel.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/CounsalterFollowUpProgramComponent.dart';
import 'package:counsalter5/config.dart';
import 'package:persian_date/persian_date.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params_result.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
import 'package:shamsi_date/shamsi_date.dart';
class CounsalterFollowUp extends StatefulWidget {
  @override
  _CounsalterFollowUpState createState() => _CounsalterFollowUpState();
}

class _CounsalterFollowUpState extends State<CounsalterFollowUp> {
  TextStyle titleStyle = TextStyle(
      fontSize: Conf.p2t(27),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w500,
      color: Colors.black);
  int indexSelectdate;
  String monthChoosen,
      dateChoosen,
      dayChoosen,
      yearChoosen = "1400";
  String final_Date_Shamsi, date_Shamsi, final_Date_Shamsi_Month;
  List selectMonth = [];
  List daysList = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ];

  List firstSixMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ];
  List secondSixMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30
  ];
  List esfandMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29
  ];
  List doctorDateList = [12, 13, 14, 15, 16, 17, 18, 19];
  int indexMonth = 0;
  String monthName = "",finalDate="";
  bool loading;
  CounsalterFollowListModel counsalterFollowListModel;
  List<CounsalterFollowModel> counsalterFollowLst;
  int count,pages,limit;
  SearchAdvCubit get searchBloc => context.bloc<SearchAdvCubit>();
  ScrollController sc;
  QueryListParams qlp;
  QueryListParamsResult listResult;
  List<String> monthList = [
    "فروردین",
    "اردیبهشت",
    "خرداد",
    "تیر",
    "مرداد",
    "شهریور",
    "مهر",
    "آبان",
    "اذر",
    "دی",
    "بهمن",
    "اسفند"
  ];
  String changeDayFormate(String str) {
    str = "0" + str;
    return str;
  }
  int convertStringMonthTOInteger(String m) {
    switch (m) {
      case "فروردین":
        return 1;
        break;
      case "اردیبهشت":
        return 2;
        break;
      case "خرداد":
        return 3;
        break;
      case "تیر":
        return 4;
        break;
      case "مرداد":
        return 5;
        break;
      case "شهریور":
        return 6;
        break;
      case "مهر":
        return 7;
        break;
      case "آبان":
        return 8;
        break;
      case "آذر":
        return 9;
        break;
      case "دی":
        return 10;
        break;
      case "بهمن":
        return 11;
        break;
      case "اسفند":
        return 12;
        break;
    }
  }
  List dayShow() {
    if (indexMonth == 0 ||
        indexMonth == 1 ||
        indexMonth == 2 ||
        indexMonth == 3 ||
        indexMonth == 4 ||
        indexMonth == 5) {
      return firstSixMonth;
    }
    if (indexMonth == 6 ||
        indexMonth == 7 ||
        indexMonth == 8 ||
        indexMonth == 9 ||
        indexMonth == 10) {
      return secondSixMonth;
    } else {
      return esfandMonth;
    }
  }

  Future getAllFollowUp() async {
    dateChoosen = yearChoosen + convertStringMonthTOInteger(monthName).toString() + changeDayFormate(dayChoosen);
    print("__dateChoosen ===> ${dateChoosen}");
    DateTime dt = new DateTime(int.parse(yearChoosen), int.parse(convertStringMonthTOInteger(monthName).toString()), int.parse(changeDayFormate(dayChoosen)));
    PersianDate persianDate = PersianDate(gregorian: dt.toString());
    finalDate = persianDate.jalaliToGregorian(dt.toString()).toString();
    finalDate = finalDate.split(" ").first;
    print("finalDate.split(" ").first ===> ${finalDate.split(" ").first}");
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.create("cu/follow",{"date_at":finalDate});
      counsalterFollowListModel= CounsalterFollowListModel.fromJson(data);
      print("counsalterFollowListModel.rows ===> ${ counsalterFollowListModel.rows }");
      count = counsalterFollowListModel.count;
      limit = counsalterFollowListModel.limit;
      pages = counsalterFollowListModel.page;
      counsalterFollowLst=[];
      counsalterFollowLst.addAll(counsalterFollowListModel.rows);

      print("counsalterFollowLst*** ===> ${ counsalterFollowLst }");
    } catch (e) {
      print("CatchError getAllFollowUp()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }
  @override
  void initState() {
    Gregorian g = Gregorian(Gregorian.now().year, Gregorian.now().month, Gregorian.now().day);
    Jalali jj=g.toJalali();
    print("jj ===> ${ jj }");
    int m=jj.month;
    print("monthName initState===> ${ m }");
    print("Conf.token ===> ${ Conf.token }");
    print("Conf.idLogin ===> ${ Conf.idLogin }");
    print("Conf.idLoginDoctor ===> ${ Conf.idLoginDoctor }");
    monthName = monthList[m-1];
    selectMonth = firstSixMonth;

    counsalterFollowListModel=CounsalterFollowListModel();
    counsalterFollowLst=[];
    qlp = QueryListParams();
    listResult = QueryListParamsResult();
    sc = ScrollController();
    qlp.page = 0;
    searchBloc.setQlp(qlp);
    sc.addListener(() {
      if (sc.offset >= sc.position.maxScrollExtent - 100 &&
          !sc.position.outOfRange &&
          qlp.page < pages) {
        setState(() {
          qlp.page++;
          searchBloc.setQlp(qlp);
          getAllFollowUp();
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      SafeArea(
        child: Scaffold(
            body: Container(
//              color: Colors.brown,
      child: Stack(children: [
        Positioned(
          width: Conf.p2w(100),
          top: Conf.p2h(4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                  "پیگیری",
                  style: Conf.headline.copyWith(
                      color: Colors.black87,
                      fontSize: Conf.p2t(25),
                      fontWeight: FontWeight.w500,
                      decoration: TextDecoration.none)
              ),
            ],),
        ),
        Positioned(
          top: Conf.p2h(4),
          left: Conf.p2w(6),
          child: Column(
            children: [
              GestureDetector(
              onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: Conf.p2w(11),
                  height: Conf.p2w(11),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Conf.orang),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.navigate_next,
                      size: Conf.p2t(23),
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],),
        ),
        Positioned(
          right: Conf.p2w(0),
          left: Conf.p2w(0),
          bottom: Conf.p2w(0),
          child: Container(
            height: Conf.p2h(82),
            decoration: BoxDecoration(
                color: Colors.grey.shade200,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 8,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(Conf.p2w(6.25)),
                    topLeft: Radius.circular(Conf.p2w(6.25))
                    )),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        icon: Icon(Icons.navigate_before),
                        onPressed: () {
                          setState(() {
                            indexMonth--;
                            print(
                                "before indexMonth ===> ${indexMonth}");
                            if (indexMonth == -1) {
                              indexMonth = 11;
                            }
                            monthName =
                                monthList.elementAt(indexMonth);
                          });
                        }),
                    Text(
                      monthName,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: Conf.p2t(24)),
                    ),
                    IconButton(
                        icon: Icon(Icons.navigate_next),
                        onPressed: () {
                          setState(() {
                            indexMonth++;
                            print("after indexMonth ===> ${indexMonth}");
                            if (indexMonth == 12) {
                              indexMonth = 0;
                            }
                            monthName =
                                monthList.elementAt(indexMonth);
                          });
                        })
                  ],
                ),
                Flex(
                  direction: Axis.vertical,
                  children: [
                    SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Padding(
                          padding: EdgeInsets.only(top: Conf.p2h(2)),
                          child: Row(
                              children: dayShow()
                                  .map((e) =>
                                  CounsalterReserveDateRowComponent(
                                      model:
                                      CounsalterReserveDateRowModel(
                                          numberDate:e.toString()),
                                      isSelectedRow: dayShow()
                                          .indexOf(e) ==
                                          indexSelectdate
                                          ? true
                                          : false,
                                      onTap: () {
                                        setState(() {
                                          dayChoosen = e.toString();
                                          print("dayChoosen ===> ${dayChoosen}");
                                          indexSelectdate = dayShow().indexOf(e);
                                          dayChoosen.length == 1?changeDayFormate(dayChoosen) : dayChoosen.toString();
                                          getAllFollowUp();
                                        });
                                      }))
                                  .toList()),
                        )),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: Conf.p2h(2)),
                  child: VerticalDivider(),
                ),
                Container(
                  width: Conf.p2w(90),
                  height: Conf.p2h(0.25),
                  color: Colors.grey.withOpacity(0.3),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    controller: sc,
                      scrollDirection: Axis.vertical,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: Conf.xlSize),
                      child: Container(
                        color: Colors.grey[200],
                        padding: EdgeInsets.all(20.0),
                        child:  counsalterFollowLst ==[]?
                      Container(child: Center(child: Text("اطلاعاتی موجود نیست"),),):
//                       Container(child: Text("dss"),)
                        Column(
                          children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children:counsalterFollowLst.map((e) {
                              return  CounsalterFollowUpProgramComponent(
                                    model: e,
                                    index: counsalterFollowLst.indexOf(e)==0?0:
                                    counsalterFollowLst.indexOf(e)==1?1:
                                    counsalterFollowLst.indexOf(e)==2?2:
                                    counsalterFollowLst.indexOf(e)==3?3:
                                    counsalterFollowLst.indexOf(e)==4?4:
                                    counsalterFollowLst.indexOf(e)==5?0:
                                    (counsalterFollowLst.indexOf(e)%limit==0.0)?0:
                                    (counsalterFollowLst.indexOf(e)%limit==1.0)?1:
                                    (counsalterFollowLst.indexOf(e)%limit==2.0)?2:
                                    (counsalterFollowLst.indexOf(e)%limit==3.0)?3:
                                    (counsalterFollowLst.indexOf(e)%limit==4.0)?4:2,

//                                            onTap: (key) {
//                                              Navigator.push(
//                                                  context,
//                                                  PageTransition(
//                                                      type: PageTransitionType.leftToRight,
//                                                      duration: Duration(milliseconds: 500),
//                                                      alignment: Alignment.centerLeft,
//                                                      curve: Curves.easeOutCirc,
//                                                      child: HomePage()
//                                                  ));
//                                        setState(() {});
//                                            }
//                                  ),
                              );
                            }).toList()),
//                    ),
//                  ),
//                ),
//                          Text('Red container should be scrollable'),
//                          Container(
//                            width: double.infinity,
//                            height: 700.0,
//                            padding: EdgeInsets.all(10.0),
//                            color: Colors.white.withOpacity(0.7),
//                            child: Text('I will have a column here'),
//                          )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),

              ]),
    ))
        ,loading==true?Conf.circularProgressIndicator():Container()]))));
  }
}
