import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'config.dart';

class ListViewContainerComponent extends StatelessWidget {
  String title1, title2;
  IconData iconData;
  Function onTap;
  Color containerColor;

  TextStyle txtStyle2AboveListViewContainer =
      TextStyle(fontWeight: FontWeight.w900, fontSize: 16);
  List<Widget> lst;

  ListViewContainerComponent(
      {this.title2,
      this.lst,
      this.iconData,
      this.title1,
      this.onTap,
      this.containerColor});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: Conf.smEdgeleftBottomTopX,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: Conf.smEdgeRight11,
                child:
                    FittedBox(fit: BoxFit.cover,
                      child: Text(title1 ?? '', softWrap: true,
                          overflow: TextOverflow.fade, style:Conf.subtitle.copyWith(fontWeight: FontWeight.w900, fontSize: 16)
//                          txtStyle2AboveListViewContainer
                      ),
                    ),
              ),
              InkWell(
                onTap: onTap,
                child: Padding(
                  padding: Conf.smEdgeleft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FittedBox(fit: BoxFit.cover,
                        child: Text(title2 ?? '',  softWrap: true,
                          overflow: TextOverflow.fade,style:Conf.body1.copyWith(color: Conf.orang,fontWeight: FontWeight.w900,
                                fontSize: 12)
//                          TextStyle(color: Colors.orange,fontWeight: FontWeight.w900,
//                          fontSize: 12
//                        ),
                  ),
                      ),
                      Icon(
                        iconData,
                        size: 14,
                        color: Conf.orang,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: Conf.p2w(1.042),//5,
                blurRadius: Conf.p2w(1.459),//7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(45)),
            color: Colors.white,
          ),
          width: double.infinity,
          height: Conf.p2h(23.7),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: lst ?? []),
          ),
        ),
      ],
    );
  }
}
