import "package:flutter/material.dart";
class VisitListPageModel {
    String day,fname,grade, img,kind,lname,month,visitStatus,year;
    IconData icon;
    Color color;

    VisitListPageModel({this.day, this.icon,this.color,this.fname, this.grade, this.img, this.kind, this.lname, this.month, this.visitStatus, this.year});

    factory VisitListPageModel.fromJson(Map<String, dynamic> json) {
        return VisitListPageModel(
            day: json['day'], 
            color: json['color'],
            icon: json['icon'],
            fname: json['fname'],
            grade: json['grade'], 
            img: json['img'], 
            kind: json['kind'], 
            lname: json['lname'], 
            month: json['month'], 
            visitStatus: json['visitStatus'], 
            year: json['year'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['day'] = this.day;
        data['color'] = this.color;
        data['icon'] = this.icon;
        data['fname'] = this.fname;
        data['grade'] = this.grade;
        data['img'] = this.img;
        data['kind'] = this.kind;
        data['lname'] = this.lname;
        data['month'] = this.month;
        data['visitStatus'] = this.visitStatus;
        data['year'] = this.year;
        return data;
    }
}