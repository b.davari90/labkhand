import 'dart:ui';
import 'package:counsalter5/CounsalterAboutcomponent.dart';
import 'package:counsalter5/CounsalterPrice.dart';
import 'package:counsalter5/ModelBloc/DoctorCollectionModel.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:counsalter5/htify.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/widgets.dart';
import 'DoctorModel.dart';
import 'config.dart';

class CounsalterAbout extends StatefulWidget {

  int id;
  List<DoctorModel> dataList;
  DoctorModel model;
  List<DoctorModel> getTotalDataList = [];
  List<DoctorCollectionModel> doctorCollectionList = [];
  CounsalterAbout({this.id});
  bool loading=false;
  @override
  _CounsalterAboutState createState() => _CounsalterAboutState();
}
class _CounsalterAboutState extends State<CounsalterAbout> {
  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();
  DoctorModel s = DoctorModel();
  Future getData() async {
    try {
      setState(() {
        widget.loading=true;
      });
      var data = await Htify.one('cu/doctor', widget.id);
      Conf.idLoginDoctor=widget.id;
      print("idLoginDoctor ===> ${  Conf.idLoginDoctor}");
      print("____widget.id ===> ${ widget.id }");
      s=DoctorModel.fromJson(data);
    } catch (e) {
      print("e.toString() ===> ${e.toString()}");
    }
    finally{
      setState(() {
        widget.loading=false;
      });
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SafeArea(
            child: Scaffold(
                body:
                BlocBuilder<DoctorBloc, DoctorRepository>(
                    builder: (context, state) {
                      return  Stack(
                        children: [
                          Positioned(
                            top: Conf.p2w(0),
                            right: Conf.p2w(0),
                            left: Conf.p2w(0),
                            bottom: Conf.p2w(60),
                            child: Container(
                                width: Conf.p2w(100),
                                height: Conf.p2h(35),
                                color: Colors.white,
                                child:
                                Conf.image(s.avatar) ??Conf.image(null)
//                                Image.asset(
//                                    s.avatar?? "assets/images/counsalterAbout.png"
//                                )
                            ),
                          ),
                          Positioned(
                            top: Conf.p2h(4.167),
                            left: Conf.p2w(4.167),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: Conf.p2w(11),
                                height: Conf.p2w(11),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Conf.orang),
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(
                                    Icons.navigate_next,
                                    size: Conf.p2t(23),
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: Conf.p2h(30)),
//                              height: Conf.p2h(80),
//                              width: Conf.p2w(50),
//                              width: double.infinity,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 8,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(Conf.p2w(6.25)),
                                    topLeft: Radius.circular(Conf.p2w(6.25))
                                )),
                            child: Flex(direction: Axis.vertical, children: [
                              Expanded(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(Conf.p2w(6.25)),
                                      topLeft: Radius.circular(Conf.p2w(6.25))),
                                  child: SingleChildScrollView(
                                    padding: EdgeInsets.only(bottom: Conf.xlSize),
                                    child:
                                    Container(
                                      width: Conf.p2w(100),height: Conf.p2h(80),
                                      child: CounsalterAboutcomponent(
                                          model: DoctorModel(
                                              fname:s.fname??"",
                                              lname: s.lname??"",
                                              avatar:s.avatar??"",
                                              degree: 0,
                                              specialty: s.specialty??"",
                                              description:s.description.toString()??"",
                                              doctor_collections: s.doctor_collections??[]
                                          )
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                          ),

                        ],
                      );}),
                bottomNavigationBar: bottomNavigationFunc())),
        widget.loading == true ? Conf.circularProgressIndicator() : Container()
      ],

    );
  }
//
  Widget bottomNavigationFunc() {
    return
      InkWell(
      onTap: () {
        displayBottomSheet(context);
      },
      child: Stack(
        children: [
          Container(height: Conf.p2h(10),color: Colors.white,),
            Container(
              height: Conf.p2h(11),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    FeatherIcons.chevronUp,
                    color: Colors.white,
                    size: Conf.p2t(28),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        right: Conf.p2w(2.0839), top: Conf.p2w(1.668)),
                    child: Text(
                      "تعیین نوع مشاوره",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: Conf.p2t(Conf.p2w(4.1665)),
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  color: Conf.blue,
                  boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(Conf.p2w(3.4)),
                      topRight: Radius.circular(Conf.p2w(3.4)))),
            ),
        ],

      ),
    );
  }

  Widget displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(25),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        barrierColor: Colors.transparent,
        context: context,
        builder: (ctx) {
          return  Container(
            height: Conf.p2h(50), //49
            child: CounsalterPrice(),
          );
        });
  }
}
