import 'package:flutter/cupertino.dart';

class PriceModel {
    int doctor_id, fee,id,price,total;
    String icon;
    IconData iconModel;
    Map<int, String> type = {1: "متنی", 2: "تصویری", 3: "تلفنی",4:"صوتی"};

    PriceModel({this.doctor_id, this.fee, this.id, this.price, this.total,this.iconModel, this.type,this.icon});

    factory PriceModel.fromJson(Map<String, dynamic> json) {
        return PriceModel(
            doctor_id: json['doctor_id'], 
            fee: json['fee'], 
            id: json['id'], 
            icon: json['icon'],
            price: json['price'],
            total: json['total'], 
            type: json['type'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['doctor_id'] = this.doctor_id;
        data['fee'] = this.fee;
        data['id'] = this.id;
        data['icon'] = this.icon;
        data['price'] = this.price;
        data['total'] = this.total;
        data['type'] = this.type;
        return data;
    }
}