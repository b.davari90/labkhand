import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/ModelBloc/NotifyListModel.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'StatusReserveCoustomerCardComponent.dart';
import 'HomePage.dart';
import 'config.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params_result.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
import 'package:counsalter5/ModelBloc/StatusReserveModel.dart';
import 'package:counsalter5/ModelBloc/StatusReserveListModel.dart';
class StatusReserveCoustomerPage extends StatefulWidget {
  @override
  _StatusReserveCoustomerPageState createState() => _StatusReserveCoustomerPageState();
}

class _StatusReserveCoustomerPageState extends State<StatusReserveCoustomerPage> {
  bool loading=false;
  int count,pages,limit;
  StatusReserveListModel notifyListModel;
  List<StatusReserveModel> notifyLst;
  SearchAdvCubit get searchBloc => context.bloc<SearchAdvCubit>();
  ScrollController sc;
  QueryListParams qlp;
  QueryListParamsResult listResult;
//  List<NotifyModel> notifyLst;
  Future getAllNotify() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.get("cu/notify");
      print("data ===> ${ data }");
      notifyListModel= StatusReserveListModel.fromJson(data);
      count = notifyListModel.count;
      limit = notifyListModel.limit;
      pages = notifyListModel.page;
      print("count ===> ${ count }");
      print("limit ===> ${ limit }");
      print("pages ===> ${ pages }");
      print("notifyLst ===> ${ notifyLst }");
      notifyLst.addAll(notifyListModel.rows);
    } catch (e) {
      print("CatchError getAllDoctorFolder()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }
  @override
  void initState() {
    print("Conf.token ===> ${ Conf.token }");
    notifyListModel=StatusReserveListModel();
    notifyLst=[];
    qlp = QueryListParams();
    listResult = QueryListParamsResult();
    sc = ScrollController();
    qlp.page = 0;
    searchBloc.setQlp(qlp);
    sc.addListener(() {
      if (sc.offset >= sc.position.maxScrollExtent - 100 &&
          !sc.position.outOfRange &&
          qlp.page < pages) {
        setState(() {
          qlp.page++;
          searchBloc.setQlp(qlp);
          getAllNotify();
        });
      }
    }); getAllNotify();
    super.initState();
  }
  @override
  void dispose() {
    sc.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: double.infinity,
              height: Conf.p2h(100),
              color: Conf.blue,
            ),
            Positioned(
              width: Conf.p2w(100),
              top: Conf.p2w(7),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("اعلانات",
//                              softWrap: true,
//                              overflow: TextOverflow.fade,
                      style: Conf.headline.copyWith(
                          color: Colors.white,
                          fontSize: Conf.p2t(25),
                          fontWeight: FontWeight.w500,
                          decoration: TextDecoration.none)),
                ],
              ),
            ),
            Positioned(
              top: Conf.p2w(7),
              left: Conf.p2w(6),
              child: Column(
                children: [
                  MasBackBotton()
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: Conf.p2w(27)),
              decoration: BoxDecoration(
                  boxShadow: [ BoxShadow(
                      color: Colors.indigo,
                      blurRadius: 10,
                      spreadRadius: 0.1,
                      offset: Offset(-1, 1)),
                  ],
                  color: Conf.lightColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30))),
              child: ClipRRect(borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30)),
                child: notifyLst==[]||notifyLst==null?Container(child: Center(child: Text("اطلاعاتی موجود نیست"),),):
//                    :Container(child: Center(child: Text("اطلاعاتی موجود نیست"),),)
//                      :
                SingleChildScrollView(
                  controller: sc,
                    scrollDirection: Axis.vertical,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: Conf.xlSize),
                      child: Column(
//                V      crossAxisAlignment: CrossAxisAlignment.start,
                          children: notifyLst.map((e) {
                            return StatusReservCoustomerCardComponent(
                            isFirstItem: notifyLst.indexOf(e) < 1 ?true:false,
                                hasShadow: true,
                                hasDate: true,
                                index: notifyLst.indexOf(e)==0?0:
                                notifyLst.indexOf(e)==1?1:
                                notifyLst.indexOf(e)==2?2:
                                notifyLst.indexOf(e)==3?3:
                                notifyLst.indexOf(e)==4?4:
                                notifyLst.indexOf(e)==5?0:
                                (notifyLst.indexOf(e)%limit==0.0)?0:
                                (notifyLst.indexOf(e)%limit==1.0)?1:
                                (notifyLst.indexOf(e)%limit==2.0)?2:
                                (notifyLst.indexOf(e)%limit==3.0)?3:
                                (notifyLst.indexOf(e)%limit==4.0)?4:2,
                                model: e,
                                onTap: (key) {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.leftToRight,
                                          duration: Duration(milliseconds: 500),
                                          alignment: Alignment.centerLeft,
                                          curve: Curves.easeOutCirc,
                                          child: HomePage()
                                      ));
//                                        setState(() {});
                                });
                          }).toList()),
                    )),
              )
            ),
            loading==true?Conf.circularProgressIndicator():Container()
          ],
        ),
      ),
    );
  }
}
