import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'config.dart';
import 'dart:math';
import 'package:counsalter5/ModelBloc/DoctorFolderModel.dart';

class DoctorDocumentComponent extends StatefulWidget {
  DoctorFolderModel model;
  Function onTap;

  TextStyle titleStyle=TextStyle(
  color: Conf.blue,
  fontWeight: FontWeight.w600,
  fontSize: Conf.p2t(17));
  TextStyle describtionStyle= TextStyle(
  color: Colors.black54,
  fontWeight: FontWeight.w700,
  fontSize: Conf.p2t(14));
  TextStyle RowStyle=TextStyle(fontSize: Conf.p2t(10),fontWeight: FontWeight.w600);
  int index = 0;

  DoctorDocumentComponent({this.model,this.onTap,this.index});
  @override
  _DoctorDocumentComponentState createState() => _DoctorDocumentComponentState();
}
List colors = [Colors.greenAccent.withOpacity(0.5),Colors.purpleAccent.withOpacity(0.4), Colors.deepOrange.withOpacity(0.5),Colors.pink.withOpacity(0.2),Colors.yellow.withOpacity(0.5)];
Random random = new Random();

class _DoctorDocumentComponentState extends State<DoctorDocumentComponent> {
  void changeIndex() {
    setState(() => widget.index = random.nextInt(3));
  }
  @override
  Widget build(BuildContext context) {
    return
      InkWell(
      onTap: () {
//        Navigator.push(
//            context,
//            PageTransition(
//                type: PageTransitionType.leftToRight,
//                duration: Duration(milliseconds: 500),
//                alignment: Alignment.centerLeft,
//                curve: Curves.easeOutCirc,
//                child: HomePage()
//            ));
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color:Colors.grey.withOpacity(0.2)),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              offset: Offset(-3,3), // changes position of shadow
            ),
          ],
        ),
        margin: EdgeInsets.only(top: Conf.p2w(5),right: Conf.p2w(4),left: Conf.p2w(4),bottom: Conf.p2h(1)),
        width: Conf.p2w(90),
        height: Conf.p2h(18),
//        color: Colors.yellow,
        child: Row(
            children: <Widget>[
          Container(
            width: Conf.p2w(2),
            decoration: BoxDecoration(
                color: colors[widget.index],
//                color: colors[widget.index],
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(Conf.p2w(3.125)),
//                            topRight: Radius.circular(15),
                    bottomRight: Radius.circular(Conf.p2w(3.125)))),
          ),
          Padding(
            padding: EdgeInsets.only(right: Conf.p2w(2),top: Conf.p2h(3)),
            child: Column(
              children: [
                Text(widget.model.caj??"",style: widget.RowStyle),
                Padding(
                  padding: Conf.smEdgeRight1 / 3.5,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: Container(
                      color: Colors.grey.withOpacity(0.2),
                      width: Conf.p2w(14.5),
                      height: Conf.p2w(14.5),
                      child: widget.model.owner.avatar??Conf.image(null),
                    ),
                  ),
                ),
//                    Container(
////                      color: Colors.yellow,
//                      width: Conf.p2w(20),
//                      height: Conf.p2w(20),
//                      child: CircleAvatar(
//                          backgroundColor: Colors.white,
//                          child: FadeInImage.assetNetwork(
//                            placeholder: "assets/images/profpic2.png",
//                            image:
//                                "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
//                          )),
//                    ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: Conf.p2h(5),
                right: Conf.p2w(2)),
            child: Container(
              width: Conf.p2w(50),
              height: Conf.p2h(15),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.model.owner.name,
                      style:widget.titleStyle),
            Text(" شماره تماس: "+widget.model.owner.mobile,
                      softWrap: true,
                      maxLines: 5,
                      overflow: TextOverflow.visible,
                      style:widget.describtionStyle),
//                  Text(" متولد: "+widget.model.owner.,
//                      softWrap: true,
//                      maxLines: 5,
//                      overflow: TextOverflow.visible,
//                      style:widget.describtionStyle),
                  Row(
                    children: [
                      Text("جنسیت: ",
                          softWrap: true,
                          maxLines: 5,
                          overflow: TextOverflow.visible,
                          style:widget.describtionStyle),
                      Text( widget.model.owner.gender==2?"زن":widget.model.owner.gender==1?
                      "مرد":"نامشخص")
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: Conf.p2h(2)),
            child: Column(
              children: [
                Row(
                  children: [Text("فرم ها",style: widget.RowStyle,), Text(widget.model.id.toString(),style: widget.RowStyle,)],
                )
              ],
            ),
          )
        ]),
      ),
    );
  }
}
