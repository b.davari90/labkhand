import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'FolowupDateModel.dart';
import 'config.dart';

class CounsaltelFollowUpDateComponent extends StatefulWidget {
  FolowupDateModel model;
  Function onTap;
  @override
  _CounsaltelFollowUpDateComponentState createState() => _CounsaltelFollowUpDateComponentState();

  CounsaltelFollowUpDateComponent({this.model,this.onTap});
}
class _CounsaltelFollowUpDateComponentState extends State<CounsaltelFollowUpDateComponent> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){setState(() {
        if(widget.onTap is Function){
          if (widget.model.active == false) {
//                              print("44 ===> ${ 44 }");
            widget.model.active= true;
          } else
            widget.model.active= false;
        }
      });},

      child: Container(
//      color: Colors.yellow,
        margin: EdgeInsets.only(
            top: Conf.p2h(0.25),
            left: Conf.p2w(2.918),
            right: Conf.p2w(2.918)),
        width: Conf.p2w(11.5),
        height: Conf.p2w(11.5),
        child: InkWell(
          onTap: widget.model.active == true ? (){}:(){
            if(widget.onTap is Function){
              widget.onTap(widget.model);
            }},
            child: Container(
             decoration: BoxDecoration(
                 boxShadow: [BoxShadow(
                   color: Colors.grey.withOpacity(0.2),
                   spreadRadius: 1,
                   blurRadius: 1,
                   offset: Offset(0, 1), // changes position of shadow
                 ),],
                 color:widget.model.isChoose ?  Colors.blue: Colors.white,
                 borderRadius: BorderRadius.all(Radius.circular(15))),
              width: Conf.p2w(13),height:Conf.p2h(7.5),
//        foregroundColor: Colors.yellow,

              child: Center(
                  child: Text(widget.model.text,
                style:TextStyle(color:widget.model.isChoose?Colors.white:Colors.black87,fontSize: 18,fontWeight: FontWeight.w600 ),)),
//              style:TextStyle(color:widget.model.friday?Colors.red: widget.model.active?Colors.grey :Colors.lightBlueAccent),)),
            )
//        child: CircleAvatar(
////        foregroundColor: Colors.yellow,
//          backgroundColor:widget.model.isDefault ?  Colors.blue: Colors.red,
//          child: Text(widget.model.text,style:TextStyle(color:widget.model.friday?Colors.red: widget.model.active?Colors.grey :Colors.lightBlueAccent),),
//        ),
        ),
      ),
    );
  }
}
