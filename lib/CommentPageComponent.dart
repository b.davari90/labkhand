import 'dart:ui';
import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:counsalter5/config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'CommentPageModel.dart';

class CommentPageComponent extends StatelessWidget {
  CommentPageModel model;
  Function onTap;
 bool isFirstItem;

  CommentPageComponent({this.onTap, this.model,this.isFirstItem=false});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){

        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: HomePageDoctor()
            ));
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: Conf.xlSize),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.only(left: Conf.p2w(2)),
                child: Padding(
                  padding:isFirstItem?EdgeInsets.only(top: Conf.p2h(2.5)):EdgeInsets.only(top: Conf.p2h(2)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
//                    Expanded(child: VerticalDivider(color: Colors.black87)),
                      Text(
                        model.dayVertical,
                        style: TextStyle(
                            fontSize: Conf.p2t(19),
                            color: Colors.black87,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        model.monthVertical,
                        style: TextStyle(
                            fontSize: Conf.p2t(15),
                            color: Colors.black87,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        model.yearVertical,
                        style: TextStyle(
                            fontSize: Conf.p2t(13),
                            color: Colors.black87,
                            fontWeight: FontWeight.bold),
                      ),
                      Expanded(child: VerticalDivider(color: Colors.black87))
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: Conf.p2h(3), bottom: Conf.p2h(2)),
                child: Column(
                  children: [
                    IntrinsicHeight(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(13)),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.white30.withOpacity(0.4),
                                  blurRadius: 5,
                                  offset: Offset(-2, 3))
                            ],
                            color: model.colorContainer),
                        width: Conf.p2w(83),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2h(1), right: Conf.p2w(2)),
                              child: Container(
                                  width: Conf.p2w(63),
//                              color: Colors.redAccent,
                                  child: Column(
                                    children: [
                                      Text(
                                        model.comments,
                                        overflow: TextOverflow.visible,
                                        maxLines: 15,
                                        softWrap: true,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: model.colorContainerText,
                                            fontSize: Conf.p2t(13),
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(right: Conf.p2w(7),left: Conf.p2w(7)),
                                        child: Divider(
                                          color: Colors.white,
                                          thickness: 3,
                                        ),
                                      ),
                                      Container(
                                        width: Conf.p2w(90),
                                        height: Conf.p2h(7),
                                    padding: EdgeInsets.only(bottom: Conf.p2h(0.1)),
//                                    margin: EdgeInsets.only(bottom: Conf.p2h(1)),
//                                    color: Colors.red,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text("نوع مشاوره: ",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize:
                                                                Conf.p2t(13),
                                                            color: model
                                                                .colorContainerText)),
                                                    Text(model.kind,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize:
                                                                Conf.p2t(13),
                                                            color: model
                                                                .colorContainerText)),
                                                  ],
                                                ),
                                                Row(
//                                              mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                        model.year +
                                                            "/" +
                                                            model.month +
                                                            "/" +
                                                            model.day,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize:
                                                                Conf.p2t(13),
                                                            color: model
                                                                .colorContainerText)),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text("نام کاربر: ",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: Conf.p2t(13),
                                                        color: model
                                                            .colorContainerText)),
                                                Text(
                                                    "       " +
                                                        model.coustomerName +
                                                        " " +
                                                        model.coustomerFamily,
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: Conf.p2t(13),
                                                        color: model
                                                            .colorContainerText)),
                                              ],
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  )),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2h(2), left: Conf.p2w(3)),
                              child: Container(
                                width: Conf.p2w(12),
                                height: Conf.p2w(12),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(43.0),
                                  child: FadeInImage.assetNetwork(
                                    placeholder: "assets/images/prof1.jpg",
                                    image: model.img,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
