//
//import 'package:counsalter5/DoctorModel.dart';
//import 'package:counsalter5/config.dart';
//
//class DoctorSearchModel {
//    DoctorModel doctorModel;
//    DoctorSearchModel activeDoctor;
//    String doctorName,code,levelStr,title;
//    List<DoctorSearchModel> doctorProvince;
//    List<DoctorSearchModel> childeren;
//    bool hasChilderen;
//    int id,level,parentId;
//    bool isActive=false;
//    DoctorSearchModel parent;
//    List<DoctorSearchModel> selectedList=[];
//    bool get isActiveParentNonActive {
//        bool r;
//        if (parent == null)
//            r = true;
//        else if (parent.isActive == false)
//            r = true;
//        else
//            r = false;
//        return isActive && r;
//    }
//    DoctorSearchModel({this.activeDoctor,this.childeren,this.doctorModel,this.code,this.doctorName, this.doctorProvince, this.hasChilderen, this.id, this.isActive, this.level, this.levelStr, this.parent, this.parentId, this.selectedList, this.title});
//
//    factory DoctorSearchModel.fromJson(Map<String, dynamic> json) {
//        try {
//            var instance = DoctorSearchModel(
//                title :json['title'],
//                id:json['id'],
//                doctorModel:json['doctorModel'],
//                childeren:json['childeren'],
//                activeDoctor :json['activeDoctor'],
//                code:json['code'], isActive: json['isActive'],
//                doctorProvince:json['doctorProvince'],
//                doctorName: json['doctorName'],
//                levelStr: json['levelStr'],
//                parent: json['parent'],
//                parentId:json['parentId'],
//                selectedList:json['selectedList'],
//                level:json['level'],
//                hasChilderen: json['hasChilderen'],
//            );
////            List<DoctorSearchModel> children = [];
////            if (json.containsKey('children')) {
////                var listChildren = json['children'] as List;
////                children = listChildren.map((f) => DoctorSearchModel.fromJson(f, parent: instance)).toList();
////            } else {
////                children = [];
////            }
////            instance.childeren = children;
//            return instance;
//        } catch (e) {
//            throw 'City not valid => ${e}';
//        }
////        return DoctorSearchModel(
////            activeDoctor: json['activeDoctor'],
////            code: json['code'],
////            doctorName: json['doctorName'],
////            doctorProvince: json['doctorProvince'],
////            hasChilderen: json['hasChilderen'],
////            id: json['id'],
////            isActive: json['isActive'],
////            level: json['level'],
////            levelStr: json['levelStr'],
////            parent: json['parent'],
////            parentId: json['parentId'],
////            selectedList: json['selectedList'],
////            title: json['title'],
////        );
//    }
//
//    Map<String, dynamic> toJson() {
//        final Map<String, dynamic> data = new Map<String, dynamic>();
//        data['activeDoctor'] = this.activeDoctor;
//        data['code'] = this.code;
//        data['doctorModel'] = this.doctorModel;
//        data['childeren'] = this.childeren;
//        data['doctorName'] = this.doctorName;
//        data['doctorProvince'] = this.doctorProvince;
//        data['hasChilderen'] = this.hasChilderen;
//        data['id'] = this.id;
//        data['isActive'] = this.isActive;
//        data['level'] = this.level;
//        data['levelStr'] = this.levelStr;
//        data['parent'] = this.parent;
//        data['parentId'] = this.parentId;
//        data['selectedList'] = this.selectedList;
//        data['title'] = this.title;
//        return data;
//    }
//        static copyFrom(DoctorSearchModel doctorSearchModel) {
//            return DoctorSearchModel(
//                activeDoctor:doctorSearchModel .activeDoctor,
//                levelStr: doctorSearchModel.levelStr,
//                selectedList: doctorSearchModel.selectedList,
//                level: doctorSearchModel.level,
//                childeren: doctorSearchModel.childeren,
//                hasChilderen: doctorSearchModel.hasChilderen,
//                code: doctorSearchModel.code,
//                parentId: doctorSearchModel.parentId,
//                id: doctorSearchModel.id,
//                title: doctorSearchModel.title,
//                doctorProvince: doctorSearchModel.doctorProvince,
//                parent: doctorSearchModel.parent);
//        }
//
//        static clear() {
//            return DoctorSearchModel(
//                childeren: null,
//                isActive: null,
//                selectedList: null,
//                id: null,
//                title: null,
//                parentId: null,
//                code: null,hasChilderen: null,level: null,
//                activeDoctor: null,
//                doctorName: null,
//                levelStr: null,
//                doctorProvince: null,
//                parent: null);
//        }
//}
//class DoctorSearchModelCubit {
//    static List<DoctorSearchModel> doctors = [];
//    static DoctorSearchModel activeDoctor;
//    int level = 1;
//    DoctorSearchModel parent;
//    List<DoctorSearchModel> selectedDoctors = [];
//    List<DoctorSearchModel> activeDoctorsList = [];
//
//    static get getDoctorLen {
//        int i = 0;
//        DoctorSearchModelCubit.myDoctors.forEach((element) {
//            if (element.level == 1 && element.isActive) {
//                i += element.childeren.length;
//            } else if (element.level == 2 && element.isActive) {
//                i++;
//            }
//        });
//        return i;
//    }
//
//    List<DoctorSearchModel> get activeDoctors => doctors.where((element) => element.isActive).toList();
//
//    set activeDoctors(List<DoctorSearchModel> input) => input.forEach((element) {
//        element.isActive = false;
//    });
//
//    DoctorSearchModelCubit({this.parent, this.level = 1, this.activeDoctorsList});
//
//    get children => activeDoctor.childeren;
//
//    get selectedList => null;
//
//    static List<DoctorSearchModel> get myDoctors {
//        List<DoctorSearchModel> ss = [];
//        ss.addAll(DoctorSearchModelCubit.doctors.where((element) => element.isActive && (element.parentId == null || !element.parent.isActive)).toList());
//        return ss;
//    }
//
//    bool get isSelectAll => activeDoctors?.length == activeDoctor?.childeren?.length;
//
//    List<DoctorSearchModel> get currentList => DoctorSearchModelCubit.doctors.where((element) => element.level == this.level && element.parentId == parent?.id).toList();
//
//    set doctorProvince(List cityProvince) {}
//
//    set selectedList(a) => null;
//
//    static sync() {
//        var ids = Conf.cityFilterId ?? [];
//        DoctorSearchModelCubit.doctors.forEach((element) {
//            if (ids.contains(element.id)) element.isActive = true;
//        });
//    }
//
//    static DoctorSearchModelCubit copy(DoctorSearchModelCubit cityCubitModel) {
//        return DoctorSearchModelCubit(parent: cityCubitModel.parent, level: cityCubitModel.level, activeDoctorsList: cityCubitModel.activeDoctorsList);
//    }
//}