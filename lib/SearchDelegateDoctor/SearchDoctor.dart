//import 'package:counsalter5/DoctorModel.dart';
//import 'package:counsalter5/SearchDelegateDoctor/DoctorSearchModel.dart';
//import 'package:counsalter5/SearchDelegateDoctor/Doctor_provience_bloc2.dart';
//import 'package:counsalter5/htify.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:counsalter5/SearchDelegateCityTown/city.dart';
//import 'package:counsalter5/config.dart';
//import 'package:counsalter5/DoctorInformation/DoctorInformationBasicInfo.dart';
//import 'package:page_transition/page_transition.dart';
//
//class SearchDoctor extends StatefulWidget {
//  DoctorSearchModel model;
//  List<DoctorModel> doctorModelList = [];
//  Function onEvent;
//  TextEditingController controller;
//
//  SearchDoctor({this.model, this.onEvent, this.doctorModelList});
//
//  @override
//  State<StatefulWidget> createState() => _SearchDoctorState();
//}
//
//class _SearchDoctorState extends State<SearchDoctor> with AutomaticKeepAliveClientMixin {
//  Doctorproviencebloc2 get doctorSearchBloc => context.bloc<Doctorproviencebloc2>();
//  String textSearch;
//  bool loading;
//
//  List<DoctorSearchModel> getList(
//      DoctorSearchModelCubit state, bool backButton) {
//    List<DoctorSearchModel> b = [];
//    if (DoctorSearchModelCubit.activeDoctor == null) {
//      b = doctorSearchBloc.state.currentList;
//    } else {
//      b = DoctorSearchModelCubit.activeDoctor.childeren;
//    }
//    b = b.where((element) {
//      if (textSearch == null || textSearch == '') return true;
//      return element.title.contains(textSearch);
//    }).toList();
//    return b;
//  }
//  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();
//  Future getDoctorList() async {
//    try {
//      setState(() {
//        loading = true;
//      });
//      var data = await Htify.get('cu/doctor');
//      print(data);
//      List doctorList = data;
//      print("____doctorList ===> ${doctorList.toString()}");
//      widget.doctorModelList=doctorList.map((item) => DoctorModel.fromJson(item)).toList();
//      print("___widget ===> ${widget.doctorModelList.map((e) => e.fname)}");
////      widget.doctorModelList.map((e) => e.fname);
////      widget.doctorModelList.map((e) => e.lname);
////      widget.doctorModelList.map((e) => e.avatar??"");
////      doctorBloc.setDoctorList( widget.doctorModelList);
//    }
//    catch (e) {
//      print("CATCH ERROR getDoctorList===> ${e.toString()}");}
//    finally {
//      setState(() {
//        loading = false;
//      });
//    }
//  }
//  @override
//  void initState() {
//    getDoctorList();
//    doctorSearchBloc.setActiveDoctor(null);
//    super.initState();
//    widget.controller = TextEditingController();
//    widget.model = DoctorSearchModel();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return SafeArea(
//        top: true,
//        bottom: true,
//        child: WillPopScope(
//          onWillPop: () {
//            if (DoctorSearchModelCubit.activeDoctor == null) {
//              Navigator.pop(context);
//            } else {
//              doctorSearchBloc
//                  .setActiveDoctor(DoctorSearchModelCubit.activeDoctor.parent);
//            }
//          },
//          child: BlocBuilder<Doctorproviencebloc2, DoctorSearchModelCubit>(
//              builder: (context, state) {
//            return Container(
//              height: Conf.p2w(100),
//              width: Conf.p2w(100),
//              child: Scaffold(
//                appBar: AppBar(
//                  automaticallyImplyLeading: false,
//                  flexibleSpace: SizedBox(
//                    width: Conf.p2w(100),
//                    height: Conf.p2w(100),
//                    child: Column(
//                      children: [
//                        Row(
//                          children: [
//                            Padding(
//                              padding: EdgeInsets.only(top: Conf.p2w(5)),
//                              child: IconButton(
//                                  icon: Icon(
//                                    Icons.arrow_back,
//                                    color: Conf.lightColor,
//                                  ),
//                                  onPressed: () {
//                                    if (CityCubitModel.activeCity == null) {
//                                      print("11111 ===> ${11111}");
//                                      Navigator.pop(context);
//                                    } else {
//                                      doctorSearchBloc.setActiveDoctor(
//                                          DoctorSearchModelCubit
//                                              .activeDoctor.parent);
//                                    }
//                                  }),
//                            ),
//                            Expanded(
//                              child: Container(
//                                height: Conf.p2w(12),
//                                decoration: BoxDecoration(
//                                    color: Colors.white70,border: Border.all(color: Colors.white),
//                                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
////                                padding: EdgeInsets.only(right: Conf.p2w(5),left: Conf.p2w(5)),
//                                margin: EdgeInsets.only(
//                                    right: Conf.p2w(2),
//                                    left: Conf.p2w(10),
//                                    top: Conf.p2w(5)),
//                                child: TextField(
//                                  decoration: InputDecoration(
//                                      border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
//                                      filled: true,
//                                      hintStyle: TextStyle(color: Colors.grey.withOpacity(0.4),height: 3.5),hintText: "نام پزشک، نوع تخصص، زمینه مشاوره ...",
//                                      contentPadding:
//                                      EdgeInsets.only(right: Conf.p2w(1))),
//                                  expands: false,
////                                  enabled: false,
//                                  onChanged: (value) {
//                                    setState(() {
//                                      textSearch = value;
//                                    });
//                                  },
//                                  onTap: () {
//                                    FocusScopeNode currentFocus =
//                                        FocusScope.of(context);
//                                    if (!currentFocus.hasPrimaryFocus) {currentFocus.unfocus();}
//                                  },
//                                  textDirection: TextDirection.rtl,
//                                  style: TextStyle(
//                                      color: Colors.black87,
//                                      fontWeight: FontWeight.bold,
//                                      fontSize: Conf.p2t(15)),
//                                  controller: widget.controller,
//                                ),
////                                TextField(
////                                  expands: false ,
////                                  enabled: true,
////                                  onChanged: (value) {
////                                    setState(() {textSearch = value;});},
////                                  onTap: () {
////                                    FocusScopeNode currentFocus =
////                                    FocusScope.of(context);
////                                    if (!currentFocus.hasPrimaryFocus) {
////                                      currentFocus.unfocus();
////                                    }
////                                  },
////                                  textDirection: TextDirection.rtl,
////                                  style: TextStyle(
////                                      color: Colors.black87,
////                                      fontWeight: FontWeight.bold),
////                                  controller: widget.controller,
////                                  decoration: InputDecoration(
////                                    hintStyle: TextStyle(
////                                        height: 2.5,
////                                        color: Colors.black45,
////                                        fontWeight: FontWeight.w600,
////                                        fontSize: Conf.p2t(15)),
////                                    isDense: true,
////                                    border: InputBorder.none,
////                                    contentPadding: EdgeInsets.only(
////                                        top: Conf.p2w(2.5),
////                                        right: Conf.p2w(2.0839)),
////                                    hintText: 'نام پزشک، نوع تخصص، زمینه مشاوره ...',
////                                  ),
////                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                      ],
//                    ),
//                  ),
//                  bottom: PreferredSize(
//                      child: Container(),
//                      preferredSize: Size.fromHeight(Conf.p2w(7))),
//                ),
//                body: BlocBuilder<Doctorproviencebloc2, DoctorSearchModelCubit>(
//                    builder: (context, state) {
//                  return DoctorSelectorList(
//                    list: getList(state, false),
//                    onSelect: (DoctorSearchModel doctorSearch) {
//                      doctorSearchBloc.setActiveDoctor(doctorSearch);
//                      if (doctorSearch.childeren == null && doctorSearch.childeren.isEmpty){widget.onEvent(CityCubitModel.activeCity);print("____11111 ===> ${11111}");}
//                    },
//                  );
//                }),
//              ),
//            );
//          }),
//        ));
//  }
//
//  @override
//  bool get wantKeepAlive => true;
//}
//
//class DoctorSelectorList extends StatelessWidget {
//  final List<DoctorSearchModel> list;
//  final Function onSelect;
//
//  DoctorSelectorList({this.list, this.onSelect});
//
//  @override
//  Widget build(BuildContext context) {
//    return new SingleChildScrollView(
//      child: Column(
//        children: list
//            .map(
//              (item) => Column(
//                children: [
//                  ListTile(
//                      title: Text(
//                        item.title,
//                        style: Conf.body2,
//                      ),
//                      trailing: item.childeren.isEmpty
//                          ? Icon(
//                              Icons.arrow_back_ios_sharp,
//                              color: Conf.transparentColor,
//                              size: Conf.p2w(4),
//                            )
//                          : Icon(Icons.arrow_back_ios_sharp,
//                              color: Conf.blue, size: Conf.p2w(4)),
//                      onTap: () {
//                        item.childeren.isNotEmpty
//                            ? this.onSelect(item)
//                            : Navigator.push(
//                                context,
//                                PageTransition(
//                                    type: PageTransitionType.leftToRight,
//                                    duration: Duration(milliseconds: 500),
//                                    alignment: Alignment.centerLeft,
//                                    curve: Curves.easeOutCirc,
//                                    child: DoctorInformationBasicInfo()));
//                      }),
//                  Padding(
//                    padding: EdgeInsets.symmetric(horizontal: Conf.xlSize),
//                    child: Conf.divider(),
//                  )
//                ],
//              ),
//            )
//            .toList(),
//      ),
//    );
//  }
//}
