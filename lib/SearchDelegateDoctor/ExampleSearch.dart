import 'dart:ui';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/CounsalterAbout.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DoctorSearchFilter extends StatefulWidget {
  TextEditingController controller = TextEditingController();
  bool loading = false;
  bool val = false;
  Function onSelect;
  List<DoctorModel> doctorModelList = [];
  String result, name;
  int rowId;

  @override
  _DoctorSearchFilterState createState() => _DoctorSearchFilterState();
}

class _DoctorSearchFilterState extends State<DoctorSearchFilter> {
  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();
  String textSearch;

  List<DoctorModel> get doctorList {
    if (textSearch == null || textSearch.isEmpty || textSearch == ''||(textSearch.length>0 &&textSearch.length<2)) {
//      print("____textSearch.length ===> ${ textSearch.length }");
      return widget.doctorModelList;
    } else {
//      print("____textSearch ELSE===> ${textSearch.length}");
      return widget.doctorModelList
              .where((element) =>
                  (element.name??"").contains(textSearch))
              .toList() ??
          "یافت نشد".toString();
    }
  }
  Future getDoctorList() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get('cu/doctor');
      print(data);
      List doctorList = data;
      print("____doctorList ===> ${doctorList.toString()}");
      widget.doctorModelList =
          doctorList.map((item) => DoctorModel.fromJson(item)).toList();

//      print("___widget.names ===> ${x.toString()}");
//      print("___widget.names ===> ${widget.names}");
    } catch (e) {
      print("CATCH ERROR getDoctorList===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  @override
  void initState() {
    getDoctorList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: Conf.p2h(100),
              color: Conf.lightColor,
            ),
            Positioned(
              width: Conf.p2w(100),
              top: Conf.p2h(4),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("لیست مشاوران",
                      style: Conf.headline.copyWith(
                          color: Colors.white,
                          fontSize: Conf.p2t(25),
                          fontWeight: FontWeight.w500,
                          decoration: TextDecoration.none)),
                ],
              ),
            ),
            Positioned(
              top: Conf.p2h(4),
              left: Conf.p2w(6),
              child: Column(
                children: [MasBackBotton()],
              ),
            ),
            Positioned(
              top: Conf.p2h(11.5), //123,
              left: Conf.p2w(6.25),
              right: Conf.p2w(6.25),
              child: Container(
//                margin: EdgeInsets.all(16).copyWith(bottom: 0),
                width: Conf.p2w(100),
                height: Conf.p2w(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.withOpacity(0.4)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: Conf.p2w(6.25), //30,
                      )
                    ]),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: Conf.smEdgeRight1 / 1.2,
                      child: Icon(
                        FeatherIcons.search,
                        size: Conf.p2w(3.75), //18,
                        color: Colors.grey.withOpacity(0.4),
                      ),
                    ),
                    SizedBox(
                      width: Conf.p2w(60),
                      height: Conf.p2h(10),
                      child: TextField(
                        decoration: InputDecoration(
                            hintStyle: TextStyle(
                                color: Colors.grey.withOpacity(0.4),
                                height: 3.5),
                            hintText: "نام پزشک، نوع تخصص، زمینه مشاوره ...",
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: Conf.p2w(2))),
                        onChanged: (value) {
                          widget.val = false;
                          if (value == null || value == '') {
                            setState(() {
                              widget.val = false;
                            });
                          } else {
                            setState(() {
                              widget.val = true;
                              textSearch = value;
                            });
                          }
                        },
                        onTap: () {},
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold,
                            fontSize: Conf.p2t(15)),
                        controller: widget.controller,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: Conf.p2w(39),
              bottom: Conf.p2w(0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey.shade50,
                    border: Border.all(color: Colors.grey.shade100, width: 1),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(13),
                        topLeft: Radius.circular(13))),
                width: Conf.p2w(87),
                height: Conf.p2w(100),
                margin: EdgeInsets.only(
                    left: Conf.xlSize,
                    right: Conf.xlSize + 10,
                    top: Conf.xlSize),
                child: SingleChildScrollView(
                  child: Column(
                    children: doctorList
                        .map(
                          (item) => Column(
                            children: [
                              ListTile(
                                  title: Text(item.name??""),
//                                  widget.val == true ?
//                                  widget.doctorModelList.map((e) =>
//                                      Text(e.name.startsWith(widget.result)
//                                          .toString()))
//                      : Text(item.name ?? ""),
//                                  widget.result.length
//                                  widget.doctorModelList.where((w) => w.contains("US")).join(" ")
//                                  Text(widget.doctorModelList.firstWhere((element) => element.name.startsWith(widget.result)).toString(), style: Conf.body2,)??Text("یافت نشد") : Text(item.name?? "", style: Conf.body2,),
//                                  Text(widget.doctorModelList.map((e) => e.name.startsWith(widget.result)).toList().toString(), style: Conf.body2,)??Text("یافت نشد") : Text(item.name?? "", style: Conf.body2,),

                                  onTap: () {
                                    widget.rowId = item.id;
                                    DoctorModel d = DoctorModel();
                                    d.name = item.name;
                                    d.id = item.id;
                                    print(" ____d.id===> ${d.id}");
                                    print("item.name ===> ${item.name}");
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                                PageTransitionType.leftToRight,
                                            duration:
                                                Duration(milliseconds: 500),
                                            alignment: Alignment.centerLeft,
                                            curve: Curves.easeOutCirc,
                                            child: CounsalterAbout(
                                              id: widget.rowId,
                                            )));
                                  }),
                              Conf.divider(color: Colors.grey.shade200)
                            ],
                          ),
                        )
                        .toList(),
                  ),
                ),
              ),
            ),
            widget.loading == true
                ? Conf.circularProgressIndicator()
                : Container()
          ],
        ),
      ),
    );
  }
}
