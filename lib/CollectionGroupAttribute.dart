//import 'package:rahe_aval/model/GroupAttribute.dart';
//
//class CollectionGroupAttribute {
//  int collectionId;
//  GroupAttribute group;
//  int groupAttributeId;
//  int id,w;
//  int index;
//  bool required;
//
//  CollectionGroupAttribute({this.collectionId, this.group, this.groupAttributeId, this.id,this.w, this.required, this.index}) {
//    if (this.group == null) {
//      this.group = GroupAttribute();
//    }
//  }
//
//  factory CollectionGroupAttribute.fromJson(Map<String, dynamic> json) {
//    GroupAttribute group;
//    if (json.containsKey('group') && json['group'] != null) {
//      group = GroupAttribute.fromJson(json['group']);
//    }
//
//    return CollectionGroupAttribute(
//      collectionId: json['collection_id'],
//      group: group,
//      groupAttributeId: json['group_attribute_id'],
//      id: json['id'],
//      required: json['required'],
//      w: json['w']
//    );
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['collection_id'] = this.collectionId;
//    data['group'] = this.group.toJson();
//    data['group_attribute_id'] = this.groupAttributeId;
//    data['id'] = this.id;
//    data['required'] = this.required;
//    return data;
//  }
//
//  copyFrom() {
//    return CollectionGroupAttribute(id: id, group: group, collectionId: collectionId, groupAttributeId: groupAttributeId, required: required);
//  }
//
//  CollectionGroupAttribute clone() {
//    return CollectionGroupAttribute(
//        index: index, id: id, required: required, groupAttributeId: groupAttributeId, collectionId: collectionId, group: group);
//  }
//}
