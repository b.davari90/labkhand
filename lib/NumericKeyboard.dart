//library numeric_keyboard;
//
//import 'package:flutter/material.dart';
//
//import 'config.dart';
//
//typedef KeyboardTapCallback = void Function(String text);
//
//class Keyboard extends StatefulWidget {
//  final Color textColor;
//  final Icon rightIcon;
//  final Function() rightButtonFn;
//  final Icon leftIcon;
//  double keyboardHeight;
//  final Function() leftButtonFn;
//  final KeyboardTapCallback onKeyboardTap;
//  final MainAxisAlignment mainAxisAlignment;
//
//  Keyboard({Key key,
//    @required this.onKeyboardTap,
//    this.textColor = Colors.black,
//    this.rightButtonFn,
//    this.keyboardHeight,
//    this.rightIcon,
//    this.leftButtonFn,
//    this.leftIcon,
//
//    this.mainAxisAlignment = MainAxisAlignment.spaceEvenly})
//      : super(key: key);
//
//  @override
//  _NumericKeyboardState createState() =>_NumericKeyboardState();
//}
//
//class _NumericKeyboardState extends State<Keyboard> {
//  @override
//  Widget build(BuildContext context) {
//    return Container(width: Conf.p2w(100),height: widget.keyboardHeight,
////      padding: EdgeInsets.only(top: Conf.p2h(widget.keyboardHeight)),
////      alignment: Alignment.center,
//      child: Column(
//        children: <Widget>[
//          ButtonBar(
//            alignment: widget.mainAxisAlignment,
//            children: <Widget>[
//              _calcButton('1'),
//              _calcButton('2'),
//              _calcButton('3'),
//            ],
//          ),
//          ButtonBar(
//            alignment: widget.mainAxisAlignment,
//            children: <Widget>[
//              _calcButton('4'),
//              _calcButton('5'),
//              _calcButton('6'),
//            ],
//          ),
//          ButtonBar(
//            alignment: widget.mainAxisAlignment,
//            children: <Widget>[
//              _calcButton('7'),
//              _calcButton('8'),
//              _calcButton('9'),
//            ],
//          ),
//          ButtonBar(
//            alignment: widget.mainAxisAlignment,
//            children: <Widget>[
//              InkWell(
//                  borderRadius: BorderRadius.circular(45),
//                  onTap: widget.leftButtonFn,
//                  child: Container(
//                      alignment: Alignment.center,
//                      width: Conf.p2w(10),
//                      child: widget.leftIcon)),
//              _calcButton('0'),
//              InkWell(
//                  borderRadius: BorderRadius.circular(45),
//                  onTap: widget.rightButtonFn,
//                  child: Container(
//                      alignment: Alignment.center,
//                      height:  Conf.p2h(3),
//                      width: Conf.p2w(10),
//                      child: widget.rightIcon))
//            ],
//          ),
//        ],
//      ),
//    );
//  }
//
//  Widget _calcButton(String value) {
//    return InkWell(
//        borderRadius: BorderRadius.circular(45),
//        onTap: () {
//          widget.onKeyboardTap(value);
//        },
//        child: Container(
//          alignment: Alignment.center,
//          width: Conf.p2w(26),
//          height: Conf.p2h(5),
//          child: Text(
//            value,
//            style: TextStyle(
//                fontSize: Conf.p2t(22),
//                fontWeight: FontWeight.bold,
//                color: widget.textColor),
//          ),
//        ));
//  }
//}
import 'package:flutter/material.dart';

import 'config.dart';

typedef KeyboardTapCallback = void Function(String text);

class NumericKeyboard extends StatefulWidget {
  final Color textColor;
  final Icon rightIcon;
  final Function() rightButtonFn;
  final Icon leftIcon;
  final Function() leftButtonFn;
  final KeyboardTapCallback onKeyboardTap;
  final MainAxisAlignment mainAxisAlignment;
  final Color splashColor;

  NumericKeyboard(
      {Key key,
        @required this.onKeyboardTap,
        this.textColor = Colors.black,
        this.rightButtonFn,
        this.rightIcon,
        this.leftButtonFn,
        this.leftIcon,
        this.splashColor,
        this.mainAxisAlignment = MainAxisAlignment.spaceEvenly})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _NumericKeyboardState();
  }
}

class _NumericKeyboardState extends State<NumericKeyboard> {
  @override
  Widget build(BuildContext context) {
    Color splashColor = widget.splashColor;
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
//      padding: const EdgeInsets.only(left: 32, right: 32, top: 20),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          ButtonBar(
            alignment: widget.mainAxisAlignment,
            children: <Widget>[
              _calcButton('1'),
              _calcButton('2'),
              _calcButton('3'),
            ],
          ),
          ButtonBar(
            alignment: widget.mainAxisAlignment,
            children: <Widget>[
              _calcButton('4'),
              _calcButton('5'),
              _calcButton('6'),
            ],
          ),
          ButtonBar(
            alignment: widget.mainAxisAlignment,
            children: <Widget>[
              _calcButton('7'),
              _calcButton('8'),
              _calcButton('9'),
            ],
          ),
          ButtonBar(
            alignment: widget.mainAxisAlignment,
            children: <Widget>[
              Material(
                color: Colors.transparent,
                type: MaterialType.button,
                child: InkWell(
                    enableFeedback: true,
                    borderRadius: BorderRadius.circular(45),
                    onTap: widget.leftButtonFn,
                    splashColor:splashColor,
                    child: Container(
                        alignment: Alignment.center,
                        width: 50,
                        height: 50,
                        child: widget.leftIcon)),
              ),
              _calcButton('0'),
              Material(
                color: Colors.transparent,
                type: MaterialType.button,
                child: InkWell(
                    enableFeedback: true,

                    splashColor: splashColor,
                    borderRadius: BorderRadius.circular(45),
                    onTap: widget.rightButtonFn,
                    child: Container(
                        alignment: Alignment.center,
                        width: 50,
                        height: 50,
                        child: widget.rightIcon)),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _calcButton(String value) {
    return Padding(
      padding: EdgeInsets.only(
          right: Conf.p2w(1.5),
          left: Conf.p2w(1.5)
      ),
      child: Container(
        alignment: Alignment.center,
        width: Conf.p2w(20),
        height:Conf.p2h(5),
//        color: Colors.red,
        child: Material(
          color: Colors.transparent,
          type: MaterialType.card,
          child: InkWell(
//            borderRadius: BorderRadius.circular(50),
            onTap: () {
              widget.onKeyboardTap(value);
            },
            child: Container(
              width: 50,
              child: Center(
                child: Text(
                  value,
                  style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                      color: widget.textColor),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}