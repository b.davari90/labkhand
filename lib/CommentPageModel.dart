import 'package:flutter/material.dart';
import 'package:tinycolor/tinycolor.dart';
import 'config.dart';

class CommentPageModel {
    String comments;
    String coustomerFamily;
    String coustomerName;
    String day;
    String dayVertical;
    String img;
    String kind;
    String month;
    String monthVertical;
    String year;
    String yearVertical;
   Color colorContainer,colorContainerText;
    CommentPageModel({this.colorContainer,this.colorContainerText,this.comments, this.coustomerFamily, this.coustomerName, this.day, this.dayVertical, this.img, this.kind, this.month, this.monthVertical, this.year, this.yearVertical});

    factory CommentPageModel.fromJson(Map<String, dynamic> json) {
        return CommentPageModel(
            comments: json['comments'], 
            coustomerFamily: json['coustomerFamily'], 
            coustomerName: json['coustomerName'], 
            day: json['day'], 
            dayVertical: json['dayVertical'], 
            img: json['img'], 
            kind: json['kind'], 
            month: json['month'], 
            monthVertical: json['monthVertical'], 
            year: json['year'], 
            yearVertical: json['yearVertical'],
            colorContainer: json['colorContainer']=!null?TinyColor.fromString(json['colorContainer']).color:Conf.lightColor,
            colorContainerText: json['colorContainerText']=!null?TinyColor.fromString(json['colorContainerText']).color:Conf.lightColor,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['comments'] = this.comments;
        data['coustomerFamily'] = this.coustomerFamily;
        data['coustomerName'] = this.coustomerName;
        data['day'] = this.day;
        data['dayVertical'] = this.dayVertical;
        data['img'] = this.img;
        data['kind'] = this.kind;
        data['month'] = this.month;
        data['monthVertical'] = this.monthVertical;
        data['year'] = this.year;
        data['yearVertical'] = this.yearVertical;
        data['colorContainer'] = this.colorContainer;
        data['colorContainerText'] = this.colorContainerText;
        return data;
    }
}