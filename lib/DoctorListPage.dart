import 'dart:ui';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/SearchDelegateDoctor/ExampleSearch.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'BottomNavigation.dart';
import 'DoctorCardComponent.dart';
import 'config.dart';
import 'package:counsalter5/CounsalterAbout.dart';
import 'DoctorModel.dart';

class DoctorListPage extends StatefulWidget {
  TextEditingController controller = TextEditingController();
  bool loading = false;
  int rowId;
  List<DoctorModel> doctorModelList = [];

  @override
  _DoctorListPageState createState() => _DoctorListPageState();
}

class _DoctorListPageState extends State<DoctorListPage> {
  Future getDoctorList() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get('cu/doctor');
      print("data ===> ${data}");
      List doctorList = data;
      print("____doctorList ===> ${doctorList.toString()}");
      widget.doctorModelList =
          doctorList.map((item) => DoctorModel.fromJson(item)).toList();
      print("___widget ===> ${widget.doctorModelList.map((e) => e.name)}");
    } catch (e) {
      print("CATCH ERROR getDoctorList===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  @override
  void initState() {
    print("token ===> ${Conf.token}");
    getDoctorList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child:
          Scaffold(
            body: Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: Conf.p2h(100),
                  color: Conf.blue,
                ),
                Positioned(
                  width: Conf.p2w(100),
                  top: Conf.p2h(4),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("لیست مشاوران",
                          style: Conf.headline.copyWith(
                              color: Colors.white,
                              fontSize: Conf.p2t(25),
                              fontWeight: FontWeight.w500,
                              decoration: TextDecoration.none)),
                    ],
                  ),
                ),
                Positioned(
                  top: Conf.p2h(4),
                  left: Conf.p2w(6),
                  child: Column(
                    children: [MasBackBotton()],
                  ),
                ),
                Positioned(
                  top: Conf.p2h(11.5),
                  left: Conf.p2w(6.25),
                  right: Conf.p2w(6.25),
                  child: Container(
                    width: Conf.p2w(100),
                    height: Conf.p2w(15),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: Colors.grey.withOpacity(0.4)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: Conf.p2w(6.25), //30,
                          )
                        ]),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: Conf.smEdgeRight1 / 1.2,
                          child: Icon(
                            FeatherIcons.search,
                            size: Conf.p2w(3.75), //18,
                            color: Colors.grey.withOpacity(0.4),
                          ),
                        ),
                        SizedBox(
                          width: Conf.p2w(60),
                          height: Conf.p2h(10),
                          child: TextField(
                            decoration: InputDecoration(
                                hintStyle: TextStyle(
                                    color: Colors.grey.withOpacity(0.4),
                                    height: 3.5),
                                hintText: "نام پزشک ...",
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding:
                                    EdgeInsets.only(right: Conf.p2w(2))),
//                        onChanged: (value) {
//                          if (value != "" || value != null) {print("value.toString() ===> ${ value.toString() }");}
//                        },
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.leftToRight,
                                      duration: Duration(milliseconds: 500),
                                      alignment: Alignment.centerLeft,
                                      curve: Curves.easeOutCirc,
                                      child: DoctorSearchFilter()));
                            },
//                        onTap: () {
//                          showSearch(
//                            context: context,
//                            delegate: CounsalterSearchPage(),
//                          );
//                        },
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                                fontSize: Conf.p2t(15)),
                            controller: widget.controller,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: Conf.p2w(42),
                  right: 0,
                  left: 0,
                  bottom: 0,
                  child: Column(
                    children: [
                      Expanded(
                        child: Container(
//                    margin: EdgeInsets.only(top: Conf.p2h(18.5)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(Conf.p2w(3.125)),
                                  topRight: Radius.circular(Conf.p2w(3.125)))),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Container(
//                    width: 200,height: 400,
//                    color: Colors.red,
                              margin: EdgeInsets.only(top: Conf.p2h(2)),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: widget.doctorModelList
                                      .map((e) => DoctorCardComponent(
                                            model: e,
                                            isOnline: true,
                                            hasLightShadow: true,
                                            onTap: () {
                                              widget.rowId = e.id;
                                              DoctorModel d = DoctorModel();
                                              d.name = e.name;
                                              d.id = e.id;
                                              d.avatar = e.avatar;
                                              d.description = e.description;
                                              print(" ____d.id===> ${d.id}");
                                              print(
                                                  "___e.describtion ===> ${e.description}");
                                              print("item.name ===> ${e.name}");
                                              Navigator.push(
                                                  context,
                                                  PageTransition(
                                                      type: PageTransitionType
                                                          .leftToRight,
                                                      duration: Duration(
                                                          milliseconds: 500),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      curve: Curves.easeOutCirc,
                                                      child: CounsalterAbout(
                                                        id: widget.rowId,
                                                      )));
                                            },
                                            hour: "",
                                            minute: "",
                                            msgNumber: "",
                                            hasTime: false,
                                            hasShadow: true,
                                            hasUnSeenMsg: false,
                                          ))
                                      .toList()),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                widget.loading == true
                    ? Conf.circularProgressIndicator()
                    : Container()
              ],
            )
//          Stack(
//            children: <Widget>[

//            ],
//          ),
            ,
            bottomNavigationBar: BottomNavigation(1),
          ),
//
//        ],
//      ),
    );
  }
}
