import 'package:bloc/bloc.dart';
import 'package:counsalter5/ModelBloc/DoctorCollectionModel.dart';
import 'package:counsalter5/ModelBloc/DocumentModel.dart';
import 'package:counsalter5/DoctorTypeModel.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:counsalter5/ModelBloc/DoctorPriceModel.dart';

class DoctorModel {
  bool active = false,
      isSelelectedTimeSlot = false,
      inf_bank = false,
      inf_personal = false,
      inf_doc = false,
      inf_collection = false;
  int countLike;
  String avatar,
      shaba_number,
      account_number,
      n_code,
      n_doctor,
      birth_day,
      phone,
      address,
      city_code,
      city,
      email,
      fname,
      lname,
      mobile,
      description,
      name,
      specialty;
  int id, city_id, type_doc, sts, acceptor_id, degree;
  List<DocumentModel> document;
  List<DoctorCollectionModel> doctor_collections = [];
  List<DoctorPresentsModel> doctor_present = [];
  List<DoctorTypeModel> types = [];
  List<DoctorPriceModel> price = [];

  DoctorModel(
      {this.active,
      this.city,
      this.avatar,
      this.shaba_number,
      this.acceptor_id,
      this.city_id,
      this.n_code,
      this.countLike,
      this.doctor_present,
      this.type_doc,
      this.price,
      this.id,
      this.inf_bank,
      this.inf_collection,
      this.inf_doc,
      this.inf_personal,
      this.n_doctor,
      this.phone,
      this.account_number,
      this.birth_day,
      this.address,
      this.document,
      this.types,
      this.city_code,
      this.doctor_collections,
      this.degree,
      this.specialty,
      this.email,
      this.fname,
      this.isSelelectedTimeSlot,
      this.description,
      this.lname,
      this.mobile,
      this.name,
      this.sts});

  factory DoctorModel.fromJson(Map<String, dynamic> json) {
    try {
      List<DoctorCollectionModel> dColMod = [];
      if (json.containsKey("doctor_collections") &&
          json["doctor_collections"] is List) {
        List dList = json["doctor_collections"];
        dColMod = dList.map((e) => DoctorCollectionModel.fromJson(e)).toList();
      }
//      List<DoctorPresentsModel> dCPresent =[];
//      if (json.containsKey('present') && json['present'] != null ) {
//        List dList = json['present'];
//        dCPresent = dList.map((e) => DoctorPresentsModel.fromJson(e)).toList();
//      }

      List<DoctorPriceModel> p = [];
      if (json.containsKey("price") && json["price"] is List) {
        List pList = json["price"];
        p = pList.map((e) => DoctorPriceModel.fromJson(e)).toList();
      }
      List<DoctorTypeModel> types = [];
      if (json.containsKey("types") && json["types"] is List) {
        List tList = json["types"];
        types = tList.map((e) => DoctorTypeModel.fromJson(e)).toList();
      }
      List<DocumentModel> document;
      if (json['doc'] != null && json.containsKey('doc')) {
        List dList = json['doc'];
        document = dList.map((e) => DocumentModel.fromJson(e)).toList();
      }
      return DoctorModel(
        active: json['active'],
        acceptor_id: json['acceptor_id'],
        id: json['id'],
        countLike: json["countLike"],
        city: json['city'],
//        doctor_present: dCPresent,
        account_number: json['account_number'],
        address: json['address'],
        birth_day: json['birth_day'],
        inf_doc: json['inf_doc'],
        inf_collection: json['inf_collection'],
        inf_bank: json['inf_bank'],
        inf_personal: json['inf_personal'],
        type_doc: json['type_doc'],
        city_code: json['city_code'],
        n_code: json['n_code'],
        phone: json['phone'],
        city_id: json['city_id'],
        document: document,
        avatar: json['avatar'],
        shaba_number: json['shaba_number'],
        n_doctor: json['n_doctor'],
        doctor_collections: dColMod,
        types: types,
        price: p,
        degree: json['education'],
        email: json['email'],
        specialty: json['specialty'],
        isSelelectedTimeSlot: json['isSelelectedTimeSlot'],
        fname: json['fname'],
//       timeSlot: json['timeSlot'],
        description: json['description'],
//        last_visit: json['last_visit'],
        lname: json['lname'],
        mobile: json['mobile'],
        name: json['name'],
//       password: json['password'],
//        score: json['score'],
        sts: json['sts'],
      );
    } catch (e) {
      print("Catch error Doctor DataModel ===> ${e.toString()}");
      throw e;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['account_number'] = this.account_number;
    data['address'] = this.address;
    data['id'] = this.id;
    data['countLike'] = this.countLike;
    data['city'] = this.city;
    data['price'] = this.price;
//    data['price']=this.price;
    data['acceptor_id'] = this.acceptor_id;
    data['address'] = this.address;
    data['birth_day'] = this.birth_day;
    data['city_id'] = this.city_id;
    data['inf_bank'] = this.inf_bank;
    data['inf_personal'] = this.inf_personal;
    data['inf_doc'] = this.inf_doc;
    data['types'] = this.types;
    data['present'] = this.doctor_present;
    data['inf_collection'] = this.inf_collection;
    data['document'] = this.document;
    data['type_doc'] = this.type_doc;
    data['city_code'] = this.city_code;
//    data['types'] = this.types;
    data['phone'] = this.phone;
    data['shaba_number'] = this.shaba_number;
//    data['last_seen_at'] = this.last_seen_at;
    data['n_doctor'] = this.n_doctor;
    data['n_code'] = this.n_code;
    data['avatar'] = this.avatar;
    data['education'] = this.degree;
    data['doctor_collections'] = this.doctor_collections;
    data['email'] = this.email;
    data['fname'] = this.fname;
//    data['last_visit'] = this.last_visit;
    data['specialty'] = this.specialty;
    data['lname'] = this.lname;
    data['description'] = this.description;
    data['mobile'] = this.mobile;
    data['name'] = this.name;
//    data['password'] = this.password;
//    data['score'] = this.score;
    data['sts'] = this.sts;
    return data;
  }

  DoctorModel copy() {
    return DoctorModel(
        lname: lname,
        fname: fname,
        types: types,
        price: price,
        city: city,
        countLike: countLike,
        doctor_present: doctor_present,
//        price: price,
        id: id,
        acceptor_id: acceptor_id,
        inf_bank: inf_bank,
        inf_collection: inf_collection,
        isSelelectedTimeSlot: isSelelectedTimeSlot,
        inf_doc: inf_doc,
        inf_personal: inf_personal,
        account_number: account_number,
        address: address,
        type_doc: type_doc,
        birth_day: birth_day,
        city_code: city_code,
//        types: types,
        city_id: city_id,
//        last_seen_at: last_seen_at,
        n_code: n_code,
        document: document,
        n_doctor: n_doctor,
        shaba_number: shaba_number,
        phone: phone,
//        timeSlot: timeSlot,
        doctor_collections: doctor_collections,
        avatar: avatar,
        description: description,
        active: active,
//        password: password,
        mobile: mobile,
        specialty: specialty,
        email: email,
        name: name,
        degree: degree,
//        last_visit: last_visit,
//        score: score,
        sts: sts);
  }
}

class DoctorRepository {
  DoctorModel doctor;
  String priceText = "",
      priceVideo = "",
      priceChat = "",
      priceCall = "",
      date = "";
  bool ischat=false,iscall=false,isvideo=false,istext=false;
  List<DoctorModel> doctorAvatarList = [];
  List<DoctorModel> doctorList = [];
  int priceDrType;
  String cityDoctorName;
  int cityDoctorCode;
  String imagePath1, imagePath2, imagePath3;

  int file1IdServer, file2IdServer, file3IdServer;

  DoctorRepository(
      {this.doctor,
      this.imagePath1,
      this.imagePath2,
      this.imagePath3,
      this.file1IdServer,
      this.file3IdServer,
      this.file2IdServer,
      this.doctorAvatarList,
      this.doctorList,
        this.iscall,this.istext,this.ischat,this.isvideo,
      this.priceCall,
      this.cityDoctorCode,
      this.cityDoctorName,
      this.priceDrType,
      this.priceText,
      this.priceChat,
      this.date,
      this.priceVideo}) {
    if (this.doctor == null) {
      this.doctor = new DoctorModel();
    }
  }

  DoctorRepository copy() {
    return DoctorRepository(
        doctor: doctor,
        iscall: iscall,ischat: ischat,istext: istext,isvideo: isvideo,
        file1IdServer: file1IdServer,
        file2IdServer: file2IdServer,
        file3IdServer: file3IdServer,
        imagePath1: imagePath1,
        imagePath2: imagePath2,
        imagePath3: imagePath3,
        priceDrType: priceDrType,
        priceCall: priceCall,
        priceChat: priceChat,
        priceText: priceText,
        cityDoctorCode: cityDoctorCode,
        cityDoctorName: cityDoctorName,
        priceVideo: priceVideo,
        doctorList: doctorList,
        doctorAvatarList: doctorAvatarList);
  }

  DoctorRepository clearDocumentRepository() {
    return DoctorRepository(
        doctor: doctor,
        priceCall: priceCall,
        isvideo: isvideo,istext: istext,iscall: iscall,ischat: ischat,
        priceChat: priceChat,
        cityDoctorName: cityDoctorName,
        cityDoctorCode: cityDoctorCode,
        priceDrType: priceDrType,
        priceText: priceText,
        priceVideo: priceVideo,
        doctorAvatarList: doctorAvatarList,
        doctorList: doctorList);
  }
}

class DoctorBloc extends Cubit<DoctorRepository> {
  DoctorBloc(DoctorRepository state) : super(state);

  void setDoctor(DoctorModel doctor) {
    state.doctor = doctor;
    print("state.doctor DOCTOR MODEL===> ${state.doctor}");
    refresh();
  }

  void setPriceDrType(int price) {
    state.priceDrType = price;
    print("state.priceDrType Model===> ${state.priceDrType}");
    refresh();
  }

  void setDoctorDate(String date) {
    state.date = date;
    print("setDoctorDate MODEL===> ${state.date}");
    refresh();
  }

  void setDoctorCityName(String name) {
    state.cityDoctorName = name;
    refresh();
  }

  void setDoctorCityCode(int code) {
    state.cityDoctorCode = code;
    refresh();
  }

  void setDoctorList(List<DoctorModel> doctors) {
    if (state.doctorList == null) {
      state.doctorList = [];
    }
    state.doctorList.addAll(doctors);
    refresh();
  }

  void setImageNezam(String img) {
    state.imagePath1 = img;
    print("state.state.imagePath1  MODEL===> ${state.imagePath1}");
    refresh();
  }

  void setImageNezamID(int id) {
    state.file1IdServer = id;
    print("state.state.file1IdServer  MODEL===> ${state.file1IdServer}");
    refresh();
  }

  void setImageMeli(String img) {
    state.imagePath2 = img;
    print("state.state.imagePath2  MODEL===> ${state.imagePath2}");
    refresh();
  }

  void setImageMeliID(int id) {
    state.file2IdServer = id;
    print("state.state.file2IdServer  MODEL===> ${state.file2IdServer}");
    refresh();
  }

  void setImageEducation(String img) {
    state.imagePath3 = img;
    print("state.state.imagePath3  MODEL===> ${state.imagePath3}");
    refresh();
  }

  void setImageEducationID(int id) {
    state.file3IdServer = id;
    print("state.state.file3IdServer  MODEL===> ${state.file3IdServer}");
    refresh();
  }

  void removeAvatar(DoctorModel doctor) {
    state.doctorAvatarList.removeWhere((item) => item.id == doctor.id);
    refresh();
  }

  setMobile(String mobile) {
    state.doctor.mobile = mobile;
    refresh();
  }

  setId(int id) {
    state.doctor.id = id;
    refresh();
  }

  void setAvatar(String avatar) {
    state.doctor.avatar = avatar;
    refresh();
  }

  setTextPrice(String Ptext) {
    state.priceText = Ptext;
    refresh();
  }

  setVideoPrice(String pVideo) {
    state.priceVideo = pVideo;
    refresh();
  }

  setCallPrice(String pCall) {
    state.priceCall = pCall;
    refresh();
  }

  setChatPrice(String pChat) {
    state.priceChat = pChat;
    refresh();
  }
  setisText(bool x) {
    state.istext = x;
    refresh();
  }
  setisVideo(bool x) {
    state.isvideo = x;
    refresh();
  }
  setisCall(bool x) {
    state.iscall = x;
    refresh();
  }
  setisChat(bool x) {
    state.ischat = x;
    refresh();
  }
  void refresh() {
    emit(state.copy());
  }
}
