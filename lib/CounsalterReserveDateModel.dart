class CounsalterReserveDateModel {
  bool actived,active;
  int date;

  CounsalterReserveDateModel({this.active, this.actived,this.date});

  factory CounsalterReserveDateModel.fromJson(Map<String, dynamic> json) {
    return CounsalterReserveDateModel(
      active: json['active'],
      actived: json['actived'],
      date: json['time'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['actived'] = this.actived;
    data['time'] = this.date;

    return data;
  }
}