import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/SearchDelegateCityTown/city.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc2.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/DoctorInformation/DoctorInformationBasicInfo.dart';
import 'package:page_transition/page_transition.dart';

class ProvinceCityPage2 extends StatefulWidget {
  City model;
  List<City> cityProvince = [];
  Function onEvent;
  TextEditingController controller;
String name,fname,birthday,tel,nezamValue,meliValue,emailValue,adressValue;
  @override
  _ProvinceCityPage2State createState() => _ProvinceCityPage2State();

   ProvinceCityPage2({this.model, this.onEvent, this.cityProvince,this.name,this.nezamValue,this.meliValue
     ,this.adressValue,this.emailValue,this.birthday,this.tel,this.fname});
}

class _ProvinceCityPage2State extends State<ProvinceCityPage2>with AutomaticKeepAliveClientMixin {
  // CityProvinceCubit get cityBloc => context.bloc<CityProvinceCubit>();

  CityProvinceCubit2 get cityBloc2 => context.bloc<CityProvinceCubit2>();
  String textSearch;

  City parentCity;

  List<City> getList(CityCubitModel state, bool backButton) {
    List<City> b = [];
    if (CityCubitModel.activeCity == null) {
      b = cityBloc2.state.currentList;
    } else {
      b = CityCubitModel.activeCity.children;
    }
    b = b.where((element) {
      if (textSearch == null || textSearch == '') return true;
      return element.title.contains(textSearch);
    }).toList();
    print("b List<City> getList===> ${ b }");
    return b;
  }

  @override
  void initState() {
    print("widget.tel, ===> ${ widget.tel }");
    print("widget.birthday, ===> ${ widget.birthday }");
    print("widget.fname, ===> ${ widget.fname }");
    print("widget.name, ===> ${ widget.name }");
    print("widget.adressValue ===> ${ widget.adressValue }");
    print("widget.nezamValue ===> ${ widget.nezamValue }");
    print("widget.meliValue ===> ${ widget.meliValue }");
    print("widget.emailValue ===> ${ widget.emailValue }");

    cityBloc2.state.selectedCities=[];cityBloc2.state.activeCitiesList=[];
    cityBloc2.setActiveCity(null);
    super.initState();
    widget.controller=TextEditingController();
    widget. model=City();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: WillPopScope(
          onWillPop: () {
            if (CityCubitModel.activeCity == null) {
              Navigator.pop(context);
            } else {
              cityBloc2.setActiveCity(CityCubitModel.activeCity.parent);
              print(" WillPopScope===> ${ cityBloc2.state.activeCitiesList.map((e) => e.parent) }");
            }
          },
          child: BlocBuilder<CityProvinceCubit2, CityCubitModel>(builder: (context, state) {
            return Container(height: Conf.p2w(100),width: Conf.p2w(100),
              child: Scaffold(
                appBar:
                AppBar(
                  automaticallyImplyLeading: false,
                  flexibleSpace: SizedBox(
                    width: Conf.p2w(100),height: Conf.p2w(100),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding:EdgeInsets.only(top:Conf.p2w(5)),
                              child: IconButton(
                                  icon: Icon(
                                    Icons.arrow_back,
                                    color: Conf.lightColor,
                                  ),
                                  onPressed: () {
                                    if (CityCubitModel.activeCity == null) {
                                      print("11111 ===> ${ 11111 }");
                                      Navigator.pop(context);
                                    } else {
                                      cityBloc2.setActiveCity(CityCubitModel.activeCity.parent);
                                      print("ELSE 11111 ===> ${ 11111 }");
                                    }
                                  }),
                            ),
                            Expanded(
                              child: Container(color: Colors.white70,height: Conf.p2w(10),
                                padding: EdgeInsets.only(right: Conf.p2w(5),left: Conf.p2w(5)),
                                margin:  EdgeInsets.only(right: Conf.p2w(2),left: Conf.p2w(10),top:Conf.p2w(5)),
                                child: TextField(
                                  expands: false ,
                                  enabled: true,
                                  onChanged: (value) {
                                    setState(() {textSearch = value;
                                    print("textSearch ===> ${ textSearch }");});},
                                  onTap: () {
                                    print("___onTap is true; ===> ${ true}");
                                    FocusScopeNode currentFocus = FocusScope.of(context);
                                    if (!currentFocus.hasPrimaryFocus) {
                                      print("__currentFocus ===> ${ currentFocus.toString() }");
                                      currentFocus.unfocus();
                                    }
                                  },
                                  textDirection: TextDirection.rtl,
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold),
                                  controller: widget.controller,
                                  decoration: InputDecoration(
                                    hintStyle: TextStyle(
                                        height: 2.5,
                                        color: Colors.black45,
                                        fontWeight: FontWeight.w600,
                                        fontSize: Conf.p2t(15)),
                                    isDense: true,
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(
                                        top: Conf.p2w(2.5),
                                        right: Conf.p2w(2.0839)),
                                    hintText: 'جست و جوی شهر',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  bottom: PreferredSize(child: Container(), preferredSize: Size.fromHeight(Conf.p2w(10))),
                ),
                body: BlocBuilder<CityProvinceCubit2, CityCubitModel>(
                  builder: (context, state) {
                    return
                      LocationSelectorList(
                        name: widget.name,birthday: widget.birthday,fname: widget.fname,tel: widget.tel,adres: widget.adressValue,
                      email: widget.emailValue,meli: widget.meliValue,nezam: widget.nezamValue,
                      list: getList(state, false),
                      onSelect: (City city) {
                        cityBloc2.setActiveCity(city);

                        cityBloc2.setActiveCity(city);
                        cityBloc2.setCityCode(city);

                        print("__city ===> ${ city.toString() }");
                        print("__city activeCities===> ${ cityBloc2.state.activeCities.map((e) => e.title??"ندارد") }");
                        print("__city activeCitiesList===> ${ cityBloc2.state.activeCitiesList.map((e) => e.title??"نیست") }");
                        print("__city level===> ${ cityBloc2.state.level }");
                        print("__city selectedCities===> ${ cityBloc2.state.selectedCities.map((e) => e.title) }");

                        if (city.children == null && city.children.isEmpty) {
                          widget.onEvent(CityCubitModel.activeCity);
                          print("___CityCubitModel.activeCity ===> ${ CityCubitModel.activeCity }");
                          print("____ChilDeren NULL __body: BlocBuilder===> ${ 11111 }");
                        }
                      },
                    );
                  }
                ),
              ),
            );
          }
          ),
        ));
  }

  @override
  bool get wantKeepAlive =>true;
}

class LocationSelectorList extends StatelessWidget {
  final List<City> list;
  final Function onSelect;
  CityProvinceCubit2 cityBloc2;
  String fname,name,birthday,tel,nezam,email,adres,meli;
  LocationSelectorList({this.list,this.birthday,this.tel,this.nezam,this.meli,this.adres,this.email,this.name,this.fname, this.onSelect});
//  CityProvinceCubit2 get cityBloc2 => context.bloc<CityProvinceCubit2>();
  @override
  Widget build(BuildContext con) {
    return new
    SingleChildScrollView(
      child: Column(
        children: list
            .map(
              (item) => Column(
                children: [
                  ListTile(
                      title: Text(
                        item.title,
                        style: Conf.body2,
                      ),
                      trailing: item.children.isEmpty
                          ? Icon(
                              Icons.arrow_back_ios_sharp,
                              color: Conf.transparentColor,
                              size: Conf.p2w(4),
                            )
                          : Icon(
                              Icons.arrow_back_ios_sharp,
                        color: Conf.blue,
                              size: Conf.p2w(4)),
                      onTap: () {
                          print("____item ===> ${ item.title }");
                          print("____item ===> ${ item.code }");

                        item.children.isNotEmpty?
                        this.onSelect(item):
                        Navigator.push(
                            con,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                duration: Duration(milliseconds: 500),
                                alignment: Alignment.centerLeft,
                                curve: Curves.easeOutCirc,
                                child: DoctorInformationBasicInfo(adressValue: adres,emailValue:email ,
                                    meliValue: meli,nezamValue: nezam,
                                    birthdayValue:birthday,fnameValue: fname,nameValue: name,telValue: tel,
                                    nameCity: item.title,codeCity: item.code,cityId:item.id)));
//                        print("item.id ===> ${ item.id }");
//                        cityBloc2.setCity(item);
//                        cityBloc2.setCityCode(item);
//                        cityBloc2.setCityId(item);
//                            print("this.onSelect(item): ===> ${ item.toString() }");




//                        saveCity(cnx: con,city: item);
//                        print("_____save city ===> ${ con.toString() }");
                      }),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: Conf.xlSize),
                    child: Conf.divider(),
                  )
                ],
              ),
            )
            .toList(),
      ),
    );
  }
//  void saveCity({BuildContext cnx,City city}){
//    City c=City();
//    c=city;
//    print("c Is===> ${ c }");
//    print("city Is===> ${ city }");
//    cityBloc2.setCity(c);
//    cityBloc2.setCityCode(c);
//    cityBloc2.setCityId(c);
//    print("city.toString() ===> ${ c.toString() }");
////    print("cityBloc2.state.cityChoosen ===> ${ cityBloc2.state.cityChoosen }");
////    cityBloc2.state.activeCitiesList.addAll(city.title);
//    Navigator.push(
//        cnx,
//        PageTransition(
//            type: PageTransitionType.leftToRight,
//            duration: Duration(milliseconds: 500),
//            alignment: Alignment.centerLeft,
//            curve: Curves.easeOutCirc,
//            child: DoctorInformationBasicInfo()));
//  }
}
