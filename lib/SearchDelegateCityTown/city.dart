  import '../config.dart';

  class City {
    int id, level, parentId;
    String title, code, levelStr, cityName;
    bool hasChildren;
    City  cityChoosen;
    List<City> children;
    List<City> cityProvince;//مرکز استان
    City parent;
    List<City> selectedList = [];
    City activeCity;
    bool isActive = false;

    bool get isActiveParentNonActive {
      bool r;
      if (parent == null)
        r = true;
      else if (parent.isActive == false)
        r = true;
      else
        r = false;
      return isActive && r;
    }

    City(
        {this.cityName,
        this.selectedList,
        this.parent,
        this.cityProvince,
        this.id,
          this.cityChoosen,
        this.title,
        this.children,
        this.parentId,
        this.code,
        this.hasChildren,
        this.level,
        this.levelStr,
        this.activeCity});

    static City fromJson(Map<String, dynamic> json, {City parent}) {
      try {
        // City parent;
        // if (json.containsKey('parent') && json['parent'] != null) {
        //   parent = City.fromJson(json['parent']);
        // }

        var instance = City(
            id: json['id'],
            title: json['title'],
            cityChoosen: json['cityChoosen'],
            parentId: json['parent_id'],
            code: json['code'],
            hasChildren: json['has_children'],
            level: json['level'],
            levelStr: json['level_str'],
            parent: parent);
        List<City> children = [];
        if (json.containsKey('children')) {
          var listChildren = json['children'] as List;
          children = listChildren.map((f) => City.fromJson(f, parent: instance)).toList();
        } else {
          children = [];
        }
        instance.children = children;
        return instance;
      } catch (e) {
        throw 'City not valid => ${e}';
      }
    }

    bool isSelectAll = false;

    Map<String, dynamic> toMap() {
      return {
        "id": id,
        "title": title,
        "cityChoosen": cityChoosen,
        'children': children.map((e) => e.toMap()).toList(),
        'parent_id': parentId,
        'code': code,
        'has_children': hasChildren,
        'level': level,
        'level_str': levelStr,
        'parent': parent.toMap()
      };
    }

    static copyFrom(City city) {
      return City(
          activeCity: city.activeCity,
          levelStr: city.levelStr,
          selectedList: city.selectedList,
          level: city.level,
          hasChildren: city.hasChildren,
          code: city.code,
          cityChoosen: city.cityChoosen,
          parentId: city.parentId,
          children: city.children,
          id: city.id,
          title: city.title,
          cityProvince: city.cityProvince,
          parent: city.parent);
    }

    static clear() {
      return City(
          title: null,
          activeCity: null,
          selectedList: null,
          id: null,
          children: null,
          cityChoosen: null,
          parentId: null,
          code: null,
          hasChildren: null,
          level: null,
          levelStr: null,
          cityProvince: null,
          parent: null);
    }
  }

  class CityCubitModel {
    static List<City> cities = [];
    static City activeCity;
String nameeee,coooode;
    int level = 1;
    City parent;
    City city;
    List<City> selectedCities = [];
    List<City> activeCitiesList = [];

    static get getCityLen {
      int i = 0;
      CityCubitModel.myCities.forEach((element) {
        if (element.level == 1 && element.isActive) {
          i += element.children.length;
        } else if (element.level == 2 && element.isActive) {
          i++;
        }
      });
      return i;
    }

    List<City> get activeCities => cities.where((element) => element.isActive).toList();

    set activeCities(List<City> input) => input.forEach((element) {
          element.isActive = false;
        });

    CityCubitModel({this.parent, this.level = 1, this.activeCitiesList,this.city});

    get children => activeCity.children;

    get selectedList => null;

    static List<City> get myCities {
      List<City> ss = [];
      ss.addAll(CityCubitModel.cities.where((element) => element.isActive && (element.parentId == null || !element.parent.isActive)).toList());
      return ss;
    }

    bool get isSelectAll => activeCities?.length == activeCity?.children?.length;

    List<City> get currentList => CityCubitModel.cities.where((element) => element.level == this.level && element.parentId == parent?.id).toList();

    set cityProvince(List cityProvince) {}

    set selectedList(a) => null;

    static sync() {
      var ids = Conf.cityFilterId ?? [];
      CityCubitModel.cities.forEach((element) {
        if (ids.contains(element.id)) element.isActive = true;
      });
    }

    static CityCubitModel copy(CityCubitModel cityCubitModel) {
      return CityCubitModel(parent: cityCubitModel.parent, level: cityCubitModel.level, activeCitiesList: cityCubitModel.activeCitiesList,city:cityCubitModel.city);
    }
  }
