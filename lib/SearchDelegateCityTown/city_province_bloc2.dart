import 'package:bloc/bloc.dart';
import 'package:counsalter5/SearchDelegateCityTown/city.dart';

class CityProvinceCubit2 extends Cubit<CityCubitModel> {
  CityProvinceCubit2(CityCubitModel state) : super(state);
  cityTitle(String title){state.parent.title=title;}
  void setProvinceCity(List<City> provinceCity) {
//    state.currentList.addAll(provinceCity);
    CityCubitModel.cities = provinceCity;
    return emit(CityCubitModel.copy(state));
  }
  void setCity(City city){
    print("city **===> ${ city.cityName }");
    state.city=city;
    refresh();
  }
  void setCityCode(City item){
    state.parent.code=item.code;
    print("__state.parent.code ===> ${state.parent.code  }");
    refresh();
  }

  void setCityId(City item){
    state.parent.id=item.id;
    print("___state.parent.id ===> ${ state.parent.id }");
    refresh();
  }
  refresh() {
    return emit(CityCubitModel.copy(state));
  }

  void removeCity(City item) {
    item.isActive = false;
    refresh();
  }

  void addCity(City item) {
    if (state.selectedCities == null) {
      state.selectedCities= [];
    }
    state.selectedList.add(item);
    refresh();
  }

  void addAllActiveCityList(List<City> cityList){
    if(state.activeCitiesList == null){
      state.activeCitiesList = [];
    }
    state.activeCitiesList.addAll(cityList);
    refresh();
  }

  void incLevel() {
    state.level++;
    refresh();
  }

  void decLevel() {
    if(state.level == 1) return;
    state.level--;
    state.parent = state.parent.parent;
    refresh();
  }

  void clean({City city}) {
    // return emit(City.clear());
  }

  void setActiveCity(City city) {
    CityCubitModel.activeCity = city;
    state.parent = city;
    refresh();
  }

  void setList(List<City> cities) {
    List<City> fin = [];
    cities.forEach((element) {
      fin.add(element);
      (element.children ?? []).forEach((element) {
        fin.add(element);
        (element.children ?? []).forEach((element) {
          fin.add(element);
          (element.children ?? []).forEach((element) {
            fin.add(element);
            (element.children ?? []).forEach((element) {
              fin.add(element);
              (element.children ?? []).forEach((element) {
                fin.add(element);
              });
            });
          });
        });
      });
    });
    CityCubitModel.cities = fin;
    refresh();
  }

  returnChild(City city) {
    if (city.children != null && city.children.length > 0) {
      // return ;
    }
    return city;
    // return []..addAll(returnChild(city.children));
  }

  void toggleCity(City e, bool val) {
    e.isActive = val;
    if (val) {
      if (e.selectedList == null) {
        e.selectedList = [];
      }
      e.selectedList.add(e);
    }else if (e?.selectedList?.contains(e) ?? false) {
      e.selectedList.remove(e);
    }
    refresh();
  }

  void selectAll() {
    if (state.isSelectAll) {
      state.activeCities = [];
    } else {
      state.activeCities = CityCubitModel?.activeCity?.children;
    }
    refresh();
  }

  void clear() {
    state.level = 1;
    state.parent = null;
    CityCubitModel.cities.forEach((element) {element.isActive = false;});

    refresh();
  }

  void removeCities(List<City> children) {
    state.activeCities.removeWhere((element) => children.contains(element));
    refresh();
  }


  void setLevel(int i) {
    state.level = i;
    refresh();
  }

  void clearChildren(City c) {

    CityCubitModel.cities.where((element) => element.parentId == c.id).forEach((element) {element.isActive = false;});
  }
}
