import 'package:counsalter5/HomePage.dart';
import 'package:counsalter5/SearchDelegateCityTown/city.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/DoctorInformation/DoctorInformationBasicInfo.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../config.dart';

class ShowProvinceCity extends StatefulWidget {
  City model;
  List<City> cityProvince = [];
  bool multiSelectMode = true;
  int maxLevel = 2;
  TextEditingController controller;

  @override
  _ShowProvinceCityState createState() => _ShowProvinceCityState();

  ShowProvinceCity(
      {this.model,
      this.cityProvince,
      this.multiSelectMode = true,
      this.maxLevel = 2});
}

class _ShowProvinceCityState extends State<ShowProvinceCity> {
  CityProvinceCubit get cityBloc => context.bloc<CityProvinceCubit>();
  List b = [];
  List<City> activeCity = [];

  List<City> get cityList {
    List a = cityBloc.state.currentList.where((element) {
      if (textSearch == null || textSearch == '') return true;
      else
      return element.title.contains(textSearch);
    }).toList();
//    if (cityBloc.state.parent != null) {
//      a.insert(0, cityBloc.state.parent);
//    }
//    if (cityBloc.state.currentList.map((e) => e.parent) != null) {
//      a.insert(0, cityBloc.state.parent);
//    }
//    print("cityBloc.state.currentList.map((e) => e.title ===> ${ cityBloc.state.currentList.map((e) => e)}");
    print("cityBloc.state.currentList.map((e) => e.title ===> ${ cityBloc.state.currentList.map((e) => e.children.map((e) => e.title))}");
    return a;
  }
//   cityBloc.state.currentList.map((e) => e.children.map((e) => e.title));


//List<City> get cityChildren{
//    List b=cityBloc.state.currentList.where((element) {
//      if (cityBloc.state.parent==null) return true;
//      List<City> c=<City> element.children.map((e) => e.title).toList();
//      cityBloc.setProvinceCity(c);
////        cityBloc.state.currentList.map((e) => e.children.map((e) => e.title));
//    }).toList();
//    print("b ===> ${ b }");
//    return b;
//}
  String textSearch;
  Future setCityFilter() async {
    print('666 ===> ${666}');
    // final storage = new FlutterSecureStorage();

    final SharedPreferences sDelegate = await SharedPreferences.getInstance();
    cityBloc.addAllActiveCityList(CityCubitModel.myCities.toSet().toList());

    Conf.cityFilter = CityCubitModel.myCities.length == 1 &&
            CityCubitModel.myCities.first.level == 2
        ? CityCubitModel.myCities.first.title
        : '${CityCubitModel.getCityLen} شهر';
    Conf.cityFilterId = CityCubitModel.myCities.map((e) => e.id).toList();
//    await sDelegate.setString('cityFilter', Conf.cityFilter);
//    await sDelegate.setStringList('cityFilterId', Conf.cityFilterId);

    Navigator.pushAndRemoveUntil(
        context,
        PageTransition(
            child: DoctorInformationBasicInfo(),
            type: PageTransitionType.leftToRightWithFade),
        (route) => false);
  }

  firstRun() async {
    print('666 ===> ${666}');
    final SharedPreferences sDelegate = await SharedPreferences.getInstance();
    b = sDelegate.getStringList('cityFilterId');
  }

  @override
  void initState() {
    if (cityBloc.state.level == 2) {
      cityBloc.decLevel();
    }
    CityCubitModel.sync();
    firstRun();
    widget.controller = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    print("cityBloc.state.currentList.ma ===> ${ cityBloc.state.currentList.map((e) => e.title).toList() }");
    return SafeArea(
        top: true,
        bottom: true,
        child: BlocBuilder<CityProvinceCubit, CityCubitModel>(
            builder: (context, state) {
          return WillPopScope(
            onWillPop: () {
              if (cityBloc.state.level == 1) {
                Navigator.pop(context);
                return;
              }
              cityBloc.decLevel();
            },
            child: Scaffold(
              bottomNavigationBar: Container(
                  height: Conf.p2w(30),
                  decoration: Conf.boxDecoration(),
                  padding: Conf.xlEdge,
                  width: double.infinity,
                  child: MasRaisedButton(
                    onPress: () async {
                      await setCityFilter();
                    },
                    text: 'تایید',textColor: Conf.lightColor,
                  )),
              appBar: AppBar(
                automaticallyImplyLeading: false,
                flexibleSpace: Column(
                  children: [
                    Row(
                      children: [
                        IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Conf.lightColor,
                            ),
                            onPressed: cityBloc.state.level == 1 &&
                                    getHeadCities().isEmpty
                                ? () async {
                                    await setCityFilter();
                                  }
                                : () {
                                    if (cityBloc.state.level == 1) {
                                      setCityFilter();
                                      // Navigator.pop(context);
                                      return;
                                    }

                                    cityBloc.decLevel();
                                  }),
                        Expanded(
                          child: Padding(
                            padding: Conf.xlEdge.copyWith(right: 0, left: 0),
                            child: Container(
                              margin: EdgeInsets.all(16).copyWith(bottom: 0),
                              width: Conf.p2w(100),
                              height: Conf.p2w(15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(13),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.2)),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
//            blurRadius: 10,spreadRadius: 1,
                                        offset: Offset(-1, 2.7))
                                  ]),
                              child: IntrinsicWidth(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(
                                            right: Conf.p2w(3.5)),
                                        child: IconButton(
                                          icon: Icon(
                                            FeatherIcons.search,
                                            size: Conf.p2w(3.5),
                                            color: Colors.grey.withOpacity(0.9),
                                          ),
                                          onPressed: () {
                                            widget.controller.text = "";
                                          },
                                        )
                                        ),
                                    Expanded(
                                      child: TextField(
                                        expands: true,
                                        minLines: null,
                                        maxLines: null,
                                        enableSuggestions: true,
                                        enabled: true,
                                        onChanged: (value) {
                                          setState(() {
                                          textSearch = value;
//                                          cityBloc.contains(textSearch);
//                                          print("a ===> ${ a }");
                                          });
                                        },
                                        onTap: () {
                                          FocusScopeNode currentFocus =
                                              FocusScope.of(context);
                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        textDirection: TextDirection.rtl,
                                        style: TextStyle(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.bold),
                                        controller: widget.controller,
                                        decoration: InputDecoration(
                                          hintStyle: TextStyle(
                                              height: 2.5,
                                              color: Colors.black45,
                                              fontWeight: FontWeight.w600,
                                              fontSize: Conf.p2t(15)),
                                          isDense: true,
                                          border: InputBorder.none,
                                          contentPadding: EdgeInsets.only(
                                              top: Conf.p2w(2.5),
                                              right: Conf.p2w(2.0839)),
                                          hintText: 'جست و جوی شهر',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                            icon: Icon(Icons.refresh,color: Conf.lightColor),
                            onPressed: () {cityBloc.clear();})
                      ],
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.only(right: Conf.xlSize),
                        child: Row(
                          children: state.activeCities == null
                              ? <Widget>[]
                              : getHeadCities(state: state)
                                  ?.map((e) => Padding(
                                        padding:
                                            EdgeInsets.only(left: Conf.xlSize),
                                        child: Chip(
                                          label: Text(state.level <= e.level
                                              ? e.title
                                              : ' همه شهرهای ${e.title}'),
                                          onDeleted: () {
                                            cityBloc.removeCity(e);
                                          },
                                          backgroundColor: Conf.lightColor,
                                          deleteIconColor: Conf.primaryColor,
                                        ),
                                      ))
                                  ?.toList(),
                        ),
                      ),
                    )
                  ],
                ),
                bottom: PreferredSize(
                    child: Container(),
                    preferredSize: Size.fromHeight(Conf.p2w(15))),
              ),
              body: SingleChildScrollView(
                child: widget.model != null
                    ? Column(
                        children: cityList.map(
                        (e) {
                          // print('e ===> ${state.activeCities?.contains(e)}');
                          return Column(
                            children: [
                              Row(
                                children:
                                [
                                  Checkbox(
                                    onChanged: (value) {
                                      cityBloc.toggleCity(e, value);
                                    },
                                    value: e.isActive,
                                    activeColor: Conf.primaryColor,
                                  ),
                                  Text(
                                      state.level <= e.level
                                      ?
                                      "همه شهرهای"
                                      : ' همه شهرهای ${e.title}'
                                  ),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: Conf.xlSize),
                                child: Conf.divider(),
                              )
                            ],
                          );
                        },
                      ).toList())
                    : Column(
                        children: cityList
                            .where((element) {
                              if (textSearch == null || textSearch == '')
                                return true;
                              return element.title.contains(textSearch);
                            })
                            .map(
                              (item) => Column(
                                children: [
                                  ListTile(
                                    title: Text(state.level <= item.level
                                        ? item.title
                                        : ' همه شهرهای ${item.title}'),
                                    trailing: getTrailingWidget(item),
                                    onTap: () {
                                      if (!(item.children != null &&
                                          item.children.isNotEmpty)) {
                                        cityBloc.toggleCity(
                                            item, !item.isActive);
                                      }
                                      if (item.level == cityBloc.state.level) {
                                        if (item.children != null &&
                                            item.children.isNotEmpty) {
                                          cityBloc.incLevel();
                                          cityBloc.setActiveCity(item);
                                        }
                                      }
                                    },
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: Conf.xlSize),
                                    child: Conf.divider(),
                                  )
                                ],
                              ),
                            )
                            .toList(),
                      ),
              ),
            ),
          );
        }));
  }

  Widget getTrailingWidget(City element) {
    return !(widget.multiSelectMode && widget.maxLevel == cityBloc.state.level)
        ? Icon(
            Icons.arrow_back_ios_sharp,
            size: Conf.p2w(4),
          )
        : Checkbox(
            activeColor: Conf.primaryColor,
            onChanged: (val) {
              if (element.level < cityBloc.state.level) {
                cityBloc.clearChildren(element);
              }

              if (val &&
                  element.parentId != null &&
                  !element.parent.isActive &&
                  element.parent.children
                          .where((element) => element.isActive)
                          .length ==
                      element.parent.children.length - 1) {
                cityBloc.clearChildren(element.parent);
                cityBloc.toggleCity(element.parent, true);
                return;
              }

              if (!val && element.parentId != null && element.parent.isActive) {
                CityCubitModel.cities
                    .where((el) =>
                        el.parentId == element.parentId && el.id != element.id)
                    .forEach((ele) {
                  ele.isActive = true;
                });
                element.parent.isActive = false;
              }

              cityBloc.toggleCity(element, val);
            },
            value: element.isActive ||
                ((element?.parent?.isActive ?? false) &&
                    element.children
                        .every((element) => element.isActive == false)),
          );
  }

  List<City> getHeadCities({CityCubitModel state}) {
    return CityCubitModel.cities
            .where((element) => element.isActiveParentNonActive)
            .toList() ??
        [];
  }
}
