import 'package:counsalter5/HomePage.dart';
import 'package:counsalter5/IncreaseMoneyPriceComponent.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/ModelBloc/WalletModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
class IncreaseMoneyEdited extends StatefulWidget {
  @override
  _IncreaseMoneyEditedState createState() => _IncreaseMoneyEditedState();
}

class _IncreaseMoneyEditedState extends State<IncreaseMoneyEdited> {
  TextEditingController controllerPrice = new TextEditingController();
  String text = '',price;
  bool isKeyboardOpen = false;
  Function onTap;
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(17),
      fontWeight: FontWeight.bold,
      color: Colors.black87);
  TextStyle numberStyle = TextStyle(
      fontSize: Conf.p2t(19),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  final myController = TextEditingController();
  var priceArr = [10000, 30000, 50000];

  _IncreaseMoneyEditedState({this.isKeyboardOpen, this.onTap});

  createCart() {
    return Padding(
      padding: EdgeInsets.only(right: Conf.p2w(4), top: Conf.p2h(2)),
      child: Row(
        children: priceArr
            .map((item) => IncreaseMoneyPriceComponent(
                  isGreyColor: true,
                  containerPrice: item,
                  onTap: () {
                    print("item ===> ${item}");
                    controllerPrice.value =
                        TextEditingValue(text: item.toString());
//                      print("controllerPrice ===> ${ controllerPrice }");
                    setState(() {
//                        selectPrice(containerPrice);
                    });
                  },
//                    model: item,
                ))
            .toList(),
      ),
    );
  }

  @override
  void initState() {
    createCart();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: BlocBuilder<WalletBloc, WalletRepository>(
        builder: (context, state) {
      return
          Scaffold(
              bottomNavigationBar: Container(
//                width: Conf.p2w(50),
                  height: Conf.p2w(22),
                  color: Conf.grayColor200,
                  child:
//            Container()
                      MasRaisedButton(
                margin: Conf.xlEdge,
                    text: "تایید",
                    textColor: Conf.lightColor,
                    onPress: () {
                    price=  state.wallet.amount==0||state.wallet.amount==null?"0"+controllerPrice.text:state.wallet.amount.toString()+controllerPrice.text;
                    print("price ===> ${ price }");
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.leftToRight,
                              duration: Duration(milliseconds: 500),
                              alignment: Alignment.centerLeft,
                              curve: Curves.easeOutCirc,
                              child: HomePage()));
                    },
                  )),
              body: Container(
//              color: Colors.brown,
                child: Stack(children: [
                  Positioned(
                    width: Conf.p2w(100),
                    top: Conf.p2h(4),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("افزایش موجودی",
                            style: Conf.headline.copyWith(
                                color: Colors.black87,
                                fontSize: Conf.p2t(25),
                                fontWeight: FontWeight.w500,
                                decoration: TextDecoration.none)),
                      ],
                    ),
                  ),
                  Positioned(
                    top: Conf.p2h(4),
                    left: Conf.p2w(6),
                    child: Column(
                      children: [
                        MasBackBotton()
//                ],)
//              ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: Conf.p2w(0),
                    left: Conf.p2w(0),
                    bottom: Conf.p2w(0),
                    top: Conf.p2w(23),
                    child: Container(
                      height: Conf.p2h(82),
                      decoration: BoxDecoration(
                          color: Conf.grayColor200,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 8,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(Conf.p2w(6.25)),
//                  topRight: Radius.circular(30),
                              topLeft: Radius.circular(Conf.p2w(6.25))
//                  topLeft: Radius.circular(30)
                              )),
                      child: Flex(
                        direction: Axis.vertical,
                        children:[ Expanded(
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(Conf.p2w(6.25)),
//                  topRight: Radius.circular(30),
                                topLeft: Radius.circular(Conf.p2w(6.25))
//                  topLeft: Radius.circular(30)
                            ),
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: Conf.p2w(9), top: Conf.p2h(2)),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "موجودی فعلی:   ",
                                          style: txtStyle,
                                        ),
                                        Text(
                                          state.wallet.amount==0||state.wallet.amount==null?"0":state.wallet.amount.toString(),
                                          style: numberStyle,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: Conf.p2h(0.75)),
                                          child: Text(
                                            "  تومان",
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: Conf.p2t(15),
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2h(4)),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: Conf.p2w(50),
                                          child: TextFormField(
                                            controller: controllerPrice,
                                            showCursor: true,
                                            textDirection: TextDirection.ltr,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700,
                                                fontSize: Conf.p2t(20)),
//                          initialValue: "0",
//                          onTap: () {
//                            setState(() {
//                              if (widget.onTap is Function) {
//                                widget.onTap();
////                                setState(() {
////                                  if(widget.isKeyboardOpen==false){widget.isKeyboardOpen=true;}
////                                  else {widget.isKeyboardOpen=true;}
////                                });
//                              }
//                            });
//                          },
                                            keyboardType: TextInputType.number,
                                            textAlign: TextAlign.center,
//                      controller: _controller1,
                                            decoration: InputDecoration(
                                                hintStyle: new TextStyle(
                                                    height: 0.5,
                                                    fontSize: Conf.p2t(14),
                                                    color:
                                                    Colors.black45.withOpacity(0.3),
                                                    fontWeight: FontWeight.w500),
                                                hintText: 'مبلغ مورد نظر'),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: Conf.p2w(6), right: Conf.p2w(2)),
                                          child: Text(
                                            "تومان",
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
//                          Container(
//                            width: Conf.p2w(90),
//                            height: Conf.p2h(0.25),
//                            color: Colors.grey.withOpacity(0.3),
//                          ),
                                  createCart(),
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2h(5)),
                                    child: Container(
                                      width: Conf.p2w(70),
                                      height: Conf.p2h(35),
                                      decoration: BoxDecoration(
                                        color: Colors.grey.shade200,
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
//                              colorFilter: ColorFilter.linearToSrgbGamma(),
                                          image: AssetImage(
                                            "assets/images/increasemoneyedited.png",
//                                "assets/images/Doctor.jpg",
                                          ),
                                        ),
//                      boxShadow: [
//                        BoxShadow(
//                            color: Colors.grey.withOpacity(0.5), blurRadius: 8)
//                      ],
//                          borderRadius: BorderRadius.only(
//                              bottomLeft: Radius.circular(30),
//                              bottomRight: Radius.circular(30))
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),]
                      ),
                    ),
                  ),
                ]),
              ));}
        ));
  }
}
