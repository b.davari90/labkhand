import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounsalterAboutcomponent extends StatefulWidget {
  DoctorModel model;
  Function function;
  BuildContext context;
  TextStyle txtStyletitle = Conf.subtitle.copyWith(
      fontSize: Conf.p2t(20),
      fontWeight: FontWeight.w700,
      color: Colors.black87);
  TextStyle txtStyleSubtitle = Conf.subtitle.copyWith(
      fontSize: Conf.p2t(14),
      fontWeight: FontWeight.w500,
      color: Colors.black87);
  TextStyle txtStyleRate =
      TextStyle(fontWeight: FontWeight.bold, fontSize: Conf.p2t(16));
  bool loading = true;
  List<DoctorModel> doctorModelList;

  CounsalterAboutcomponent({this.model});

  @override
  _CounsalterAboutcomponentState createState() =>
      _CounsalterAboutcomponentState();
}

class _CounsalterAboutcomponentState extends State<CounsalterAboutcomponent> {
  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  Future getDoctorList() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get('cu/doctor');
      print(data);
      List doctorList = data;
      print("doctorList ===> ${doctorList.toString()}");
      widget.doctorModelList =
          doctorList.map((item) => DoctorModel.fromJson(item)).toList();
      DoctorModel doctorModel = DoctorModel();
      doctorModel.avatar = data["avatar"];
      doctorModel.fname = data["fname"];
      doctorModel.lname = data["lname"];
      print("___doctorModel.fname ===> ${doctorModel.fname}");
      print("___doctorModel.lname ===> ${doctorModel.lname}");
      doctorBloc.setDoctor(doctorModel);
      print("____getDoctorList ===> ${doctorList.toString()}");
    } catch (e) {
      print("CATCH ERROR getDoctorList===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  @override
  void initState() {
    print("_____ Conf.token.toString() ===> ${Conf.token.toString()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: EdgeInsets.only(right: Conf.p2w(9), top: Conf.p2h(3)),
        child: Flex(
          direction: Axis.vertical,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text(" مشاور", style: widget.txtStyletitle),
                    Text(widget.model.fname + " ", style: widget.txtStyletitle),
                    Text(widget.model.lname, style: widget.txtStyletitle)
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: Conf.p2w(4)),
                  child: Row(
                    children: [
                      Icon(Icons.star,
                          color: Colors.yellow.shade600, size: Conf.p2t(18)),
                      Text(
                        "(",
                        style: widget.txtStyleRate,
                      ),
                      Text(
                        "10 ",
                        style: TextStyle(
                            fontSize: Conf.p2t(15),
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "نظر",
                        style: TextStyle(
                            fontSize: Conf.p2t(Conf.p2w(2.2)),
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        " )",
                        style: widget.txtStyleRate,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: Conf.p2w(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "متخصص",
              style: widget.txtStyleSubtitle,
            ),
            Text(
              widget.model.specialty,
              style: widget.txtStyleSubtitle,
            )
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: Conf.p2h(4)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                width: Conf.p2w(10),
                height: Conf.p2h(2),
                decoration: BoxDecoration(
                    color: Conf.blue,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10)))),
            Padding(
              padding: EdgeInsets.only(right: Conf.p2w(1)),
              child: Text(
                "درباره مشاور",
                style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w700,
                    fontSize: Conf.p2t(18)),
              ),
            ),
          ],
        ),
      ),
      FittedBox(
        fit: BoxFit.contain,
        child: Padding(
          padding: EdgeInsets.only(top: Conf.p2h(1), right: Conf.p2w(2)),
          child: Container(
            width: Conf.p2w(80),
            child: Text(widget.model.description,
                style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w600,
                    fontSize: Conf.p2t(16)),
                maxLines: 5,
                overflow: TextOverflow.visible),
          ),
        ),
      ),
      Row(
        children: [
          Container(
            width: Conf.p2w(10),
            height: Conf.p2h(2),
            decoration: BoxDecoration(
                color: Conf.blue,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10))),
          ),
          Padding(
            padding: EdgeInsets.only(right: Conf.p2w(1), top: Conf.p2h(1)),
            child: Text(
              "تخصص های مشاور",
              style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w700,
                  fontSize: Conf.p2t(18)),
            ),
          ),
        ],
      ),
//      widget.model.doctor_collections.map((e) => e.collection)==null?Container(child: Text("qqaq"),):Container(child: Text("sssss"),)
      Padding(
        padding: EdgeInsets.symmetric(horizontal: Conf.xlSize - 2),
        child: Container(
          alignment: Alignment.topRight,
          margin: EdgeInsets.only(right: Conf.p2w(7), top: Conf.p2w(5)),
          child:
//          widget.model.doctor_collections.map((e) => e.collection.title)==null?Text(""):
              SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Wrap(
                      textDirection: TextDirection.rtl,
                      runSpacing: Conf.p2w(1.9),
                      spacing: Conf.p2w(2),
                      children: widget.model.doctor_collections
                                  .map((e) => e.collection.title)
                                  .toList() ==
                              null
                          ?
//                widget.model.doctor_collections == null ?[]:
//                widget.model.doctor_collections?.map((e) =>e.collection.title==null?
                          widget.model.doctor_collections
                              .map((e) => blueBoxContainerCounsalterAbout(""))
                              .toList()
                          : widget.model.doctor_collections
                              .map((e) => blueBoxContainerCounsalterAbout(
                                  e.collection.title))
                              .toList())
////        :
//            Wrap(textDirection: TextDirection.rtl,
//              runSpacing: Conf.p2w(1.Docto9),
//              spacing: Conf.p2w(2),
//              children:widget.model.doctor_collections.map((e) =>blueBoxContainerCounsalterAbout(e.collection.title)).toList(),)

////    Wrap(
////    textDirection: TextDirection.rtl,
////    runSpacing: Conf.p2w(1.9),
////    spacing: Conf.p2w(2),
////    children:
////    widget.model.doctor_collections.map((e) => e.collection.title).toList()==null?
//////                widget.model.doctor_collections == null ?[]:
//////                widget.model.doctor_collections?.map((e) =>e.collection.title==null?
////    widget.model.doctor_collections.map((e) =>blueBoxContainerCounsalterAbout("")).toList():
////    widget.model.doctor_collections.map((e) =>blueBoxContainerCounsalterAbout(e.collection.title)).toList()
////    )
//            ),
                  ),
        ),
      ),
    ]);
  }

  Widget blueBoxContainerCounsalterAbout(String title) {
    return Padding(
      padding: EdgeInsets.only(right: 0),
      child: IntrinsicWidth(
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(color: Conf.blue, width: 1.5)),
          margin: EdgeInsets.only(left: Conf.p2w(0.2), right: Conf.p2h(0.25)),
//    width: Conf.p2w(20),
//    height: Conf.p2h(4),
          child: Center(
              child: FittedBox(
            fit: BoxFit.cover,
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(
                    top: Conf.p2h(0.25),
                    bottom: Conf.p2h(0.25),
                    right: Conf.p2w(0.9),
                    left: Conf.p2w(0.9)),
                child: Text(
                  title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: Conf.p2t(12),
                      color: Colors.black87),
                ),
              ),
            ),
          )),
        ),
      ),
    );
  }
}
