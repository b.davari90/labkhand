import 'package:counsalter5/CoustomerPage.dart';
import 'package:counsalter5/CustomerModel.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:image_picker/image_picker.dart';
import 'config.dart';
import 'package:counsalter5/htify.dart';
import 'CustomerModel.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:counsalter5/GetUrl.dart';
import 'package:flutter/widgets.dart';
import 'package:flash/flash.dart';
import 'MasBackBotton_ui.dart';
import 'raised_button_ui.dart';
import 'package:counsalter5/ModelBloc/DocumentModel.dart';
import 'package:counsalter5/ModelBloc/CustomerChilderenModel.dart';
import 'htify.dart';

class CounsalterProfileChilderen extends StatefulWidget {
  TextStyle txtStyle = TextStyle(
      height: 0.5,
      fontSize: Conf.p2t(15),
      color: Colors.black54.withOpacity(0.4),
      fontWeight: FontWeight.w900);
  String dpValSingel, dpValueKind;
  GetUrl getUrl;
  bool  loading1 = false;
  TextStyle txtWhiteStyle = TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: Conf.p2t(15));
  CustomerChilderenModel userLogin, customerModel;

  final _formKey = GlobalKey<FormBuilderState>();
  TextEditingController controllergender,
      controllerMarried,
      controllerName,
      controllerFamily,
      controllerEmail;
  File _image;
  String avatar="";
  SubDataDocument subData;
  List<String> genderList = ["زن", "مرد"];
  List<String> marriedList = ["مطلقه", "متاهل", "مجرد"];

  @override
  _CounsalterProfileChilderenState createState() => _CounsalterProfileChilderenState();
}
class _CounsalterProfileChilderenState extends State<CounsalterProfileChilderen> {
  CoustomerChilderenBloc get bloc => context.bloc<CoustomerChilderenBloc>();
  CustomerChilderenModel customer = CustomerChilderenModel();
  final _formKey = GlobalKey<FormBuilderState>();
  int index2, index;
  bool loading=false;
  CustomerModel customerModel=CustomerModel();
  CoustomerBloc get coustomerBloc => context.bloc<CoustomerBloc>();

  void _showPicker(context) {
    showModalBottomSheet(
        barrierColor: Conf.transparentColor,
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: Conf.p2w(40),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.8),
                    spreadRadius: 1,
                    blurRadius: 10,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                color: Conf.blue,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
            child: Padding(
              padding: EdgeInsets.only(top: Conf.p2w(5), right: Conf.p2w(5)),
              child: Wrap(
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: Conf.xlSize),
                          child: Icon(
                            FeatherIcons.folder,
                            size: Conf.p2w(4),
                            color: Colors.white,
                          ),
                        ),
                        Text('انتخاب از گالری',style:widget.txtWhiteStyle),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(top: Conf.xlSize, bottom: Conf.xlSize),
                    child: Divider(
                      color: Colors.white70,
                      thickness: 0.5,
                      endIndent: 20,
                      indent: 10,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: Conf.xlSize),
                          child: Icon(FeatherIcons.camera,
                              color: Colors.white, size: Conf.p2w(4)),
                        ),
                        Text(
                          'دوربین',
                          style:widget.txtWhiteStyle,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      widget._image = image;
      widget.avatar=widget._image.path.split('/').last;
      sendDataDocument(
          imagePath:widget. _image.path,
          title: "",
          avatar: widget.avatar,
          thumb: 0,
          type: 1,
          waterMark: 0);
    });
  }
  _imgFromGallery() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      widget._image = image;
      widget.avatar=widget._image.path.split('/').last;
      bloc.setAvatar(widget.avatar);
      print("***widget.avatar ===> ${ widget.avatar }");
      sendDataDocument(
          imagePath: widget._image.path,
          title: widget.avatar,
          avatar:widget.avatar,
          thumb: 0,
          type: 1,
          waterMark: 0);
    });
  }
  Future sendDataDocument(
      {int fileStatue,String title,
        String avatar,
        String imagePath,
        int waterMark,
        int thumb,
        int type,
      }) async {
    try {
      FormData formData = FormData.fromMap({"data":{
        "title": title,
        "has_watermark": waterMark,
        "has_thumb": thumb,
        "type": type,
        "file": await MultipartFile.fromFile(imagePath, filename: title),
      }

      });
      var response = await Htify.sendFile("cu/file", formData);
      print("***12112response ===> ${ response }");
      CustomerChilderenModel c=CustomerChilderenModel();
      c.avatar=response["src"];
      print("c.avatar ===> ${ c.avatar }");
      bloc.setUser(c);
      print("2121 ===> ${ 2121 }");
    } catch (e) {
      print("@@@@@ sendAvatar   e ===> ${e.toString()}");
    }
  }
  @override
  void initState() {
    print("init state Conf.token ===> ${Conf.token}");
    widget.controllergender = TextEditingController();
    widget.controllerMarried = TextEditingController();
    widget.controllerFamily = TextEditingController();
    widget.controllerName = TextEditingController();
    widget.controllerEmail = TextEditingController();
    widget.subData=SubDataDocument();
    widget.userLogin = CustomerChilderenModel();
    print("___bloc.state.user.avatar ===> ${ bloc.state.userChild.avatar }");
    super.initState();
  }
  @override
  void dispose() {
    widget.controllergender.dispose();
    widget.controllerMarried.dispose();
    widget.controllerName.dispose();
    widget.controllerFamily.dispose();
    widget.controllerEmail.dispose();
    super.dispose();
  }
  _CounsalterProfileChilderenState({this.index2, this.index});
  Future setProfileInfo() async {
    print("sending start.... ===> ${setProfileInfo.toString()}");
    print("bloc.state.userChild.avatar ===> ${ bloc.state.userChild.avatar }");
    print("bloc.state.userChild.fname ===> ${ bloc.state.userChild.fname }");
    print("bloc.state.userChild.lname ===> ${ bloc.state.userChild.lname }");
    print("index ===> ${ index }");
    print("index2 ===> ${ index2}");
    print("bloc.state.userChild.gender ===> ${ bloc.state.userChild.gender }");
    print("bloc.state.userChild.married ===> ${ bloc.state.userChild.married }");
    print("bloc.state.userChild.email ===> ${ bloc.state.userChild.email }");
    print("bloc.state.userChild.mobile ===> ${ bloc.state.userChild.mobile }");
    setState(() {
      widget.loading1 = false;
    });
    try {
      setState(() {
        loading=true;
      });
      var data = await Htify.create("cu/customer", {
        "data": {
          "avatar": bloc.state.userChild.avatar??"",
          "fname": bloc.state.userChild.fname??"",
          "lname": bloc.state.userChild.lname??"",
            "mobile": bloc.state.userChild.mobile,
          "gender": index,
          "married": index2,
          "email": bloc.state.userChild.email??"",
        }
      });
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: CoustomerPage(
                gender: index,
                single: index2,
              )));
    } catch (e) {
      print("CAtch ERROR setProfileInfo() ===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            resizeToAvoidBottomPadding: true,
            resizeToAvoidBottomInset: true,
            body: BlocBuilder<CoustomerChilderenBloc, CustomerChilderenRepository>(
                builder: (context, state) {
                  return Column(children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Padding(
                        padding: EdgeInsets.only(left: Conf.p2w(4), top: Conf.p2h(3)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [MasBackBotton()],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        _showPicker(context);
                      },
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100.0),
                            child: Container(
                              color: Colors.grey.withOpacity(0.2),
                              width: Conf.p2w(34),
                              height: Conf.p2w(34),
                              child:
                              Conf.image(state.userChild.avatar)
//                                  ??
//                  Conf.image(null),
                            ),
                          ),
                          Positioned(
                            top: Conf.p2h(10),
                            bottom: Conf.p2h(0),
                            right: Conf.p2w(0),
                            child: Container(
                                width: Conf.p2w(8),
                                height: Conf.p2w(8),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                          spreadRadius: Conf.p2w(0.055),
                                          color: Conf.blue)
                                    ]),
                                child: Icon(
                                  FeatherIcons.camera,
                                  color: Conf.blue,
                                )),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: Conf.p2h(5)),
                        width: Conf.p2w(100),
                        height: Conf.p2h(62),
                        decoration: BoxDecoration(
                            color: Colors.white10.withOpacity(0.7),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 8,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30))),
                        child: FormBuilder(
                          key: widget._formKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: SingleChildScrollView(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2w(2)),
                                    child: Conf.textFieldFormBuilder(
                                        "نام",
                                        "fname",
                                            () {},
                                        false,
                                        Icons.call,
                                        null,
                                        [
                                          FormBuilderValidators.required(
                                              errorText: "*الزامی"),
                                          FormBuilderValidators.min(4,
                                              errorText: "حداقل 3 کارکتر "),
                                        ],
                                        TextInputType.text,
                                        true,
//                                        initialValue: state.user.fname ?? "",
                                        isActive: false
//                                initialValue:bloc?.state?.user?.fname??""
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2w(2)),
                                    child: Conf.textFieldFormBuilder(
                                        " نام خانوادگی",
                                        "lname",
                                            () {},
                                        false,
                                        null,
                                        null,
                                        [
                                          FormBuilderValidators.required(
                                              errorText: "*الزامی"),
                                          FormBuilderValidators.min(3,
                                              errorText: "حداقل 3 کارکتر "),
                                        ],
                                        TextInputType.text,
                                        true,
//                                        initialValue: state.user.lname ?? "",
                                        isActive: false),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2w(2)),
                                    child: Conf.textFieldFormBuilder(
                                        "ایمیل",
                                        "email",
                                            () {},
                                        false,
                                        null,
                                        null,
                                        [
                                          FormBuilderValidators.required(
                                              errorText: "*الزامی"),
                                          FormBuilderValidators.minLength(11,
                                              errorText:
                                              "حداقل عبارت ایمیل 11 کارکتر میباشد"),
                                        ],
                                        TextInputType.text,
                                        true,
//                                        initialValue: state.user.email ?? "",
                                        isActive: false),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(top: Conf.p2w(2)),
                                      child: Conf.textFieldFormBuilder(
                                          "موبایل",
                                          "mobile",
                                              () {},
                                          false,
                                          null,
                                          null,
                                          [   FormBuilderValidators.required(
                                              errorText: "*الزامی"),
                                            FormBuilderValidators.minLength(11,errorText: "شماره تلفن باید یازده رقم باشد"),
                                            FormBuilderValidators.maxLength(11,errorText: "شماره تلفن باید یازده رقم باشد")],
                                          TextInputType.number,
                                          true,
//                                          initialValue: state.user.mobile ?? "",
                                          isActive: false)),
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2w(2)),
                                    child: FormBuilder(
                                      key: _formKey,
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                                right: Conf.p2w(5.5),
                                                left: Conf.p2w(5.5)),
                                            child: FormBuilderDropdown(
                                              readOnly: false,
                                              onChanged: (value) {
                                                setState(() {
//                                                  if (state.userChild.gender == 1 ||
//                                                      state.userChild.gender == 2) {
//                                                    state.userChild.gender = 0;
//                                                  }
                                                  if (value == "زن") {
                                                    index = 1;
                                                    print("index 1 onChanged===> ${ index  }");
                                                  }
                                                  if (value == "مرد") {
                                                    index = 2;
                                                    print("index 2 onChanged===> ${ index  }");
                                                  }
                                                });
                                              },
//                                      initialValue: state.user.gender,
                                              attribute: 'gender',
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(
                                                    Radius.circular(13),
                                                  ),
                                                  borderSide: BorderSide(
                                                      color:
                                                      Conf.grey.withOpacity(0.1)),
                                                ),
                                                enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(13)),
                                                    borderSide: BorderSide(
                                                        color: Colors.grey
                                                            .withOpacity(0.4))),
                                              ),
                                              allowClear: true,
                                              hint: Text("جنسیت",
//                                                  state.userChild.gender == 1
//                                                      ? "زن"
//                                                      : state.userChild.gender == 2
//                                                      ? "مرد"
//                                                      : 'جنسیت',
                                                  style: widget.txtStyle),
                                              items: widget.genderList
                                                  .map((e) => DropdownMenuItem(
                                                value:
                                                e,
                                                child: Text('$e'),
                                              ))
                                                  .toList(),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: Conf.p2w(2),
                                                right: Conf.p2w(5.5),
                                                left: Conf.p2w(5.5)),
                                            child: FormBuilderDropdown(
                                              readOnly: false,
//                                      initialValue: state.user.married,
                                              onChanged: (value) {
                                                setState(() {
//                                                  if (state.userChild.married == 1 ||
//                                                      state.userChild.married == 2 ||
//                                                      state.userChild.married == 3) {
//                                                    state.userChild.married = 0;
//                                                  }
                                                  if (value == "متاهل") {
                                                    index2 = 2;
                                                    print("index2 2 onChanged===> ${ index2 }");
                                                  }
                                                  if (value == "مجرد") {
                                                    index2 = 1;
                                                    print("index2 1 onChanged===> ${ index2 }");
                                                  }
                                                  if (value == "مطلقه") {
                                                    index2 = 3;
                                                    print("index2  3 onChanged===> ${ index2 }");
                                                  }
                                                });
                                              },
                                              attribute: 'married',
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(
                                                    Radius.circular(13),
                                                  ),
                                                  borderSide: BorderSide(
                                                      color:
                                                      Conf.grey.withOpacity(0.1)),
                                                ),
                                                enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(13)),
                                                    borderSide: BorderSide(
                                                        color: Colors.grey
                                                            .withOpacity(0.4))),
                                              ),
                                              allowClear: true,
                                              hint: Text(
//                                                  state.user.married == 1
//                                                      ? "مجرد"
//                                                      : state.user.married == 2
//                                                      ? "متاهل"
//                                                      : state.user.married == 3
//                                                      ? "مطلقه"
//                                                      :
                                              'وضعیت تاهل',
                                                  style: widget.txtStyle),
                                              items: widget.marriedList
                                                  .map((e) => DropdownMenuItem(
                                                value:
                                                    e,
                                                child: Text('$e'),
                                              )).toList(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                          width: Conf.p2w(88),
                                          height: Conf.p2h(12),
                                          margin: EdgeInsets.only(
                                              top: Conf.p2w(6), bottom: Conf.p2w(20)),
                                          child: MasRaisedButton(
                                              margin: EdgeInsets.only(
                                                  right: Conf.p2w(0),
                                                  left: Conf.p2w(0),
                                                  top: Conf.p2w(1),
                                                  bottom: Conf.p2w(6)),
                                              borderSideColor: Conf.orang,
                                              text: "تایید",
                                              textColor: Colors.white,
                                              color: Conf.orang,
                                              onPress: () async {
                                                widget._formKey.currentState.save();
                                                print("***********&&&****_formKey.currentState.value.save() ===> ${widget._formKey.currentState.value}");
                                                bool validate = widget._formKey.currentState.validate();
                                                if (validate) {
                                                  var result = widget._formKey.currentState.value;
                                                  customer.fname = result["fname"];
                                                  customer.email = result["email"];
                                                  customer.lname = result["lname"];
                                                  customer.married = result["married"];
                                                  customer.gender = result["gender"];
                                                  customer.mobile = result["mobile"];
                                                  print("validate is ok");
                                                  CustomerChilderenModel t = bloc.state.userChild;
                                                  t.fname = result["fname"];
                                                  t.lname = result["lname"];
                                                  t.email = result["email"];
                                                  t.mobile = result["mobile"];
                                                  t.gender = result["gender"];
                                                  t.married = result["married"];
//                                                  t.gender = index;
//                                                  t.married = index2??state.userChild.married;
                                                  print("t.gender ===> ${t.gender}");
                                                  print(" t.married===> ${t.married}");
                                                  print("t.lname ===> ${t.lname}");
                                                  print("t.mobile ===> ${ t.mobile }");
                                                  bloc.setUser(t);
                                                  print("state.user.gender ===> ${state.userChild.gender}");
                                                  print("state.user.fname ===> ${state.userChild.fname}");
                                                  await setProfileInfo();
                                                } else {
                                                  print("validation failed");
                                                  _showTopFlash();
                                                }
                                              }))
                                    ],
                                  ),
                                ]),
                          ),
                        )),
                  ]);
                }),
          ),
          loading == true
              ? Conf.circularProgressIndicator()
              : Container()
        ],

      ),
    );
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              'اطلاعات بدرستی وارد نشده است',
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
