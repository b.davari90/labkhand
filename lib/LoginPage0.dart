import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:counsalter5/htify.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flash/flash.dart';
import 'LoginPage.dart';
import "CustomerModel.dart";
import "HomePage.dart";
import 'config.dart';
import 'package:shared_preferences/shared_preferences.dart';
class LoginPage0 extends StatefulWidget {
  bool loading = true;
  List<CustomerModel> listContent = [];
  SharedPreferences sharedPreferences;

  @override
  _LoginPage0State createState() => _LoginPage0State();
}

class _LoginPage0State extends State<LoginPage0> {
  Future KnowUser(BuildContext cnx) async {
    widget.sharedPreferences = await SharedPreferences.getInstance();
    String x=await widget.sharedPreferences.getString('token');
    int d=await widget.sharedPreferences.getInt("doctorType");
    int c=await widget.sharedPreferences.getInt("customerType");
    Conf.typeCoustomer=c;
    Conf.typeDoctor=d;
    Conf.token=x;
    print("********## Now User is Log Out ===> ${x}");
    print("Conf.typeCoustomer ===> ${ Conf.typeCoustomer }");
    print("Conf.typeDoctor ===> ${ Conf.typeDoctor }");
  }
  @override
  void initState() {
    KnowUser(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Conf.calcWH(context);
    Conf.textStyle(context);
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Stack(children: <Widget>[
          Container(
            width: double.infinity,
            height: Conf.p2h(100),
            color: Conf.blue,
            child: Padding(
              padding: EdgeInsets.only(top: Conf.p2h(5), right: Conf.p2w(3)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "خوش امدید",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: Conf.p2t(22)),
                  ),
                  Container(
                    width: Conf.p2w(80),
                    height: Conf.p2h(60),
                    child: Image.asset(
                      "assets/images/login1.png",
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: Conf.p2h(2), left: Conf.p2w(3)),
                    child: SizedBox(
                      width: double.infinity,
                      height: Conf.p2w(15),
                      child: RaisedButton(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(13.0),
                        ),
                        splashColor: Colors.blueAccent,
                        elevation: 2,
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: Text(
                            "شروع ",
                            softWrap: true,
                            overflow: TextOverflow.fade,
                            style: Conf.subtitle.copyWith(
                              fontSize: Conf.p2t(20),
                              fontWeight: FontWeight.w700,
                              color: Conf.blue,
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (Conf.token != null&&Conf.typeCoustomer==1 ) {
                            print("IF LoginPAge0 is running....");
                            print(
                                "IF Conf.customerToken != null ===> ${Conf.token}");
                            print(
                                "IF  Conf.loginType == 1 ===> ${Conf.loginType}");
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.leftToRight,
                                    duration: Duration(milliseconds: 500),
                                    alignment: Alignment.centerLeft,
                                    curve: Curves.easeOutCirc,
                                    child: HomePage()));
                          } else if (Conf.token != null&&Conf.typeDoctor==2 ) {
                            print("ELSE IF LoginPAge0 is running....");
                            print("Conf.dorctorToken != null  ===> ${Conf.token}");
                            print(" Conf.loginType == 2 ===> ${Conf.loginType}");
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.leftToRight,
                                    duration: Duration(milliseconds: 500),
                                    alignment: Alignment.centerLeft,
                                    curve: Curves.easeOutCirc,
                                    child: HomePageDoctor()));
                          } else {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.leftToRight,
                                    duration: Duration(milliseconds: 500),
                                    alignment: Alignment.centerLeft,
                                    curve: Curves.easeOutCirc,
                                    child: LoginPage()));
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          widget.loading == false
              ? Conf.circularProgressIndicator()
              : Container()
        ]),
      ),
    );
  }

  Future checkUserExist(BuildContext context) async {
    setState(() {
      widget.loading = false;
    });
    try {
      widget.sharedPreferences = await SharedPreferences.getInstance();
      print(
          "widget.sharedPreferences token===> ${widget.sharedPreferences.getString("token")}");
      if (widget.sharedPreferences.getString("token").isNotEmpty ||
          widget.sharedPreferences.getString("token") != null) {
        getListData();
        print("**************user is Exist before ===> ${getListData()}");
      } else {
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight, child: HomePage()));
      }
    } catch (e) {
      print("******&&&&&&&&&&&&&&****user not exist ===> ${e}");
      _showTopFlash();
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight, child: LoginPage()));
    } finally {
      setState(() {
        widget.loading = true;
      });
    }
  }

  Future getListData() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get("auth/login");
      print(data);
      List contetntList = data;
      print("contetntList ===> ${contetntList}");
      widget.listContent =
          contetntList.map((item) => CustomerModel.fromJson(item)).toList();
      print(widget.listContent);
    } on ServerError catch (e) {
      print("ServerError is running...Catch");
      if (e is ServerError401) {
        showAlertDialog(context, "401 error");
      }
      if (e is ServerError403) {
        showAlertDialog(context, "403 error");
      }
      if (e is ServerError404) {
        showAlertDialog(context, "404 error");
      }
      if (e is ServerError500) {
        showAlertDialog(context, "500 error");
      }
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  showAlertDialog(BuildContext context, String title) {
    Widget okButton = FlatButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: Text("ok"),
    );
    showAlertDialog(context, "Error");
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds:4),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Icon(
              FeatherIcons.alertOctagon,
              color: Colors.red,
              size: 50,
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w400,
                  color: Colors.red),
            ),
            message: Text(
              'اطلاعات وارد شده نامعتبر است...',
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w200,
                  color: Colors.black),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
