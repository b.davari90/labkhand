import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'config.dart';
import 'RegisterNumberLoginPageDoctor.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'htify.dart';

class
RegisterPageDoctor extends StatefulWidget {
  @override
  _RegisterPageDoctorState createState() => _RegisterPageDoctorState();
}

class _RegisterPageDoctorState extends State<RegisterPageDoctor> {
  final _formKey = GlobalKey<FormBuilderState>();
  SharedPreferences sharedPreferences;
  bool isChecked = true;
  DoctorModel doctorLogin;
  bool loading = true;
  bool _passwordVisible = false;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _passwordVisible = !_passwordVisible;
    });
  }
  TextStyle txtStyleOrange = TextStyle(
    color: Conf.orang,
    fontWeight: FontWeight.w700,
    fontSize: 15,
    decoration: TextDecoration.none,
  );
  Future setLogin() async {
    setState(() {
      loading = false;
    });
    try {
      print("doctorLogin.mobile ===> ${doctorLogin.mobile}");
      var data = await Htify.create("auth/login", {
        "username": doctorLogin.mobile,
        "provider": "doctor"
      });
var x=data["id"];
Conf.idLoginDoctor=x;
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: RegisterNumberLoginPageDoctor()
            ));
//      }
      print("%%%%%%%Conf.idDoctor ===> ${ Conf.idLoginDoctor}");

    } catch (e) {
      print("e ===> ${e}");
      _showTopFlash();
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
    doctorLogin = DoctorModel();
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                width: double.infinity,
                color: Conf.blue,
              ),
              Positioned(
                top: Conf.p2h(4),
                width: Conf.p2w(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ثبت نام",
                        style:Conf.body2.copyWith(
                            color: Colors.white.withOpacity(0.9),
                            fontSize: Conf.p2t(20),
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.none)),
//                      ),
                  ],
                ),
              ),
              Positioned(
                width: Conf.p2w(100),
                top: Conf.p2h(4),
                left: Conf.p2h(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    MasBackBotton()
                  ],
                ),
              ),
              Positioned(
                bottom: Conf.p2h(0),
                top: Conf.p2h(15),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: ClipRRect(
                    borderRadius:  BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                    child: SingleChildScrollView(
                      child: FormBuilder(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: Padding(
                          padding: Conf.xlEdge,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "ثبت نام",
                                softWrap: true,
                                overflow: TextOverflow.fade,
                                style: Conf.body1.copyWith(
                                  color: Colors.black87.withOpacity(0.75),
                                  fontWeight: FontWeight.w700,
                                  fontSize: Conf.p2t(20),
                                  decoration: TextDecoration.none,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(0.7)),
                                child: Text("اطلاعات خود را وارد نمائید",
                                    softWrap: true,
                                    overflow: TextOverflow.fade,
                                    style: Conf.body1.copyWith(
                                      color: Colors.black45,
                                      fontWeight: FontWeight.w800,
                                      fontSize: Conf.p2t(19.5),
                                      decoration: TextDecoration.none,
                                    )),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: Conf.p2w(3),
//                                  bottom: Conf.p2w(2)
                                ),
                                child: Conf.textFieldFormBuilder(
                                    'تلفن همراه خود را وارد نمائید...',
                                    "mobile",(){},
                                    true,
                                    Icons.call,null,
                                    [
                                      FormBuilderValidators.required(
                                          errorText: "*الزامی"),
                                      FormBuilderValidators.numeric(
                                          errorText: "عدد وارد کنید"),
                                      FormBuilderValidators.minLength(11,
                                          errorText:
                                          "*تلفن همراه خود را به صورت 11 رقمی وارد کنید"),
                                      FormBuilderValidators.maxLength(11,
                                          errorText:
                                          "* همراه خود را به صورت 11 رقمی وارد کنید"),
                                    ],
                                    TextInputType.number,
                                    true,hasPadding: true),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: Conf.p2w(10)),
                                width: Conf.p2w(100),
                                height: Conf.p2w(15),
                                child: RaisedButton(
                                  color: Conf.blue,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(13.0),
                                      side: BorderSide(
                                        color: Conf.blue,
                                      )),
                                  onPressed: () async {
                                    print("userLogin.mobile ===> ${doctorLogin.mobile} }");
                                    _formKey.currentState.save();
                                    print("_formKey.currentState.value ===> ${_formKey.currentState.value}");
                                    if (_formKey.currentState.validate()) {
//                                      print("userLogin.pasword ===> ${doctorLogin.password} }");
                                    doctorLogin.mobile = _formKey.currentState.value["mobile"];
//                                        userLogin.password = _formKey.currentState.value["password"];
                                      await setLogin();
                                    } else {
                                      print("validation failed");
                                    }
                                  }
                                  ,child: FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text("ورود",
                                        softWrap: true,
                                        overflow: TextOverflow.fade,
                                        style: Conf.body2.copyWith(
                                            color: Colors.white,
                                            fontSize: Conf.p2t(19.5),
                                            fontWeight: FontWeight.w600)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              loading == false ? Conf.circularProgressIndicator() : Container()
            ],
          ),
        ));
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context:context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Icon(FeatherIcons.alertOctagon,color: Colors.red,size: 50,),
            title: Text('خطا',style: TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w400,color: Colors.red),),
            message: Text('اتصال خود را به اینترنت بررسی کنید...',style: TextStyle(fontSize:Conf.p2t(12),fontWeight: FontWeight.w200,color: Colors.black),),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
