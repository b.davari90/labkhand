import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'config.dart';
import 'dart:math';
import 'package:counsalter5/ModelBloc/StatusReserveModel.dart';
class StatusReservCoustomerCardComponent extends StatefulWidget {
  StatusReserveModel model;
  Function onTap;
  bool hasShadow, hasDate,isFirstItem;
  int index;
  StatusReservCoustomerCardComponent({this.model, this.onTap, this.hasShadow,
    this.hasDate, this.isFirstItem, this.index});
  @override
  _StatusReservCoustomerCardComponentState createState() => _StatusReservCoustomerCardComponentState();
}

class _StatusReservCoustomerCardComponentState extends State<StatusReservCoustomerCardComponent> {
  List colors = [Colors.greenAccent.withOpacity(0.5),Colors.purpleAccent.withOpacity(0.4), Colors.deepOrange.withOpacity(0.5),Colors.pink.withOpacity(0.2),Colors.yellow.withOpacity(0.5)];
  Random random = new Random();
  void changeIndex() {
    setState(() => widget.index = random.nextInt(4));
  }
  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
        child: GestureDetector(
          onTap: (){
//            Navigator.push(
//                context,
//                PageTransition(
//                    type: PageTransitionType.leftToRight,
//                    duration: Duration(milliseconds: 500),
//                    alignment: Alignment.centerLeft,
//                    curve: Curves.easeOutCirc,
//                    child: VisitListPage()
//                ));
          },
          child: Padding(
            padding:widget.isFirstItem?EdgeInsets.only(top: Conf.p2h(5)):EdgeInsets.only(top: Conf.p2h(2)),
            child: Container(
                margin: EdgeInsets.only(
                  right: Conf.p2w(5.5), //18,//
                  left: Conf.p2w(5.5), //18,//Conf.p2h(2.1095)
                  bottom: Conf.p2h(0.5), //7//
                  top: Conf.p2h(0.5), //7//
                ),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(13),
                    boxShadow: [
                      BoxShadow(
                          color: widget.hasShadow
                              ? Colors.grey.withOpacity(0.4)
                              : Colors.white,
                          blurRadius: 10,
                          spreadRadius: 0.1,
                          offset: Offset(-1, 1))
                    ]),
                child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: Conf.p2w(2),
                        margin: EdgeInsets.only(right: Conf.p2w(0.1),top: Conf.p2h(0.1)),
//                height: Conf.p2h(5),
                        decoration: BoxDecoration(
//                    color: Colors.blue,
                            color:colors[widget.index],
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(Conf.p2w(3.125)),
//                            topRight: Radius.circular(15),
                                bottomRight: Radius.circular(Conf.p2w(3.125)))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: Conf.p2h(4),
                          right: Conf.p2w(2),
                          bottom: Conf.p2w(4),
//                            left: Conf.p2w(5)
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(width: Conf.p2w(50),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text(widget.model.title,
                                        softWrap: true,
                                        overflow: TextOverflow.visible,
                                        style: Conf.body1.copyWith(
                                            color: Colors.black,
                                            fontSize: Conf.p2t(16.5),
                                            fontWeight: FontWeight.w600)),
                                  ),
                                  Text(widget.model.payload,
                                      softWrap: true,
                                      overflow: TextOverflow.visible,
                                      style: Conf.body1.copyWith(
                                          color: Colors.grey,
                                          fontSize: Conf.p2t(14),
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                            ),
                            Container(width: Conf.p2w(30),
//                              color: Colors.yellowAccent,
                              padding: EdgeInsets.only(left: Conf.p2w(2)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  FittedBox(
                                      fit: BoxFit.cover,
                                      child:Row(
                                          children: [
                                            Text(widget.model.caj.split("-").last,
                                              style: TextStyle(
                                                  fontSize: Conf.p2t(9),
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black54),
                                            ),
                                            Text(
                                              widget.model.caj.split("-").first,
                                              style: TextStyle(
                                                  fontSize: Conf.p2t(9),
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black54),
                                            ),
                                          ])
                                  ),
                                  Container(
                                      width: Conf.p2w(20),
                                      height: Conf.p2h(4),
                                      decoration: BoxDecoration(
                                          color:widget.model.sts_str=="خوانده شده"?Colors.grey:widget.model.sts_str=="جدید"?Conf.orang:
                                          Colors.transparent,
                                          borderRadius: BorderRadius.all(Radius.circular(10))),
                                      child: Center(child: Text(widget.model.sts_str,
                                          style:TextStyle(fontWeight: FontWeight.w400,color: Colors.white,fontSize: Conf.p2t(10)))
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ])),
          ),
        ));
  }
  Widget createContainer(String title, Color color) {
    return Container(
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: Conf.p2t(12),
                color: Colors.white),
          ),
        ),
        height: Conf.p2w(5),
        width: Conf.p2w(15),
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.all(Radius.circular(13))));
  }

}
