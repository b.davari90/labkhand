import 'package:counsalter5/CommentPage.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'ForgotPasswordPage2.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'config.dart';

class ForgotPasswordPage1 extends StatefulWidget {
  @override
  _ForgotPasswordPage1State createState() => _ForgotPasswordPage1State();
}

class _ForgotPasswordPage1State extends State<ForgotPasswordPage1> {
  TextEditingController _controller1;
  final _formKey = GlobalKey<FormBuilderState>();
  TextStyle txttStyleBlack = TextStyle(
    color: Colors.black87.withOpacity(0.6),
    fontWeight: FontWeight.w800,
    fontSize: Conf.p2t(20),
    decoration: TextDecoration.none,
  );

  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _controller1 = TextEditingController();
  }

  void dispose() {
    _controller1.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                width: double.infinity,
                height: double.infinity,
                color: Conf.blue,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: Conf.xlSize),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: Conf.xlSize)
                        .copyWith(right: Conf.xxlSize * 11.3),
                    child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              EdgeInsets.symmetric(vertical: Conf.xlSize * 1.2),
                          child:  MasBackBotton()
//                          GestureDetector(
//                            onTap: () {
//                              Navigator.pop(context);
//                            },
//                            child: Container(
//                              width: Conf.p2w(8.3),
//                              height: Conf.p2h(4.5),
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(10),
//                                  color: Conf.orang),
//                              child: Icon(
//                                FeatherIcons.x,
//                                size: 23,
//                                color: Colors.white,
//                              ),
//                            ),
//                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: Conf.p2w(0),
                top: Conf.p2w(0),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child: Container(
                  height: double.infinity,
                  margin: Conf.smEdgeTop2 * 5.787,
//                  margin: EdgeInsets.only(top: 150),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(38),
                          topRight: Radius.circular(38))),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              top: Conf.p2h(5.125), right: Conf.p2w(6)),
                          child: Text(
                            "رمز عبور خود را فراموش کرده اید؟",
                            softWrap: true,
                            overflow: TextOverflow.fade,
                            style: txttStyleBlack,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: Conf.p2h(2),bottom: Conf.p2h(2), right: Conf.p2w(6)),
                          child: FittedBox(
                            fit: BoxFit.cover,
                            child: Text(
                                "برای بازیابی رمز عبور خود از طریق ایمیل،ایمیل خود را وارد نمائید",
                                softWrap: true,
                                overflow: TextOverflow.fade,
                                style: Conf.body1.copyWith(
                                    color: Colors.black54.withOpacity(0.5),
                                    fontWeight: FontWeight.w600,
                                    fontSize: Conf.p2t(15),
                                    decoration: TextDecoration.none)),
                          ),
                        ),
                        FormBuilder(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: Conf.textFieldFormBuilder('ایمیل  را وارد نمائید...',"email",(){},true,
                              Icons.mail,null,
                              [
                              FormBuilderValidators.email(
                                  errorText:
                                      "ایمیل به طور صحیح وارد نشده است"),
                              FormBuilderValidators.min(11,
                                  errorText:
                                      "حداقل تعداد کارکتر 11 میباشد"),
                              FormBuilderValidators.required(
                                  errorText: "پرکزدن فیلد الزامی است ")
                              ],
                            TextInputType.text,true),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: Conf.p2h(5.125), right: Conf.p2w(6)),
                          child: Container(
                            width: Conf.p2w(88),
                            height: Conf.p2w(18),
                            child:
                            MasRaisedButton(
                              margin: EdgeInsets.only(
                                  right: Conf.p2w(0),
                                  left: Conf.p2w(1),
                                  top: Conf.p2w(2),bottom: Conf.p2w(2)),
                              borderSideColor: Conf.lightColor,text: "ارسال  رمز  عبور  یکبار  مصرف",color: Conf.blue,
                              textColor: Colors.white,
                              onPress:(){
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: CommentPage()
                                    ));
//                    Navigator.pushReplacement(
//                        context,
//                        MaterialPageRoute(
//                        builder: (context) => Increasemoney()));
                              } ,)
//                            RaisedButton(
//                              elevation: 2,
//                              color: Conf.blue,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: BorderRadius.circular(13.0),
//                                  side: BorderSide(color: Conf.blue)),
//                              onPressed: () {},
//                              child: FittedBox(
//                                fit: BoxFit.cover,
//                                child: Text("ارسال  رمز  عبور  یکبار  مصرف",
//                                    softWrap: true,
//                                    overflow: TextOverflow.fade,
//                                    style: Conf.body1.copyWith(
//                                        color: Colors.white.withOpacity(0.9),
//                                        fontSize: Conf.p2t(22),
//                                        fontWeight: FontWeight.w500)
////                                TextStyle(
////                                    color: Colors.white,
////                                    fontSize: 18,
////                                    fontWeight: FontWeight.w900),
//                                    ),
//                              ),
//                            ),
                          ),
                        ),
                        Center(
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: Conf.p2h(2)),
                            child: FittedBox(
                              fit: BoxFit.cover,
                              child: Row(
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.leftToRight,
                                              duration: Duration(milliseconds: 500),
                                              alignment: Alignment.centerLeft,
                                              curve: Curves.easeOutCirc,
                                              child: ForgotPasswordPage2()
                                          ));
                                    },
                                    child: Text(
                                        " با استفاده از شماره موبایل میخواهم رمز عبور را دریافت کنم ",
                                        softWrap: true,
                                        overflow: TextOverflow.fade,
                                        style: Conf.body2.copyWith(
                                          color: Conf.orang,
                                          fontWeight: FontWeight.w600,
                                          fontSize: Conf.p2t(15),
                                          decoration: TextDecoration.none,
                                        )),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
