import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'CounsalterReserveDateModel.dart';
import 'config.dart';

class CounsalterReserveDateComponent extends StatefulWidget {
  CounsalterReserveDateModel model;
  Function onTap;
  bool active;
  @override
  _CounsalterReserveDateComponentState createState() => _CounsalterReserveDateComponentState();

  CounsalterReserveDateComponent({this.model,this.onTap,this.active=false});
}
class _CounsalterReserveDateComponentState extends State<CounsalterReserveDateComponent> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){setState(() {
        if(widget.onTap is Function){
          if (widget.model.active == false) {
//                              print("44 ===> ${ 44 }");
            widget.model.active= true;
          } else
            widget.model.active= false;
        }
      });},

      child: Container(
          child: Center(
              child: Text(
                widget.model.date.toString(),
                style: TextStyle(
                    fontSize: Conf.p2t(20),
                    fontWeight: FontWeight.w700,
                    color:  widget.model.active ? Colors.black87 : Colors.white),
              )),
          margin: EdgeInsets.only(
              top: Conf.p2h(0.25),
              left: Conf.p2w(2.918),
              right: Conf.p2w(2.918)),
          width: Conf.p2w(11.5),
          height: Conf.p2w(11.5),
          decoration: BoxDecoration(
              color: widget.model.active  ? Colors.white : Conf.blue,
              border: Border.all(
                color: widget.model.active  ? Colors.white: Conf.blue,
              ),
              boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.2), offset: Offset(-1,3),blurRadius: 1.5)],
              borderRadius: BorderRadius.all(
                  Radius.circular(Conf.p2w(2.71)
                  )
              ))),
    );
  }
}
