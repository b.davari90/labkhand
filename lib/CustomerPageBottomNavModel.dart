class CustomerPageBottomNavModel {
    String img;
    String title;
    bool active;

    CustomerPageBottomNavModel({this.img, this.active,this.title});

    factory CustomerPageBottomNavModel.fromJson(Map<String, dynamic> json) {
        return CustomerPageBottomNavModel(
            img: json['img'], 
            title: json['title'],
            active: json['active'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['img'] = this.img;
        data['title'] = this.title;
        data['active'] = this.active;
        return data;
    }
}