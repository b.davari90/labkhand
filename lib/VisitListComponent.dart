import 'dart:ui';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:counsalter5/HomePage.dart';
import 'package:counsalter5/AboutUs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'config.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';

import 'package:counsalter5/ModelBloc/VisitModel.dart';
class VisitListComponent extends StatelessWidget {
  VisitModel model;
  Function onTap;
  String date;
  int sts,icon;
  bool isOnline, hasShadow,hasDate, hasPicture,isFirstItem;
TextStyle titleStyle= Conf.body1.copyWith(
    color: Colors.black,
    fontSize: Conf.p2t(16),
    fontWeight: FontWeight.w600);
  TextStyle alertBtnStyle= Conf.body1.copyWith(
      color: Colors.white,
      fontSize: Conf.p2t(16),
      fontWeight: FontWeight.w600);
  TextStyle titleAlertStyle = Conf.body1.copyWith(
      color: Colors.black, fontSize: Conf.p2t(16), fontWeight: FontWeight.w600);

  VisitListComponent({this.model, this.icon,this.sts,this.onTap,this.hasDate, this.isOnline,this.hasShadow,this.isFirstItem=false,
      this.date, this.hasPicture}); //

//  Widget alertBtn(String title,BuildContext ctx,Widget navigator){
//    return Container(
//      width: Conf.p2w(32),
////      margin: EdgeInsets.only(right: Conf.p2w(2),left: Conf.p2w(2)),
//      decoration: BoxDecoration(
//        color: Conf.orang,
//        borderRadius: BorderRadius.circular(13),
//        boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.6),spreadRadius: 1,blurRadius: 5,offset: Offset(-2,2))]),
//      child: FlatButton(
//        height: Conf.p2h(5),padding: EdgeInsets.only(left: Conf.p2w(4),right:Conf.p2w(4)),
//        shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(13.0)),
//        child: Text(title,style: alertBtnStyle),
//        onPressed:  () {
//          Navigator.push(
//              ctx,
//              PageTransition(
//                  type: PageTransitionType.leftToRight,
//                  duration: Duration(milliseconds: 500),
//                  alignment: Alignment.centerLeft,
//                  curve: Curves.easeOutCirc,
//                  child: navigator
//              ));
//         },
//      ),
//    );
//  }//  Color co
//  showAlertDialog(BuildContext context) {
//
//    Container cancelButton = alertBtn("قبول درخواست", context,HomePage()),continueButton = alertBtn("رد درخواست",context,ReserveTimeDoctor());
//
//    AlertDialog alert = AlertDialog(
//      content:  Icon(FeatherIcons.alertTriangle,color: Colors.red,size: 70),
//      shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
//      title: Text("آیا مایل به قبول درخواست این کاربر هستید؟",style: titleStyle,) ,
////      actionsPadding: EdgeInsets.all(2),
//      actions: [
//        Center(
//          child: Container(
////            color: Colors.red,
//            width: Conf.p2w(70),height: Conf.p2h(5),
//            child: Row(
////          mainAxisAlignment: MainAxisAlignment.spaceBetween,
////        crossAxisAlignment: CrossAxisAlignment.center,
//              children: [
//                Container(width: Conf.p2w(35),
//                  child: RaisedButton(
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(13.0),
//                    ),
//                    color: Conf.orang,
//                    onPressed: () { Navigator.push(
//                        context,
//                        PageTransition(
//                            type: PageTransitionType.leftToRight,
//                            duration: Duration(milliseconds: 500),
//                            alignment: Alignment.centerLeft,
//                            curve: Curves.easeOutCirc,
//                            child: HomePage()
//                        ));},
//                    child: Text("قبول درخواست", style: alertBtnStyle),
//                  ),
//                ),
//                Container(width: Conf.p2w(35),
//                  child: RaisedButton(
//                    color: Conf.orang,
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(13.0),
//                    ),
//                    onPressed: () {Navigator.push(
//                        context,
//                        PageTransition(
//                            type: PageTransitionType.leftToRight,
//                            duration: Duration(milliseconds: 500),
//                            alignment: Alignment.centerLeft,
//                            curve: Curves.easeOutCirc,
//                            child: SelectSlotTimesDoctor()
//                        ));},
//                    child: Text("رد درخواست", style: alertBtnStyle),
//                  ),
//                )
////            cancelButton,
////            continueButton,
//              ],
//            ),
//          ),
//        )
//      ],
//    );
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return alert;
//      },
//    );
//  }
  showAlertDialog(BuildContext cntx) {
//    Container cancelButton = alertBtn("بله", cntx, true),
//        continueButton = alertBtn("خیر", cntx, false);

    AlertDialog alert = AlertDialog(
      content: Icon(FeatherIcons.alertTriangle, color: Colors.red, size: 70),
      shape:
      new RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      title: Text(
        "آیا مایل به قبول درخواست این کاربر هستید؟",
        style: titleAlertStyle,
      ),
//      actionsOverflowButtonSpacing: 5,
      actions: [
        Center(
          child: Container(
//            color: Colors.red,
            width: Conf.p2w(75),height: Conf.p2h(11),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding:EdgeInsets.only(left: Conf.p2w(1.5),right: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.blue,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      color: Conf.orang,
                      onPressed: () {
                        Navigator.pushReplacement(
                            cntx,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                duration: Duration(milliseconds: 500),
                                alignment: Alignment.centerLeft,
                                curve: Curves.easeOutCirc,
                                child: AboutUs()
                            ));},
                      child: Text("قبول درخواست", style: alertBtnStyle),
                    ),
                  ),
                ),
                Padding(
                  padding:EdgeInsets.only(right: Conf.p2w(1.5),left: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.purple,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      color: Conf.orang,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      onPressed: () {  Navigator.pushReplacement(
                          cntx,
                          PageTransition(
                              type: PageTransitionType.leftToRight,
                              duration: Duration(milliseconds: 500),
                              alignment: Alignment.centerLeft,
                              curve: Curves.easeOutCirc,
                              child: HomePage()
                          ));
                      },
                      child: Text("رد درخواست", style: alertBtnStyle),
                    ),
                  ),
                )
//            cancelButton,
//            continueButton,
              ],
            ),
          ),
        )
      ],
    );
    showDialog(
      context: cntx,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    print(Conf.p2h(10));
    return Padding(
        padding: isFirstItem?EdgeInsets.only(top: Conf.p2h(5)):EdgeInsets.zero,
        child: IntrinsicHeight(
            child: GestureDetector(
              onTap: (){
                showAlertDialog(context);
            },
              child: Container(
                  padding: Conf.xlEdge,
                  margin: EdgeInsets.only(
                    right: Conf.p2w(5), //18,//
                    left: Conf.p2w(5), //18,//Conf.p2h(2.1095)
                    bottom: Conf.p2h(2.5), //7//
                  ),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                      borderRadius: BorderRadius.circular(13),
                      boxShadow: [
                        BoxShadow(
                            color: hasShadow
                                ? Colors.grey.withOpacity(0.4)
                                : Colors.white,
                            blurRadius: 10,
                            spreadRadius: 0.1,
                            offset: Offset(-1, 1))
                      ]),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: hasDate?CrossAxisAlignment.end:CrossAxisAlignment.center,
                      children: <Widget>[
                        Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            Padding(
                              padding: Conf.smEdgeRight1 / 3.5,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(50.0),
                                child:hasPicture? Container(
                                  color: Colors.grey.withOpacity(0.2),
                                  width: Conf.p2w(14.5),
                                  height: Conf.p2w(14.5),
                                  child: Conf.image(model.doctor.avatar),
                                ):null
                              ),
                            ),
                            Positioned(
                              left: Conf.p2w(0.9), //4,//
                              top: Conf.p2w(12), //61,//
                              child: Container(
                                decoration: BoxDecoration(
                                    color: isOnline
                                        ? Color.fromRGBO(1, 255, 0, 1.0)
                                        : Colors.transparent,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50)),
                                    border: isOnline
                                        ? Border.all(
                                            color: Colors.white,
                                            width: Conf.p2w(0.2345) //2//
                                            )
                                        : Border.all(color: Colors.transparent)),
                                width: Conf.p2w(2.5),
                                height: Conf.p2w(2.5),
                              ),
                            )
                          ],
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(1.2), right: Conf.p2w(1.5)),
//                    padding: const EdgeInsets.only(top: 18),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                FittedBox(
                                  fit: BoxFit.cover,
                                  child: Row(
                                    children: <Widget>[
                                      FittedBox(
                                        fit: BoxFit.cover,
                                        child: Text(model.doctor.fname??"",
                                            softWrap: true,
                                            overflow: TextOverflow.fade,
                                            style:titleStyle
//                                        txtStyleName,
                                            ),
                                      ),
                                      FittedBox(
                                        fit: BoxFit.cover,
                                        child: Text(model.doctor.lname??"",
                                            softWrap: true,
                                            overflow: TextOverflow.fade,
                                            style:titleStyle
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                FittedBox(
                                  fit: BoxFit.cover,
                                  child: Row(
                                    children: <Widget>[
                                      Text("متخصص "+ " - ",
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                          style: Conf.body2.copyWith(
                                              color:
                                                  Colors.black87.withOpacity(0.5),
                                              fontSize: Conf.p2t(12),
                                              fontWeight: FontWeight.w700)
//                                txtStyleAboutDoctor
                                          ),
                                      Text(model.collection.title,
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                          style: Conf.body2.copyWith(
                                              color:
                                                  Colors.black87.withOpacity(0.5),
                                              fontSize: Conf.p2t(12),
                                              fontWeight: FontWeight.w700)
//                                txtStyleAboutDoctor
                                          ),

                                    ],
                                  ),
                                ),
                                createContainerIcon(icon)
                              ],
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              date??"",
                              style: TextStyle(
                                  fontSize: Conf.p2t(12),
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black87),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Conf.p2h(0.5)),
                              child: statusContainer(sts),
                            )
                          ],
                        )
                      ])),
            )));
  }
Widget statusContainer(int sts){
    return Container(
        child: Center(
          child: FittedBox(fit: BoxFit.cover,
            child: Text(
              sts==1? "در انتظار پرداخت":sts==2?"در انتظار تایید":sts==3? "کنسل شده":
                  sts==4? "تایید شده":sts==5?"رد شده":sts==6?"انجام شده":"",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: Conf.p2t(12),
                  color: Colors.black87),
            ),
          ),
        ),
        height: Conf.p2w(5),
        width: Conf.p2w(17),
        decoration: BoxDecoration(
            color:  sts==1? Colors.grey:sts==2?Colors.yellowAccent:sts==3? Colors.orange:
            sts==4?  Colors.amber:sts==5?Colors.red:sts==6? Colors.lightGreenAccent:Colors.transparent,
            borderRadius: BorderRadius.all(Radius.circular(13))));
}

  Widget createContainerIcon(int type) {
    return Container(
        child: Center(
          child: FittedBox(fit: BoxFit.cover,
            child: Icon(type==1?FeatherIcons.phoneCall:type==2?FeatherIcons.messageSquare
              :type==3?FeatherIcons.video:type==4?FeatherIcons.phone:Icon(null),color: Colors.white,size: 15,),
          ),
        ),
        height: Conf.p2w(6),
        width: Conf.p2w(8),
        decoration: BoxDecoration(
            color:
            type==1?Conf.blue:type==2?Colors.red
                :type==3?Colors.orange:type==4?Colors.limeAccent:Colors.transparent
            , borderRadius: BorderRadius.all(Radius.circular(5))));
  }
}
