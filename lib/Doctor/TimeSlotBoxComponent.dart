import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:flutter/material.dart';
class TimeSlotBoxComponent extends StatefulWidget {
//  DoctorPresentsModel model;
  String time;
  Function onTap;
  bool isChoosen;
  TimeSlotBoxComponent({this.time, this.onTap,this.isChoosen});

  @override
  _TimeSlotBoxComponentState createState() => _TimeSlotBoxComponentState();
}
class _TimeSlotBoxComponentState extends State<TimeSlotBoxComponent> {
  @override
  Widget build(BuildContext context) {
    return
      GestureDetector(
        onTap: () {
          if(widget.onTap is Function) {
            setState(() {
              if (widget.isChoosen == false) {
                print(".isChoosen ===> ${ widget.isChoosen }");
                widget.isChoosen= true;
              } else
                widget.isChoosen= false;
              print(".isChoosen ===> ${ widget.isChoosen }");
            });
          };
          print("10 ===> ${10}");
//        isActive = b.contains(num);
        },
        child:
        Container(
            width: Conf.p2w(19),
            height: Conf.p2w(19),
            decoration: BoxDecoration(
                color: widget.isChoosen? Conf.blue: Colors.red,
                border: Border.all(
                  color: widget.isChoosen?  Conf.blue : Colors.red,
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      offset: Offset(-1, 3),
                      blurRadius: 1.5)
                ],
                borderRadius: BorderRadius.all(Radius.circular(Conf.p2w(2.71)))),
            child: Center(
                child: Text(widget.time,
//                  widget.model.times,
//                      ??
//                "",
                  style: TextStyle(
                      fontSize: Conf.p2t(20),
                      fontWeight: FontWeight.w700,
                      color: Colors.white),
                )),
           ),
      );
  }
}
