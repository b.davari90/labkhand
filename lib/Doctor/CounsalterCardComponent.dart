import 'dart:ui';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/CustomerModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:shamsi_date/shamsi_date.dart';
class CounsalterCardComponent extends StatelessWidget {

  TextStyle dateStyle=TextStyle(color: Colors.black,fontSize: Conf.p2t(11),fontWeight: FontWeight.w500);
  TextStyle nameStyle=TextStyle(color: Colors.black,fontSize: Conf.p2t(15),fontWeight: FontWeight.w500);

  CustomerModel model;
  Function onTap;
  bool isUsedForBottomSheet;
  CounsalterCardComponent(
      {this.model,
      this.onTap,
        this.isUsedForBottomSheet=false
      });
  String todayDate(){

    Jalali jNow = Jalali.now();
    String y=jNow.year.toString().length==1?"0"+jNow.year.toString():jNow.year.toString();
    String m=jNow.month.toString().length==1?"0"+jNow.month.toString():jNow.month.toString();
    String d=jNow.day.toString().length==1?"0"+jNow.day.toString():jNow.day.toString();
    return y.toString()+" / "+m.toString()+" / "+d.toString();
  }
  @override
  Widget build(BuildContext context) {
    print(Conf.p2h(10));
    return IntrinsicHeight(
      child: GestureDetector(
        onTap: (){
          if(onTap is Function){
            this.onTap();
          }
//          Navigator.push(
//              context,
//              MaterialPageRoute(
//                  builder: (context) => CounsalterListPage()));
        },
        child: Padding(
          padding: isUsedForBottomSheet?EdgeInsets.only(top: Conf.p2h(1)):EdgeInsets.zero,
          child: Container(
            padding: Conf.xlEdge,
            margin:isUsedForBottomSheet? EdgeInsets.only(
              right: Conf.p2w(5), //18,//
              left: Conf.p2w(5), //18,//Conf.p2h(2.1095)
              bottom: Conf.p2h(0.5), //7//
            ):EdgeInsets.only(
              right: Conf.p2w(5), //18,//
              left: Conf.p2w(5), //18,//Conf.p2h(2.1095)
            ),
            width: double.infinity,
//          height: Conf.p2w(20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(13),
                boxShadow: [
                  BoxShadow(
                      color:
                           Colors.grey.withOpacity(0.4),
                      blurRadius: 10,
                      spreadRadius: 0.4,
                      offset: Offset(-1, 4))
                ]),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: Conf.smEdgeRight1 / 3.5,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: Container(
                        color: Colors.grey.withOpacity(0.2),
                        width: Conf.p2w(13),
                        height: Conf.p2w(13),
                        child: Conf.image(null),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: Conf.p2h(1), right: Conf.p2w(1.5)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                           Row(children: [
                             FittedBox(
                               fit: BoxFit.cover,
                               child: Text(model.name,
                                   softWrap: true,
                                   overflow: TextOverflow.fade,
                                   style: nameStyle
//                                        txtStyleName,
                               ),
                             ),
                             FittedBox(
                               fit: BoxFit.cover,
                               child: Text(model.name??"",
                                   softWrap: true,
                                   overflow: TextOverflow.fade,
                                   style: nameStyle),
                             ),
                           ],)
                              ,Column(children: [
                                Text(todayDate(),style: dateStyle,)
                              ],)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
