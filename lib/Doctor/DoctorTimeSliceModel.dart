import 'package:bloc/bloc.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
class DoctorTimeSliceModel {
 static Duration endTimeSlice, startTimeSlice;

  DoctorTimeSliceModel(
      {endTimeSlice = const Duration(hours: 0, minutes: 0, seconds: 0),
        startTimeSlice = const Duration(hours: 0, minutes: 0, seconds: 0)
      });

  factory DoctorTimeSliceModel.fromJson(Map<String, dynamic> json) {
    return DoctorTimeSliceModel(
      endTimeSlice: json['endTimeSlice'],
      startTimeSlice: json['startTimeSlice'],
    );
  }

  get totalMinutes => (endTimeSlice - startTimeSlice).inMinutes;

  get end => endTimeSlice = Duration(seconds: 0, minutes: 0, hours: 0);

  get start => startTimeSlice = Duration(seconds: 0, minutes: 0, hours: 0);
  

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['endTimeSlice'] = endTimeSlice;
    data['startTimeSlice'] = startTimeSlice;
    return data;
  }
}

class DoctorTimeSliceRepository {
  DoctorTimeSliceModel doctorTimeSlice;
  List<DoctorPresentsModel> doctorPresentList=[];

  DoctorTimeSliceRepository({this.doctorTimeSlice,this.doctorPresentList}) {

    if (this.doctorTimeSlice == null) {
      this.doctorTimeSlice = new DoctorTimeSliceModel();
    }
  }

  DoctorTimeSliceRepository copy() {
    print("DoctorTimeSliceRepository copy() lenght ===> ${ doctorPresentList.length }");
    return DoctorTimeSliceRepository(
        doctorTimeSlice: doctorTimeSlice,
        doctorPresentList: doctorPresentList
    );
  }

  DoctorTimeSliceRepository clearDoctorTimeSliceRepository() {
    return DoctorTimeSliceRepository(
        doctorTimeSlice: DoctorTimeSliceModel(),
        doctorPresentList:[]
    );
  }
}
class DoctorTimeSliceBloc extends Cubit<DoctorTimeSliceRepository> {
  DoctorTimeSliceBloc(DoctorTimeSliceRepository state) : super(state);

  void setTimeSlice(DoctorTimeSliceModel doctorTimeSlice) {
    state.doctorTimeSlice = doctorTimeSlice;
    refresh();
  }
  void setTimeSliceList(List<DoctorPresentsModel> drPeresent) {
    if (state.doctorPresentList == null) {
      state.doctorPresentList = [];
    }
    state.doctorPresentList.addAll(drPeresent);
    print("setTimeSliceList() bloc Lenght===> ${ state.doctorPresentList.length }");
    refresh();
  }
  void removeTimeSlice(List<DoctorTimeSliceModel> times) {
    if(state.doctorPresentList==null){
      state.doctorPresentList=[];
    }
    state.doctorPresentList.remove(times);
    print("setTimeSlice bloc REMOVE===> ${ state.doctorPresentList.map((e) => e) }");
    refresh();
  }
  void refresh() {
    emit(state.copy());
  }
}
