import 'package:flutter/material.dart';
import 'package:counsalter5/config.dart';
import 'dart:convert';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/Doctor/DoctorTimeSliceModel.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:counsalter5/raised_button_ui.dart';

class BottomSheetEditeDrTime extends StatefulWidget {
  int slot, rowId;
  String startTime, endTime;
  bool hasRemover;

  BottomSheetEditeDrTime(
      {this.slot, this.startTime, this.endTime, this.hasRemover, this.rowId});

  @override
  _BottomSheetEditeDrTimeState createState() => _BottomSheetEditeDrTimeState();
}

class _BottomSheetEditeDrTimeState extends State<BottomSheetEditeDrTime> {
  Function onTap;
  int timeSlat = 0, index;
  var totalTime;

  String dateName;
  bool loading;
  DoctorTimeSliceModel d;
  DoctorModel doctor;
  TextStyle headerStyle = TextStyle(
      fontSize: Conf.p2t(18),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);

  DoctorPresentsBloc get doctorPresentBloc =>
      context.bloc<DoctorPresentsBloc>();

  List changeFormateType(List t) {
    List x = [];
    if (t == null || t == []) return [];
    if (t.contains("تلفنی") && t.contains("متنی") && t.contains("تصویری")) {
      return x = [1, 2, 3];
    }
    if (t.contains("تلفنی") && t.contains("متنی")) {
      return x = [1, 3];
    }
    if (t.contains("تلفنی") && t.contains("تصویری")) {
      return x = [3, 2];
    }
    if (t.contains("متنی") && t.contains("تصویری")) {
      return x = [2, 1];
    }
    if (t.contains("تلفنی")) {
      return x = [3];
    }
    if (t.contains("متنی")) {
      return x = [1];
    }
    if (t.contains("تصویری")) {
      return x = [2];
    }
    return x;
  }

  Future sendData() async {
    setState(() {
      loading = false;
    });
    try {
      var t = jsonEncode({
        "time_start": widget.startTime,
        "time_end": widget.endTime,
        "minutes": widget.slot,
        "times_free": doctorPresentBloc.state.doctorpresent.times_free,
        "type": changeFormateType(doctorPresentBloc.state.typeList)
//        "type":changeFormateType(doctorPresentBloc.state.typeList.sublist(0).removeLast())
      });
      print("t ===> ${t}");
      var data = await Htify.update("dr/present", widget.rowId, {
        "time_start": widget.startTime,
        "time_end": widget.endTime,
        "minutes": widget.slot,
        "times_free": doctorPresentBloc.state.doctorpresent.times_free,
        "type": changeFormateType(doctorPresentBloc.state.typeList)
      });
      _showTopFlash(
          title: "",
          title2: "اطلاعات با موفقیت تصحیح شد",
          clr: Colors.green,
          txtstyle2: TextStyle(
              fontSize: Conf.p2t(14),
              fontWeight: FontWeight.w600,
              color: Colors.white),
          txtstyle: TextStyle(
              fontSize: Conf.p2t(12),
              fontWeight: FontWeight.w600,
              color: Colors.white));
      Navigator.pop(context);
    } catch (e) {
      print("CATCH ERROR sendData SelectSlotTime()===> ${e.toString()}");
      _showTopFlash(
          title: "خطای سرور",
          title2: "عدم تصحیح داده",
          clr: Colors.red,
          txtstyle2: TextStyle(
              fontSize: Conf.p2t(14),
              fontWeight: FontWeight.w600,
              color: Colors.white),
          txtstyle: TextStyle(
              fontSize: Conf.p2t(12),
              fontWeight: FontWeight.w600,
              color: Colors.white));
    } finally {
      setState(() {
        loading = true;
      });
    }
  }

  @override
  void initState() {
    print("widget.slot ===> ${widget.slot}");
    print("widget.endTime ===> ${widget.endTime}");
    print("widget.startTime ===> ${widget.startTime}");
    print("widget.hasRemover ===> ${widget.hasRemover}");
    print("widget.rowId ===> ${widget.rowId}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("تغییر برش زمانی"),
            IntrinsicHeight(
              child: IntrinsicWidth(
                child: Container(
//                              height: Conf.p2h(9),
                  margin: EdgeInsets.only(
                      right: Conf.p2w(2),
//                      left: Conf.p2w(15),
                      top: Conf.p2h(3),
                      bottom: Conf.p2h(3)),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.withOpacity(0.3)),
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          if (onTap is Function) {
                            onTap();
                          }
                          setState(() {
                            if (widget.slot > 31) {
                              widget.slot = 0;
                              print("widget.slot>30 ===> ${widget.slot}");
                            } else {
                              widget.slot += 10;
                              print("widget.slot+10 ===> ${widget.slot}");
                            }
                          });
                        },
                        child: Container(
                          width: Conf.p2w(10),
                          height: Conf.p2w(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Conf.orang),
                          child: Icon(
                            Icons.add,
                            size: Conf.p2t(23),
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        width: Conf.p2w(35),
                        alignment: Alignment.center,
                        child: Text(
                          widget.slot.toString() + "دقیقه",
                          style: headerStyle,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (onTap is Function) {
                            onTap();
                          }
                          setState(() {
                            if (widget.slot < 10) {
                              widget.slot = 0;
                              print("widget.slot<0 ===> ${widget.slot}");
                            } else {
                              widget.slot -= 10;
                              print("widget.slot+10 ===> ${widget.slot}");
                            }
                          });
                        },
                        child: Container(
                          width: Conf.p2w(10),
                          height: Conf.p2w(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Conf.orang),
                          child: Icon(
                            Icons.remove,
                            size: Conf.p2t(23),
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
//        Divider(
//          color: Colors.grey.withOpacity(0.3),
//          thickness: 2,
//          indent: 30,
//          endIndent: 30,
//        ),
        Padding(
          padding: EdgeInsets.only(top: Conf.p2h(5)),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("تغییر زمان"),
                Padding(
                  padding: EdgeInsets.only(right: Conf.p2w(2)),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(7)),
                        border: Border.all(color: Colors.grey.withOpacity(0.1)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.01),
                              offset: Offset(-2, 3))
                        ]),
                    width: Conf.p2w(28),
                    height: Conf.p2h(8),
                    child: RaisedButton(
                      child: Text(
                        widget.startTime,
                        style: txtStyle,
                      ),
                      color: Colors.white,
                      onPressed: () {
                        _showTimePicker(1);
                      },
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: Conf.p2w(3), left: Conf.p2w(3)),
                  child: Text("تا", style: txtStyle),
                ),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      color: Colors.white,
                      border: Border.all(color: Colors.grey.withOpacity(0.1)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.01),
                            offset: Offset(-2, 3))
                      ]),
                  width: Conf.p2w(28),
                  height: Conf.p2h(8),
                  child: RaisedButton(
                    child: Text(widget.endTime.toString(), style: txtStyle),
                    color: Colors.white,
                    onPressed: () {
                      _showTimePicker(2);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
            width: Conf.p2w(80),
            height: Conf.p2h(9),
            margin: EdgeInsets.only(top: Conf.p2h(8)),
            child: MasRaisedButton(
              margin: EdgeInsets.only(top: 0),
              text: "ویرایش",
              textColor: Conf.lightColor,
              onPress: () {
//                _showTopFlash(clr: Colors.lightGreen,title2: "تصحیح با موفقیت",title: "")
                sendData();
              },
            ))
      ],
    );
  }

  Duration _showTimePicker(int a) {
    showDialog(
      context: context,
      builder: (BuildContext _) {
        return PersianDateTimePicker(
          initial: '14:45',
          type: 'time',
          onSelect: (date) {
            if (a == 1) {
              setState(() {
                widget.startTime = date.toString();
                DoctorTimeSliceModel.startTimeSlice =
                    parseDuration(widget.startTime);
                widget.endTime = "زمان پایان";
                print(
                    "DoctorTimeSliceModel.startTimeSlice.toString() ===> ${DoctorTimeSliceModel.startTimeSlice.toString()}");
                return DoctorTimeSliceModel.startTimeSlice;
              });
            }
            if (a == 2) {
              if (widget.startTime == "" ||
                  DoctorTimeSliceModel.startTimeSlice == null) {
                widget.endTime = "زمان پایان";
              } else {
                setState(() {
                  widget.endTime = date.toString();
                  DoctorTimeSliceModel.endTimeSlice =
                      parseDuration(widget.endTime);
                  if (DoctorTimeSliceModel.startTimeSlice.inHours <=
                          DoctorTimeSliceModel.endTimeSlice.inHours &&
                      DoctorTimeSliceModel.startTimeSlice.inMinutes <
                          DoctorTimeSliceModel.endTimeSlice.inMinutes) {
                    print(
                        "DoctorTimeSliceModel.endtime.toString() ===> ${DoctorTimeSliceModel.endTimeSlice.toString()}");
                    return DoctorTimeSliceModel.endTimeSlice;
                  } else {
                    widget.endTime = "زمان پایان";
                    return widget.endTime;
                  }
                });
              }
            }
          },
        );
      },
    );
  }

  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    hours = int.parse(s.split(':').first);
    minutes = int.parse(s.split(':').last);
    return Duration(hours: hours, minutes: minutes);
  }

  void _showTopFlash(
      {FlashStyle style = FlashStyle.floating,
      String title,
      title2,
      TextStyle txtstyle,
      txtstyle2,
      Color clr}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: clr,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              title,
              style: txtstyle,
            ),
            message: Text(
              title2,
              style: txtstyle2,
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
//CounsalterTimeComponent
