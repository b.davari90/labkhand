import 'package:flutter/material.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
class CounsalterKindComponent extends StatefulWidget {
  Function onTap;
  DoctorModel model;
  DoctorPresentsModel model2;
List<DoctorPresentsModel> addLIst;
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);

  CounsalterKindComponent({this.onTap, this.model,this.model2,this.addLIst});

  @override
  _CounsalterKindComponentState createState() => _CounsalterKindComponentState();
}

class _CounsalterKindComponentState extends State<CounsalterKindComponent> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
//      onTap: () {
//        widget.onTap();
//        setState(() {
//          if (widget.onTap is Function) {
//            widget.onTap();
//          }
//        });
//      },
      onTap: () {
        if(widget.onTap is Function) {
          setState(() {
            if (widget.model2. isSelectCounsalterKind== false) {
              widget.model2.isSelectCounsalterKind= true;
              widget.onTap();
//              widget.addLIst.add((widget.model2.type.values.toString()));
//              print("** widget.addLIst.length ===> ${  widget.addLIst.length }");
            }
            else {
              widget.model2.isSelectCounsalterKind = false;
              widget.onTap();
//              widget.addLIst.removeWhere((element) => element.type.values.toString()==widget.model2.type.values.toString());
//              print("ELSE widget.addLIst.length ===> ${widget.addLIst.length }");
            }});
        }
      },
      child: Padding(
        padding:EdgeInsets.only(right: Conf.p2w(1),left: Conf.p2w(1)),
        child: Container(
          width: Conf.p2w(25),
          height: Conf.p2h(6),
          child: Center(
              child: Text(
                widget.model2.type.toString(),
                style: widget.txtStyle,
              )),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              color: widget.model2.isSelectCounsalterKind ? Conf.blue : Colors.white,
              border: Border.all(
                  color: widget.model2.isSelectCounsalterKind ? Conf.blue : Colors.grey.withOpacity(0.2)),
              boxShadow: [
                BoxShadow(
                    color: widget.model2.isSelectCounsalterKind ? Conf.blue : Colors.grey.withOpacity(0.4),
                    offset: Offset(-1, 2.7))
              ]),
        ),
      ),);
  }
}
