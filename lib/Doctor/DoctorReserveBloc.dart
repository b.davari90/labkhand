import 'package:bloc/bloc.dart';
import 'package:counsalter5/DoctorModel.dart';

class Reserve {
  Date date;
  List<List<Date>> dateList;
  String timeSlot;
  DoctorModel doctor; // baraye counsalterKind
  List<List<Time>> timeList;

  Reserve(
      {this.doctor, this.date, this.dateList, this.timeList, this.timeSlot});

  Reserve.clone(
      {this.doctor, this.date, this.dateList, this.timeList, this.timeSlot});

  Reserve copy() {
    return Reserve.clone(
        doctor: doctor,
        dateList: dateList,
        timeList: timeList,
        date: date,
        timeSlot: timeSlot);
  }

  factory Reserve.fromJson(Map<String, dynamic> json) {
    try {
      Date date;
      if (json['date'] != null) {
        date = Date.fromJson(json['date']);
      } else {
        date = null;
      }
      Time time;
      if (json['time'] != null) {
        time = Time.fromJson(json['time']);
      } else {
        time = null;
      }
      DoctorModel doctor;
      if (json['doctor'] != null) {
        doctor = DoctorModel.fromJson(json['doctor']);
      } else {
        doctor = null;
      }

      return Reserve(
        doctor: json['doctor'],
        dateList: json["dateList"],
        timeList: json['timeList'],
        timeSlot: json['timeSlot'],
      );
    } catch (e) {
      print("e ===> ${e}");
      throw "reserve not valid";
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['doctor'] = this.doctor;
    data['date'] = this.date;
    data['dateList'] = this.dateList;
    data['timeList'] = this.timeList;
    data['timeSlot'] = this.timeSlot;
    return data;
  }
}

class Date {
  int id;
  String day, month, year;

  Date({this.id, this.day, this.month, this.year});

  static Date fromJson(json) {}
}

class SelectedDate {
  int selectedId;
  String selectedDay, selectedMonth, selectedYear;

  SelectedDate(
      {this.selectedDay,
      this.selectedMonth,
      this.selectedId,
      this.selectedYear});
}

class Time {
  int id;
  String endTime, startTime;
  DateTime a, b;
  Duration c;

  static Time fromJson(json) {}

  Time(
      {this.id,
      this.endTime,
      this.startTime,
      this.a,
      this.b,
      this.c}); //  get time => DateTime.parse(endTime)-DateTime.parse(startTime);

  Duration get time {
    a = DateTime.parse(endTime);
    b = DateTime.parse(startTime);
    c = a.difference(b);
    return c;
  }
}

class DoctorReserveBloc extends Cubit<Reserve> {
  DoctorReserveBloc(Reserve state) : super(state);

  void setSelectedDate(List<Date> dates) {
    if (state.dateList is! List) {
      state.dateList = [];
    }
    state.dateList.add(dates);
    refresh();
  }

  void setTimeList(List<Time> time) {
    state.timeList.add(time);
    refresh();
  }

  void seTimeSlot(String slot) {
    state.timeSlot = slot;
    refresh();
  }

//  void setCounsalterKind(Map<int, String> kind) {
//    state.doctor.counsalterKind = kind;
//    refresh();
//  }

  get totalTime {
    int sum = 0;
//    state.timeList.forEach((e) => sum += e.time);
    return sum;
  }

  refresh() {
    emit(state.copy());
  }
}
