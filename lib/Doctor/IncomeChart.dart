//import 'package:counsalter5/config.dart';
//import 'package:fl_chart/fl_chart.dart';
//import 'package:flutter/material.dart';
//
//class LineChartSample1 extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() => LineChartSample1State();
//  List<int> totals=[];
//  List<String> months=[];
//  LineChartSample1({this.totals, this.months});
//}
//class LineChartSample1State extends State<LineChartSample1> {
//  bool isShowingMainData;
//  @override
//  void initState() {
//    super.initState();
//    isShowingMainData = true;
//  }
//  @override
//  Widget build(BuildContext context) {
//    return SafeArea(
//      child: Scaffold(
//        body: Container(
//            width: Conf.p2w(100),height: Conf.p2w(50),
//          decoration: const BoxDecoration(
//            borderRadius: BorderRadius.all(Radius.circular(18)),
//          ),
//          child: Stack(
//            children: <Widget>[
//              Column(
//                crossAxisAlignment: CrossAxisAlignment.stretch,
//                children: <Widget>[
//                  const SizedBox(
//                    height: 10,
//                  ),
//                  const Text(
//                    'درامد ماهیانه',
//                    style: TextStyle(
//                      color: Color(0xff827daa),
//                      fontSize: 16,
//                    ),
//                    textAlign: TextAlign.center,
//                  ),
//                  Expanded(
//                    child: Padding(
//                      padding: const EdgeInsets.only(right: 16.0, left: 6.0),
//                      child: LineChart(
//                        sampleData1(),
//                        swapAnimationDuration: const Duration(milliseconds: 250),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//
//  LineChartData sampleData1() {
//    return LineChartData(
////      lineTouchData: LineTouchData(
////        touchTooltipData: LineTouchTooltipData(
////          tooltipBgColor: Colors.blueGrey.withOpacity(0.8),),
////        touchCallback: (LineTouchResponse touchResponse) {},
////        handleBuiltInTouches: false),
//      gridData: FlGridData(show: true),
//      titlesData: FlTitlesData(
//        bottomTitles: SideTitles(
//          showTitles: true,
//          reservedSize: 30,
//          getTextStyles: (value) => TextStyle(
//            color: Color(0xff72719b),
//            fontWeight: FontWeight.bold,
//            fontSize: 12),
//          margin: 10,
////          getTitles: (value) {
////            switch (value.toInt()) {
////              case 1:
////                return 'بهار';
////              case 4:
////                return 'تابستان';
////              case 8:
////                return 'پاییز';
////              case 12:
////                return 'زمستان';
////            }
////            return '';
////          },
//        ),
//        leftTitles: SideTitles(
//          showTitles: true,
//          getTextStyles: (value) => TextStyle(
//            color: Color(0xff75729e),
//            fontWeight: FontWeight.bold,
//            fontSize: 11,
//          ),
//          getTitles: (value) {
//            switch (value.toInt()) {
//              case 1:
//                return '150';
//              case 2:
//                return '500';
//              case 3:
//                return '900';
//              case 4:
//                return '15000';
//            }
//            return '';
//          },
//          margin: 8,
//          reservedSize: 30,
//        ),
//      ),
//      borderData: FlBorderData(
//        show: true,
//        border: const Border(
//          bottom: BorderSide(
//            color: Color(0xff4e4965),
//            width: 1,
//          ),
//          left: BorderSide(
//            color: Colors.transparent,
//          ),
//          right: BorderSide(
//            color: Colors.transparent,
//          ),
//          top: BorderSide(
//            color: Colors.transparent,
//          ),
//        ),
//      ),
//      minX: 0,
//      maxX: 14,
//      maxY: 5,
//      minY: 0,
//      lineBarsData: linesBarData1(),
//    );
//  }
//
//  List<LineChartBarData> linesBarData1() {
//    final LineChartBarData lineChartBarData1 = LineChartBarData(
//      spots: [
//        FlSpot(1, 1),
//        FlSpot(3, 3),
//        FlSpot(5, 1.4),
//        FlSpot(7, 0.4),
//        FlSpot(10, 2),
//        FlSpot(12, 2.2),
//        FlSpot(13, 1.8),
////        FlSpot(17, 1.0),
//      ],
//      isCurved: false,
//      colors: [
//        Conf.orang
////        const Color(0xff4af699),
//      ],
//      barWidth: 3,
//      isStrokeCapRound: true,
//      dotData: FlDotData(
//        show: false,
//      ),
//    );
//    return [
//      lineChartBarData1];
//  }
//}
