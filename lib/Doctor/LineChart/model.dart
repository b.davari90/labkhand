import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
class WidgetCategory {
  WidgetCategory(
      [this.categoryName,
      this.controlList,
      this.mobileCategoryId,
      this.webCategoryId,
      this.showInWeb]);
  factory WidgetCategory.fromJson(Map<String, dynamic> json) {
    return WidgetCategory(json['categoryName'], json['controlList'],
        json['mobileCategoryId'], json['webCategoryId'], json['showInWeb']);
  }
  String categoryName;
  List<dynamic> controlList;
  final int mobileCategoryId;
  final int webCategoryId;
  final bool showInWeb;
  int selectedIndex = 0;
}
class Control {
  Control(this.title, this.description, this.image, this.status,
      this.displayType, this.subItems, this.controlId, this.showInWeb);
  factory Control.fromJson(Map<String, dynamic> json) {
    return Control(
        json['title'],
        json['description'],
        json['image'],
        json['status'],
        json['displayType'],
        json['subItems'],
        json['controlId'],
        json['showInWeb']);
  }
  final String title;
  final String description;
  final String image;
  final String status;
  final int controlId;
  final String displayType;
  final bool showInWeb;
  List<SubItem> sampleList;
  List<SubItem> childList;
  List<dynamic> subItems;
}
class SubItem {
  SubItem(
      [this.type,
      this.displayType,
      this.title,
      this.key,
      this.codeLink,
      this.description,
      this.status,
      this.subItems,
      this.showInWeb,
      this.sourceLink,
      this.sourceText,
      this.needsPropertyPanel]);
  factory SubItem.fromJson(Map<String, dynamic> json) {
    return SubItem(
        json['type'],
        json['displayType'],
        json['title'],
        json['key'],
        json['codeLink'],
        json['description'],
        json['status'],
        json['subItems'],
        json['showInWeb'],
        json['sourceLink'],
        json['sourceText'],
        json['needsPropertyPanel']);
  }
  final String type;
  final String displayType;
  final String title;
  final String key;
  final String codeLink;
  final String description;
  final String status;
  final bool showInWeb;
  final String sourceLink;
  final String sourceText;
  List<dynamic> subItems;
  final bool needsPropertyPanel;
  String categoryName;
  String breadCrumbText;
  int parentIndex;
  int childIndex;
  int sampleIndex;
  Control control;
}
class SampleModel extends Listenable {
  SampleModel() {
    searchControlItems = <Control>[];
    sampleList = <SubItem>[];
    searchResults = <SubItem>[];
    searchSampleItems = <SubItem>[];
    categoryList = SampleModel._categoryList;
    controlList = SampleModel._controlList;
    routes = SampleModel._routes;
    searchControlItems.addAll(controlList);
    for (int index = 0; index < controlList.length; index++) {
      if (controlList[index].sampleList != null) {
        for (int i = 0; i < controlList[index].sampleList.length; i++) {
          searchSampleItems.add(controlList[index].sampleList[i]);
        }
      } else if (controlList[index].childList != null) {
        for (int i = 0; i < controlList[index].childList.length; i++) {
          for (int j = 0;
              j < controlList[index].childList[i].subItems.length;
              j++) {
            if (controlList[index].childList[i].subItems[j].types != 'child') {
              searchSampleItems
                  .add(controlList[index].childList[i].subItems[j]);
            } else {
              //ignore: prefer_foreach
              for (final SubItem sample
                  in controlList[index].childList[i].subItems[j].subItems) {
                searchSampleItems.add(sample);
              }
            }
          }
        }
      } else {
        for (int i = 0; i < controlList[index].subItems.length; i++) {
          for (int j = 0;
              j < controlList[index].subItems[i].subItems.length;
              j++) {
            for (int k = 0;
                k < controlList[index].subItems[i].subItems[j].subItems.length;
                k++) {
              searchSampleItems
                  .add(controlList[index].subItems[i].subItems[j].subItems[k]);
            }
          }
        }
      }
    }
  }
  static SampleModel instance = SampleModel();
  static List<Control> _controlList = <Control>[];
  static List<WidgetCategory> _categoryList = <WidgetCategory>[];
  List<WidgetCategory> categoryList;
  List<Control> controlList, searchControlItems;
  List<SubItem> sampleList;
  List<SubItem> searchSampleItems, searchResults;
  Color backgroundColor = const Color.fromRGBO(0, 116, 227, 1);
  Color paletteColor = const Color.fromRGBO(0, 116, 227, 1);
  Color currentPrimaryColor = const Color.fromRGBO(0, 116, 227, 1);
  ThemeData themeData;
  Color textColor = const Color.fromRGBO(51, 51, 51, 1);
  Color drawerTextIconColor = Colors.black;
  Color bottomSheetBackgroundColor = Colors.white;
  Color cardThemeColor = Colors.white;
  Color webBackgroundColor = const Color.fromRGBO(246, 246, 246, 1);
  Color webIconColor = const Color.fromRGBO(55, 55, 55, 1);
  bool isWeb = false;
  Color webInputColor = const Color.fromRGBO(242, 242, 242, 1);
  Color webOutputContainerColor = Colors.white;
  Color cardColor = Colors.white;
  Color dividerColor = const Color.fromRGBO(204, 204, 204, 1);
  Size oldWindowSize, currentWindowSize;
  static List<SampleRoute> _routes;
  List<SampleRoute> routes;
  dynamic currentRenderSample;
  String currentSampleKey;
  List<Color> paletteColors;
  List<Color> paletteBorderColors;
  List<Color> darkPaletteColors;
  ThemeData currentThemeData;
  Color currentPaletteColor = const Color.fromRGBO(0, 116, 227, 1);
  int selectedThemeIndex = 0;
  bool isCardView = true;
  bool isMobileResolution;
  ThemeData systemTheme;
  TextEditingController editingController = TextEditingController();
  GlobalKey<State> propertyPanelKey;
  bool needToMaximize = false;
  dynamic outputContainerState;
  void changeTheme(ThemeData _themeData) {
    themeData = _themeData;
    switch (_themeData.brightness) {
      case Brightness.dark:
        {
          dividerColor = const Color.fromRGBO(61, 61, 61, 1);
          cardColor = const Color.fromRGBO(48, 48, 48, 1);
          webIconColor = const Color.fromRGBO(230, 230, 230, 1);
          webOutputContainerColor = const Color.fromRGBO(23, 23, 23, 1);
          webInputColor = const Color.fromRGBO(44, 44, 44, 1);
          webBackgroundColor = const Color.fromRGBO(33, 33, 33, 1);
          drawerTextIconColor = Colors.white;
          bottomSheetBackgroundColor = const Color.fromRGBO(34, 39, 51, 1);
          textColor = const Color.fromRGBO(242, 242, 242, 1);
          cardThemeColor = const Color.fromRGBO(33, 33, 33, 1);
          break;
        }
      default:
        {
          dividerColor = const Color.fromRGBO(204, 204, 204, 1);
          cardColor = Colors.white;
          webIconColor = const Color.fromRGBO(55, 55, 55, 1);
          webOutputContainerColor = Colors.white;
          webInputColor = const Color.fromRGBO(242, 242, 242, 1);
          webBackgroundColor = const Color.fromRGBO(246, 246, 246, 1);
          drawerTextIconColor = Colors.black;
          bottomSheetBackgroundColor = Colors.white;
          textColor = const Color.fromRGBO(51, 51, 51, 1);
          cardThemeColor = Colors.white;
          break;
        }
    }
  }
  final Set<VoidCallback> _listeners = Set<VoidCallback>();
  @override
  void addListener(VoidCallback listener) {
    _listeners.add(listener);
  }
  @override
  void removeListener(VoidCallback listener) {
    _listeners.remove(listener);
  }
  @protected
  void notifyListeners() {
    _listeners.toList().forEach((VoidCallback listener) => listener());
  }
}
Future<void> updateControlItems() async {
  bool _isSample = false;
  bool _isChild = false;
  const bool _isWeb = kIsWeb;
  final String _jsonText =
      await rootBundle.loadString('lib/sample_details.json');
  List<SubItem> _firstLevelSubItems = <SubItem>[];
  List<SubItem> _secondLevelSubItems = <SubItem>[];
  List<SubItem> _thirdLevelSubItems = <SubItem>[];
  final List<SampleRoute> sampleRoutes = <SampleRoute>[];

  final List<dynamic> categoryList = json.decode(_jsonText);
  for (int index = 0; index < categoryList.length; index++) {
    SampleModel._categoryList.add(WidgetCategory.fromJson(categoryList[index]));
    List<Control> controlList = <Control>[];
    if (!_isWeb || SampleModel._categoryList[index].showInWeb != false) {
      for (int i = 0;
          i < SampleModel._categoryList[index].controlList.length;
          i++) {
        controlList.add(
            Control.fromJson(SampleModel._categoryList[index].controlList[i]));
        if (!_isWeb || controlList[i].showInWeb != false) {
          for (int j = 0; j < controlList[i].subItems.length; j++) {
            _firstLevelSubItems
                .add(SubItem.fromJson(controlList[i].subItems[j]));
            if (_firstLevelSubItems[j].type == 'parent') {
              for (int k = 0; k < _firstLevelSubItems[j].subItems.length; k++) {
                if (!_isWeb ||
                    SubItem.fromJson(_firstLevelSubItems[j].subItems[k])
                            .showInWeb !=
                        false) {
                  _secondLevelSubItems.add(
                      SubItem.fromJson(_firstLevelSubItems[j].subItems[k]));
                  for (int l = 0;
                      l <
                          _secondLevelSubItems[_secondLevelSubItems.length - 1]
                              .subItems
                              .length;
                      l++) {
                    if (!_isWeb ||
                        SubItem.fromJson(_secondLevelSubItems[
                                        _secondLevelSubItems.length - 1]
                                    .subItems[l])
                                .showInWeb !=
                            false) {
                      _thirdLevelSubItems.add(SubItem.fromJson(
                          _secondLevelSubItems[_secondLevelSubItems.length - 1]
                              .subItems[l]));
                    }
                    _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                        .parentIndex = j;
                    _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                        .childIndex = k;
                    _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                        .sampleIndex ??= _thirdLevelSubItems.length - 1;
                    _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                        .control = controlList[i];
                    final String breadCrumbText = ('/' +
                            controlList[i].title +
                            '/' +
                            _firstLevelSubItems[j].title +
                            '/' +
                            _secondLevelSubItems[
                                    _secondLevelSubItems.length - 1]
                                .title +
                            '/' +
                            _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                                .title)
                        .replaceAll(' ', '-')
                        .toLowerCase();
                    _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                        .breadCrumbText = breadCrumbText;
                    _thirdLevelSubItems[_thirdLevelSubItems.length - 1]
                            .categoryName =
                        SampleModel._categoryList[index].categoryName;
                    sampleRoutes.add(SampleRoute(
                        routeName: breadCrumbText,
                        subItem: _thirdLevelSubItems[
                            _thirdLevelSubItems.length - 1]));
                  }
                  _secondLevelSubItems[_secondLevelSubItems.length - 1]
                      .subItems = _thirdLevelSubItems;
                  _thirdLevelSubItems = <SubItem>[];
                }
              }
              _firstLevelSubItems[j].subItems = _secondLevelSubItems;
              _secondLevelSubItems = <SubItem>[];
            } else if (_firstLevelSubItems[j].type == 'child') {
              if (!_isWeb || _firstLevelSubItems[j].showInWeb != false) {
                _isChild = true;
                for (int k = 0;
                    k < _firstLevelSubItems[j].subItems.length;
                    k++) {
                  if (!_isWeb ||
                      SubItem.fromJson(_firstLevelSubItems[j].subItems[k])
                              .showInWeb !=
                          false) {
                    _secondLevelSubItems.add(
                        SubItem.fromJson(_firstLevelSubItems[j].subItems[k]));
                    _secondLevelSubItems[_secondLevelSubItems.length - 1]
                        .childIndex = j;
                    _secondLevelSubItems[_secondLevelSubItems.length - 1]
                        .sampleIndex ??= k;
                    _secondLevelSubItems[_secondLevelSubItems.length - 1]
                        .control = controlList[i];
                    final String breadCrumbText = ('/' +
                            controlList[i].title +
                            '/' +
                            _firstLevelSubItems[j].title +
                            '/' +
                            _secondLevelSubItems[
                                    _secondLevelSubItems.length - 1]
                                .title)
                        .replaceAll(' ', '-')
                        .toLowerCase();
                    _secondLevelSubItems[_secondLevelSubItems.length - 1]
                        .breadCrumbText = breadCrumbText;
                    _secondLevelSubItems[_secondLevelSubItems.length - 1]
                            .categoryName =
                        SampleModel._categoryList[index].categoryName;
                    sampleRoutes.add(SampleRoute(
                        routeName: breadCrumbText,
                        subItem: _secondLevelSubItems[
                            _secondLevelSubItems.length - 1]));
                  }
                }
                _firstLevelSubItems[j].subItems = _secondLevelSubItems;
                _secondLevelSubItems = <SubItem>[];
              } else {
                _firstLevelSubItems.removeAt(j);
                controlList[i].subItems.removeAt(j);
                j--;
              }
            } else {
              _isSample = true;
              _firstLevelSubItems[j].sampleIndex ??= j;
              if (!_isWeb || _firstLevelSubItems[j].showInWeb != false) {
                final String breadCrumbText = ('/' +
                        controlList[i].title +
                        '/' +
                        _firstLevelSubItems[j].title)
                    .replaceAll(' ', '-')
                    .toLowerCase();
                _firstLevelSubItems[j].breadCrumbText = breadCrumbText;
                _firstLevelSubItems[j].control = controlList[i];
                _firstLevelSubItems[j].categoryName =
                    SampleModel._categoryList[index].categoryName;
                sampleRoutes.add(SampleRoute(
                    routeName: breadCrumbText,
                    subItem: _firstLevelSubItems[j]));
                _secondLevelSubItems.add(_firstLevelSubItems[j]);
              }
            }
          }
          if (_isSample) {
            controlList[i].sampleList = _secondLevelSubItems;
            controlList[i].subItems = _secondLevelSubItems;
            _secondLevelSubItems = <SubItem>[];
          } else if (_isChild) {
            controlList[i].childList = _firstLevelSubItems;
            _secondLevelSubItems = <SubItem>[];
            _isChild = false;
          }
          (!_isSample)
              ? controlList[i].subItems = _firstLevelSubItems
              : _isSample = false;

          _firstLevelSubItems = <SubItem>[];
        } else {
          controlList.removeAt(i);
          SampleModel._categoryList[index].controlList.removeAt(i);
          i--;
        }
      }

      SampleModel._categoryList[index].controlList = controlList;
      SampleModel._controlList.addAll(controlList);
    } else {
      categoryList.removeAt(index);
      SampleModel._categoryList.removeAt(index);
      index--;
    }
  }
  SampleModel._routes = sampleRoutes;
  for (int i = 0; i < SampleModel._categoryList.length; i++) {
    SampleModel._categoryList[i].controlList
        .sort((dynamic a, dynamic b) => a.controlId.compareTo(b.controlId));
  }
  if (_isWeb) {
    SampleModel._categoryList.sort((WidgetCategory a, WidgetCategory b) =>
        a.webCategoryId.compareTo(b.webCategoryId));
  } else {
    SampleModel._categoryList.sort((WidgetCategory a, WidgetCategory b) =>
        a.mobileCategoryId.compareTo(b.mobileCategoryId));
  }
}
class SampleRoute {
  SampleRoute({this.routeName, this.subItem});
  final String routeName;
  final SubItem subItem;
}
