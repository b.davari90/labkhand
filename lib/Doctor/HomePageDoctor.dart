import 'package:counsalter5/Doctor/BottomNavigationDoctor.dart';
import 'package:counsalter5/Doctor/CounsalterListPageBottomSheet.dart';
import 'package:counsalter5/DoctorDocument.dart';
import 'package:counsalter5/htify.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/config.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:page_transition/page_transition.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:counsalter5/ModelBloc/IncomeChartModel.dart';
import 'package:counsalter5/DoctorInformation/PieChartSample2.dart';
import 'package:counsalter5/ModelBloc/PieChartModel.dart';

class HomePageDoctor extends StatefulWidget {
  @override
  _HomePageDoctorState createState() => _HomePageDoctorState();
}

class _HomePageDoctorState extends State<HomePageDoctor> {
  bool loading = false, load = true, loadin1 = false;
  DoctorModel doctorInformationCounsalter;
  int id;
  double r, rankPercent, maxYIncom;
  int call, video, chat, voice;
  String rank, callValue, videoValue, chatValue, voiceValue;
  int dOpenFolder = 0,
      dTotalFolder = 0,
      countVisit = 0,
      videoPercent = 0,
      chatPercent = 0,
      voicePercent = 0,
      tellPercent = 0;
  PageController controller;
  List<IncomeChartModel> income;
  List c;
  List c2;
  List<PieChartModel> PieChartList;
  List counts, type_strs, types;
  double ca = 0, ch = 0, vo = 0, vi = 0;
  var totalList;
  var month_faList;
  TextStyle txtStyle = Conf.title.copyWith(
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontSize: Conf.p2t(16),
    decoration: TextDecoration.none,
  );
  TextStyle txtUnitStyle = Conf.title.copyWith(
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontSize: Conf.p2t(14),
    decoration: TextDecoration.none,
  );
  TextStyle txtCounsalterKindStyle = Conf.title.copyWith(
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontSize: Conf.p2t(11),
    decoration: TextDecoration.none,
  );
  TextStyle numberStyle = Conf.title.copyWith(
    color: Conf.orang,
    fontWeight: FontWeight.w500,
    fontSize: Conf.p2t(25),
    decoration: TextDecoration.none,
  );
  List<DoctorModel> doctorModelList;

  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  Future getDoctorBaseInfo() async {
    try {
      setState(() {
        load = true;
      });
      Map data = await Htify.get('dr/doctor');
      doctorInformationCounsalter.fname = data["fname"];
      doctorInformationCounsalter.avatar = data["avatar"];
      doctorInformationCounsalter.lname = data["lname"];
      doctorInformationCounsalter.name = data["name"];
      doctorInformationCounsalter.city_code = data["city_code"];
      doctorInformationCounsalter.n_code = data["n_code"];
      doctorInformationCounsalter.n_doctor = data["n_doctor"];
      doctorInformationCounsalter.phone = data["phone"];
      doctorInformationCounsalter.email = data["email"];
      doctorInformationCounsalter.address = data["address"];
      doctorInformationCounsalter.birth_day = data["birth_day"];
      doctorInformationCounsalter.city_id = data["city_id"];
      doctorInformationCounsalter.mobile = data["mobile"];
      doctorInformationCounsalter.inf_personal = data["inf_personal"];
      doctorInformationCounsalter.inf_bank = data["inf_bank"];
      doctorInformationCounsalter.inf_doc = data["inf_doc"];
      doctorInformationCounsalter.inf_collection = data["inf_collection"];
      doctorInformationCounsalter.isSelelectedTimeSlot = data["isSelelectedTimeSlot"];
      doctorInformationCounsalter.active = data["active"];
      doctorInformationCounsalter.specialty = data["specialty"];
      doctorInformationCounsalter.description = data["describtion"];
      print("d.specialty ===> ${doctorInformationCounsalter.specialty}");
      print("d.describtion ===> ${doctorInformationCounsalter.description}");
      doctorBloc.setDoctor(doctorInformationCounsalter);
      print("____d.fname  ===> ${doctorInformationCounsalter.fname}");
    } catch (e) {
      print("CATCH ERROR getDoctor===> ${e.toString()}");
    } finally {
      setState(() {
        load = false;
      });
    }
  }

  Future getCharts() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.get('dr/chart');
      print("getCharts() data ===> ${data}");
      setState(() {
        dOpenFolder = data["open_folder"];
        countVisit = data["visit"];
        dTotalFolder = data["folder"];
        rank = data["rank"];
        c = data["income"];
        print("c ===> ${c}");
        income = c.map((e) => IncomeChartModel.fromJson(e)).toList();
        print("income ===> ${income}");
        totalList = income.map((e) => e.total);
        print("totalList ===> ${totalList}");
        month_faList = income.map((e) => e.month_fa);
        print(month_faList);
        print("00000 ===> ${00000}");
        c2 = data["type"];
        PieChartList = c2.map((e) => PieChartModel.fromJson(e)).toList();
        counts = PieChartList.map((e) => e.count).toList();
        type_strs = PieChartList.map((e) => e.type_str).toList();
        types = PieChartList.map((e) => e.type).toList();
        print("__types ===> ${types}");
        final cnt = counts.asMap();
        call = cnt[0];
        chat = cnt[1];
        video = cnt[2];
        voice = cnt[3];
        double sumCount = 0;
        if(call==null||video==null||chat==null||voice==null){sumCount=0;}
        else{sumCount = (call + video + chat + voice).toDouble();}
        print("__sumCount ===> ${sumCount}");

        ca = (call / sumCount) * 100;
        vo = (voice / sumCount) * 100;
        vi = (video / sumCount) * 100;
        ch = (chat / sumCount) * 100;
        print("___ch ===> ${ch}");
        print("___call ===> ${call}");
        print("___voice ===> ${voice}");
        print(889900);
      });
    } catch (e) {
      print("CATCH ERROR getCharts()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    doctorInformationCounsalter = DoctorModel();
    getDoctorBaseInfo();
    print("init state Conf.token ===> ${Conf.token}");
    counts = [];
    c = [];
    c2 = [];
    PieChartList = [];
    type_strs = [];
    types = [];
    income=[];
    getCharts();
    super.initState();
  }

  Widget makeMonthText(String name, double number) {
    return IntrinsicWidth(
        child: Container(
      padding: EdgeInsets.only(right: number),
      height: 20,
      child: Text(
        name,
        style: TextStyle(fontSize: 11, fontWeight: FontWeight.w600),
      ),
    ));
  }

  @override
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            appBar: AppBar(
              backgroundColor: Conf.lightColor,
              automaticallyImplyLeading: false,
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(Conf.p2w(8)),
                child: Container(),
              ),
              flexibleSpace: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(top: Conf.p2h(3), right: Conf.p2w(5)),
                    child: BlocBuilder<DoctorBloc, DoctorRepository>(
                        builder: (context, state) {
                      print(
                          "**state.doctor.avatar ===> ${state.doctor.avatar}");
                      print("**state.doctor.name ===> ${state.doctor.name}");
                      return Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100.0),
                            child: Container(
                                color: Colors.grey.withOpacity(0.2),
                                width: Conf.p2w(12),
                                height: Conf.p2w(12),
                          child:
                          Conf.image(doctorBloc.state.doctor.avatar)??Conf.image(null))
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: Conf.xlSize),
                            child: Text(state.doctor.name ?? " مشاور",
                                style: txtStyle),
                          )
                        ],
                      );
                    }),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: Conf.p2h(4), left: Conf.p2w(4)),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Conf.blue,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      width: Conf.p2w(8),
                      height: Conf.p2w(8),
                      child: Icon(
                        Icons.notifications,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            body: SingleChildScrollView(
              padding: EdgeInsets.only(bottom: Conf.xlSize, top: Conf.xlSize),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      "درآمد (ماه-ریال)",
                      style: txtCounsalterKindStyle,
                    ),
                  ),
//      Container(child: Text(""),):
      Container(
                      margin:
                          EdgeInsets.only(top: Conf.p2h(3), right: Conf.p2h(2)),
                      height: Conf.p2h(25),
                      width: Conf.p2w(85),
                      child: income==null?
                      SizedBox(
                        width: 300,
                        height: 140,
                        child:
                        LineChart(
                          LineChartData(
                            lineTouchData: LineTouchData(enabled: true),
                            lineBarsData: [
                            LineChartBarData(
                                spots:income.map((e) => FlSpot(
                                            double.parse(e.month),
                                            double.parse(e.total.toString())))
                                        .toList() ,
                                    //[] به جای این خط بود
                                isCurved: true,
                                barWidth: 2,
                                colors: [
                                  Conf.orang,
                                ],
                                dotData: FlDotData(
                                  show: false,
                                ),
                              ),
                            ]
                            ,axisTitleData: FlAxisTitleData(bottomTitle: AxisTitle(showTitle: true,titleText: "ماه های کارکرد",textStyle: txtUnitStyle,margin: 10),
                              leftTitle: AxisTitle(showTitle: true,titleText: "درآمد",textStyle: txtUnitStyle,margin: 10)),
                            betweenBarsData: [
                              BetweenBarsData(
                                fromIndex: 0,
                                toIndex: 0,
                                colors: [Conf.orang],
                              )
                            ],
                            minY: 0,
                            maxY: maxYIncom,
                            titlesData: FlTitlesData(
//            topTitles: SideTitles(
//                getTitles: (a){return 'درآمد( ماه_ریال)';} ,
//                showTitles: true,interval: 50000
//            ,getTextStyles: (val){return TextStyle(
//                  fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black87
//                );}),
//            ),
                              bottomTitles: SideTitles(
                                  showTitles: true,
                                  getTextStyles: (value) => const TextStyle(
                                      fontSize: 10,
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold),
                                  rotateAngle: 90,
                                  getTitles: (value) {
                                    switch (value.toInt()) {
                                      case 1:
                                        return 'فروردین';
                                      case 2:
                                        return 'اردیبهشت';
                                      case 3:
                                        return 'خرداد';
                                      case 4:
                                        return 'تیر';
                                      case 5:
                                        return 'مرداد';
                                      case 6:
                                        return 'شهریور';
                                      case 7:
                                        return 'مهر';
                                      case 8:
                                        return 'آبان';
                                      case 9:
                                        return 'آذر';
                                      case 10:
                                        return 'دی';
                                      case 11:
                                        return 'بهمن';
                                      case 12:
                                        return 'اسفند';
                                      default:
                                        return '';
                                    }
                                  }),
                              leftTitles: SideTitles(
                                showTitles: true,
                                margin: 10,
                                interval: 15000,
//              getTextStyles: (val){return TextStyle(
//                  fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black87
//                );},
                                getTitles: (value) {
                                  return '${value.toInt()} ';
                                },
//                return '${value.toInt()} ریال';},
                              ),
                            ),
                            gridData: FlGridData(
                              show: true,
                              checkToShowHorizontalLine: (double value) {
                                return value == 1 ||
                                    value == 6 ||
                                    value == 4 ||
                                    value == 5;
                              },
                            ),
                          ),
                        ),
                      ):
                      SizedBox(
                        width: 300,
                        height: 140,
                        child:
                        LineChart(
                          LineChartData(
                            lineTouchData: LineTouchData(enabled: true),
                            lineBarsData: [
                              LineChartBarData(
                                spots: [
                                  FlSpot(0, 0),
                                  FlSpot(1, 0),
                                  FlSpot(2, 0),
                                  FlSpot(3, 0),
                                  FlSpot(4, 0),
                                  FlSpot(5, 0),
                                  FlSpot(6, 0),
                                  FlSpot(7, 0),
                                  FlSpot(8, 0),
                                  FlSpot(9, 0),
                                  FlSpot(10, 0),
                                  FlSpot(11, 0),
                                ],
                                isCurved: true,
                                barWidth: 2,
                                colors: [
                                  Conf.orang,
                                ],
                                dotData: FlDotData(
                                  show: false,
                                ),
                              ),
                            ]
                            ,axisTitleData: FlAxisTitleData(bottomTitle: AxisTitle(showTitle: true,titleText: "ماه های کارکرد",textStyle: txtUnitStyle,margin: 10),
                              leftTitle: AxisTitle(showTitle: true,titleText: "درآمد",textStyle: txtUnitStyle,margin: 10)),
                            betweenBarsData: [
                              BetweenBarsData(
                                fromIndex: 0,
                                toIndex: 0,
                                colors: [Conf.orang],
                              )
                            ],
                            minY: 0,
                            maxY: maxYIncom,
                            titlesData: FlTitlesData(
//            topTitles: SideTitles(
//                getTitles: (a){return 'درآمد( ماه_ریال)';} ,
//                showTitles: true,interval: 50000
//            ,getTextStyles: (val){return TextStyle(
//                  fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black87
//                );}),
//            ),
                              bottomTitles: SideTitles(
                                  showTitles: true,
                                  getTextStyles: (value) => const TextStyle(
                                      fontSize: 10,
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold),
                                  rotateAngle: 90,
                                  getTitles: (value) {
                                    switch (value.toInt()) {
                                      case 1:
                                        return 'فروردین';
                                      case 2:
                                        return 'اردیبهشت';
                                      case 3:
                                        return 'خرداد';
                                      case 4:
                                        return 'تیر';
                                      case 5:
                                        return 'مرداد';
                                      case 6:
                                        return 'شهریور';
                                      case 7:
                                        return 'مهر';
                                      case 8:
                                        return 'آبان';
                                      case 9:
                                        return 'آذر';
                                      case 10:
                                        return 'دی';
                                      case 11:
                                        return 'بهمن';
                                      case 12:
                                        return 'اسفند';
                                      default:
                                        return '';
                                    }
                                  }),
                              leftTitles: SideTitles(
                                showTitles: true,
                                margin: 10,
                                interval: 15000,
//              getTextStyles: (val){return TextStyle(
//                  fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black87
//                );},
                                getTitles: (value) {
                                  return '${value.toInt()} ';
                                },
//                return '${value.toInt()} ریال';},
                              ),
                            ),
                            gridData: FlGridData(
                              show: true,
                              checkToShowHorizontalLine: (double value) {
                                return value == 1 ||
                                    value == 6 ||
                                    value == 4 ||
                                    value == 5;
                              },
                            ),
                          ),
                        ),
                      )
                      ),
                  Padding(
                    padding: EdgeInsets.only(top: Conf.p2h(3)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: GestureDetector(
                            onTap: () {
                              showModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(25),
                                  ),
                                ),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                context: context,
                                isScrollControlled: true,
                                barrierColor: Colors.transparent,
                                builder: (context) => Container(
                                  height: MediaQuery.of(context).size.height * 0.78,
                                  decoration: new BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: new BorderRadius.only(
                                      topLeft: const Radius.circular(25.0),
                                      topRight: const Radius.circular(25.0),
                                    ),
                                  ),
                                  child: Center(
                                      child: CounsalterListPageBottomSheet()),
                                ),
                              );
                            },
                            child: Container(
                              height: Conf.p2h(16),
                              width: Conf.p2w(43),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.grey.withOpacity(0.2)),
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(-1, 2),
                                      color: Colors.grey.withOpacity(0.2))
                                ],
                                borderRadius: BorderRadius.circular(13),
                                color: Colors.white,
                              ),
                              child: FittedBox(
                                fit: BoxFit.contain,
                                child: Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2h(2)),
                                        child: Text(
                                          "ویزیت های امروز",
                                          style: txtStyle,
                                        ),
                                      ),
                                      Text(
                                        countVisit.toString() == ''
                                            ? "0"
                                            : countVisit.toString(),
                                        style: numberStyle,
                                      ),
                                      Text(
                                        "نفر",
                                        style: txtUnitStyle,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: Conf.p2w(5)),
                          child: Container(
                            height: Conf.p2h(16),
                            width: Conf.p2w(43),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Colors.grey.withOpacity(0.2)),
                              boxShadow: [
                                BoxShadow(
//                                spreadRadius: Conf.p2w(0.055),
                                    offset: Offset(-1, 2),
                                    color: Colors.grey.withOpacity(0.2))
                              ],
                              borderRadius: BorderRadius.circular(13),
                            ),
                            child:
//                        Column(children: [
//                          PieChartSample2(),
//                        ],)
                                FittedBox(
                              fit: BoxFit.contain,
                              child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Padding(
                                        padding:
                                            EdgeInsets.only(top: Conf.p2h(2)),
                                        child: Text("رضایت کاربران",
                                            style: txtStyle)),
                                    CircularPercentIndicator(
                                      radius: 60.0,
                                      lineWidth: 10.0,
                                      percent: rank==""||rank==null?0.0:double.parse(rank),
//                                  rankPercent,
//                                  rankPercent.toDouble(),
                                      center: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text("%",
                                              style:
                                                  TextStyle(color: Conf.blue)),
                                          Text(rank==""||rank==null?"0":
                                            rank.toString().replaceAll(RegExp(r'.0'), ""),
                                            style: TextStyle(color: Conf.blue),
                                          ),
                                        ],
                                      ),
                                      progressColor: Conf.blue,
                                      backgroundColor: Colors.grey,
                                    ),
                                    DotsIndicator(
                                      dotsCount: 3,
                                      reversed: true,
                                      position: 0,
                                      decorator: DotsDecorator(
                                        color: Colors.grey, // Inactive color
                                        activeColor: Conf.blue,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: Conf.p2h(2)),
                        child: Container(
                            height: Conf.p2h(32),
                            width: Conf.p2w(43),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey.withOpacity(0.2)),
                              boxShadow: [
                                BoxShadow(
//                                spreadRadius: Conf.p2w(0.055),
                                    offset: Offset(-1, 2),
                                    color: Colors.grey.withOpacity(0.2))
                              ],
                              borderRadius: BorderRadius.circular(13),
                              color: Colors.white,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(1),
                                  child: PieChartSample2(
                                      voice: vo.floorToDouble() - 2,
                                      chat: ch.floorToDouble() - 2,
                                      call: ca.floorToDouble() - 2,
                                      video: vi.floorToDouble() - 2,
                                      voiceVal: vo.toStringAsFixed(1) ?? "",
                                      chatVal: ch.toStringAsFixed(1) ?? "",
                                      callVal: ca.toStringAsFixed(1) ?? "",
                                      videoVal: vi.toStringAsFixed(1) ?? ""),
//                                  child: PieChartSample2(chat:10 ,sms:15 ,text:30 ,video:30 ,chatVal:"10%" ,smsVal:"15%" ,textVal:"30%" ,videoVal:"30%" ,),
                                ),
                                FittedBox(
                                  fit: BoxFit.cover,
                                  child: Padding(
                                    padding: EdgeInsets.only(top: Conf.p2h(2)),
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          backgroundColor: Colors.orange,
                                          radius: 5,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: Conf.p2w(1)),
                                          child: Text("پیام",
                                              style: txtCounsalterKindStyle),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: Conf.p2w(2),
                                              left: Conf.p2w(1)),
                                          child: CircleAvatar(
                                            backgroundColor:
                                                Colors.purple.withOpacity(0.9),
                                            radius: 5,
                                          ),
                                        ),
                                        Text(
                                          "تصویری",
                                          style: txtCounsalterKindStyle,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: Conf.p2w(2),
                                              left: Conf.p2w(1)),
                                          child: CircleAvatar(
                                            backgroundColor: Colors.pink,
                                            radius: 5,
                                          ),
                                        ),
                                        Text(
                                          "چت",
                                          style: txtCounsalterKindStyle,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: Conf.p2w(2),
                                              left: Conf.p2w(1)),
                                          child: CircleAvatar(
                                            backgroundColor:
                                                Colors.lightGreenAccent,
                                            radius: 5,
                                          ),
                                        ),
                                        Text(
                                          "متنی",
                                          style: txtCounsalterKindStyle,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(2), right: Conf.p2w(5)),
                            child: GestureDetector(
                              onTap: () {
//                                Navigator.push(
//                                    context,
//                                    PageTransition(
//                                        type: PageTransitionType.leftToRight,
//                                        duration: Duration(milliseconds: 500),
//                                        alignment: Alignment.centerLeft,
//                                        curve: Curves.easeOutCirc,
//                                        child: DoctorDocument()));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.2)),
                                  boxShadow: [
                                    BoxShadow(
//                                spreadRadius: Conf.p2w(0.055),
                                        offset: Offset(-1, 2),
                                        color: Colors.grey.withOpacity(0.2))
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(13),
                                ),
                                height: Conf.p2h(15),
                                width: Conf.p2w(43),
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding:
                                              EdgeInsets.only(top: Conf.p2h(2)),
                                          child: Text("پرونده های باز",
                                              style: txtStyle),
                                        ),
                                        Text(
                                            dOpenFolder.toString() == ''
                                                ? "0"
                                                : dOpenFolder.toString(),
                                            style: numberStyle),
                                        Text("نفر", style: txtUnitStyle)
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(2), right: Conf.p2w(5)),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.leftToRight,
                                        duration: Duration(milliseconds: 500),
                                        alignment: Alignment.centerLeft,
                                        curve: Curves.easeOutCirc,
                                        child: DoctorDocument()));
                              },
                              child: Container(
                                height: Conf.p2h(15),
                                width: Conf.p2w(43),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.2)),
                                  boxShadow: [
                                    BoxShadow(
//                                spreadRadius: Conf.p2w(0.055),
                                        offset: Offset(-1, 2),
                                        color: Colors.grey.withOpacity(0.2))
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(13),
                                ),
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text("پرونده ها", style: txtStyle),
                                        Text(
//                                      "",
                                            dTotalFolder.toString() == ''
                                                ? "0"
                                                : dTotalFolder.toString(),
                                            style: numberStyle),
                                        Text("نفر", style: txtUnitStyle)
                                      ],
//                    ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigationDoctor(3),
          ),
          loading == true && load == true
              ? Conf.circularProgressIndicator()
              : Container()
        ],
      ),
    );
  }
}

class LineChartSample7 extends StatelessWidget {
  double maxY;
  List<IncomeChartModel> x;

  LineChartSample7({this.maxY, this.x});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      height: 140,
      child: LineChart(
        LineChartData(
          lineTouchData: LineTouchData(enabled: true),
          lineBarsData: [
            LineChartBarData(
              spots: x
                      ?.map((e) => FlSpot(double.parse(e.month),
                          double.parse(e.total.toString())))
                      ?.toList() ??
                  [],
              isCurved: true,
              barWidth: 2,
              colors: [
                Conf.orang,
              ],
              dotData: FlDotData(
                show: false,
              ),
            ),
          ],
          betweenBarsData: [
            BetweenBarsData(
              fromIndex: 0,
              toIndex: 0,
              colors: [Conf.orang],
            )
          ],
          minY: 0,
          maxY: maxY,
          titlesData: FlTitlesData(
//            topTitles: SideTitles(
//                getTitles: (a){return 'درآمد( ماه_ریال)';} ,
//                showTitles: true,interval: 50000
//            ,getTextStyles: (val){return TextStyle(
//                  fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black87
//                );}),
//            ),
            bottomTitles: SideTitles(
                showTitles: true,
                getTextStyles: (value) => const TextStyle(
                    fontSize: 10,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold),
                rotateAngle: 90,
                getTitles: (value) {
                  switch (value.toInt()) {
                    case 1:
                      return 'فروردین';
                    case 2:
                      return 'اردیبهشت';
                    case 3:
                      return 'خرداد';
                    case 4:
                      return 'تیر';
                    case 5:
                      return 'مرداد';
                    case 6:
                      return 'شهریور';
                    case 7:
                      return 'مهر';
                    case 8:
                      return 'آبان';
                    case 9:
                      return 'آذر';
                    case 10:
                      return 'دی';
                    case 11:
                      return 'بهمن';
                    case 12:
                      return 'اسفند';
                    default:
                      return '';
                  }
                }),
            leftTitles: SideTitles(
              showTitles: true,
              margin: 10,
              interval: 15000,
//              getTextStyles: (val){return TextStyle(
//                  fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black87
//                );},
              getTitles: (value) {
                return '${value.toInt()} ';
              },
//                return '${value.toInt()} ریال';},
            ),
          ),
          gridData: FlGridData(
            show: true,
            checkToShowHorizontalLine: (double value) {
              return value == 1 || value == 6 || value == 4 || value == 5;
            },
          ),
        ),
      ),
    );
  }
}

class PieChartSample2 extends StatefulWidget {
  @override
  _PieChartSample2State createState() => _PieChartSample2State();
  int touchedIndex;
  double voice, chat, call, video;
  String voiceVal, chatVal, callVal, videoVal;
  bool loading;
  List c = [];
  List<PieChartModel> PieChartList = [];
  List counts = [], type_strs = [], types = [];
  List<Map<int, int>> list = [];

  PieChartSample2(
      {this.touchedIndex,
      this.voice,
      this.chat,
      this.call,
      this.video,
      this.voiceVal,
      this.chatVal,
      this.callVal,
      this.videoVal,
      this.loading,
      this.c,
      this.PieChartList,
      this.counts,
      this.type_strs,
      this.types,
      this.list});

  calculatePieChart() {
    print("calculatePieChart.....");
    for (int i; i < types.length; i++) {
      if (types.contains(1)) {
        call = 1;
        print("_call ===> ${call}");
      }
      if (types.contains(1)) {
        chat = 2;
        print("_chat ===> ${chat}");
      }
      if (types.contains(1)) {
        video = 3;
        print("_video ===> ${video}");
      }
      if (types.contains(1)) {
        voice = 4;
        print("__voice ===> ${voice}");
      }
    }
    type_strs.forEach((element) {
      if (element == "تلفنی") {
        callVal = " تلفنی";
        print("**callVal ===> ${callVal}");
      }
      if (element == "چت") {
        chatVal = "چت";
        print("_chatVal ===> ${chatVal}");
      }
      if (element == "تصویری") {
        videoVal = "تصویری";
        print("_videoVal ===> ${videoVal}");
      }
      if (element == "صوتی") {
        voiceVal = "صوتی";
        print("__voiceval ===> ${voiceVal}");
      }
    });
  }
}

class _PieChartSample2State extends State<PieChartSample2> {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1, //1.3
      child: Row(
        children: <Widget>[
          const SizedBox(height: 14),
          Expanded(
            child: AspectRatio(
              aspectRatio: 0.5, //1
              child: PieChart(
                PieChartData(
                    pieTouchData:
                        PieTouchData(touchCallback: (pieTouchResponse) {
                      setState(() {
                        if (pieTouchResponse.touchInput is FlLongPressEnd ||
                            pieTouchResponse.touchInput is FlPanEnd) {
                          widget.touchedIndex = -1;
                        } else {
                          widget.touchedIndex =
                              pieTouchResponse.touchedSectionIndex;
                        }
                      });
                    }),
                    borderData: FlBorderData(show: false),
                    sectionsSpace: 0,
                    centerSpaceRadius: 20,
                    sections: showingSections(
                        video: widget.video,
                        text: widget.call,
                        voice: widget.voice,
                        chat: widget.chat,
                        voiceVal: widget.voiceVal ?? "",
                        textVal: widget.callVal ?? "",
                        videoVal: widget.videoVal ?? "",
                        chatVal: widget.chatVal ?? "")),
              ),
            ),
          ),
          const SizedBox(
            width: 10,
//            height: 50,
          ),
        ],
      ),
    );
  }

  List<PieChartSectionData> showingSections(
      {double voice,
      double chat,
      double text,
      double video,
      String voiceVal,
      String chatVal,
      String textVal,
      String videoVal}) {
    return List.generate(4, (i) {
      final isTouched = i == widget.touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.purple.withOpacity(0.9),
            value: video,
            title: videoVal,
//            title: videoVal+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: Colors.orange,
            value: voice,
            title: voiceVal,
//            title: smsVal+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color: Colors.lightGreenAccent,
//            color: const Color(0xff845bef),
            value: text,
            title: textVal,
//            title: textVal+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 3:
          return PieChartSectionData(
            color: Colors.pink,
            value: chat,
            title: chatVal,
//            title: chatVal+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
}
