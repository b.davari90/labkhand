import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/rendering.dart';
import 'package:counsalter5/Doctor/DoctorTimeSliceModel.dart';
import 'package:counsalter5/Doctor/SelectSchaduleDoctor.dart';
import 'package:counsalter5/Doctor/BottomNavigationDoctor.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:counsalter5/Doctor/CounsalterKindComponent.dart';
import 'package:counsalter5/Doctor/CounsalterTimeComponent.dart';
import 'package:persian_date/persian_date.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/htify.dart';

class ReserveTimeDoctor extends StatefulWidget {
  @override
  _ReserveTimeDoctorState createState() => _ReserveTimeDoctorState();
}

class _ReserveTimeDoctorState extends State<ReserveTimeDoctor> {
  TextStyle headerStyle = TextStyle(
      fontSize: Conf.p2t(18),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  int timeSlat = 0, index;
  var totalTime;
  String dateName;
  bool loading;
  Function onTap;
  DoctorTimeSliceModel d;
  List<DoctorPresentsModel> dataList;
  List<DoctorPresentsModel> dataListTime;
  TextEditingController textEditingController;
  PersianDatePickerWidget persianDatePicker;
  DoctorModel doctor;
  SharedPreferences sharedPreferences;
  List<String> addTolistDrKind = [];
  List times = [];
  List endList = [];

  DoctorTimeSliceBloc get doctorTimeSliceBloc =>context.bloc<DoctorTimeSliceBloc>();

  DoctorPresentsBloc get doctorPresentBloc =>context.bloc<DoctorPresentsBloc>();

  Future sendToServer() async {
    setState(() {
      loading = false;
    });
    print("send times ===> ${times}");
    print("send times.lenght ===> ${times.length}");
    print("send times.last.lenght ===> ${times.remove(times.last)}");
    print("send date ===> ${doctorPresentBloc.state.doctorpresent.date}");
    print("send addTolistDrKind ===> ${addTolistDrKind}");
    print("send minutes ===> ${doctorPresentBloc.state.doctorpresent.minutes}");
    try {
      var t = jsonEncode({
        "time_list": times,
        "date": doctorPresentBloc.state.doctorpresent.date,
        "type": addTolistDrKind,
        "minutes": doctorPresentBloc.state.doctorpresent.minutes,
      });
      print("t ===> ${t}");
      var data = await Htify.create("dr/present", {
        "time_list": times,
        "date": doctorPresentBloc.state.doctorpresent.date,
        "type": addTolistDrKind,
        "minutes": doctorPresentBloc.state.doctorpresent.minutes,
      });
      Navigator.pushReplacement(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: SelectSchaduleDoctor()));
    } catch (e) {
      _showTopFlash(title: "لطفا صبور باشید");
      print("Catch ERROR sendToServer ===> ${e.toString()}");
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
  createCard2() {
    dataListTime = [
      DoctorPresentsModel(time_start: "زمان شروع", time_end: "زمان پایان"),
    ];
    return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: dataListTime.map((e) {
          return CounsalterTimeComponent(
            model2: e,
            onTap: () {
              setState(() {});
            },
          );
        }).toList());
  }
  createCard() {
    dataList = [
      DoctorPresentsModel(type: "متنی", isSelectCounsalterKind: false),
      DoctorPresentsModel(type: "تلفنی", isSelectCounsalterKind: false),
      DoctorPresentsModel(type: "تصویری", isSelectCounsalterKind: false)
    ];
    return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: dataList.map((e) {
          return CounsalterKindComponent(
            model2: e,
            onTap: () {
              setState(() {});
            },
          );
        }).toList());
  }
  void initState() {
    createCard();
    createCard2();
    textEditingController = TextEditingController();
    persianDatePicker = PersianDatePicker(controller: textEditingController).init();
    print("reserveTime Conf.token ===> ${Conf.token}");
    print("initstate times ===> ${dataListTime}");
    print("initstate date ===> ${doctorPresentBloc.state.doctorpresent.date}");
    print("initstate addTolistDrKind ===> ${addTolistDrKind}");
    print("initstate minutes ===> ${doctorPresentBloc.state.doctorpresent.minutes}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: true,
      child: Stack(
        children: [
          Scaffold(
            body: BlocBuilder<DoctorPresentsBloc, DoctorPresentsRepository>(
                builder: (context, state) {
                  return Stack(children: <Widget>[
                    Positioned(
                        top: Conf.p2h(0),
                        left: Conf.p2w(0),
                        right: Conf.p2w(0),
                        child: Container(
                          width: Conf.p2w(100),
                          height: Conf.p2h(53),
                          color: Conf.blue,
                          child: Column(children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(
                                        top: Conf.p2h(1), left: Conf.p2w(6)),
                                    child: MasBackBotton())
                              ],
                            ),
                          ]),
                        )),
                    Positioned(
                      top: Conf.p2h(10),
                      left: Conf.p2w(0),
                      right: Conf.p2w(0),
                      bottom: Conf.p2h(0.1),
                      child: Container(
                          width: Conf.p2w(100),
                          height: Conf.p2h(100),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(Conf.p2w(6.25))),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    margin: EdgeInsets.only(
                                        top: Conf.p2h(3), right: Conf.p2h(7)),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                        BorderRadius.all(Radius.circular(7)),
                                        border: Border.all(
                                            color: Colors.grey.withOpacity(0.1)),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey.withOpacity(0.01),
                                              offset: Offset(-2, 3))
                                        ]),
                                    width: Conf.p2w(75),
                                    height: Conf.p2h(6),
                                    child: TextField(
                                      decoration: InputDecoration(
                                          hintText:"انتخاب تاریخ",
//                                          state.doctorpresent.data_shamsi == "" ||
//                                              state.doctorpresent.data_shamsi == null ? "انتخاب تاریخ" : state.doctorpresent.data_shamsi,
                                          hintStyle: txtStyle),
                                      textAlign: TextAlign.center,
                                      enableInteractiveSelection: false,
                                      // *** this is important to prevent user interactive selection ***
                                      onTap: () {
                                        FocusScope.of(context).requestFocus(
                                            new FocusNode()); // to prevent opening default keyboard
                                        showModalBottomSheet(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return persianDatePicker;
                                            });
                                      },
                                      controller: textEditingController,
                                    )),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.p2h(3)),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                            margin: const EdgeInsets.only(
                                                left: 10.0, right: 15.0),
                                            child: Divider(
                                              color: Colors.grey,
                                              height: 50,
                                            )),
                                      ),
                                      Text(
                                        "زمان های خود را انتخاب کنید",
                                        style: headerStyle,
                                      ),
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(
                                                left: 15.0, right: 10.0),
                                            child: Divider(
                                              color: Colors.grey,
                                              height: 50,
                                            )),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      right: Conf.p2w(7), left: Conf.p2w(7)),
                                  child: IntrinsicHeight(
                                    child: Container(
                                      width: Conf.p2w(85),
                                      child: Container(
                                          color: Colors.white,
                                          width: Conf.p2w(90),
                                          child: IntrinsicHeight(
                                            child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                MainAxisAlignment.center,
                                                children: dataListTime.map((e) {
                                                  return Padding(
                                                    padding: EdgeInsets.only(
                                                        bottom: Conf.p2w(2),
                                                        right: Conf.p2w(8),
                                                        left: Conf.p2w(3)),
                                                    child: CounsalterTimeComponent(
                                                      hasRemover:
                                                      dataListTime.last != e,
                                                      model2: e,
                                                      onTap: (DoctorPresentsModel h) {
                                                        setState(() {
                                                          if (dataListTime.isNotEmpty ||
                                                              dataListTime != null) {
                                                            dataListTime.removeWhere(
                                                                    (element) =>
                                                                element
                                                                    .time_start ==
                                                                    "زمان شروع");
                                                            dataListTime.removeWhere(
                                                                    (element) =>
                                                                element.time_end ==
                                                                    "زمان پایان");
                                                          }
                                                          print(
                                                              "dataListTime.map dataListTime ===> ${dataListTime}");
                                                          print(
                                                              "onTap endTimeSlice ===> ${DoctorTimeSliceModel.endTimeSlice}");
                                                          print(
                                                              "onTap startTimeSlice ===> ${DoctorTimeSliceModel.startTimeSlice}");
                                                          d = DoctorTimeSliceModel(
                                                              endTimeSlice:
                                                              DoctorTimeSliceModel
                                                                  .endTimeSlice,
                                                              startTimeSlice:
                                                              DoctorTimeSliceModel
                                                                  .startTimeSlice);

                                                          doctorTimeSliceBloc
                                                              .setTimeSliceList(
                                                              dataListTime);
                                                          print(
                                                              "***doctorTimeSlice ===> ${doctorTimeSliceBloc.state.doctorTimeSlice}");
                                                          if (dataListTime.last == h) {
                                                            dataListTime.add(h.copy2());
                                                          } else {
                                                            dataListTime.remove(h);
                                                          }
                                                          times = dataListTime.map((e) => [e.time_start,e.time_end]).toList();
                                                          print(658962222);
                                                          print("**** times ===> ${times}");
                                                          print("777 ===> ${777}");
                                                        });
                                                      },
                                                    ),
                                                  );
                                                }).toList()),
                                          )),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.p2h(3)),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(
                                                left: 10.0, right: 15.0),
                                            child: Divider(
                                              color: Colors.grey,
                                              height: 50,
                                            )),
                                      ),
                                      Text(
                                        "برش زمانی موردنظر را انتخاب کنید",
                                        style: headerStyle,
                                      ),
                                      Expanded(
                                        child: new Container(
                                            margin: const EdgeInsets.only(
                                                left: 15.0, right: 10.0),
                                            child: Divider(
                                              color: Colors.grey,
                                              height: 50,
                                            )),
                                      ),
                                    ],
                                  ),
                                ),
                                IntrinsicHeight(
                                  child: IntrinsicWidth(
                                    child: Container(
//                              height: Conf.p2h(9),
                                      margin: EdgeInsets.only(
                                          right: Conf.p2w(23),
                                          left: Conf.p2w(15),
                                          top: Conf.p2h(3),
                                          bottom: Conf.p2h(1.5)),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.grey.withOpacity(0.3)),
                                          borderRadius: BorderRadius.circular(10)),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              if (onTap is Function) {
                                                onTap();
                                              }
                                              setState(() {
                                                if (timeSlat > 31) {
                                                  timeSlat = 0;
                                                  print("timeSlat>30 ===> ${timeSlat}");
                                                } else {
                                                  print(
                                                      "doctorTimeSliceList.lenght ===> ${doctorTimeSliceBloc.state.doctorPresentList.length}");
                                                  timeSlat += 10;
                                                  print("timeSlat+10 ===> ${timeSlat}");
                                                }
                                              });
                                            },
                                            child: Container(
                                              width: Conf.p2w(10),
                                              height: Conf.p2w(10),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(8),
                                                  color: Conf.orang),
                                              child: Icon(
                                                Icons.add,
                                                size: Conf.p2t(23),
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width: Conf.p2w(35),
                                            alignment: Alignment.center,
                                            child: Text(
                                              state.doctorpresent.minutes.toString() !=
                                                  null
                                                  ? timeSlat.toString() + "دقیقه"
                                                  : state.doctorpresent.minutes
                                                  .toString() +
                                                  "دقیقه",
                                              style: headerStyle,
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              if (onTap is Function) {
                                                onTap();
                                              }
                                              setState(() {
                                                if (timeSlat < 10) {
                                                  timeSlat = 0;
                                                  print("timeSlat<0 ===> ${timeSlat}");
                                                } else {
                                                  timeSlat -= 10;
                                                  print("timeSlat+10 ===> ${timeSlat}");
                                                }
                                              });
                                            },
                                            child: Container(
//                                      padding:EdgeInsets.only(right: Conf.p2w(2)) ,
//                                      margin: EdgeInsets.only(right: Conf.p2w(2)),
                                              width: Conf.p2w(10),
                                              height: Conf.p2w(10),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(8),
                                                  color: Conf.orang),
                                              child: Icon(
                                                Icons.remove,
                                                size: Conf.p2t(23),
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Row(children: <Widget>[
                                  Expanded(
                                    child: Container(
                                        margin: const EdgeInsets.only(
                                            left: 10.0, right: 15.0),
                                        child: Divider(
                                          color: Colors.grey,
                                          height: 50,
                                        )),
                                  ),
                                  Text(
                                    "نوع مشاوره",
                                    style: headerStyle,
                                  ),
                                  Expanded(
                                    child: Container(
                                        margin: const EdgeInsets.only(
                                            left: 10.0, right: 15.0),
                                        child: Divider(
                                          color: Colors.grey,
                                          height: 50,
                                        )),
                                  ),
                                ]),
                                Container(
                                    margin: EdgeInsets.only(
                                        left: Conf.p2w(6), right: Conf.p2w(6)),
                                    color: Colors.white,
                                    height: Conf.p2h(10),
                                    width: Conf.p2w(90),
                                    child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: dataList.map((e) {
                                          return CounsalterKindComponent(
                                            model2: e,
                                            addLIst: dataList,
                                            onTap: () {
                                              setState(() {
                                                if (addTolistDrKind.contains(e.type)) {
                                                  if (doctorPresentBloc.state.typeList
                                                      .contains(e.type)) {
                                                    addTolistDrKind.remove(e.type);
                                                    print("addTolistDrKind.length remove===> ${addTolistDrKind.length}");

                                                    doctorPresentBloc.removeTypeListPresent(e.type);
                                                    print(" list  remove===> ${addTolistDrKind.length}");
                                                    print("lis remove bloc ===> ${doctorPresentBloc.state.typeList}");
                                                  }
                                                } else {
                                                  addTolistDrKind.add(e.type);
                                                  doctorPresentBloc.setTypeListPresent(e.type);print("list add===> ${addTolistDrKind.length}");}
                                              });
                                            },
                                          );
                                        }).toList())),
                                Padding(
                                  padding: EdgeInsets.only(
//                              top: Conf.p2h(1.5),
//                              bottom: Conf.p2h(2),
                                      left: Conf.p2w(3),
                                      right: Conf.p2w(3)),
                                  child: Container(
                                      width: Conf.p2w(100),
                                      height: Conf.p2w(18),
                                      child: MasRaisedButton(
                                        margin: EdgeInsets.only(
                                            right: Conf.p2w(.5),
                                            left: Conf.p2w(1),
                                            top: Conf.p2w(1.5),
                                            bottom: Conf.p2w(3)),
                                        borderSideColor: Conf.orang,
                                        text: "تایید",
                                        color: Conf.orang,
                                        textColor: Colors.white,
                                        onPress: () {
                                          if(dataListTime.length<=1 || addTolistDrKind.length==0 || timeSlat==0||textEditingController.text==""||textEditingController.text==null)
//                                          if(timeSlat==0&&textEditingController.text=="انتخاب تاریخ" &&dataListTime.length<=1 && addTolistDrKind.length==0)
                                              {
                                            print("عدم تکمیل اطلاعات");
                                            print("dataListTime.length ===> ${ dataListTime.length }");
                                            print("addTolistDrKind.length ===> ${ addTolistDrKind.length }");
                                            print("timeSlat ===> ${ timeSlat }");
                                            print("textEditingController.text ===> ${ textEditingController.text }");
                                            _showTopFlash(title: "عدم تکمیل اطلاعات ");
                                            print("5555 ===> ${ 55550 }");
                                          }
                                          else{
                                            print(2200);
                                            save();
                                          }
                                        },
                                      )),
                                )
                              ],
                            ),
                          )),
                    ),
      loading==true?Conf.circularProgressIndicator():Container()]);
                }),
            bottomNavigationBar: BottomNavigationDoctor(2),
          ),

      ]),
    );
  }

  void save() {
    final startIndex = textEditingController.text.indexOf("/");
    final endIndex = textEditingController.text.indexOf("/", startIndex + "/".length);
    String getHour = textEditingController.text.split("/").first;
    String getDay = textEditingController.text.split("/").last;
    String getMonth = textEditingController.text.substring(startIndex + "/".length, endIndex);

    print("**m ===> ${getMonth}");
    print("**h ===> ${getHour}");
    print("**d ===> ${getDay}");
    DateTime dt = new DateTime(int.parse(getHour), int.parse(getMonth), int.parse(getDay));
    print("dt ===> ${dt}");
    var date = "${dt.year}-${dt.month}-${dt.day}";
    print("__formate2 ===> ${date}");//1400-3-3
    PersianDate persianDate = PersianDate(gregorian: dt.toString());
    print("jalaliToGregorian ===> ${ persianDate.jalaliToGregorian(dt.toString()) }"); //2021-05-24 00:00:00.000
    String finalDate=  persianDate.jalaliToGregorian(dt.toString()).toString();
    finalDate=finalDate.split(" ").first;
    print("finalDate.split(" ").first ===> ${ finalDate.split(" ").first }");

    doctorPresentBloc.set_DateGerogarian_Present(textEditingController.text);
    doctorPresentBloc.setDelayPresent(0);
    doctorPresentBloc.set_DateGerogarian_Present(finalDate);
    doctorPresentBloc.set_DateShamsi_Present(textEditingController.text);
    print("save types ===> ${doctorPresentBloc.state.typeList}");
    doctorPresentBloc.setMinutePresent(timeSlat);

    sendToServer();
  }

  void _showTopFlash({String title, FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              title,
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
          ),
        );
      },
    );
  }
}
