import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/CounsalterReserveDateRowComponent.dart';
import 'package:counsalter5/CounsalterReserveDateRowModel.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flash/flash.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/Doctor/BottomNavigationDoctor.dart';
import 'package:counsalter5/Doctor/ScaduleDoctorModel.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:persian_date/persian_date.dart';
import 'package:counsalter5/Doctor/ScaduleDoctorComponent.dart';
import 'package:counsalter5/ModelBloc/DoctorPresentListModel.dart';
import 'package:shamsi_date/shamsi_date.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params_result.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:counsalter5/Doctor/SelectSlotTimesDoctor.dart';
import 'package:shamsi_date/shamsi_date.dart';
class SelectSchaduleDoctor extends StatefulWidget {
  @override
  _SelectSchaduleDoctorState createState() => _SelectSchaduleDoctorState();
}

class _SelectSchaduleDoctorState extends State<SelectSchaduleDoctor> {
  TextStyle headerStyle = TextStyle(
      fontSize: Conf.p2t(18),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  int timeSlat = 30,
      zero = 0;
  var totalTime;
  bool loading;
  int pages, count, limit;
  String noInformation = "متاسفانه اطلاعاتی ثبت نشده است";

  SearchAdvCubit get searchBloc => context.bloc<SearchAdvCubit>();
  ScrollController sc;
  QueryListParams qlp;
  QueryListParamsResult listResult;
  List<DoctorPresentListModel> doctorPresentModelList;

  DoctorPresentListModel doctorPresentListModel;
  List<DoctorPresentsModel> doctorPresets;
  List<ScaduleDoctorModel> dataList;
  List<Widget> taskList;
  List<int> idDate;
  String monthChoosen,
      dateChoosen,
      dayChoosen,
      yearChoosen = "1400";
  String final_Date_Shamsi, date_Shamsi, final_Date_Shamsi_Month;
  List times_Serever = [],
      slots_Server = [];
  bool isShowList = false;
  int indexMap = 0;
  List types;

  importList(List<Widget> lists) {
    taskList = lists;
  }

  int indexSelectdate;
  List<String> monthList = [
    "فروردین",
    "اردیبهشت",
    "خرداد",
    "تیر",
    "مرداد",
    "شهریور",
    "مهر",
    "آبان",
    "اذر",
    "دی",
    "بهمن",
    "اسفند"
  ];
  String monthName = "";
  int indexMonth = 0;
  List<int> row_Id;
  String finalDate = "";

  List firstSixMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ];
  List secondSixMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30
  ];
  List esfandMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29
  ];
  String finalDay;
List typesLiist,timesfreeLiist;
  DoctorPresentsBloc get doctorPresentBloc =>
      context.bloc<DoctorPresentsBloc>();

  List dayOfMonthList() {
    if (indexMonth == 0 ||
        indexMonth == 1 ||
        indexMonth == 2 ||
        indexMonth == 3 ||
        indexMonth == 4 ||
        indexMonth == 5) {
      return firstSixMonth;
    }
    if (indexMonth == 6 ||
        indexMonth == 7 ||
        indexMonth == 8 ||
        indexMonth == 9 ||
        indexMonth == 10) {
      return secondSixMonth;
    } else {
      return esfandMonth;
    }
  }

  int convertStringMonthTOInteger(String m) {
    switch (m) {
      case "فروردین":
        return 1;
        break;
      case "اردیبهشت":
        return 2;
        break;
      case "خرداد":
        return 3;
        break;
      case "تیر":
        return 4;
        break;
      case "مرداد":
        return 5;
        break;
      case "شهریور":
        return 6;
        break;
      case "مهر":
        return 7;
        break;
      case "آبان":
        return 8;
        break;
      case "آذر":
        return 9;
        break;
      case "دی":
        return 10;
        break;
      case "بهمن":
        return 11;
        break;
      case "اسفند":
        return 12;
        break;
    }
  }

  String changeDayFormate(String str) {
    str = "0" + str;
    return str;
  }

  String format1(Date d) {
    final f = d.formatter;
    return '${f.wN} ${f.d} ${f.mN} ${f.yy}';
  }

  Future sendDate() async {
    dateChoosen = yearChoosen + convertStringMonthTOInteger(monthName).toString() + changeDayFormate(dayChoosen);
    print("__dateChoosen ===> ${dateChoosen}");
    DateTime dt = new DateTime(int.parse(yearChoosen), int.parse(convertStringMonthTOInteger(monthName).toString()), int.parse(changeDayFormate(dayChoosen)));
    PersianDate persianDate = PersianDate(gregorian: dt.toString());
    finalDate = persianDate.jalaliToGregorian(dt.toString()).toString();
    finalDate = finalDate.split(" ").first;
    print("finalDate.split(" ").first ===> ${finalDate.split(" ").first}");
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.create("dr/present/list", {"date": finalDate, "limit": 8, "page": 0});
      print("data __sendDate===> ${ data }");
      doctorPresentListModel = DoctorPresentListModel.fromJson(data);
      count = doctorPresentListModel.count;
      limit = doctorPresentListModel.limit;
      pages = doctorPresentListModel.page;
      doctorPresets.addAll(doctorPresentListModel.rows);
      timesfreeLiist= doctorPresentListModel.rows.map((e) => e.times_free).toList();
      typesLiist=doctorPresentListModel.rows.map((e) => e.types).toList();
      print("typeLiiist____ ===> ${typesLiist.map((e) => e) }");
      print("minutes___===> ${ doctorPresentListModel.rows.map((e) => e.minutes) }");
      print("times_free____ ===> ${  timesfreeLiist.map((e) => e)}");
      print("doctorPresets.ma ===> ${doctorPresets.map((e) => e.time_end)}");
      print("doctorPresentListModel date===> ${doctorPresentListModel.date}");
      String year_Gerogarian = doctorPresentListModel.date.split("-").first;
      final startIndex = doctorPresentListModel.date.indexOf("-");
      final endIndex = doctorPresentListModel.date.indexOf("-", startIndex + "-".length);
      String month_Gerogarian = doctorPresentListModel.date.substring(startIndex + "-".length, endIndex);
      String day_Gerogarian = doctorPresentListModel.date.split("-").last;
      Gregorian g1 = Gregorian(int.parse(year_Gerogarian),int.parse(month_Gerogarian), int.parse(day_Gerogarian));
      Jalali j1 = g1.toJalali();
      int day_shamsi = j1.day;
      int month_shamsi = j1.month;
      int year_shamsi = j1.year;
      date_Shamsi = year_shamsi.toString() + "/" + month_shamsi.toString() + "/" + day_shamsi.toString();
      print("final_Date_Shamsi ===> ${final_Date_Shamsi}"); //1400/1/5
      final_Date_Shamsi = format1(j1).split("00").first;
      finalDay = final_Date_Shamsi.substring(0, final_Date_Shamsi.indexOf("ه")) + "ه";
      times_Serever = doctorPresentListModel.rows.map((e) => [e.time_start, e.time_end]).toList();
      print("times_Serever ===> ${times_Serever}");
      slots_Server = doctorPresentListModel.rows.map((e) => e.minutes).toList();
      idDate = doctorPresentListModel.rows.map((e) => e.id).toList();
      types = doctorPresentListModel.rows.map((e) => e.types).toList();
      row_Id = doctorPresentListModel.rows.map((e) => e.id).toList();
      print("row_Id ===> ${ row_Id.map((e) => e) }");
      doctorPresentBloc.setTypeS(typesLiist);
      print("doctorPresentBloc.state.typeList ===> ${ doctorPresentBloc.state.typeList.map((e) => e) }");
      doctorPresentBloc.setFreeTimes(timesfreeLiist);
      print("doctorPresentBloc.state.timesFreelist ===> ${ doctorPresentBloc.state.doctorpresent.times_free.map((e) => e) }");
    } catch (e) {
      print("CatchError sendData SelectCschaduleDoctor()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  void initState() {
    Gregorian g = Gregorian(Gregorian.now().year, Gregorian.now().month, Gregorian.now().day);
    Jalali jj=g.toJalali();
    print("jj ===> ${ jj }");
    int m=jj.month;
//    int d=jj.day;
    typesLiist=[];
    timesfreeLiist=[];
    monthName = monthList[m-1];
    print("monthName ===> ${ monthName }");
    doctorPresentModelList = [];
    doctorPresets = [];
    types = [];
    idDate = [];
    doctorPresentListModel = DoctorPresentListModel();
    qlp = QueryListParams();
    listResult = QueryListParamsResult();
    sc = ScrollController();
    qlp.page = 0;
    searchBloc.setQlp(qlp);
    sc.addListener(() {
      if (sc.offset >= sc.position.maxScrollExtent - 100 &&
          !sc.position.outOfRange &&
          qlp.page < pages) {
        setState(() {
          qlp.page++;
          searchBloc.setQlp(qlp);
          sendDate();
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    sc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("doctorPresets ===> ${doctorPresets.map((e) => e.minutes)}");
    return SafeArea(
      bottom: true,
      top: true,
      child: Stack(
        children: [
          Scaffold(
            body: Stack(children: <Widget>[
              Positioned(
                  top: Conf.p2h(0),
                  left: Conf.p2w(0),
                  right: Conf.p2w(0),
                  child: Container(
                    width: Conf.p2w(100),
                    height: Conf.p2h(53),
                    color: Conf.blue,
                    child: Column(children: <Widget>[
                    ]),
                  )),
              Positioned(
                top: Conf.p2w(20), //210,
//            bottom: 0,
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(Conf.p2w(6.25))),
                  child: Container(
                    height: Conf.p2h(76),
                    decoration: BoxDecoration(
                        color: Colors.white10.withOpacity(0.7),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 2,
                            blurRadius: 8,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(Conf.p2w(6.25)),
                            topLeft: Radius.circular(Conf.p2w(6.25)))),
                    child: Column(children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                              icon: Icon(Icons.navigate_before),
                              onPressed: () {
                                setState(() {
                                  indexMonth--;
                                  print("before indexMonth ===> ${indexMonth}");
                                  if (indexMonth == -1) {
                                    indexMonth = 11;
                                  }
                                  monthName = monthList.elementAt(indexMonth);
                                  print(
                                      "monthName navigate_before===> ${monthName}");
                                });
                              }),
                          Text(
                            monthName,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: Conf.p2t(24)),
                          ),
                          IconButton(
                              icon: Icon(Icons.navigate_next),
                              onPressed: () {
                                setState(() {
                                  indexMonth++;
                                  print("after indexMonth ===> ${indexMonth}");
                                  if (indexMonth == 12) {
                                    indexMonth = 0;
                                  }
                                  monthName = monthList.elementAt(indexMonth);
                                  print(
                                      "monthName navigate_next===> ${monthName}");
                                });
                              })
                        ],
                      ),
                      Flex(
                        direction: Axis.vertical,
                        children: [
                          SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(2)),
                                child: Row(
                                    children: dayOfMonthList()
                                        .map((e) =>
                                        CounsalterReserveDateRowComponent(
                                            model: CounsalterReserveDateRowModel(numberDate:e.toString()),
                                            isSelectedRow: dayOfMonthList().indexOf(e) == indexSelectdate ? true : false,
                                            onTap: () {
                                              setState(() {
                                                dayChoosen = e.toString();
                                                print(
                                                    "dayChoosen ===> ${dayChoosen}");
                                                indexSelectdate =
                                                    dayOfMonthList()
                                                        .indexOf(e);
                                                dayChoosen.length == 1
                                                    ? changeDayFormate(
                                                    dayChoosen)
                                                    : dayChoosen.toString();
                                                sendDate();
                                              });
                                            }))
                                        .toList()),
                              )),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: Conf.p2w(3)),
                        child: Divider(
                          color: Colors.grey.withOpacity(0.5),
                          thickness: 3,
                          indent: Conf.p2w(5),
                          endIndent: Conf.p2w(5),
                        ),
                      ),
                      Expanded(
                          child: SingleChildScrollView(
                              controller: sc,
                              scrollDirection: Axis.vertical,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: Conf.p2w(2),
                                    bottom: Conf.p2w(7),
                                    top: Conf.p2w(3)),
                                child: slots_Server == null || slots_Server == [] || slots_Server.isEmpty ?
                                Container(
                                  margin: EdgeInsets.only(top: Conf.p2w(10)),
                                  child: Text(noInformation, style: txtStyle,),)
                                    : Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: List.generate(
                                        times_Serever.length,
                                            (i) =>
                                            SchaduleDoctorComponent(
                                              index:
                                              row_Id[i]==0?0:
                                              row_Id[i]==1?1:
                                              row_Id[i]==2?2:
                                              row_Id[i]==3?3:
                                              row_Id[i]==4?4:
                                              row_Id[i]==5?0:
                                             (row_Id[i]%limit==0.0)?0:
                                             (row_Id[i]%limit==1.0)?1:
                                             (row_Id[i]%limit==2.0)?2:
                                             (row_Id[i]%limit==3.0)?3:
                                             (row_Id[i]%limit==4.0)?4:2,
                                                onTap: () {
                                                  setState(() {
                                                    print("ok.........");
                                                    Navigator.push(
                                                        context,
                                                        PageTransition(
                                                            type: PageTransitionType
                                                                .leftToRight,
                                                            duration: Duration(
                                                                milliseconds: 500),
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            curve: Curves
                                                                .easeOutCirc,
                                                            child: SelectSlotTimesDoctor(
                                                              id_row: row_Id[i-1],
                                                              end:times_Serever[i-1][0],
                                                              start:times_Serever[i-1][1],
                                                              GerogarianDate: finalDate,
                                                              idDate: idDate[i -1],
                                                              minute: slots_Server
                                                                  .first,
                                                              types: types,
                                                                rowID:row_Id[i-1]
                                                            )
                                                        ));
                                                  });
                                                },
                                                rowID:row_Id[i],
                                                day: (finalDay.length <= 2 &&
                                                    finalDay
                                                        .startsWith("چ"))
                                                    ? "چهارشنبه"
                                                    : (finalDay.length <= 2 &&
                                                    finalDay.startsWith(
                                                        "س"))
                                                    ? "سه شنبه"
                                                    : finalDay,
                                                model: DoctorPresentsModel(
                                                    minutes: slots_Server[i],
                                                    time_start: times_Serever[i][0],
                                                    time_end: times_Serever[i++][1],
                                                    date: date_Shamsi)))
                                ),
                              )))
                    ]),
                  ),
                ),
              ),
              Positioned(
                child: BottomNavigationDoctor(2),
                bottom: 0,
                left: 0,
                right: 0,
              )
            ]),
          ),
          loading == true ? Conf.circularProgressIndicator() : Container()
        ],
      ),
    );
  }

  Widget _buildButton({VoidCallback onTap, String text, Color color}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: MaterialButton(
        color: color,
        minWidth: double.infinity,
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        onPressed: onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  void _showTopFlash({String title, FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              title,
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
          ),
        );
      },
    );
  }
}
