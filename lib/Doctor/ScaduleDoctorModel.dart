import 'package:flutter/material.dart';
class ScaduleDoctorModel {
    String date;
    String day;
    String endTime;
    String slot;
    String startTime;
    Color colorContainer;
    ScaduleDoctorModel({this.date, this.day, this.colorContainer,this.endTime, this.slot, this.startTime});

    factory ScaduleDoctorModel.fromJson(Map<String, dynamic> json) {
        return ScaduleDoctorModel(
            date: json['date'], 
            day: json['day'], 
            colorContainer: json['colorContainer'],
            endTime: json['endTime'],
            slot: json['slot'], 
            startTime: json['startTime'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['date'] = this.date;
        data['day'] = this.day;
        data['colorContainer'] = this.colorContainer;
        data['endTime'] = this.endTime;
        data['slot'] = this.slot;
        data['startTime'] = this.startTime;
        return data;
    }
}