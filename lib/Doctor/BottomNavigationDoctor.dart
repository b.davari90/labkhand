import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:counsalter5/DoctorInformation//DoctorInformationPage.dart';
import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:counsalter5/Doctor/ReserveTimeDoctor.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/visitPage.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:page_transition/page_transition.dart';

class BottomNavigationDoctor extends StatefulWidget {
  int currentIndex = 0;

  BottomNavigationDoctor(this.currentIndex);

  @override
  _BottomNavigationDoctorState createState() => _BottomNavigationDoctorState();
}

class _BottomNavigationDoctorState extends State<BottomNavigationDoctor> {
  @override
  void initState() {
    super.initState();
  }

  void changePage(int index) {
    if (index == widget.currentIndex) return;
    switch (index) {
      case 0:
        Navigator.push(
            context,
            PageTransition(
                type:PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: DoctorInformationPage()
            ));
        break;
      case 1:
        Navigator.push(
            context,
            PageTransition(
                type:PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: VisitPage()
            ));
        break;
      case 2:
        Navigator.push(
            context,
            PageTransition(
                type:PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: ReserveTimeDoctor()
            ));
        break;
      case 3:
        Navigator.push(
            context,
            PageTransition(
                type:PageTransitionType.leftToRight,
                duration: Duration(milliseconds: 500),
                alignment: Alignment.centerLeft,
                curve: Curves.easeOutCirc,
                child: HomePageDoctor()
            ));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Conf.p2h(11),
      color: Colors.white12,
      child: BubbleBottomBar(
        backgroundColor: Conf.blue,
        opacity: 0.999,
        currentIndex: widget.currentIndex,
        onTap: changePage,
        borderRadius: BorderRadius.vertical(top: Radius.circular(13)),
//        elevation: 30,
//            inkColor: Colors.black12,
        items: <BubbleBottomBarItem>[
          BubbleBottomBarItem(
              backgroundColor: Conf.orang,
              icon: Icon(
                Icons.person_outline,
                color: Colors.white,
              ),
              activeIcon: Icon(
                Icons.person_outline,
                color: Colors.white,
              ),
              title: FittedBox(
                fit: BoxFit.cover,
                child: Text("پروفایل من", softWrap: true,
                    overflow: TextOverflow.fade,style:
                    Conf.body2.copyWith(color: Colors.white)
//                  TextStyle(color: Colors.white)
                ),
              )),
          BubbleBottomBarItem(
              backgroundColor: Conf.orang,
              icon: Icon(
                FeatherIcons.messageSquare,
                color: Colors.white,
              ),
              activeIcon: Icon(
                FeatherIcons.messageSquare,
                color: Colors.white,
              ),
              title: Text("پیام ها", softWrap: true,
                  overflow: TextOverflow.fade,
                  style:Conf.body2.copyWith(color: Colors.white)
//                  TextStyle(color: Colors.white)
              )),
          BubbleBottomBarItem(
              backgroundColor: Conf.orang,
              icon: Icon(
                FeatherIcons.clipboard,
                color: Colors.white,
              ),
              activeIcon: Icon(
                FeatherIcons.clipboard,
                color: Colors.white,
              ),
              title: FittedBox(
                fit: BoxFit.cover,
                child: Text("زمان ها", softWrap: true,
                    overflow: TextOverflow.fade,style:
                Conf.body2.copyWith(color:Colors.white)
//                  TextStyle(color: Colors.white)
                ),
              )),
          BubbleBottomBarItem(
              backgroundColor:  Conf.orang,
//              backgroundColor: Colors.white,
              icon: Icon(
                FeatherIcons.home,
                color: Colors.white,
              ),
              activeIcon: Icon(
                FeatherIcons.home,
                color: Colors.white,
              ),
              title: FittedBox(
                fit: BoxFit.cover,
                child: Text("خانه", softWrap: true,
                  overflow: TextOverflow.fade,style:
                Conf.body2.copyWith(color: Colors.white)
//                TextStyle(color: Colors.white),
                ),
              ))
        ],
      ),
    );
  }
}

@override
Widget build(BuildContext context) {
  // TODO: implement build
  throw UnimplementedError();
}

//

