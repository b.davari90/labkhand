import 'package:counsalter5/Doctor/TimeSlotBoxComponent.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/Doctor/HomePageDoctor.dart';
import 'package:counsalter5/htify.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/Doctor/BottomNavigationDoctor.dart';
import 'package:page_transition/page_transition.dart';
import 'dart:convert';
import 'package:flutter/rendering.dart';
class SelectSlotTimesDoctor extends StatefulWidget {
  String GerogarianDate,start ,end;
  int idDate,minute;
  int id_row,rowID;
  List types;

  SelectSlotTimesDoctor({this.GerogarianDate,this.rowID,this.start,this.end, this.idDate,this.minute,this.types,this.id_row});

  @override
  _SelectSlotTimesDoctorState createState() => _SelectSlotTimesDoctorState();
}

class _SelectSlotTimesDoctorState extends State<SelectSlotTimesDoctor> {
  TextStyle headerStyle = TextStyle(
      fontSize: Conf.p2t(18),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  TextStyle txtStyle2 = TextStyle(
      fontSize: Conf.p2t(18),
      color: Colors.white,
      fontWeight: FontWeight.w700);
//  int index;
//  List timesFree;
  List<DoctorPresentsModel> timesFree;

  bool loading = false,isChoose;
  Function onTap;
  int index;
//
  List<DoctorPresentsModel> doctorTimes;
  DoctorPresentsModel doctorPresentsModel;
  List timeSelected=[];
  Future getTimes() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.one("dr/present",widget.idDate);
      doctorPresentsModel=DoctorPresentsModel.fromJson(data);
      print("times_free_______===> ${  doctorPresentsModel.times_free}");
    } catch (e) {
      print("CATCH ERROR getTimes()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }
  Future sendData() async {
    setState(() {
      loading = false;
    });
    try {
      var t =jsonEncode ({
        "time_start":widget.start,
        "time_end":widget.end,
        "minutes":widget.minute,
        "times_free":timeSelected,
        "type":changeFormateType(widget.types.sublist(0).removeLast())
      });
      print("t ===> ${ t }");
      var data = await Htify.update("dr/present", widget.rowID,{
        "time_start":widget.start,
        "time_end":widget.end,
        "minutes":widget.minute,
        "times_free":timeSelected,
        "type":changeFormateType(widget.types.sublist(0).removeLast())
      });
      _showTopFlash(title: "ثبت اطلاعات",title2: "اطلاعات با موفقیت ذخیره شد",clr:  Colors.green,
          txtstyle2:TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w600,color: Colors.white),
          txtstyle: TextStyle(fontSize:Conf.p2t(12),fontWeight: FontWeight.w600,color: Colors.white));
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: HomePageDoctor()));
    } catch (e) {
      print("CATCH ERROR sendData SelectSlotTime()===> ${e.toString()}");
      _showTopFlash(title: "خطا",title2: "خطای سرور",clr:  Colors.red,
          txtstyle2:TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w600,color: Colors.white),
          txtstyle: TextStyle(fontSize:Conf.p2t(12),fontWeight: FontWeight.w600,color: Colors.white));
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
  List changeFormateType(List t){
    List x=[];
    if (t==null||t==[])return [];
    if(t.contains("تلفنی")&&t.contains("متنی")&&t.contains("تصویری")){return x=[1,2,3];}
    if(t.contains("تلفنی")&&t.contains("متنی")){return x=[1,3];}
    if(t.contains("تلفنی")&&t.contains("تصویری")){return x=[3,2];}
    if(t.contains("متنی")&&t.contains("تصویری")){ return x=[2,1];}
    if(t.contains("تلفنی")){return x=[3];}
    if(t.contains("متنی")){return x=[1];}
    if(t.contains("تصویری")){return x=[2];}
    return x;
  }
  String  funcDate(String d){
    print("d ===> ${ d }");
    String dateWithT = d.substring(0, 8) + d.substring(8);
    print("*dateWithT ===> ${ dateWithT }");
//    DateTime dateTime = DateTime.parse(dateWithT);
//    var formate1 = "${dateTime.day}-${dateTime.month}-${dateTime.year}";
//    print("formate1 ===> ${ formate1 }");
    return dateWithT;}
  void initState() {
    doctorPresentsModel=DoctorPresentsModel();
    doctorPresentsModel.times_free=[];
    doctorPresentsModel.times=[];
    print("Conf.token ===> ${ Conf.token }");
    print("widget.rowID ===> ${ widget.rowID }");
    print("widget.id_row ===> ${ widget.id_row }");
    print("Conf.idLoginDoctor ===> ${ Conf.idLoginDoctor }");
    print("widget.types.s ===> ${ widget.types.sublist(0).removeLast() }");
    print("changeFormateType ===> ${changeFormateType(["متنی","تلفنی"])  }");
    print("100 ===> ${changeFormateType(widget.types.sublist(0).removeLast())  }");
    print("__changeFormateType ===> ${changeFormateType(widget.types)  }");
    print("funcDate(widget.GerogarianDate) ===> ${ funcDate(widget.GerogarianDate) }");
    timesFree=[];
    isChoose=false;
    getTimes();
    print("widget.types ===> ${ widget.types }");
    print("widget.minute ===> ${ widget.minute }");
    print("widget.GerogarianDate ===> ${ widget.GerogarianDate }");
    print("widget.idDate ===> ${ widget.idDate }");
    print("widget.end ===> ${ widget.end }");
    print("widget.stsrt ===> ${ widget.start }");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: true,
      child:
      Scaffold(
        body: Stack(children: <Widget>[
          Positioned(
              top: Conf.p2h(0),
              left: Conf.p2w(0),
              right: Conf.p2w(0),
              bottom: 0,
              child: Container(
                width: Conf.p2w(100),
                color: Conf.lightColor,
                child: Column(crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: Conf.p2h(4)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2h(1), right: Conf.p2w(6)),
                              child: Container(
                                width: Conf.p2w(10),
                                height: Conf.p2w(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Conf.orang),
                                child: GestureDetector(
                                  onTap: () {sendData();},
                                  child: Icon(
                                    Icons.done,
                                    size: Conf.p2w(5), //24
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Text(
                              "جدول زمانی",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: Conf.p2t(20)),
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    top: Conf.p2w(1), left: Conf.p2w(6)),
                                child: MasBackBotton()
                            ),
                          ],
                        ),
                      ),
                    ]),
              )),
          Positioned(
            top: Conf.p2w(26), //210,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
//                    borderRadius: BorderRadius.circular(Conf.p2w(6.25))
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(Conf.p2w(6.25)),
                        topLeft: Radius.circular(Conf.p2w(6.25)))
                ),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.7),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.6),
                          spreadRadius: 2,
                          blurRadius: 8,
                          offset: Offset(
                              0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(Conf.p2w(6.25)),
                          topLeft: Radius.circular(Conf.p2w(6.25)))),
                  child: Flex(
                      direction: Axis.vertical,
                      children:[ Expanded(
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(Conf.p2w(6.25)),
                              topLeft: Radius.circular(Conf.p2w(6.25))),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.p2w(10),right: Conf.p2w(2),left:  Conf.p2w(2)),
                                  child:
                                  Wrap(
                                    runAlignment:WrapAlignment.spaceBetween,
                                    alignment:WrapAlignment. start,
                                    crossAxisAlignment:WrapCrossAlignment.start,
                                    textDirection:TextDirection.ltr,
                                    runSpacing:15,
                                    spacing:Conf.p2w(5),
                                    children:
                                    doctorPresentsModel.times.map((e) {
                                      return
                                        GestureDetector(
                                            onTap: (){
                                              setState(() {
                                                if(timeSelected.contains(e)){
                                                  timeSelected.remove(e);
                                                }else{
                                                  timeSelected.add(e);
                                                }
                                              });
                                            },
                                            child: Container(
                                            width: Conf.p2w(19),
                                            height: Conf.p2w(19),
                                            child: Center(child: Text(e.toString().substring(0,5),style: txtStyle2,)),
                                            decoration: BoxDecoration(
                                              color:timeSelected.contains(e) ?Conf.blue:Colors.red,
                                              border: Border.all(
                                                color:timeSelected.contains(e)?Conf.blue:Colors.red,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.grey.withOpacity(0.4),
                                                    offset: Offset(-1, 3),
                                                    blurRadius: 1.5)
                                              ],
                                              borderRadius: BorderRadius.all(Radius.circular(Conf.p2w(2.71))),
                                            )));
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),]
                  ),
                )),
          ),
          loading==true?Conf.circularProgressIndicator():Container()
        ]
        ),
        bottomNavigationBar: BottomNavigationDoctor(2),

      ),
    );
  }
  void _showTopFlash({FlashStyle style = FlashStyle.floating,String title,title2,TextStyle txtstyle,txtstyle2,Color clr}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor:clr,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Padding(
            padding:EdgeInsets.only(right: Conf.p2w(2)),
            child: Icon(FeatherIcons.alertOctagon,color: Colors.white,size: 40,),
          ),
            title: Text(title,style: txtstyle,),
            message: Text(title2,style: txtstyle2,),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
