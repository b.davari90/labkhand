import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/Doctor/DoctorTimeSliceModel.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounsalterTimeComponent extends StatefulWidget {
  Function onTap, function;
  DoctorPresentsModel model2;
  BuildContext context;
  CounsalterTimeComponent c;
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  String endTime;
  bool hasError = false;
  bool hasRemover = false;
  var end,start;
  DoctorTimeSliceModel doctorTimeSliceModel;
  DoctorTimeSliceModel timeSliceModel;
  CounsalterTimeComponent({this.onTap, this.context, this.model2, this.hasRemover});

  @override
  _CounsalterTimeComponentState createState() =>
      _CounsalterTimeComponentState();
}

class _CounsalterTimeComponentState extends State<CounsalterTimeComponent> {
  DoctorPresentsBloc get doctorPresentBloc => context.bloc<DoctorPresentsBloc>();
  DoctorTimeSliceBloc get doctorTimeSliceBloc => context.bloc<DoctorTimeSliceBloc>();
  DoctorTimeSliceModel d;
  @override
  void initState() {
    doctorTimeSliceBloc.state.doctorPresentList=[];
//    widget.doctorTimeSliceModel=DoctorTimeSliceModel();
//    widget.timeSliceList=[];
    super.initState();
  }
  void _showTopFlash({String title, FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              title,
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
          ),
        );
      },
    );
  }
  Duration _showTimePicker(int a) {
    showDialog(
      context: context,
      builder: (BuildContext _) {
        return PersianDateTimePicker(
          initial: '14:45',
          type: 'time',
          onSelect: (date) {
            if (a == 1) {
              setState(() {
                widget.model2.time_start = date.toString();
                DoctorTimeSliceModel.startTimeSlice=parseDuration(widget.model2.time_start);
                widget.model2.time_end="زمان پایان";
                print("DoctorTimeSliceModel.startTimeSlice.toString() ===> ${ DoctorTimeSliceModel.startTimeSlice.toString() }");
                widget.hasError=false;
                return DoctorTimeSliceModel.startTimeSlice;
              });
            }
            if (a == 2) {
              if(widget.model2.time_start==""||DoctorTimeSliceModel.startTimeSlice==null){
                widget.model2.time_end="زمان پایان";
                _showTopFlash(title: "ابتدا باید زمان شروع انتخاب شود");
              }
              else{
                setState(() {
                  widget.model2.time_end = date.toString();
                  DoctorTimeSliceModel.endTimeSlice=parseDuration(widget.model2.time_end);
                  if(DoctorTimeSliceModel.startTimeSlice.inHours<=DoctorTimeSliceModel.endTimeSlice.inHours && DoctorTimeSliceModel.startTimeSlice.inMinutes<DoctorTimeSliceModel.endTimeSlice.inMinutes){
                    print("DoctorTimeSliceModel.endtime.toString() ===> ${ DoctorTimeSliceModel.endTimeSlice.toString() }");
                    widget.hasError=false;
                    return DoctorTimeSliceModel.endTimeSlice;
                  }else {
                  _showTopFlash(title: 'دقیقه یا ساعت زمان پایان از زمان شروع نمیتواند کمتر یا برابر باشد...');
                  widget.model2.time_end="زمان پایان";
                  widget.hasError=true;
                  return widget.model2.time_end;
                  }
                });
              }
            }
          },
        );
      },
    );
  }
  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    hours = int.parse(s.split(':').first);
    minutes = int.parse(s.split(':').last);
    return Duration(hours: hours, minutes: minutes);
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {},
        child: Padding(
            padding: EdgeInsets.only(right: Conf.p2w(1), left: Conf.p2w(1)),
            child:
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      border: Border.all(color: Colors.grey.withOpacity(0.1)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.01),
                            offset: Offset(-2, 3))
                      ]),
                  width: Conf.p2w(25),
                  height: Conf.p2h(6),
                  child: RaisedButton(
                    child: Text(
                      widget.model2.time_start.toString() ??
                          widget.model2.time_start.toString(),
                      style: widget.txtStyle,
                    ),
                    color: Colors.white,
                    onPressed: () {
                      _showTimePicker(1);

                    },
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(right: Conf.p2w(3), left: Conf.p2w(3)),
                  child: Text("تا", style: widget.txtStyle),
                ),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      color: Colors.white,
                      border: Border.all(color: Colors.grey.withOpacity(0.1)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.01),
                            offset: Offset(-2, 3))
                      ]),
                  width: Conf.p2w(25),
                  height: Conf.p2h(6),
                  child: RaisedButton(
                    child: widget.hasError ? Text("زمان پایان", style: widget.txtStyle) : Text(widget.model2.time_end.toString(), style: widget.txtStyle),
                    color: Colors.white,
                    onPressed: () {
                      _showTimePicker(2);
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    if ( DoctorTimeSliceModel.endTimeSlice==null&&!widget.hasError&&DoctorTimeSliceModel.startTimeSlice==null) {
                      _showTopFlash(title: " لطفا ابتدا زمان شروع و زمان پایان را انتخاب کنید سپس زمان دیگر را اضافه کنید");
                      return;
                    } else{
                      widget.onTap(widget.model2);
                    }
                  },
                  child:widget.hasRemover? Container(
                      margin: EdgeInsets.only(right: Conf.xlSize),
                      width: Conf.p2w(10),
                      height: Conf.p2w(10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.redAccent),
                      child:Icon(
                        Icons.remove,
                        size: Conf.p2t(23),
                        color: Colors.white,
                      )):Container(
                  margin: EdgeInsets.only(right: Conf.xlSize),
                    width: Conf.p2w(10),
                    height: Conf.p2w(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Conf.orang),child:Icon(
                        Icons.add,
                        size: Conf.p2t(23),
                        color: Colors.white,
                      )
                ),
                )],
            )));
  }
}
