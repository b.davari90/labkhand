import 'package:counsalter5/Doctor/CounsalterCardComponent.dart';
import 'package:counsalter5/Doctor/CounsalterListComponentModel.dart';
import 'package:counsalter5/Doctor/CounsalterListPage.dart';
import 'package:counsalter5/config.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:counsalter5/htify.dart';
import 'package:shamsi_date/shamsi_date.dart';
import 'package:counsalter5/ModelBloc/DoctorTodayVisitModel.dart';
class CounsalterListPageBottomSheet extends StatefulWidget {

  @override
  _CounsalterListPageBottomSheetState createState() => _CounsalterListPageBottomSheetState();
}

class _CounsalterListPageBottomSheetState extends State<CounsalterListPageBottomSheet> {
  List<Widget> taskList;
  bool loading=false;
  List<CounsalterListComponentModel> dataList;
  Jalali j2;
List rows;
int cnt;
  importList(List<Widget> list) {
    taskList = list;
  }
  String todayDate(){

    Jalali jNow = Jalali.now();
    String y=jNow.year.toString().length==1?"0"+jNow.year.toString():jNow.year.toString();
    String m=jNow.month.toString().length==1?"0"+jNow.month.toString():jNow.month.toString();
    String d=jNow.day.toString().length==1?"0"+jNow.day.toString():jNow.day.toString();
    return y.toString()+m.toString()+d.toString();
  }
  Future getTodayVisits() async {
    setState(() {
      loading= false;
    });
    try {
      var data = await Htify.get("dr/chart");
    print("data ===> ${ data is Map }");
    Map d=data;
    print("d ===> ${ d }");
      rows=data["rows"];
      print("rows ===> ${ rows }");
    } catch (e) {
      print("CAtch ERROR getTodayVisits() ===> ${e.toString()}");
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
  @override
  void initState() {
    todayDate();
    getTodayVisits();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body:
        Container(decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.975),
            boxShadow: [
              BoxShadow(
                color:
                Colors.black,
//                              blurRadius: 50,//10
                              spreadRadius: 5,//0.4
                            offset: Offset(5, -50)
              )
            ]
        ),
          child: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: Conf.xlSize),
            child: Column(mainAxisAlignment:MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: Conf.p2h(2),bottom: Conf.p2h(2)),
                  child: Center(
                    child: Text(
                    "ویزیت های امروز",
                    style: Conf.headline.copyWith(
                        color: Colors.black,
                        fontSize: Conf.p2t(20),
                        fontWeight: FontWeight.w500,
                        decoration: TextDecoration.none)
            ),
                  ),),
                rows==null||rows==[]?Container(height: Conf.p2h(50),child: Center(
                    child: Text("هیچ بازدیدی وجود ندارد",
                  style:TextStyle(color: Colors.black87,fontSize: 16),))):
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: rows
                    .map((e) => CounsalterCardComponent(
                  onTap: () {
//                        Navigator.push(
//                            context,
//                            PageTransition(
//                                type:PageTransitionType.leftToRight,
//                                duration: Duration(milliseconds: 500),
//                                alignment: Alignment.centerLeft,
//                                curve: Curves.easeOutCirc,
//                                child: CounsalterListPage()
//                            ));
                  },
                  isUsedForBottomSheet: true,
                  model: e,
                ))
                    .toList()
                ),
            ],),
          ),
        ),
//        bottomNavigationBar: BottomNavigation(1),
      ),
    );
  }
}
