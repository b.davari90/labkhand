import 'package:counsalter5/Doctor/SelectSlotTimesDoctor.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:counsalter5/config.dart';
import 'dart:ui';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'dart:math';
import 'package:counsalter5/Doctor/BottomSheetEditeDrTime.dart';
import 'package:random_color/random_color.dart';
import 'package:counsalter5/htify.dart';
class SchaduleDoctorComponent extends StatefulWidget {
  DoctorPresentsModel model;
  Function onTap;
  int index = 0,rowID;
  bool isFirstItem, loading = false,loading1=false;
  String fromHour, fromSlot, day;
  RandomColor _randomColor = RandomColor();
  TextStyle timeDateStyle = TextStyle(
      color: Colors.black87.withOpacity(0.6),
      fontSize: 12,
      fontWeight: FontWeight.w600);
  TextStyle alertBtnStyle = Conf.body1.copyWith(
      color: Colors.white, fontSize: Conf.p2t(18), fontWeight: FontWeight.w600);
  TextStyle titleAlertStyle = Conf.body1.copyWith(
      color: Colors.black, fontSize: Conf.p2t(16), fontWeight: FontWeight.w600);
  TextStyle timeStyle = TextStyle(
      color: Colors.black87.withOpacity(0.6),
      fontSize: 15,
      fontWeight: FontWeight.w600);
  TextStyle describtionStyle = TextStyle(
      color: Colors.black87.withOpacity(0.6),
      fontSize: 16,
      fontWeight: FontWeight.w700);
  DoctorPresentsModel doctorPresentModel;

  SchaduleDoctorComponent(
      {this.model,
      this.onTap,
      this.isFirstItem,
        this.rowID,
      this.fromHour,
      this.fromSlot,this.index,
      this.day});

  _SchaduleDoctorComponentState createState() =>
      _SchaduleDoctorComponentState();
}

class _SchaduleDoctorComponentState extends State<SchaduleDoctorComponent> {
  DoctorPresentsBloc get doctorPresentBloc => context.bloc<DoctorPresentsBloc>();
  List colors = [Colors.greenAccent.withOpacity(0.5),Colors.purpleAccent.withOpacity(0.4), Colors.deepOrange.withOpacity(0.5),Colors.pink.withOpacity(0.2),Colors.yellow.withOpacity(0.5)];
  Random random = new Random();
  void changeIndex() {
    setState(() => widget.index = random.nextInt(4));
  }
  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.only(top: Conf.p2h(1), bottom: Conf.p2h(1)),
      child: GestureDetector(
        onTap: () {
          setState(() {
            if (widget.onTap is Function) {
              widget.onTap();
            }
          });
        },
        child: Container(
          height: Conf.p2h(12),
          child: Padding(
            padding: EdgeInsets.only(right: Conf.p2w(3)),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  Padding(
                    padding:
//                    widget.isFirstItem
//                        ? EdgeInsets.only(top: Conf.p2h(2))
//                        :
                        EdgeInsets.only(top: Conf.p2h(0.3)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                            child: VerticalDivider(
                          color: Colors.black45,
                        )),
                        Text(
                          widget.day ?? "",
                          style: widget.timeDateStyle,
                        ),
                        Text(
                          widget.model.date ?? "",
                          style: widget.timeDateStyle,
                        ),
                        Expanded(
                            child: VerticalDivider(
                          color: Colors.black45,
                        )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: Conf.p2w(3)),
                    child: Container(
                      width: Conf.p2w(70),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.withOpacity(0.1)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.01),
                              offset: Offset(-2, 3))
                        ],
                        color:colors[widget.index],
//                        widget._randomColor.randomColor(
//                            colorHue: ColorHue.orange,
//                            colorBrightness: ColorBrightness.veryLight,
//                            colorSaturation: ColorSaturation.mediumSaturation),
//                        color: Conf.purplecontainerColor,
//                        color: widget.model.colorContainer,
                        borderRadius: BorderRadius.circular(13),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(2), right: Conf.p2w(3)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                widget.fromHour != null
                                    ? Text(
                                        widget.fromHour,
                                        style: widget.describtionStyle,
                                      )
                                    : Row(
//                              mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "از ساعت ",
                                            style: widget.describtionStyle,
                                          ),
                                          Text(
                                            widget.model.time_start ?? "",
                                            style: widget.timeStyle,
                                          ),
                                          Text(
                                            "تا " ?? "",
                                            style: widget.describtionStyle,
                                          ),
                                          Text(
                                            widget.model.time_end ?? "",
                                            style: widget.timeStyle,
                                          )
                                        ],
                                      ),
                                widget.fromSlot != null
                                    ? Text(widget.fromSlot)
                                    : Row(
                                        children: [
                                          Text(
                                            "برش های زمانی",
                                            style: widget.describtionStyle,
                                          ),
                                          Text(
                                            widget.model.minutes.toString() ??
                                                "",
                                            style: widget.describtionStyle,
                                          ),
                                          Text(
                                            "دقیقه ای",
                                            style: widget.describtionStyle,
                                          ),
                                        ],
                                      )
                              ],
                            ),
                          ),
                          Row(
//                    mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                contextMenu(
                                    title: "title",
                                    selectionIsNull: "selection is null",
                                    subTitle: "subTitle")
                              ])
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  Future deleteRow(int rowId) async {
    setState(() {
      widget.loading = false;
    });
    try {
      var x= await Htify.delete("dr/present",rowId );
      print("x ===> ${ x }");
      print("delete row id is Succesful rowId ===> ${ rowId }");
      Navigator.pop(context);
    } catch (e) {
      print("deleteRow CATCH ERROR  ===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading1 = true;
      });
    }
  }
  Widget contextMenu({String title, subTitle, selectionIsNull}) {
    String _selection;
    return Container(
      width: Conf.p2w(15),
      height: Conf.p2h(10),
      child: PopupMenuButton<String>(
        onSelected: (String value) {
          setState(() {
            _selection = value;
            if (value == "Value3") {
              showAlertDialog(context);
              print("3333 ===> ${3333}");

            }  if (value == "Value2") {
              print("widget.model.id displayBottomSheet===> ${ widget.rowID }");
              print("widget.model.time_start***__ ===> ${ widget.model.time_start }");
              displayBottomSheet(context);
              print("2222 ===> ${2222}");
            }});
        },
        child: ListTile(
          subtitle: IconButton(icon: Icon(FeatherIcons.moreVertical)),
        ),
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: 'Value2',
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: Conf.p2w(2)),
                  child: Icon(
                    FeatherIcons.edit,
                    size: Conf.p2w(3),
                  ),
                ),
                Text('ویرایش', style: widget.timeStyle),
              ],
            ),
          ),
          PopupMenuItem<String>(
            value: 'Value3',
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: Conf.p2w(2)),
                  child: Icon(
                    FeatherIcons.fileMinus,
                    size: Conf.p2w(3),
                  ),
                ),
                  Text(
                    'حذف',
                    style: widget.timeStyle,
                  ),
//                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  Widget displayBottomSheet(BuildContext context) {

    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(25),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        barrierColor: Colors.transparent,
//        backgroundColor: Colors.white,
        context: context,
        builder: (ctx) {
          return  Container(
            decoration: BoxDecoration(  border: Border.all(color: Colors.grey.withOpacity(0.5)),
              boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
//              blurRadius: 100.0,
//                offset: Offset(-5, 10), // changes position of shadow
              ),
            ],),
            height: Conf.p2h(60), //49
            child: Column(
              children: [
                GestureDetector(onTap: (){Navigator.pop(context);},
                  child: Padding(
                    padding: EdgeInsets.only(top: Conf.p2w(2)),
                    child: Icon(
                      FeatherIcons.chevronDown,
                      color: Colors.black38,
                      size: Conf.p2t(28),
                    ),
                  ),
                ),
                BottomSheetEditeDrTime(
                  hasRemover: false,
                  rowId: widget.rowID ,
                  startTime: widget.model.time_start,
                  endTime:widget.model.time_end
                  ,slot:widget.model.minutes,),
              ],
            ),
          );
        });
  }
  showAlertDialog(BuildContext cntx) {
    AlertDialog alert = AlertDialog(
      content: Icon(FeatherIcons.alertTriangle, color: Colors.red, size: 70),
      shape:
      new RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      title: Text(
        "آیا میخواهید ساعت مورد نظر را حذف کنید؟",
        style:widget. titleAlertStyle,
      ),
      actions: [
        Center(
          child: Container(
            width: Conf.p2w(70), height: Conf.p2h(11),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: Conf.p2w(1.5), right: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      color: Conf.orang,
                      onPressed: () {
                        deleteRow(widget.rowID);
                      },
                      child: Text("بله", style:widget. alertBtnStyle),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      right: Conf.p2w(1.5), left: Conf.p2w(1.5)),
                  child: Container(
                    width: Conf.p2w(30),
//                    color: Colors.purple,
                    height: Conf.p2h(7.5),
                    child: RaisedButton(
                      color: Conf.orang,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("خیر", style:widget. alertBtnStyle),
                    ),
                  ),
                )
//            cancelButton,
//            continueButton,
              ],
            ),
          ),
        )
      ],
    );
    showDialog(
      context: cntx,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
