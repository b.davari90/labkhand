import 'package:counsalter5/IncreaseMoneyEdited.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/doctor/BottomNavigationDoctor.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class CounsalterPageFormDoctor extends StatefulWidget {
  String dpValue;
  final _formKey = GlobalKey<FormBuilderState>();
  TextStyle titleStyle = Conf.headline.copyWith(
      color: Colors.black,
      fontSize: Conf.p2t(25),
      fontWeight: FontWeight.w500,
      decoration: TextDecoration.none);
  TextStyle subTitleStyle = Conf.title.copyWith(
      fontSize: Conf.p2t(14),
      fontWeight: FontWeight.w700,
      color: Colors.black87);
  TextStyle radiobuttonStyle = Conf.title.copyWith(
      fontSize: Conf.p2t(13),
      fontWeight: FontWeight.w500,
      color: Colors.black87);
  TextStyle timeDateStyle = Conf.body1.copyWith(
      color: Colors.black, fontWeight: FontWeight.w500, fontSize: Conf.p2t(15));

  @override
  _CounsalterPageFormDoctorState createState() =>
      _CounsalterPageFormDoctorState();
}

class _CounsalterPageFormDoctorState extends State<CounsalterPageFormDoctor> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
//            bottomNavigationBar: BottomNavigationDoctor(2),
          resizeToAvoidBottomInset: true,
          resizeToAvoidBottomPadding: true,
          bottomNavigationBar:
          Container(height: Conf.p2w(20),
            width: double.infinity,color: Conf.lightColor,
            child: MasRaisedButton(
              margin: EdgeInsets.only(right:Conf.p2w(3),left:Conf.p2w(3),top: Conf.p2w(2),bottom: Conf.p2w(2)),
              borderSideColor: Conf.orang,
              text: "تایید",
              textColor: Colors.white,color: Conf.orang,onPress: (){
                Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.leftToRight,
                              duration: Duration(milliseconds: 500),
                              alignment: Alignment.centerLeft,
                              curve: Curves.easeOutCirc,
                              child: IncreaseMoneyEdited()));

              },),
          )
//          Container(
//            color: Conf.lightColor,
//            child: Container(
//              margin: Conf.xlEdge,
//              width: double.infinity,height: Conf.p2h(6.5),color: Colors.white,
//                child: RaisedButton(
//                  color: Conf.orang,
//                  shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(13.0),
//                      side: BorderSide(
//                        color: Conf.orang,
//                      )),
//                  onPressed: () {
//                    Navigator.push(
//                        context,
//                        PageTransition(
//                            type: PageTransitionType.leftToRight,
//                            duration: Duration(milliseconds: 500),
//                            alignment: Alignment.centerLeft,
//                            curve: Curves.easeOutCirc,
//                            child: IncreaseMoneyEdited()));
//                            child: IncreaseMoneyEdited()));
//                  },
//                  child: FittedBox(
//                    fit: BoxFit.cover,
//                    child: Text(
//                      "تایید",
//                      softWrap: true,
//                      overflow: TextOverflow.fade,
//                      style: TextStyle(
//                          fontSize: Conf.p2t(22),
////                            fontSize: 26,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                ),
//              ),
//          ),
          ,body: Stack(
          children: [
            Positioned(top: 0,left: 0,right: 0,child: Container(
              width: double.infinity,
              padding: EdgeInsets.only(right: Conf.xlSize,top: Conf.xxlSize),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(children: [
                      Container(
//                        color: Colors.red,
                        width: Conf.p2w(16.8),
                        height: Conf.p2w(16.8),
//                        height: Conf.p2w(9.55),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(43.0),
                          child: FadeInImage.assetNetwork(
                            placeholder: "assets/images/prof1.jpg",
                            image:
                            "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: Conf.p2w(2.0839)),
//                    padding: const EdgeInsets.only(right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FittedBox(
                              fit: BoxFit.cover,
                              child: Row(
                                children: <Widget>[
                                  Text("مشاور حسین",
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                      style: widget.titleStyle
//                                        txtStyleName,
                                  ),
                                  Text(" کلایی",
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                      style: widget.titleStyle),
                                ],
                              ),
                            ),
                            Text("مرد",
                                softWrap: true,
                                overflow: TextOverflow.fade,
                                style: widget.subTitleStyle
//                                txtStyleAboutDoctor
                            )
                          ],
                        ),
                      ),
                    ],),
                  )


                  ,Padding(
                    padding:
                    EdgeInsets.only(top: Conf.p2w(0), left: Conf.p2w(5)),
                    child: MasBackBotton(),
                  ),
                ],
              ),
            ),),
            Positioned(top:Conf.p2w(30),bottom: 0,left:0,right: 0
                ,child: Container(
//                  height: Conf.p2h(100),
              decoration: BoxDecoration(
                  color: Colors.white,
//                      color: Colors.white10.withOpacity(0.5),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 10,
//                              offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30))),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
                child: Flex(
                  direction: Axis.vertical,
                  children:[ Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2w(5.2), right: Conf.p2w(6.25)),
//                          top: Conf.p2w(6.25), right: Conf.p2w(6.25)),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.calendar_today,
                                  color: Conf.blue,
                                  size: Conf.p2t(25),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      right: Conf.p2w(1.042), top: Conf.p2h(0.25)),
                                  child: Flex(direction: Axis.vertical, children: [
                                    RichText(
                                      text: TextSpan(children: [
                                        TextSpan(
                                          text: "شنبه ",
                                          style: widget.timeDateStyle,
                                        ),
                                        TextSpan(
                                            text: " 15 ",
                                            style: widget.timeDateStyle),
                                        TextSpan(
                                            text: "اردیبهشت ",
                                            style: widget.timeDateStyle),
                                        TextSpan(
                                            text: " 99 ",
                                            style: widget.timeDateStyle),
                                        TextSpan(
                                            text: "،ساعت: ",
                                            style: widget.timeDateStyle),
                                        TextSpan(
                                            text: " 15:40 ",
                                            style: widget.timeDateStyle),
                                        TextSpan(
                                            text: "دقیقه",
                                            style: widget.timeDateStyle),
                                      ]),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(0.6),
                                right: Conf.p2w(6.25),
                                bottom: Conf.p2w(2.0839)),
                            child: Text(
                              "دارو و پیگیری ها",
                              style: widget.timeDateStyle,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: Conf.xlSize + 5),
                            width: Conf.p2w(100),
                            height: Conf.p2h(15),
                            child: TextField(
                                keyboardType: TextInputType.text,
                                expands: true,
                                textAlignVertical: TextAlignVertical.top,
                                maxLines: null,
                                decoration: InputDecoration(
                                  alignLabelWithHint: false,
                                  errorMaxLines: 10,
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10),
                                    ),
                                  ),
                                  filled: true,
                                  hintStyle: new TextStyle(
                                    fontSize: Conf.p2t(14),
                                    color: Colors.black12.withOpacity(0.6),
                                  ),
                                  hintText: "دارو و پیگیری ها را وارد کنید",
                                  fillColor: Colors.grey.withOpacity(0.01),
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(0.6),
//                          top: Conf.p2h(1),
                                right: Conf.p2w(6.25),
                                bottom: Conf.p2w(2.0839)),
//                  const EdgeInsets.only(top: 20, right: 30, bottom: 10),
                            child: Text(
                              "خلاصه مشاوره ها",
                              style: widget.timeDateStyle,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: Conf.xlSize + 5),
                            width: Conf.p2w(100),
                            height: Conf.p2h(15),
                            child: TextField(
                                keyboardType: TextInputType.text,
                                expands: true,
                                textAlignVertical: TextAlignVertical.top,
                                maxLines: null,
                                decoration: InputDecoration(
                                  alignLabelWithHint: false,
                                  errorMaxLines: 10,
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10),
                                    ),
                                  ),
                                  filled: true,
                                  hintStyle: new TextStyle(
                                    fontSize: Conf.p2t(14),
                                    color: Colors.black12.withOpacity(0.6),
                                  ),
                                  hintText: "خلاصه مشاوره ها را وارد کنید",
                                  fillColor: Colors.grey.withOpacity(0.01),
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                right: Conf.p2w(5.5), top: Conf.p2h(2)),
                            child: Text(
                              "آیا متقاضی توانسته مشکل خود را به طور کامل توضیح دهد؟",
                              style: widget.timeDateStyle,
                            ),
                          ),
                          FormBuilder(
                              key: widget._formKey,
                              child: Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: Conf.p2w(2.5), top: Conf.p2h(0)),
                                    child: Container(
                                      width: Conf.p2w(70),
//                                    height: Conf.p2h(9),
                                      child: FormBuilderRadioGroup(
                                        activeColor: Conf.orang,
                                        decoration:
                                        InputDecoration(border: InputBorder.none),
                                        options: ["بله", "خیر"]
                                            .map(
                                              (e) => FormBuilderFieldOption(
                                            value: e,
                                            child: Text(
                                              "$e",
                                              style: widget.radiobuttonStyle,
                                            ),
                                          ),
                                        )
                                            .toList(growable: false),
                                        validators: [
                                          FormBuilderValidators.required(
                                              errorText: "الزامی"),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              )),
                          Padding(
                            padding: EdgeInsets.only(
                              right: Conf.p2w(5.5),
                            ),
                            child: Text(
                              "آیا مشکل وی با آنچه تشخیص شماست مطابقت دارد؟",
                              style: widget.timeDateStyle,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              right: Conf.p2w(2.5),
//                                top: Conf.p2h(1)
                            ),
                            child: FormBuilderRadioGroup(
                              decoration:
                              InputDecoration(border: InputBorder.none),
                              activeColor: Conf.orang,
                              options: ["بله", "خیر", "هنوز قابل تشخیص نیست"]
                                  .map(
                                    (e) => FormBuilderFieldOption(
                                  value: e,
                                  child: Text(
                                    "$e",
                                    style: widget.radiobuttonStyle,
                                  ),
                                ),
                              )
                                  .toList(growable: false),
                              validators: [
                                FormBuilderValidators.required(
                                    errorText: "الزامی"),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),]
                ),
              ),
            ))
          ],
        )

       ));
  }
}
