class CounsalterListComponentModel {
    String day,family,img,month,name,year;

    CounsalterListComponentModel({this.day, this.family, this.img, this.month, this.name, this.year});

    factory CounsalterListComponentModel.fromJson(Map<String, dynamic> json) {
        return CounsalterListComponentModel(
            day: json['day'],
            family: json['family'],
            img: json['img'],
            month: json['month'],
            name: json['name'],
            year: json['year'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['day'] = this.day;
        data['family'] = this.family;
        data['img'] = this.img;
        data['month'] = this.month;
        data['name'] = this.name;
        data['year'] = this.year;
        return data;
    }
}