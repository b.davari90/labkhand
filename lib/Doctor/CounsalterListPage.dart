import 'package:counsalter5/BottomNavigation.dart';
import 'package:counsalter5/Doctor/CounsalterCardComponent.dart';
import 'package:counsalter5/Doctor/CounsalterListComponentModel.dart';
import 'package:counsalter5/Doctor/CounsalterPageFormDoctor.dart';
import 'package:counsalter5/config.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:page_transition/page_transition.dart';

class CounsalterListPage extends StatefulWidget {
  @override
  _CounsalterListPageState createState() => _CounsalterListPageState();
}

class _CounsalterListPageState extends State<CounsalterListPage> {
  List<Widget> taskList;
  List<CounsalterListComponentModel> dataList;

  importList(List<Widget> list) {
    taskList = list;
  }

//  createCard() {
//    dataList = [
//      CounsalterListComponentModel(
//          day: "26",
//          year: "99",
//          month: "11",
//          name: "رضا",
//          family: "پاکروان",
//          img: "assets/images/son.png"),
//      CounsalterListComponentModel(
//          day: "14",
//          year: "99",
//          month: "02",
//          name: "حسین",
//          family: "کلایی",
//          img: "assets/images/prof1.jpg"),
//      CounsalterListComponentModel(
//          day: "01",
//          year: "99",
//          month: "08",
//          name: "علیرضا",
//          family: "بهاری",
//          img: "assets/images/prof1.jpg"),
//      CounsalterListComponentModel(
//          day: "18",
//          year: "99",
//          month: "07",
//          name: "مهدی",
//          family: "رضایی",
//          img: "assets/images/prof1.jpg"),
//      CounsalterListComponentModel(
//          day: "06",
//          year: "99",
//          month: "07",
//          name: "محمد",
//          family: "محمدی",
//          img: "assets/images/prof1.jpg"),
//      CounsalterListComponentModel(
//          day: "12",
//          year: "99",
//          month: "01",
//          name: "علی",
//          family: "رحیمی",
//          img: "assets/images/prof1.jpg"),
//    ];
//    return SingleChildScrollView(
//        scrollDirection: Axis.horizontal,
//        child: Padding(
//          padding: const EdgeInsets.only(left: 25, top: 4, bottom: 10),
//          child: Column(
//            children: dataList
//                .map((item) => CounsalterCardComponent(
//                    model: item,
//                    onTap: (key) {
//                      Navigator.push(
//                          context,
//                          PageTransition(
//                              type:PageTransitionType.leftToRight,
//                              duration: Duration(milliseconds: 500),
//                              alignment: Alignment.centerLeft,
//                              curve: Curves.easeOutCirc,
//                              child: CounsalterPageFormDoctor()
//                          ));
////
////                      setState(() {
//////                        taskList = item.taskList;
////                      });
//                    }))
//                .toList(),
//          ),
//        ));
//  }
  @override
  void initState() {
//    createCard();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
            bottomNavigationBar: BottomNavigation(1),
            body: Container(color: Colors.white,
              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                Padding(
                  padding:EdgeInsets.only(top: Conf.p2h(5)),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(width: Conf.p2w(30),),
                      Text("لیست کاربران",
                          style: Conf.headline.copyWith(
                              color: Colors.black,
                              fontSize: Conf.p2t(25),
                              fontWeight: FontWeight.w500,
                              decoration: TextDecoration.none)),
                      Container(width: Conf.p2w(25),),
//                   MasBackButton()
                    ],
                  ),
                ),
                Expanded(
                    child: Container(
//                      padding: EdgeInsets.only(top: Conf.p2h(2)),
                        margin: EdgeInsets.only(top: Conf.p2h(8)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 10,
                                  spreadRadius: 0.4,
                                  offset: Offset(-1, 4))
                            ]),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text("")
//                          Column(
//                              crossAxisAlignment:
//                                  CrossAxisAlignment.start,
//                              children: dataList
//                                  .map((e) => Padding(
//                                    padding:EdgeInsets.only(top: Conf.p2h(3)),
//                                    child: CounsalterCardComponent(
//                                          onTap: () {
//                                            Navigator.push(
//                                                context,
//                                                PageTransition(
//                                                    type:PageTransitionType.leftToRight,
//                                                    duration: Duration(milliseconds: 500),
//                                                    alignment: Alignment.centerLeft,
//                                                    curve: Curves.easeOutCirc,
//                                                    child: CounsalterPageFormDoctor()
//                                                ));
//                                          },
//                                      isUsedForBottomSheet: false,
//                                          model: e,
//                                        ),
//                                  ))
//                                  .toList()),
                        )))
              ]),
            )));
  }
}
