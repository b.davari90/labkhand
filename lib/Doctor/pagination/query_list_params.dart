

class QueryListParams {
//    List<QLPFilter> filters;
    String export;
    int limit;
    List<String> order;
    int page;
    List<String> select;
    String textSearch;
    Map search;

    QueryListParams({ this.export, this.limit, this.order, this.page, this.select, this.textSearch,this.search});

    factory QueryListParams.fromJson(Map<String, dynamic> json) {
        return QueryListParams(
            export: json['export'], 
            limit: json['limit'], 
            order: json['order'], 
            page: json['page'], 
            select: json['select'], 
            textSearch: json['search'],
        );
    }

    Map<String, dynamic> toJson({bool swap}) {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        // if(swap == true){
        //     // data['export'] = this.export;
        //     data['limit'] = this.limit;
        //     data['order'] = this.order;
        //     data['page'] = this.page;
        //     data['select'] = this.select;
        //     data['search'] = this.textSearch;
        //     // data['filters'] = this.filters.map((item) => item.toJson()).toList() ?? null;
        //     data['op'] = 'swap';
        // }else{
            // data['export'] = this.export;
            data['limit'] = this.limit;
            data['order'] = this.order;
            data['page'] = this.page;
            data['select'] = this.select;
            data['search'] = search ?? {};
            // data['filters'] = this.filters.map((item) => item.toJson()).toList() ?? null;
        // }
        return data;
    }
}