import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:counsalter5/Doctor/pagination/search_repository.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
class SearchAdvCubit extends Cubit<SearchRepository>{
  SearchAdvCubit(SearchRepository state) : super(state);

  // void setParentCity(City parentCity){
  //   state.parentCity = parentCity;
  //   refresh();
  // }

  void setHasLogo(bool hasLogo){
    state.hasLogo = hasLogo;
    refresh();
  }

  void setCharityTitle(String charityTitle){
    state.charityName = charityTitle;
    refresh();
  }

  void setManagerName(String managerName){
    state.managerName = managerName;
    refresh();
  }

  void setAboutUs(String aboutUs){
    state.aboutUs = aboutUs;
    refresh();
  }

  void setIsMelli(bool isMelli){
    state.isMelli= isMelli;
    refresh();
  }

  // void setCity(City city){
  //   state.setCity = city;
  //   refresh();
  // }

  void setHasImage(bool hasImage){
    state.hasImage = hasImage;
    refresh();
  }

  void setHasVideo(bool hasVideo){
    state.hasVideo = hasVideo;
    refresh();
  }

  void setFromDate(String fromDate){
    state.fromDate = fromDate;
    refresh();
  }

  void setToDate(String fromDate){
    state.toDate = fromDate;
    refresh();
  }

  // void addAttrValue(AttrValue item){
  //   if(state.attrValue is !List){
  //     state.attrValue = [];
  //   }
  //   state.attrValue.add(item);
  //   print('state.attrValue.first ===> ${state.attrValue.first.value}');
  //   refresh();
  // }
  //
  // void clearAttrValue(){
  //   if(state.attrValue is !List){
  //     state.attrValue = [];
  //   }
  //   state.attrValue.clear();
  //   refresh();
  // }
  //
  // void setCollection(Collection collection) {
  //   state.collection = collection;
  //   refresh();
  // }

  void setFromPrice(String fromPrice){
    print('fromPrice ===> ${fromPrice}');
    state.fromPrice = fromPrice == null || fromPrice == ''  ? null: num.parse(fromPrice.toString());
    refresh();
  }

  void setToPrice(String toPrice){
    state.toPrice = toPrice == null || toPrice == '' ? null:num.parse(toPrice.toString());
    refresh();
  }

  // void setCollections(List<Collection> collectionList){
  //   if(state.collections is !List){
  //     state.collections = [];
  //   }
  //   state.collections = collectionList;
  //   refresh();
  // }
  //
  void setQlp(QueryListParams qlp){
    state.qlp = qlp;
    refresh();
  }

  void setFast(bool fast){
    state.fast = fast;
    refresh();
  }

  void setTime(int time){
    state.time = time ?? 0;
    refresh();
  }

  void setPhotographer(bool photo){
    state.photographer = photo;
    refresh();
  }

  void setCityNames(List cityName){
    if(state.cityName is !List){
      state.cityName = [];
    }
    state.cityName = cityName;
    refresh();
    print('state.cityName ===> ${state.cityName}');
  }


  void setAddCityId(List cityId){
    if(state.cities is !List){
      state.cities = [];
    }
    state.cities = cityId;
    refresh();
  }
  //
  // void setAddFilter(QLPFilter filter){
  //   if(state.qlpFilters is !List){
  //     state.qlpFilters = [];
  //   }
  //   state.qlpFilters.add(filter);
  //   print('state.qlpFilters.length ===> ${state.qlpFilters.map((e) => e.title).toList()}');
  //   refresh();
  // }

  // void setRemoveFilter(QLPFilter filter){
  //   if(state.qlpFilters is !List){
  //     state.qlpFilters = [];
  //   }
  //   state.qlpFilters.removeWhere((element) => element == filter);
  //   refresh();
  // }
  //
  // void setCollectionAttributes(List<CollectionAttribute> colAttribute){
  //   if(state.collectionAttribute is !List){
  //     state.collectionAttribute = [];
  //   }
  //   state.collectionAttribute = colAttribute;
  //   refresh();
  // }

  void setFieldValue(String key , dynamic initValue){
    state.setFieldValue(key, initValue);
    refresh();
  }


  // void setAddAdvCompare(Adv item){
  //   state.advCompareList.add(item);
  //   refresh();
  // }
  //
  // void setRemoveAdvCompare(){
  //   state.advCompareList.removeLast();
  //   refresh();
  // }

  // removeOfQlpFilter(String field){
  //   state.qlpFilters.removeWhere((element) => element.field == field);
  //   refresh();
  // }
  //
  // removeOfQlpFilterForAttributes(String field , var value,bool attrId){
  //   if(attrId == true){
  //     state.qlpFilters.removeWhere((element) => element.field == field && element.value == value);
  //   }
  //   else{
  //     state.qlpFilters.removeWhere((element) => element.field == field && element.id.toString() == value.toString());
  //   }
  //   refresh();
  // }

  void cleanSearchAdvCubit(){
    return emit(SearchRepository.clear());
  }

  refresh(){
    return emit(SearchRepository.copyFrom(state));
  }

}

