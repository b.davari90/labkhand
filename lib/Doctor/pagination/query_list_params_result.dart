

class QueryListParamsResult {
  int page, limit, count;
  String textSearch;
  List<Map> filters;
  List<String> select, order;
  List rows;

  QueryListParamsResult(
      {this.page,
      this.limit,
      this.textSearch,
      this.filters,
      this.select,
      this.order,
      this.count,
      this.rows});

  factory QueryListParamsResult.fromJson(Map<String, dynamic> json) {
    try {
      return QueryListParamsResult(
        page: int.parse(json['query']['page'].toString()) ?? null,
        limit: json['query']['limit'] ?? null,
        rows: json['rows'] ?? null,
        count: json['query']['total'] ?? null,
      );
    } catch (e) {
      throw 'QueryListResult not valid => ${e}';
    }
  }

  // factory QueryListParamsResult.fromJson(Map<String, dynamic> json) {
  //   try {
  //     return QueryListParamsResult(
  //       page: int.parse(json['page'].toString()) ?? null,
  //       limit: json['limit'] ?? null,
  //       rows: json['rows'] ?? null,
  //       count: json['count'] ?? null,
  //     );
  //   } catch (e) {
  //     throw 'QueryListResult not valid => ${e}';
  //   }
  // }

  Map<String, dynamic> toMap() {
    return {
      "page": page ?? null,
      "limit": limit ?? null,
      "search": textSearch ?? null,
      "filters": filters ?? null,
      "select": select ?? null,
      "order": order ?? null
    };
  }

  addFilter(Map map) {
    this.filters.add(map);
  }

  addSelect(String string){
    this.select.add(string);
  }

  addOrder(String string){
    this.order.add(string);
  }
}
