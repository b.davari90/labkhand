import 'package:counsalter5/Doctor/pagination/query_list_params.dart';


class SearchRepository {
  QueryListParams qlp;
//  List<QLPFilter> qlpFilters;
  List cities;
  List cityName;
  num fromPrice, toPrice;
  int filterNumber,time;
  Map attributesValues;
  bool fast, photographer,isMelli,hasImage,hasVideo,hasLogo;
  String charityName,managerName,aboutUs,fromDate,toDate;

  SearchRepository(
      {this.fast,
        this.fromDate,this.toDate,
        this.hasLogo,
        this.charityName,
        this.managerName,
        this.aboutUs,
        this.hasVideo,
        this.hasImage,
        this.isMelli,
      this.cityName,
      this.time,
//      this.qlpFilters,
      this.photographer,
      this.qlp,
      this.cities,
      this.fromPrice,
      this.toPrice,
      this.filterNumber,
      this.attributesValues});

  static copyFrom(SearchRepository srp) {
    return SearchRepository(
      fromDate: srp.fromDate,
      toDate: srp.toDate,
      hasLogo: srp.hasLogo,
      charityName: srp.charityName,
      managerName: srp.managerName,
      aboutUs: srp.aboutUs,
        hasImage: srp.hasImage,
        hasVideo: srp.hasVideo,
        isMelli: srp.isMelli,
        time: srp.time ?? 0,
        cityName: srp.cityName,
        fast: srp.fast ,
//        qlpFilters: srp.qlpFilters ?? [],
        photographer: srp.photographer ,
        attributesValues: srp.attributesValues,
        cities: srp.cities,
        filterNumber: srp.filterNumber,
        fromPrice: srp.fromPrice,
        toPrice: srp.toPrice,
        qlp: srp.qlp);
  }

  static clear() {
    return SearchRepository(
      fromDate: null,
      toDate: null,
      hasLogo: false,
      charityName: '',
      managerName: '',
      aboutUs: '',
      hasImage: false,
      hasVideo: false,
      isMelli: false,
      qlp: null,
      toPrice: null,
      time: null,
//      qlpFilters: [],
      cityName: [],
      fromPrice: null,
      filterNumber: null,
      cities: [],
      attributesValues: {},
      fast: false,
      photographer: false
    );
  }

  setFieldValue(key, value) {
    try {
      if (this.attributesValues is !Map) {
        this.attributesValues = Map();
      }
      this.attributesValues[key] = value;
      print('advAttributesValues ===> ${attributesValues}');
    } catch (e) {}
  }

  int lengthOfQlpFilters(){
    int a = 0 ;
    // a = qlpFilters?.where((element) => element.field != 'attributes.attribute.value')?.toList()?.length;
    // qlpFilters?.removeWhere((element) => element.field == 'attributes.attribute_id' );
    // return qlpFilters?.length;
    return a;
  }

}
