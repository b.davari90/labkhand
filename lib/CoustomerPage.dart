import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/CounsalterProfile.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'AboutUs.dart';
import 'dart:convert';
import 'CommentPage.dart';
import 'package:counsalter5/CustomerDocument.dart';
import 'CoustomerContainerComponent1.dart';
import 'package:flutter/material.dart';
import 'CustomerPageBottomNavComponent.dart';
import 'CustomerPageBottomNavModel.dart';
import 'CustomerRaisedBtnComponent.dart';
import 'TransActionListPage.dart';
import 'config.dart';
import 'package:counsalter5/htify.dart';
import 'CustomerModel.dart';
import 'CounsalterProfileChilderen.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:counsalter5/ModelBloc/MyCounsalterListModel.dart';
class CoustomerPage extends StatefulWidget {
  @override
  _CoustomerPageState createState() => _CoustomerPageState();
  int gender, single,index;
  CoustomerPage({this.gender, this.single});
}
class _CoustomerPageState extends State<CoustomerPage> {
  List<CustomerPageBottomNavModel> dataList;
  bool keybordShow = false;
  double percent=0.5;
  bool loading1=false,loading2=false;
  TextStyle txtWhiteStyle = TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: Conf.p2t(15));
  Function onTap;

  CustomerModel customerModel;
  List<MyCounsalterListModel> myCounsalterListModels;
  CoustomerBloc get coustomerBloc => context.bloc<CoustomerBloc>();
  double fillingInformationPercent(){
    if( coustomerBloc.state.user.married!=null && coustomerBloc.state.user.email!=""){percent=0.8;}
    if(coustomerBloc.state.user.gender!=null && coustomerBloc.state.user.married!=null ){percent=0.8;}
    if(coustomerBloc.state.user.gender!=null &&coustomerBloc.state.user.married!=null && coustomerBloc.state.user.email!=""){percent=1;}
    return percent;
  }
  Future getCustomer() async {
    try {
      setState(() {
        loading1 = true;
      });
      var data = await Htify.get('cu/customer');
    setState(() {
      customerModel=CustomerModel.fromJson(data);
      coustomerBloc.setUser(customerModel);
      print("___coustomerBloc NAME===> ${ coustomerBloc.state.user.name }");
    });
    } catch (e) {
      print("___CATCH ERROR getCustomer===> ${e.toString()}");
    } finally {
      setState(() {
        loading1 = false;
      });
    }
  }

  Future getMycounsalterList() async {
    try {
      setState(() {
        loading2 = true;
      });
      var data = await Htify.get("cu/doctor/list");
      List list=data;
      myCounsalterListModels=list.map((e) => MyCounsalterListModel.fromJson(e)).toList();
   print(3020202);
   print("myCounsalterListModels.m ===> ${ myCounsalterListModels.map((e) => e.id) }");
    } catch (e) {
      print("CatchError getMycounsalterList()===> ${e.toString()}");
    } finally {
      setState(() {
        loading2 = false;
      });
    }
  }
  @override
  void initState() {
    print("Conf.token ===> ${ Conf.token }");
    CoustomerPage c = CoustomerPage(gender: this.widget.gender, single: this.widget.single);
    fillingInformationPercent();
    customerModel=CustomerModel();
    myCounsalterListModels=[];
    getMycounsalterList();
    if (customerModel.childeren == null) {
        customerModel.childeren = [];}
    getCustomer();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: true,
      child: BlocBuilder<CoustomerBloc, CoustomerRepository>(
        builder: (context, state) {
          return Scaffold(
              body: Stack(children: <Widget>[
            Positioned(
                top: Conf.p2h(0),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child: Container(
                  width: Conf.p2w(100),
                  height: Conf.p2h(53),
                  color: Conf.blue,
                  child: Column(children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                top: Conf.p2h(3), left: Conf.p2w(6)),
                            child: MasBackBotton())
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: Conf.p2w(4)),
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(right: Conf.p2w(1)),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100.0),
                                  child: Container(
                                    color: Colors.grey.withOpacity(0.2),
                                    width: Conf.p2w(20),
                                    height: Conf.p2w(20),
                                    child: Conf.image(customerModel.avatar)??Conf.image(null),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: Conf.p2w(3)),
                          child:
                              BlocBuilder<CoustomerBloc, CoustomerRepository>(
                                  builder: (context, state) {
                            return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
//                                      Text(
//                                        "مشاور",
////                                      state.user.fname??"",
//                                        style: Conf.title.copyWith(
//                                          color: Colors.white,
//                                          fontWeight: FontWeight.w500,
//                                          fontSize: Conf.p2t(22),
//                                          decoration: TextDecoration.none,
//                                        ),
//                                        softWrap: true,
//                                        overflow: TextOverflow.fade,
//                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: Conf.p2w(1.5)),
                                        child: Text(
                                          state.user.name??"",
                                          style: Conf.title.copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: Conf.p2t(22),
                                            decoration: TextDecoration.none,
                                          ),
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: Conf.p2w(0.8)), //2
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType.leftToRight,
                                                    duration:Duration(milliseconds: 500),
                                                    alignment:Alignment.centerLeft,
                                                    curve: Curves.easeOutCirc,
                                                    child:CounsalterProfile()));
                                          },
                                          child: Icon(
                                            Icons.edit,
                                            size: Conf.p2w(4.7918),
                                            color: Colors.white,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          "اعتبار من:  ",
                                          style: Conf.body1.copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: Conf.p2t(15),
                                            decoration: TextDecoration.none,
                                          ),
//                                  title2,
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                        ),
                                        Text(
                                          " 230,000 ",
                                          style: Conf.body1.copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                            fontSize: Conf.p2t(20),
                                            decoration: TextDecoration.none,
                                          ),
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                        ),
                                        Text(
                                          "تومان",
                                          style: Conf.body1.copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: Conf.p2t(12),
                                            decoration: TextDecoration.none,
                                          ),
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                        )
                                      ]),
                                  Row(children: <Widget>[
                                    Text(
                                      "مشخصات کامل ",
                                      style: Conf.body2.copyWith(
                                        color: Conf.orang,
                                        fontWeight: FontWeight.w700,
                                        fontSize: Conf.p2t(14),
                                        decoration: TextDecoration.none,
                                      ),
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                    Text(
                                      (fillingInformationPercent()*100).toString().split(".").first,
                                      style: Conf.body2.copyWith(
                                        color: Conf.orang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        decoration: TextDecoration.none,
                                      ),
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                    Text(
                                      "%",
                                      style: Conf.body2.copyWith(
                                        color: Conf.orang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: Conf.p2t(12),
                                        decoration: TextDecoration.none,
                                      ),
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    )
                                  ]),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: Conf.p2h(0.15)),
                                    child: LinearPercentIndicator(
                                      width: 190.0,
                                      animation: true,
                                      backgroundColor: Colors.grey.shade200,
                                      animationDuration: 700,
                                      lineHeight: 12.0,
                                      percent: fillingInformationPercent()??percent,
                                      isRTL: true,
                                      progressColor: Conf.orang,
                                    ),
//                              LinearPercentIndicator(
////                                isRTL: true,
//                                width: Conf.p2w(62),
//                                lineHeight: Conf.p2h(0.5),
//                                percent: 0.9,
//                                backgroundColor: Conf.lightColor ,
//                                progressColor: Conf.lightColor ,fillColor: Conf.lightColor,
//                              ),
                                  ),
                                ]);
                          }),
                        ),
                      ],
                    ),
                  ]),
                )),
            Positioned(
              top: Conf.p2h(26),
              bottom: Conf.p2h(0),
              left: Conf.p2w(0),
              right: Conf.p2w(0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(Conf.p2w(6.25))),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(Conf.p2w(6.25)),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              top: Conf.p2h(4), right: Conf.p2w(2.5)),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: Conf.smEdgeRight1 / 6,
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .leftToRight,
                                              duration:
                                                  Duration(milliseconds: 500),
                                              alignment: Alignment.centerLeft,
                                              curve: Curves.easeOutCirc,
                                              child: TransActionListPage()));
                                    },
                                    child: CustomerRaisedBtnComponent(
                                        icon: FeatherIcons.creditCard,
                                        title: "تراکنش ها",
                                        isTapped: false),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                                PageTransitionType.leftToRight,
                                            duration:
                                                Duration(milliseconds: 500),
                                            alignment: Alignment.centerLeft,
                                            curve: Curves.easeOutCirc,
                                            child: CustomerDocument()));
                                  },
                                  child: CustomerRaisedBtnComponent(
                                    icon: FeatherIcons.clipboard,
                                    title: "پرونده ها ",
                                    isTapped: false,
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                                PageTransitionType.leftToRight,
                                            duration:
                                                Duration(milliseconds: 500),
                                            alignment: Alignment.centerLeft,
                                            curve: Curves.easeOutCirc,
                                            child: AboutUs()));
                                  },
                                  child: CustomerRaisedBtnComponent(
                                    icon: FeatherIcons.calendar,
                                    title: " ملاقات ها",
                                    isTapped: false,
                                  ),
                                ),
                                Stack(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType
                                                    .leftToRight,
                                                duration:
                                                    Duration(milliseconds: 500),
                                                alignment: Alignment.centerLeft,
                                                curve: Curves.easeOutCirc,
                                                child: CommentPage()));},
                                      child: CustomerRaisedBtnComponent(
                                        icon: FeatherIcons.mail,
                                        title: "پیام ها",
                                        isTapped: true,
                                      ),
                                    ),
                                    Positioned(
                                      left: Conf.p2w(12.125), //63,
                                      top: Conf.p2h(1.75), //18,
                                      child: CircleAvatar(
                                        maxRadius: 10,
                                        backgroundColor: Conf.orang,
                                        child: Center(
                                            child: FittedBox(
                                          fit: BoxFit.cover,
                                          child: Text("1",
                                              softWrap: true,
                                              overflow: TextOverflow.fade,
                                              style: Conf.caption.copyWith(
                                                  fontSize: Conf.p2t(20),
                                                  color: Colors.white)
                                              ),
                                        )),
                                      ),
                                    )
                                  ],
                                ),
                              ]),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2h(3), right: Conf.p2w(5.209)),
                              child: Text("لیست مشاوره های من",
                                  softWrap: true,
                                  overflow: TextOverflow.fade,
                                  style: Conf.body1.copyWith(
                                      fontWeight: FontWeight.w500,
                                      fontSize: Conf.p2t(17),
                                      color: Colors.black87,
                                      decoration: TextDecoration.none)
                                  ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: Conf.p2w(4.1665), top: Conf.p2h(2)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("بیشتر",
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                      style: Conf.body1.copyWith(
                                        fontWeight: FontWeight.bold,
                                        fontSize: Conf.p2t(16),
                                        color: Conf.orang,
                                        decoration: TextDecoration.none)),
                                  Icon(Icons.navigate_next,
                                      size: Conf.p2w(5.417), //26,
                                      color: Conf.orang)
                                ],
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              right: Conf.p2w(2.0839), top: Conf.p2w(4)
                              ),
                          child: Container(
                            height: Conf.p2h(25),
                            child:myCounsalterListModels==[]||myCounsalterListModels==null?Container(height: Conf.p2h(15),
                              width: Conf.p2w(30),color: Colors.red,
                              child: Center(child: Text("مشاوره ای وجود ندارد"),),):
                            SingleChildScrollView(
//                                scrollDirection: Axis.horizontal,
                                child: Row(
                                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
//                                    textDirection: TextDirection.ltr,
                                    children:
                                    myCounsalterListModels.map(
                                            (e) => CoustomerContainerComponent1(
                                                model: e,
//                                              name: nameD,
//                                              avatar:avatarD ,
//                                              speciallity: specialityD,
                                                ))
                                        .toList())
                            ) ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: Conf.p2h(0),
              right: Conf.p2w(0),
              left: Conf.p2w(0),
              child: Container(
                height: Conf.p2h(20),
                decoration: BoxDecoration(
                    color: Conf.blue,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(Conf.p2w(6.25)),
                        topRight: Radius.circular(Conf.p2w(6.25))
                        )),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              right: Conf.p2w(6),
                              left: Conf.p2w(3),
                              top: Conf.p2h(0.5)),
                          child: ClipOval(
                            child: Material(
                              color: Conf.blue, // button color
                              child: InkWell(
                                child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(250)),
                                        border:
                                            Border.all(color: Colors.white)),
                                    padding: EdgeInsets.all(5),
                                    width: Conf.p2w(12),
                                    height: Conf.p2w(12),
                                    child: Icon(
                                      FeatherIcons.plus,
                                      color: Colors.white,
                                    )),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              CounsalterProfileChilderen()));
                                },
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: Conf.p2h(0.5), right: Conf.p2w(2)),
                          child: Text("اضافه کردن",
                              style: Conf.body2.copyWith(color: Colors.white)),
                        )
                      ],
                    ),
                    Container(
                        width: Conf.p2w(2),
                        height: Conf.p2h(8),
                        child: VerticalDivider(
                          color: Colors.white.withOpacity(0.7),
                        )),
                    Container(
                      color: Conf.blue,
                      width: Conf.p2w(77),
//                      height: Conf.p2h(15),
                      child:
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                            children: customerModel.childeren
                                .map((e) =>
                                CustomerPageBottomNavComponent(
                                  model: e,
                                  onTap: () {
                                    setState(() {
                                      widget.index =
                                          customerModel.childeren
                                              .indexOf(e);});
                                  },active:  customerModel.childeren
                                    .indexOf(e) ==
                                    widget.index
                                    ? true
                                    : false,
                                ))
                                .toList()),
                      ),
                    )
                  ],
                ),
              ),
            )
              ,(loading1==true&&loading2==true) ? Conf.circularProgressIndicator() : Container()
          ]));
        },
      ),
    );
  }
}
