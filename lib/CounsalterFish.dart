import 'dart:ui';
import 'package:counsalter5/HomePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'raised_button_ui.dart';
import 'package:page_transition/page_transition.dart';
import 'config.dart';
import 'package:counsalter5/CustomerModel.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';

class CounsalterFish extends StatefulWidget {
  TextStyle titleStyle = TextStyle(
      fontSize: Conf.p2w(5.8),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w500,
      color: Colors.black);
  TextStyle describtionStyle = TextStyle(
      fontSize: Conf.p2w(4.1665),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w600,
      color: Colors.black);
  TextStyle subDescribtionStyle = TextStyle(
      fontSize: Conf.p2w(3.425),
      decoration: TextDecoration.none,
      fontWeight: FontWeight.w800,
      color: Colors.black45);
  String selectedTime, gerogarianDate, subject;
  int price;

  CounsalterFish(
      {this.selectedTime, this.gerogarianDate, this.price, this.subject});

  @override
  _CounsalterFishState createState() => _CounsalterFishState();
}

class _CounsalterFishState extends State<CounsalterFish> {
  @override
  void initState() {
    print("widget.selectedTime ===> ${widget.selectedTime}");
    print("widget.subject ===> ${widget.subject}");
    print("widget.gerogarianDate ===> ${widget.gerogarianDate}");
    print("widget.price ===> ${widget.price}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: [
              Positioned(
                  top: Conf.p2h(3),
                  bottom: Conf.p2h(0),
                  right: Conf.p2w(0),
                  left: Conf.p2w(3),
                  child: Flex(
                    direction: Axis.vertical,
                    children: [
                      Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
//                        spreadRadius: 1,
                            blurRadius: 50,
//                        offset: Offset(0, 10), // changes position of shadow
                          ),
                        ]),
                        child: Image.asset(
                          "assets/images/fishbackground.png",
                          color: Colors.white,
                          fit: BoxFit.fill,
                          height: Conf.p2h(85),
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding:EdgeInsets.only(top: Conf.p2h(10)),
                child: Column(
                  children: [
                    BlocBuilder<CoustomerBloc, CoustomerRepository>(
                        builder: (context, state) {
                          return Column(children: [
                            Center(
                              child: Text(
                                "رسید درخواست مشاور",
                                style: widget.titleStyle,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Conf.p2h(2)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text(
                                      "نام و نام خانوادگی کاربر",
                                      style: widget.describtionStyle,
                                    ),
                                  ),
                                  FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text(state.user.name ?? "",
                                          style: widget.describtionStyle)),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Conf.p2h(2)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text("موضوع درخواست",
                                        style: widget.describtionStyle),
                                  ),
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text(widget.subject ?? "",
                                        style: widget.describtionStyle),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Conf.p2h(5)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Row(
                                    children: [
                                      Container(width: Conf.p2w(10),
//                                        height: Conf.p2h(5),color: Colors.red,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: Conf.p2w(1)),
                                        child: Icon(
                                          FeatherIcons.calendar,
                                          color: Conf.blue,size: 20,
                                        ),
                                      ),
                                      FittedBox(
                                        fit: BoxFit.cover,
                                        child: Text(
                                          "تاریخ",
                                          style: widget.describtionStyle,
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Container(width: Conf.p2w(5)
//                                        ,height: Conf.p2h(5),color: Colors.red,
                                      ),
                                      Padding(
                                        padding:EdgeInsets.only(left: Conf.p2w(1)),
                                        child: Icon(
                                          FeatherIcons.clock,
                                          color: Conf.blue,size: 20,
                                        ),
                                      ),
                                      FittedBox(
                                        fit: BoxFit.cover,
                                        child: Text(
                                          "زمان",
                                          style: widget.describtionStyle,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.p2h(1),),
                                  child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text(widget.gerogarianDate ?? "",
                                          style: widget.describtionStyle)),
                                ),
//                                Container(width: Conf.p2w(0.5),height: Conf.p2h(2),color: Colors.red,),
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: Conf.p2h(1),right: Conf.p2w(5)),
                                  child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text(widget.selectedTime ?? "",
                                          style: widget.describtionStyle)),
                                ),
                              ],
                            ),
                            FittedBox(
                              fit: BoxFit.cover,
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: Conf.p2h(1)),
                                  child: Text(
                                    "هزینه مشاوره",
                                    style: widget.describtionStyle,
                                  ),
                                ),
                              ),
                            ),
                          ]);
                        }),
                    BlocBuilder<DoctorBloc, DoctorRepository>(
                        builder: (context, state) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Center(
                                      child: Text(
                                        state.priceDrType.toString() ?? "0",
                                        style: TextStyle(
                                            fontSize: Conf.p2w(4.1665),
                                            decoration: TextDecoration.none,
                                            fontWeight: FontWeight.w900,
                                            color: Colors.black),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: Conf.p2w(1)),
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Center(
                                        child: Text(
                                          "تومان  ",
                                          style: TextStyle(
                                              fontSize: Conf.p2t(13),
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                              ,FittedBox(
                                fit: BoxFit.cover,
                                child: Center(
                                  child: Padding(
                                    padding: EdgeInsets.only(top: Conf.p2h(3)),
                                    child: Text(
                                      "مشخصات مشاور",
                                      style: widget.describtionStyle,
                                    ),
                                  ),
                                ),
                              ),
                              FittedBox(
                                fit: BoxFit.cover,
                                child: Center(
                                  child: Container(
                                    width: Conf.p2w(12),
                                    height: Conf.p2w(12),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(35.0),
                                      child: FadeInImage.assetNetwork(
                                        placeholder: 'assets/images/prof1.jpg',
                                        image:
                                        "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(0.5)),
//                    padding: const EdgeInsets.only(top: 8),
                                child: FittedBox(
                                  fit: BoxFit.cover,
                                  child: Row(
                                    children: [
                                      Center(
                                        child: Text(
                                          "مشاور",
                                          style: widget.titleStyle,
                                        ),
                                      ),
                                      Center(child: Text(state.doctor.name??"", style: widget.titleStyle)),
                                    ],
                                  ),
                                ),
                              ),
                              FittedBox(
                                fit: BoxFit.cover,
                                child: Row(mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "متخصص",
                                      style:widget. subDescribtionStyle,
                                    ),
                                    Text(" - "),
                                    Text(
                                      state.doctor.specialty??"",
                                      style:widget. subDescribtionStyle,
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(1.875)),
//                    padding: const EdgeInsets.only(top: 9),
                                child: FittedBox(
                                  fit: BoxFit.cover,
                                  child: Center(
                                    child: Text(
                                      "شماره سریال پیگیری",
                                      style: widget.describtionStyle,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(0.834)),
//                    padding: const EdgeInsets.only(top: 4),
                                child: FittedBox(
                                  fit: BoxFit.cover,
                                  child: Center(
                                    child: Text(
                                      state.doctor.id.toString(),
//                                "63187725468",
                                      style: TextStyle(
                                          fontSize: Conf.p2w(4.1665),
                                          decoration: TextDecoration.none,
                                          fontWeight: FontWeight.w900,
                                          color: Colors.black),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          );
                        })
                  ],
                ),
              ),
              Positioned(
                bottom: Conf.p2h(1),
                right: Conf.p2w(7),
                child:
                Container(
                    width: Conf.p2w(85),
                    height: Conf.p2h(9),
                    padding:
                    EdgeInsets.only(right: Conf.p2w(2.5), left: Conf.p2w(1.5)),
//                    padding:
//                    EdgeInsets.only(right: Conf.p2w(4), left: Conf.p2w(3)),
                    child: MasRaisedButton(
                      margin: EdgeInsets.only(top: 0),
                      text: "تایید",
                      textColor: Conf.lightColor,
                      onPress: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.leftToRight,
                                duration: Duration(milliseconds: 500),
                                alignment: Alignment.centerLeft,
                                curve: Curves.easeOutCirc,
                                child: HomePage()));
                      },
                    )
                ))])));
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1.5, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Conf.p2w(81.5),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          final boxWidth = constraints.constrainWidth();
          final dashWidth = 8.0;
          final dashHeight = height;
          final dashCount = (boxWidth / (2 * dashWidth)).floor();
          return Flex(
            children: List.generate(dashCount, (_) {
              return SizedBox(
                width: dashWidth,
                height: dashHeight,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: color),
                ),
              );
            }),
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            direction: Axis.horizontal,
          );
        },
      ),
    );
  }
}
