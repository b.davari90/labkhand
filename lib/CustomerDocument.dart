import 'package:flutter/material.dart';
import 'package:counsalter5/ModelBloc/DoctorFolderListModel.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:counsalter5/ModelBloc/CustomerFolderModel.dart';
import 'config.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/ModelBloc/CustomerFolderListModel.dart';
import 'package:counsalter5/CustomerDocumentComponent.dart';
import 'package:counsalter5/config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params_result.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
class CustomerDocument extends StatefulWidget {
  @override
  _CustomerDocumentState createState() => _CustomerDocumentState();
}

class _CustomerDocumentState extends State<CustomerDocument> {
  List<Widget> taskList;
  bool loading=false;
  CustomerFolderListModel customerFolderListModel;
  List<CustomerFolderModel> customerFolderLst;
  int count,pages,limit;
  SearchAdvCubit get searchBloc => context.bloc<SearchAdvCubit>();
  ScrollController sc;
  QueryListParams qlp;
  QueryListParamsResult listResult;
  Future getAllCustomerFolder() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.create("cu/folder",{});
      print("data ===> ${ data }");
      customerFolderListModel= CustomerFolderListModel.fromJson(data);
      count = customerFolderListModel.count;
      limit = customerFolderListModel.limit;
      pages = customerFolderListModel.page;
      print("count ===> ${ count }");
      print("limit ===> ${ limit }");
      print("pages ===> ${ pages }");
      customerFolderLst.addAll(customerFolderListModel.rows);
      print("doctorFolderLst.m ===> ${ customerFolderLst.map((e) => e.name) }");
    } catch (e) {
      print("CatchError getAllDoctorFolder()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }
  @override
  void initState() {
    customerFolderListModel=CustomerFolderListModel();
    customerFolderLst=[];
    qlp = QueryListParams();
    listResult = QueryListParamsResult();
    sc = ScrollController();
    qlp.page = 0;
    searchBloc.setQlp(qlp);
    sc.addListener(() {
      if (sc.offset >= sc.position.maxScrollExtent - 100 &&
          !sc.position.outOfRange &&
          qlp.page < pages) {
        setState(() {
          qlp.page++;
          searchBloc.setQlp(qlp);
       getAllCustomerFolder();
        });
      }
    });
    getAllCustomerFolder();
    super.initState();
  }
@override
  void dispose() {
    sc.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
            color: Conf.blue,
            child: Stack(children:
            [
              Positioned(
                  top: Conf.p2h(4.167),
//            top: Conf.p2w(4.167),
                  width: Conf.p2w(100),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "پرونده پزشکی",
                        style: TextStyle(
                            fontSize: Conf.p2t(27),
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    ],
                  )),
              Positioned(
                top: Conf.p2h(4.8),
//                   width: Conf.p2w(100),
                left: Conf.p2w(6),
                child: Row(
                  children: [
                    MasBackBotton()
                  ],
                ),
              ),
              Positioned(
//                top: Conf.p2h(5),
                right: Conf.p2w(0),
                left: Conf.p2w(0),
                bottom: Conf.p2h(0),
                child: Container(
                  height: Conf.p2h(80),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 8,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30))),
                ),
              ),
              Positioned(
                  top: Conf.p2h(17.3),
                  right: Conf.p2w(2),
                  left: Conf.p2w(2),
                  bottom: Conf.p2w(0),
                  child: Container(
                      child: SingleChildScrollView(
                        controller:sc,
                        child:customerFolderLst==null||customerFolderLst==[]?[Text("پرونده ای وجود ندارد")]: Column(
                            children: customerFolderLst
                                .map((e) => CustomerComponentDocument(
                            index: customerFolderLst.indexOf(e)==0?0:
                            customerFolderLst.indexOf(e)==1?1:
                            customerFolderLst.indexOf(e)==2?2:
                            customerFolderLst.indexOf(e)==3?3:
                            customerFolderLst.indexOf(e)==4?4:
                            customerFolderLst.indexOf(e)==5?0:
                            (customerFolderLst.indexOf(e)%limit==0.0)?0:
                            (customerFolderLst.indexOf(e)%limit==1.0)?1:
                            (customerFolderLst.indexOf(e)%limit==2.0)?2:
                            (customerFolderLst.indexOf(e)%limit==3.0)?3:
                            (customerFolderLst.indexOf(e)%limit==4.0)?4:2,
//                              index:customerFolderLst.indexOf(e)==1 && limit%1==0?1:
//                              customerFolderLst.indexOf(e)==2?2:customerFolderLst.indexOf(e)==3?3:
//                              customerFolderLst.indexOf(e)==0?4:customerFolderLst.indexOf(e)==5,
                              onTap: () {
                                Navigator.pop(context);
                              },
                              model: e,
                            ))
                                .toList()),
                      ))
//                    children:dataList.map((e) => e.
              ),
              loading==true?Conf.circularProgressIndicator():Container()]),
          )),
    );
  }
}
