import 'package:flutter/material.dart';

import 'config.dart';
class IncreaseMoneyPriceComponent extends StatefulWidget {

  Function onTap;
  bool isGreyColor,isSelected=false;
  int containerPrice;

  IncreaseMoneyPriceComponent({
    this.containerPrice,this.onTap, this.isGreyColor,this.isSelected=false});

  @override
  _IncreaseMoneyPriceComponentState createState() => _IncreaseMoneyPriceComponentState();
}
class _IncreaseMoneyPriceComponentState extends State<IncreaseMoneyPriceComponent> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Conf.p2w(1)),
      child: GestureDetector(
        onTap: () {
          widget.onTap();
          setState(() {
            if (widget.onTap is Function) {
              widget.onTap();
            }
          });
        },
        child: Container(
//        padding: EdgeInsets.only(right: Conf.p2w(3)),
          margin: EdgeInsets.only(right: Conf.p2w(0.5)),
          height: Conf.p2w(10),
          width: Conf.p2w(28),
          decoration: BoxDecoration(
              color:widget.isGreyColor?Colors.grey.shade200: Colors.white,
              border: Border.all(width: 0.7,color: widget.isSelected?Conf.orang:Colors.black),
              borderRadius: BorderRadius.circular(10)),
          child: Row(mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FittedBox(fit: BoxFit.contain,
                child: Center(
                  child: Text(widget.containerPrice.toString(),
//                    widget.model.price,
                    style: TextStyle(color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: Conf.p2t(16)),
                  ),
                ),
              ),
              Padding(
                padding:  EdgeInsets.only(top:Conf.p2h(0.5),right: Conf.p2w(1)),
                child: FittedBox(fit: BoxFit.cover,
                  child: Center(
                    child: Text("تومان",style: TextStyle(color:Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: Conf.p2t(11))),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
