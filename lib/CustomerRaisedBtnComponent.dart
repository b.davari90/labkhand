import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'config.dart';

class CustomerRaisedBtnComponent extends StatelessWidget {
  String title;
  IconData icon;
  bool isTapped;

  CustomerRaisedBtnComponent(
      {this.title,
      this.icon,
           this.isTapped = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Conf.p2w(2)),
//      padding: EdgeInsets.only(right: 9),
//      padding: Conf.smEdge * 1.5,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
            borderRadius:
                BorderRadius.all(Radius.circular(Conf.p2w(3))),
            border: Border.all(
            color: isTapped
                        ? Colors.indigo
                        :  Colors.grey.withOpacity(0.2)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                offset: Offset(-1,2.7),
              )
            ]),
        height: Conf.p2h(12),
        width: Conf.p2w(21),
//        child: RaisedButton(
//            shape: RoundedRectangleBorder(
//                borderRadius: BorderRadius.circular(Conf.p2w(3.0)), //
//                side: BorderSide(
//                    color: isTapped
//                        ? Colors.indigo
//                        : Colors.white.withOpacity(0.8))),
//            padding: Conf.smEdge,
//            color: Colors.white,
//            onPressed: () {
//
//            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Icon(icon,
                    color: Conf.blue,
//                    color: isTapped ? Colors.white : Colors.blue,
                    size: Conf.p2w(5.627) //27
                    ),
                FittedBox(
                  fit: BoxFit.cover,
                  child: Text(title,
                      softWrap: true,
                      overflow: TextOverflow.fade,
                      style: Conf.subheading.copyWith(
                          fontSize: Conf.p2t(14),
                          fontWeight: FontWeight.w700,
                          color: Colors.black54.withOpacity(0.5))
//
                      ),
                )
              ],
            )),
      );
//    );
  }
}
