import 'dart:ui';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Conf {
  static int typeCoustomer;
  static int typeDoctor;
  static int idLogin;
  static int loginType;
  static int idLoginDoctor;
//  static Map<int,String> type={1:"عکس",2:"فیلم",3:"مدارک",4:"متنی",};
  static Map<String, String> type = {
    "1": "عکس",
    "2": "فیلم",
    "3": "مدارک",
    "4": "متنی",
  };
  static Map<int, String> typeDoc = {
    1:"پروانه نظام روانشناسی",
    2:"تصویر کارت ملی",
    3:"تصویر اصل مدرك تحصیلی",
    4:"پروفایل"
  };
  static Map<int,String> gender = {
    1:"زن",
    2:"مرد",
  };
  static Map<int, String> married = {
    1:"مجرد",
    2:"متاهل",
    3:"مطلقه",
  };
  static String token;
//  static String tokenDoctor;
  static bool loading = true;
//  static String BASE_URL = 'http://kooks.ir/server';
//  static String BASE_URL = 'https://185.97.118.231/labkhand/';
  static String BASE_URL = 'https://labkhandapp.com/labkhand/';
//  static String BASE_URL = 'https://185.97.118.231/labkhand/';
//  static String BASE_URL = 'https://185.97.118.231/kernel/';

  static double w;
  static double h;
  static double t;
  static String cityFilter;
  static List cityFilterId;
//  static List<int> cityFilterId;

  static double p2w(num p) => ((p * w) / 100).toDouble();

  static double p2h(num p) => ((p * h) / 100).toDouble();

  static double p2t(num p) => (p * t).toDouble();

  static calcWH(BuildContext ctx) {
    Conf.w = MediaQuery.of(ctx).size.width;
    Conf.h = MediaQuery.of(ctx).size.height;
    Conf.t = MediaQuery.of(ctx).textScaleFactor;
  }

//  colors
//  static const TinyColor = TinyColor.fromString('#FE5567');
  static const primaryColor = Color.fromRGBO(16, 69, 91, 1);
  static const accentColor = Color.fromRGBO(255, 175, 32, 1);
  static const threeColor = Color.fromRGBO(249, 216, 2, 1);
  static const fourColor = Color.fromRGBO(94, 191, 255, 1);
  static const fastColor = Color.fromRGBO(255, 131, 120, 1);

  static const greeencontainerColor = Color.fromRGBO(0, 255, 61, 1);
  static const greeencontainerTextColor = Color.fromRGBO(12, 114, 36, 1);
  static const greencontainerColor = Color.fromRGBO(0, 255, 219, 1);
  static const greencontainerTextColor = Color.fromRGBO(5, 113, 96, 1);
  static const blluecontainerColor = Color.fromRGBO(205, 0, 254, 1);
  static const blluecontainerTextColor = Color.fromRGBO(187, 53, 217, 1);
  static const purplecontainerColor = Color.fromRGBO(215, 246, 0, 1);
  static const purplecontainerTextColor = Color.fromRGBO(171, 191, 41, 1);

  static const bluePurple = Colors.indigo;
  static const lightColor = Colors.white;
  static const shadowColor = Colors.grey;
  static const iconColor = Colors.black54;
  static const transparentColor = Colors.transparent;
  static const errorColor = Colors.red;
  static const doneColor = Colors.green;
  static final grayColor200 = Colors.grey.shade200;
  static const backgroundColor = Color.fromRGBO(239, 238, 236, 1);

  static const blue = Color.fromRGBO(0, 122, 255, 1);
  static const orang = Color.fromRGBO(245, 166, 35, 1);
  static const grey = Color.fromRGBO(56, 63, 69, 1);

//  round style
  static final primaryBorderRadius = BorderRadius.circular(Conf.h / 100);
  static final secondBorderRadius = BorderRadius.circular(50);
  static final fullBorderRadius = BorderRadius.circular(100);
  static final halfBorderRadius = BorderRadius.circular(8);
  static final bottomSheetBorderRadius = Radius.circular(20);
  static final bottomSheetBorderRadius2 = BorderRadius.circular(20);
  static final rightBorderRadius =
      BorderRadius.horizontal(right: Radius.circular(16));

//  size for spacing
  static double smSize = Conf.p2w(0.9);
  static double mdSize = smSize * 2;
  static double xlSize = smSize * 4;
  static double xxlSize = smSize * 8;

//  size for text
  static double vsmText = Conf.p2t(8);
  static double smText = Conf.p2t(11);
  static double mdText = Conf.p2t(14);
  static double lgText = Conf.p2t(16);
  static double xlText = Conf.p2t(18);
  static double xxlText = Conf.p2t(20);

  static TextStyle display1;
  static TextStyle display1W6;
  static TextStyle display2;
  static TextStyle display3;
  static TextStyle display4;
  static TextStyle title;
  static TextStyle titleW7;
  static TextStyle subtitle;
  static TextStyle headline;
  static TextStyle headlineW6;
  static TextStyle subheading;
  static TextStyle subheadingW7;
  static TextStyle subheadingW6;
  static TextStyle body1;
  static TextStyle body2;
  static TextStyle body2W7;
  static TextStyle body2W7C4;
  static TextStyle caption;
  static TextStyle captionW7;
  static TextStyle button;

  static textStyle(BuildContext ctx) {
    ThemeData theme = Theme.of(ctx);
    Conf.display1 = theme.textTheme.headline1;
    Conf.display2 = theme.textTheme.headline2;
    Conf.display3 = theme.textTheme.headline3;
    Conf.display4 = theme.textTheme.headline4;
    Conf.title = theme.textTheme.headline5;
    Conf.subtitle = theme.textTheme.headline6;
    Conf.headline = theme.textTheme.subtitle1;
    Conf.subheading = theme.textTheme.subtitle2;
    Conf.body1 = theme.textTheme.subtitle1;
    Conf.body2 = theme.textTheme.subtitle2;
    Conf.caption = theme.textTheme.caption;
    Conf.button = theme.textTheme.button;
    Conf.titleW7 = theme.textTheme.overline.copyWith(
      color: Conf.accentColor,
      fontWeight: FontWeight.w700,
    );
    Conf.subheadingW7 = theme.textTheme.subhead.copyWith(
      fontWeight: FontWeight.w700,
    );
    Conf.subheadingW6 =
        theme.textTheme.subhead.copyWith(fontWeight: FontWeight.w600);
    Conf.body2W7 = theme.textTheme.body2
        .copyWith(color: Conf.primaryColor, fontWeight: FontWeight.w700);
    Conf.body2W7C4 = theme.textTheme.body2
        .copyWith(color: Conf.fourColor, fontWeight: FontWeight.w700);
    Conf.display1W6 =
        theme.textTheme.display1.copyWith(fontWeight: FontWeight.w600);
    Conf.headlineW6 =
        theme.textTheme.headline.copyWith(fontWeight: FontWeight.w600);
    Conf.captionW7 =
        theme.textTheme.caption.copyWith(fontWeight: FontWeight.w700);
  }

//  edge size objects
  static var smEdge = EdgeInsets.all(smSize);
  static var smEdgeCustome = EdgeInsets.fromLTRB(smSize / 2, 5, 3, 8);
  static var smEdgeleft = EdgeInsets.only(right: smSize);
  static var smEdgeleft2 = EdgeInsets.only(left: smSize);
  static var smEdgeleft1 = EdgeInsets.only(right: smSize * 2);
  static var smEdgeRight2 = EdgeInsets.only(right: smSize * 10.5);
  static var smEdgeRight11 = EdgeInsets.only(right: smSize * 8);
  static var smEdgeRight111 = EdgeInsets.only(right: smSize * 7);
  static var smEdgeRight1 = EdgeInsets.only(right: smSize * 6.5);
  static var smEdgeRightTop =
      EdgeInsets.only(right: smSize * 3.5, top: mdSize * 6);
  static var smEdgeRightTop2 =
      EdgeInsets.only(right: smSize * 2, top: mdSize * 2.5);
  static var smEdgeRightTop1 =
      EdgeInsets.only(right: smSize * 16, top: mdSize * 5);
  static var smEdgeRightBottom =
      EdgeInsets.only(right: smSize * 5.5, bottom: smSize * 3.5);
  static var smEdgeRightLeft =
      EdgeInsets.only(left: smSize * 1.5, right: smSize * 3.5);
  static var smEdgeRightLeftX =
      EdgeInsets.only(left: smSize * 4.5, right: smSize * 4.5);
  static var smEdgeRightLeftXX =
      EdgeInsets.only(left: smSize * 46.5, right: smSize * 4.5);
  static var smEdgeleftX = EdgeInsets.only(left: smSize * 7.5);
  static var smEdgeleftBottomTopX = EdgeInsets.only(
      left: smSize * 8.5, bottom: smSize * 1.5, top: smSize * 1.5);
  static var smEdgeleftBottom1 =
      EdgeInsets.only(left: smSize * 8.5, bottom: smSize * 1.5);
  static var smEdgeleftBottomTopXx =
      EdgeInsets.only(left: smSize * 8.5, bottom: smSize * 5, top: smSize * 4);
  static var smEdgeTop1 = EdgeInsets.only(top: smSize * 2);
  static var smEdgeTop0 = EdgeInsets.only(top: smSize);
  static var smEdgeTop00 = EdgeInsets.only(top: smSize / 4);
  static var smEdgeTop2 = EdgeInsets.only(top: smSize * 6);
  static var smEdgeBottomTop0 =
      EdgeInsets.only(bottom: smSize / 10.5, top: smSize / 1.5);
  static var smEdgeBottomTop1 =
      EdgeInsets.only(bottom: smSize * 2, top: smSize * 2);
  static var smEdgeBottomTop2 =
      EdgeInsets.only(bottom: smSize * 2, top: smSize * 7);
  static var smEdgeBottom = EdgeInsets.only(bottom: smSize * 1.5);
  static var smEdgeleftBottom =
      EdgeInsets.only(right: smSize * 12, bottom: smSize / 5);
  static var mdEdge = EdgeInsets.all(mdSize);
  static var mdEdgeTopBottomRight =
      EdgeInsets.only(top: mdSize * 4, bottom: mdSize, right: mdSize);
  static var mdEdgeTopRight = EdgeInsets.only(top: mdSize * 2, right: mdSize);
  static var mdEdgeTopRight0 =
      EdgeInsets.only(top: mdSize / 7, right: mdSize * 2);
  static var mdEdgeTopRightBottom1 =
      EdgeInsets.only(top: mdSize * 2, right: mdSize / 2, bottom: mdSize * 14);
  static var mdEdgeleftTopRight1 = EdgeInsets.only(
      right: mdSize * 1.1,
      top: mdSize * 1.1,
      left: mdSize * 3.5,
      bottom: smSize * 2);
  static var mdEdgeleftRight1 =
      EdgeInsets.only(right: mdSize * 1.1, left: mdSize * 3.5);
  static var mdEdgeleftRight0 =
      EdgeInsets.only(right: mdSize * 1.1, left: mdSize * 1.5);
  static var mdEdgeleftTopRight =
      EdgeInsets.only(right: mdSize * 5, top: mdSize * 2, left: mdSize * 3.5);
  static var mdEdgeleftTopRight2 =
      EdgeInsets.only(right: mdSize * 4, top: mdSize * 2, left: mdSize * 3.5);
  static var xlEdge = EdgeInsets.all(xlSize);
  static var xxlEdge = EdgeInsets.all(xxlSize);
  static var xxxlEdge = EdgeInsets.only(
      right: xxlSize, left: xxlSize, bottom: xxlSize, top: smSize);

  static Decoration boxDecoration() {
    return BoxDecoration(
        color: Conf.lightColor,
        boxShadow: [BoxShadow(color: Conf.shadowColor, blurRadius: 0.8)],
        borderRadius: Conf.halfBorderRadius);
  }

  static Color getColor({Color color}) {
    return color.computeLuminance() < 0.5 ? Colors.white : Colors.black;
  }

  static final String imagePlaceholder = 'assets/images/manavatar.jpg';
//  static final String imagePlaceholder2 = 'assets/images/1.jpg';

  static Widget divider({Color color}) => Divider(
        color: color ?? Conf.shadowColor,
        height: Conf.p2w(2.5),
      );

  static Widget verticalDivider({Color color}) => VerticalDivider(
        color: color ?? Conf.shadowColor,
        width: Conf.p2w(5),
      );

  static Widget circularProgressIndicator({bool hasColor = true}) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        color: hasColor ? Colors.black.withOpacity(0.3) : Conf.transparentColor,
        alignment: Alignment.center,
        child: Container(
//          height: Conf.p2w(15),width: Conf.p2w(18),
//            margin: EdgeInsets.only(top: Conf.p2w(65),left: Conf.p2w(41)),
//          decoration: BoxDecoration(
//              color: Conf.accentColor.withOpacity(0.8),
//              borderRadius: Conf.halfBorderRadius
//          ),
//          alignment: Alignment.center,
          child: Theme(
            data: ThemeData(accentColor: Conf.lightColor),
            child: CircularProgressIndicator(
              strokeWidth: Conf.smSize * 1.5,
              backgroundColor: Conf.primaryColor,
            ),
          ),
        ));
  }

  static Widget callBtnFunc(String number) {
    return RaisedButton(
        color: Colors.transparent,
        elevation: 0,
        onPressed: () {},
        child: FittedBox(
            fit: BoxFit.cover,
            child: Text(number,
                style: Conf.body1.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 24))));
  }

  static Widget loadingLayer() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black.withOpacity(0.3),
      alignment: Alignment.center,
    );
  }

  static Widget textFieldFormBuilder(
      String lable,
      String atrribute,
      Function onPresedSufixIcon,
      bool hasIcon,
      IconData icon,
      IconData sufixicon,
      List<FormFieldValidator> validatorList,
      TextInputType keybord,
      bool hasMorePadding,
      {bool obescureText,isActive=false,Function onPoresedSuffixIcon,
      bool hasPadding = false,initialValue}) {
//    hasIcon=true;
    return Padding(
      padding: hasPadding
          ? EdgeInsets.zero
          : hasMorePadding
              ? EdgeInsets.only(
                  right: Conf.p2w(6), left: Conf.p2w(6), top: Conf.p2h(1))
              : EdgeInsets.only(
                  right: Conf.p2w(1), left: Conf.p2w(1), top: Conf.p2h(1)),
      child: FormBuilderTextField(
        obscureText: obescureText ?? false,
        attribute: atrribute,
        initialValue: initialValue,
        validators: validatorList,
        keyboardType: keybord,
        readOnly: isActive,
        textAlign: TextAlign.right,
        decoration: InputDecoration(labelText: lable,labelStyle:  TextStyle(
            height: 0.5,
            fontSize: Conf.p2t(15),
            color: Colors.black45.withOpacity(0.4),
            fontWeight: FontWeight.w900),
            fillColor: Colors.grey.withOpacity(0.5),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(13)),
                borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))),
            border: new OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(18),
              ),
              borderSide: BorderSide(color: Conf.grey.withOpacity(0.1)),
//              gapPadding: 10
            ),
            suffixIcon: Padding(
                padding: EdgeInsets.only(left: Conf.p2w(5)),
                child: IconButton(
//                    FeatherIcons.eyeOff
                  icon: Icon(sufixicon ?? null),
                  onPressed: onPresedSufixIcon ?? null,
                  color: Colors.grey.withOpacity(0.6),
                  iconSize: Conf.p2t(18),
                )),
            prefixIcon: hasIcon
                ? Icon(
                    icon,
                    color: Colors.black87,
                    size: Conf.p2t(23),
                  )
                : null
            ),
      ),
    );
  }

  static Widget textFieldFormBuilder2(
      Function onPresedSufixIcon,
      List<FormFieldValidator> validatorList,
      TextInputType keybord,
      bool hasMorePadding,
      {bool obescureText,
        Function onTap,
        Function onChange,
        String atrribute,
        String lable,hint,
      bool hasPadding = false,var initialVal,bool isActive=false,int maxLines,bool expand,int minLine,TextEditingController controller}) {
    return Padding(
      padding: hasPadding ? EdgeInsets.all(2) : hasMorePadding ?
      EdgeInsets.only(right: Conf.p2w(6), left: Conf.p2w(6), top: Conf.p2h(1)) :
      EdgeInsets.only(right: Conf.p2w(1), left: Conf.p2w(1), top: Conf.p2h(1)),
      child: FormBuilderTextField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        onTap: (){ if(onTap is Function){
          onTap();
        }},
        onChanged: onChange,
        readOnly:!isActive ,
        initialValue: initialVal,
        maxLines: maxLines,
        attribute: atrribute,
        validators: validatorList,
        keyboardType: keybord,
        textAlign: TextAlign.right,
        controller: controller,
        decoration: InputDecoration(
          labelText: lable,
          labelStyle: TextStyle(
              height: 0.8,
//              height: 0.5,
              fontSize: Conf.p2t(10.2),
              color: Colors.black45.withOpacity(0.2),
              fontWeight: FontWeight.w900),
          fillColor: Colors.grey.withOpacity(0.5),
          enabledBorder: isActive?OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(13)),
              borderSide: BorderSide(color: Conf.grey.withOpacity(0.5))):OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(13)),
              borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))),
          border:isActive? OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(18)), borderSide: BorderSide(color: Conf.blue.withOpacity(0.4))):
          OutlineInputBorder(borderRadius:BorderRadius.all(Radius.circular(18)), borderSide: BorderSide(color: Conf.grey.withOpacity(0.1)),),
        ),
      ),
    );
  }

  static Widget textFieldFormBuilder3(
      {String lable,
      String atrribute,
      List<FormFieldValidator> validatorList,
      TextInputType keybord,
      bool obescureText,
      bool hasPadding = false}) {
//    hasIcon=true;
    return FormBuilderTextField(
      expands: true,
      obscureText: obescureText ?? false,
      attribute: atrribute,
      validators: validatorList,
      keyboardType: keybord,
      minLines: null,
      maxLines: null,
      textAlign: TextAlign.right,
      decoration: InputDecoration(labelText: lable,labelStyle:  TextStyle(
          color: Colors.grey,
          fontSize: Conf.p2t(15),
          fontWeight: FontWeight.w500),
          fillColor: Colors.grey.withOpacity(0.5),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(13)),
              borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(18),
            ),
            borderSide: BorderSide(color: Conf.grey.withOpacity(0.1)),
//              gapPadding: 10
          ),
                   ),
    );
  }

  static Widget dropDownFormBuilder(
      String hint, List<String> items, int height, width, TextStyle style,{String att,Function onChange}) {
    return Padding(
      padding: EdgeInsets.only(
          right: Conf.p2w(6), left: Conf.p2w(6), top: Conf.p2h(1)),
      child: FormBuilderDropdown(
        onChanged: (value){
          onChange(value);
        },
        attribute: att,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(13),),
            borderSide: BorderSide(color: Conf.grey.withOpacity(0.1)),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(13)),
              borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))),
        ),
        allowClear: true,
        hint: Padding(
          padding: EdgeInsets.only(right: Conf.p2w(2)),
          child: Text(hint, style: style),
        ),
        validators: [FormBuilderValidators.required(errorText: "*الزامی")],
        items: items
            .map((item) => DropdownMenuItem(
                  value: item,
                  child: Padding(
                    padding: EdgeInsets.only(right: Conf.p2w(2)),
                    child: Text('$item'),
                  ),
                ))
            .toList(),
      ),
    );
  }

  static Widget dropDownFormBuilder2(
      String hint, List<String> items, TextStyle style) {
    return Padding(
      padding: EdgeInsets.only(
          right: Conf.p2w(6), left: Conf.p2w(6), top: Conf.p2h(1)),
      child: FormBuilderDropdown(
        decoration: InputDecoration(
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(15),
            ),
            borderSide: BorderSide(color: Conf.grey.withOpacity(0.1)),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(13)),
              borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))),
        ),
        allowClear: true,
        hint: Padding(
          padding: EdgeInsets.only(right: Conf.p2w(0.3)),
          child: Text(hint, style: style),
        ),
        validators: [FormBuilderValidators.required(errorText: "*الزامی")],
        items: items
            .map((item) => DropdownMenuItem(
                  value: item,
                  child: Padding(
                    padding: EdgeInsets.only(right: Conf.p2w(2)),
                    child: Text('$item'),
                  ),
                ))
            .toList(),
      ),
    );
  }

  static Widget dropDownFormBuilder3(
      String hint, List<String> items, TextStyle style) {
    return Padding(
      padding: EdgeInsets.only(right: Conf.xlSize),
      child: FormBuilderDropdown(
        decoration: InputDecoration(
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(8),
            ),
            borderSide: BorderSide(color: Colors.black45, width: 2),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(13)),
              borderSide: BorderSide(color: Colors.grey)),
        ),
        allowClear: true,
        hint: Padding(
          padding: EdgeInsets.only(right: Conf.p2w(0.8)),
          child: Text(hint, style: style),
        ),
        validators: [FormBuilderValidators.required(errorText: "*الزامی")],
        items: items
            .map((item) => DropdownMenuItem(
                  value: item,
                  child: Padding(
                    padding: EdgeInsets.only(right: Conf.p2w(2)),
                    child: Text('$item'),
                  ),
                ))
            .toList(),
      ),
    );
  }

  static Widget dropDownFormBuilder4(
      String hint, List<String> items, TextStyle style) {
    return FormBuilderDropdown(
      decoration: InputDecoration(
        border: new OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(color: Colors.black45, width: 2),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(13)),
            borderSide: BorderSide(color: Colors.grey)),
      ),
      allowClear: true,
      hint: Padding(
        padding: EdgeInsets.only(right: Conf.p2w(0.8)),
        child: Text(hint, style: style),
      ),
      validators: [FormBuilderValidators.required(errorText: "*الزامی")],
      items: items
          .map((item) => DropdownMenuItem(
                value: item.toString(),
                child: Padding(
                  padding: EdgeInsets.only(right: Conf.p2w(2)),
                  child: Text('$item'),
                ),
              ))
          .toList(),
    );
  }

//  static Widget dropDownFormBuilder5(String hint,List<dynamic> items,TextStyle style){
//    return  FormBuilderDropdown(
//      decoration: InputDecoration(
//        border: new OutlineInputBorder(
//          borderRadius:
//          BorderRadius.all(Radius.circular(15)),borderSide: BorderSide(color: Colors.black45, width: 2),
//        ),
//        enabledBorder:
//        OutlineInputBorder(
//            borderRadius: BorderRadius.all(Radius.circular(13)),
//            borderSide: BorderSide(color: Colors.grey)
//        ),
//      ),
//      allowClear: true,
//      hint: Padding(
//        padding:EdgeInsets.only(right: Conf.p2w(0.8)),
//        child: Text(hint,
//            style:
//            style),
//      ),
//      validators: [FormBuilderValidators.required(errorText: "*الزامی")],
//      items: items
//          .map((item) => DropdownMenuItem(
//        value: item,
//        child: Padding(
//          padding:EdgeInsets.only(right: Conf.p2w(2)),
//          child: Text('$item'),
//        ),
//      ))
//          .toList(),
//    );
//  }
//  static Widget defaultAppbar(dynamic context, {String title, List<Widget> action, Widget leading, bool backButton = true}) {
//    return RoundedFloatingAppBar(
//      actions: action,
//      leading: leading,
//      textTheme: TextTheme(
//        title: TextStyle(
//          color: Colors.black,
//        ),
//      ),
//      pinned: true,
////      floating: true,
////      snap: false,
//      title: Row(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        mainAxisAlignment: MainAxisAlignment.start,
//        children: <Widget>[
//          backButton
//              ? IconButton(
//              alignment: Alignment.centerRight,
//              icon: Icon(
//                Icons.arrow_back,
//                color: Colors.black,
//              ),
//              onPressed: () => Navigator.of(context).pop())
//              : Container(),
//          Padding(
//            padding: EdgeInsets.symmetric(vertical: Conf.mdSize),
//            child: Text(
//              title ?? '',
//              style: Conf.title,
//            ),
//          ),
//        ],
//      ),
//      backgroundColor: Conf.lightColor,
//    );
//  }

//  static DialogButton okAlertButton(dynamic ctx, {String titleButton, Function onPress, Color color}) {
//    return DialogButton(
//      child: Text(
//        titleButton ?? Str.OK,
//        style: TextStyle(color: Colors.white, fontSize: 20),
//      ),
//      onPressed: onPress ??
//              () {
//            Navigator.of(ctx).pop();
//          },
//      width: 120,
//      color: color ?? Conf.primaryColor,
//    );
//  }
  static Widget formBuilderTimePicker(String lable, {int hour}) {
    return
//      Container();
        Container(
      width: Conf.p2w(30),
      height: Conf.p2h(12),
      child: FormBuilderDateTimePicker(
        attribute: 'date',
//       onChanged: (){},
        inputType: InputType.time,
        decoration: InputDecoration(
          labelText: lable,
          alignLabelWithHint: true,
          contentPadding: EdgeInsets.only(right: Conf.p2w(7), top: Conf.p2h(2)),
          labelStyle: TextStyle(
              fontSize: Conf.p2t(15),
              color: Colors.grey,
              fontWeight: FontWeight.w500),
        ),
        initialTime: TimeOfDay(hour: 0),
//         initialValue: DateTime.now(),
        enabled: true,
      ),
    );
  }

  static InputDecoration textFieldDecoration(
      {IconData icon, String hintText, label}) {
    return InputDecoration(
        labelText: label,
        labelStyle: Conf.body1,
        hintText: hintText,
        hintStyle: Conf.caption.copyWith(fontSize: Conf.lgText),
        focusColor: Conf.primaryColor,
        prefixIcon: icon == null ? null : Icon(icon),
        border: OutlineInputBorder(
            borderRadius: Conf.secondBorderRadius, gapPadding: 0),
        contentPadding: EdgeInsets.symmetric(
            vertical: Conf.mdSize, horizontal: Conf.xlSize),
        focusedBorder: OutlineInputBorder(
            borderRadius: Conf.secondBorderRadius,
            gapPadding: 0,
            borderSide: BorderSide(color: Conf.primaryColor)));
  }

  static Widget singleTextField(
      {String hintText,
      labelText,
      TextAlign textAlign,
      TextInputType textInput,
      List<TextInputFormatter> mask,
      Function validator,
      onSaved}) {
    return Padding(
      padding: EdgeInsets.only(top: Conf.xlSize),
      child: TextFormField(
        onSaved: onSaved,
        inputFormatters: mask,
        validator: validator,
        style: Conf.subheading.copyWith(
          fontSize: Conf.mdText * 1.3,
          letterSpacing: 5.0,
        ),
        textInputAction: TextInputAction.done,
        textAlign: textAlign ?? TextAlign.start,
        autofocus: false,
        keyboardType: textInput,
        decoration:
            Conf.textFieldDecoration(hintText: hintText, label: labelText),
      ),
    );
  }

  static Widget checkBox({bool value, Function onChanged}) {
    return Checkbox(
      focusNode: FocusNode(),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      value: value,
      onChanged: onChanged,
      checkColor: Conf.primaryColor,
      hoverColor: Conf.primaryColor,
      activeColor: Conf.accentColor,
      focusColor: Conf.accentColor,
    );
  }

  static InputDecoration iDec() {
    return InputDecoration(
      border: OutlineInputBorder(
        borderRadius: Conf.secondBorderRadius,
      ),
      filled: true,
      contentPadding: EdgeInsets.only(
          top: Conf.xlSize,
          bottom: Conf.xlSize,
          left: Conf.xlSize,
          right: Conf.xlSize + 3),
      labelStyle: Conf.subheadingW7
          .copyWith(fontSize: Conf.xlText, fontWeight: FontWeight.w900),
      fillColor: Conf.lightColor,
      isDense: true,
    );
  }

//
//  static Widget priceFormatter(int price,Color color,{bool unitPrice = true,double fontSize}) {
//    return Row(mainAxisSize: MainAxisSize.min,
//      children: <Widget>[Text(NumberFormat.currency(symbol:'', decimalDigits: 0).format(price),
//        style: Conf.body2W7.copyWith(color: color,fontSize:fontSize ),
//      ), unitPrice ? Padding(
//        padding:EdgeInsets.symmetric(horizontal: Conf.mdSize),
//        child: Text(Str.CONFIG_UNIT_PRICE, style: Conf.body2W7.copyWith(color: color)),
//      ):Container()],
//    );
//  }

  static Widget image(String image) {
    if (image == null) {
      return Image.asset(
        'assets/images/manavatar.jpeg',
//        'assets/images/noprofile.jpeg',
        fit: BoxFit.cover,
      );
    } else
      return
        FadeInImage.assetNetwork(
        image: image,
        placeholder: Conf.imagePlaceholder,
        fit: BoxFit.cover,
      );
  }

  static ImageProvider imageProvider(image) {
    if (image == null) {
      return AssetImage(
        'assets/images/noprofile.jpeg',
      );
    } else {
      return FileImage(image);
    }
  }
}
