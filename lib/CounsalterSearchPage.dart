import 'package:flutter/material.dart';
import 'DoctorModel.dart';
class CounsalterSearchPage extends SearchDelegate {
  @override
  String get searchFieldLabel => 'جستجو...';
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.length < 3) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              "عبارت مورد جستجو باید بیشتر از دو حرف باشد...",
            ),
          )
        ],
      );
    }
//    InheritedBlocs.of(context)
//        .searchBloc
//        .searchTerm
//        .add(query);

    return Column(
      children: <Widget>[Text(("جستجو....")),
//        Build the results based on the searchResults stream in the searchBloc
        StreamBuilder(
          stream: InheritedBlocs.of(context).searchBloc.searchResults,
          builder: (context, AsyncSnapshot<List<DoctorModel>> snapshot) {
            if (!snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                ],
              );
            } else if (snapshot.data.length == 0) {
              return Column(
                children: <Widget>[
                  Text(
                    "اطلاعاتی یافت نشد.....",
                  ),
                ],
              );
            } else {
              var results = snapshot.data;
              return ListView.builder(
                itemCount: results.length,
                itemBuilder: (context, index) {
                  var result = results[index];
                  return ListTile(
                    title: Text(result.name),
                  );
                },
              );
            }
          },
        ),
      ],
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }
}
class InheritedBlocs extends InheritedWidget {
  InheritedBlocs(
      {Key key,
        this.searchBloc,
        this.child})
      : super(key: key, child: child);

  final Widget child;
  final SearchBloc searchBloc;

  static InheritedBlocs of(BuildContext context) {
    return (context. dependOnInheritedWidgetOfExactType()
    as InheritedBlocs);
  }

  @override
  bool updateShouldNotify(InheritedBlocs oldWidget) {
    return true;
  }
}

class SearchBloc {
  int id;
  String title;

  SearchBloc({this.id, this.title});

  get searchResults => null;
//  get searchResults => SearchBloc(title: title,id: id);

}