class GetUrl {
    String url;

    GetUrl({this.url});

    factory GetUrl.fromJson(Map<String, dynamic> json) {
        return GetUrl(
            url: json['url'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['url'] = this.url;
        return data;
    }
}