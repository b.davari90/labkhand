import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'config.dart';
import 'package:counsalter5/DoctorListPage.dart';
import 'Collections.dart';
import 'package:page_transition/page_transition.dart';

class HomePageCollectionDrComponent extends StatelessWidget {
  Collections model;
  TextStyle txtTitle1 = TextStyle(
      fontWeight: FontWeight.w900, fontSize: 15, color: Colors.black87);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: Conf.p2w(3.125), right: Conf.p2w(3.125)),
      child: GestureDetector(
        onTap: (){
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.leftToRight,
                  duration: Duration(milliseconds: 500),
                  alignment: Alignment.centerLeft,
                  curve: Curves.easeOutCirc,
                  child: DoctorListPage()
              ));
        },
        child: Container(
          width: Conf.p2w(32),
          height: Conf.p2w(36),
          decoration: BoxDecoration(
            color: Colors.white,
              border: Border.all(color:Colors.grey.withOpacity(0.2)),
              borderRadius: BorderRadius.all(Radius.circular(13))
          ,  boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
//            blurRadius: 10,spreadRadius: 1,
                        offset: Offset( -1,2.7)
            )
          ]
          ),
          child:Padding(
            padding:  EdgeInsets.symmetric(vertical: Conf.xlSize).copyWith(top: Conf.xxlSize),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                FittedBox(fit: BoxFit.cover,
                  child:  Container(width: Conf.p2w(16),height: Conf.p2w(16),
                    child: FadeInImage.assetNetwork(
                      placeholder:"" ,
                      image:model.thumb??
                      "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top:Conf.p2h(1.5) ),
                  child: Text(model.title,
                      softWrap: true,
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                          fontWeight: FontWeight.w800,
                          color: Colors.black87,
                          fontFamily:'ybakh' ,
                          fontSize: Conf.p2t(16))
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  HomePageCollectionDrComponent({this.model});
}
