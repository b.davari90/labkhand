import 'package:bloc/bloc.dart';

class Collections {
    List<Collections> children;
    int count_customer, count_doctor,id,parent_id;
    String created_at, deleted_at,icon,thumb,
        title,updated_at;
    bool has_in_menu;

    Collections({this.children,
        this.count_customer, this.count_doctor,
        this.created_at, this.deleted_at,
        this.has_in_menu, this.icon, this.id,
        this.parent_id, this.thumb, this.title, this.updated_at});

    factory Collections.fromJson(Map<String, dynamic> json) {
        List<Collections> children =[];
        if (json.containsKey('children') && json['children'] != null ) {
            List dList = json['children'];
            children = dList.map((e) => Collections.fromJson(e)).toList();
        }
        return Collections(
            children:children,
            count_customer: json['count_customer'], 
            count_doctor: json['count_doctor'], 
            created_at: json['created_at'], 
            deleted_at: json['deleted_at'], 
            has_in_menu: json['has_in_menu'], 
            icon: json['icon'], 
            id: json['id'], 
            parent_id: json['parent_id'], 
            thumb: json['thumb'], 
            title: json['title'], 
            updated_at: json['updated_at'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['children'] = this.children;
        data['count_customer'] = this.count_customer;
        data['count_doctor'] = this.count_doctor;
        data['created_at'] = this.created_at;
        data['deleted_at'] = this.deleted_at;
        data['has_in_menu'] = this.has_in_menu;
        data['icon'] = this.icon;
        data['id'] = this.id;
        data['parent_id'] = this.parent_id;
        data['thumb'] = this.thumb;
        data['title'] = this.title;
        data['updated_at'] = this.updated_at;
        return data;
    }
}
//Collections copy() {
//    return Collections(count_customer: );
//}
class CollectionsRepository {
    Collections collections;
    CollectionsRepository({this.collections});
    CollectionsRepository copy() {
        return CollectionsRepository(collections:collections);
    }
}
class CollectionsBloc extends Cubit<CollectionsRepository>{
    CollectionsBloc(CollectionsRepository state) : super(state);

    void setDoctorCollection(Collections collections) {
        state.collections= collections;
        print(")_)__state.collections  BloC===> ${ state.collections }");
        refresh();
    }
    void setId(int id){
        state.collections.id=id;
        refresh();
    }
    void setIcon(String icon){
        state.collections.icon=icon;
        refresh();
    }
    void setHasInMenu(bool hasInMenu){
        state.collections.has_in_menu=hasInMenu;
        refresh();
    }
    void setCountDoctor(int countDoctor){
        state.collections.count_doctor=countDoctor;
        refresh();
    }
    void setCount_customer(int count_customer){
        state.collections.count_customer=count_customer;
        refresh();
    }
    void setTitle(String title){
        state.collections.title=title;
        refresh();
    }
    void setThumb(String thumb){
        state.collections.thumb=thumb;
        refresh();
    }
//    setCollectionList(List collections){if(collections==null){collections=[];}}

    void setParenttId(int parent_id){
        state.collections.parent_id=parent_id;
        refresh();
    }

    void refresh() {
        emit(state.copy());
    }
}