import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shamsi_date/shamsi_date.dart';
import 'config.dart';
import 'package:persian_date/persian_date.dart';
class VisitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String todayMonth;
    String month(int i){
      switch(i){
        case 1:
          todayMonth="فروردین";break;
        case 2:
          todayMonth="اردیبهشت";break;
        case 4:
          todayMonth="خرداد";break;
        case 4:
          todayMonth="تیر";break;
        case 5:
          todayMonth="مرداد";break;
        case 6:
          todayMonth="شهریور";break;
        case 7:
          todayMonth="مهر";break;
        case 8:
          todayMonth="آبان";break;
        case 9:
          todayMonth="آذر";break;
        case 10:
          todayMonth="دی";break;
        case 11:
          todayMonth="بهمن";break;
        case 12:
          todayMonth="اسفند";
          print("todayMonth ===> ${ todayMonth }");break;
      }
      return todayMonth;
    }
    month(5);
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: Conf.p2h(100),
          color: Conf.blue,
          child: Padding(
            padding: Conf.smEdgeRightTop1 / 3,
            child: Column(
              children: <Widget>[
               Row(mainAxisAlignment: MainAxisAlignment.start,
                 children: [
                   MasBackBotton(color: Conf.blue,colorBackground: Conf.lightColor,icon: Icons.close,),],),
                Image.asset(
                  "assets/images/404.png",
                  height: Conf.p2h(65),
                  width: Conf.p2w(80),
                ),
                Text(
                    "صفحه در حال به روز رسانی می باشد ....", softWrap: true,
                    overflow: TextOverflow.fade,
                    style:Conf.subtitle.copyWith(
                        fontSize: 22,
                        color: Colors.white70,
                        fontWeight: FontWeight.w900)
//                      TextStyle(
//                          fontSize: 22,
//                          color: Colors.white60,
//                          fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
      ),
    );
//  color: Colors.indigo,
  }
}
