import 'package:counsalter5/PriceModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'config.dart';
import 'package:counsalter5/ModelBloc/DoctorPriceModel.dart';

class CounsalterPriceROwComponent extends StatefulWidget {
  Function onTap;
  DoctorPriceModel model;
  bool isSelectedRow;
//  IconData iconData;
  CounsalterPriceROwComponent(
      {this.onTap, this.model, this.isSelectedRow });

  @override
  _CounsalterPriceROwComponentState createState() =>
      _CounsalterPriceROwComponentState();
}

class _CounsalterPriceROwComponentState extends State<CounsalterPriceROwComponent> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (widget.onTap is Function) {
            widget.onTap();
            print("widget.isSelectedRow ===> ${ widget.isSelectedRow }");
            print("widget.onTap ===> ${ widget.onTap }");
        }
          }
          );
      },
      child: Container(
        margin: Conf.xlEdge.copyWith(bottom: 0, top: Conf.mdSize),
        padding: Conf.mdEdge,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white),
          color: widget.isSelectedRow ? Colors.white : Conf.blue,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(top: Conf.p2h(0.5), left: Conf.p2w(1)),
                  child:widget.model.type==1?Icon(Icons.call,color: widget.isSelectedRow
                      ? Colors.black87: Colors.white,):
                  widget.model.type==2?Icon( Icons.message,color: widget.isSelectedRow
                      ? Colors.black87
                      : Colors.white,):
                  widget.model.type==3?Icon(Icons.voice_chat,color:  widget.isSelectedRow
                      ? Colors.black87: Colors.white,):widget.model.type==4?Icon(Icons.settings_voice,color: widget.isSelectedRow
                      ? Colors.black87
                      : Colors.white,):
                  Icon(null)
//                  widget.model.icon!=null?Image.network(widget.model.icon,color:  widget.isSelectedRow
//                      ? Colors.black87
//                      : Colors.white):Icon(null)
                ),
                FittedBox(
                  fit: BoxFit.cover,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: Conf.p2h(0.5), right: Conf.p2w(0.3)),
                    child: Text(
                      widget.model.type==1?"تلفنی":widget.model.type==2?"متنی":widget.model.type==3?"تصویری":widget.model.type==4?"صوتی":"",
//                      widget.model.type.values.toString().replaceAll(")", "").replaceAll("(", ""),
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: Conf.p2t(15), //17,
                        color: widget.isSelectedRow
                            ? Colors.black87
                            : Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                FittedBox(
                  fit: BoxFit.cover,
                  child: Padding(
                    padding:
//                  lastRowPadding
//                      ? EdgeInsets.only(
//                      right: Conf.p2w(23), top: Conf.p2h(0.5))
//                      :
                        EdgeInsets.only(
                            right: Conf.p2w(21), top: Conf.p2h(0.5)),
                    child: Text(
                      widget.model.price.toString(),
                      style: TextStyle(
                        fontSize: Conf.p2t(Conf.p2w(4.167)),
                        fontWeight: FontWeight.bold,
                        color: widget.isSelectedRow
                            ? Colors.black87
                            : Colors.white,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(right: Conf.p2w(2), top: Conf.p2h(1)),
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: Text(
                      "تومان",
                      style: TextStyle(
                          color: widget.isSelectedRow
                              ? Colors.black87
                              : Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: Conf.p2t(12)),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
