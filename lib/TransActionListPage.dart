import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/ModelBloc/TransactionModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'TransactionCardComponent.dart';
import 'config.dart';
import 'package:counsalter5/ModelBloc/TransactionListModel.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params_result.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
class TransActionListPage extends StatefulWidget {
  @override
  _TransActionListPageState createState() => _TransActionListPageState();
}

class _TransActionListPageState extends State<TransActionListPage> {
//  String transActionSuccesful = "افزایش موجودی حساب از طریق پرداخت آنلاین";
//  String transActionFailed = "کسر هزینه نوبت مشاوره مشاور حسین کلایی";
  bool loading=false;
  int count,pages,limit;
  TransactionListModel transactionListModel;
  List<TransactionModel> transactionLst;
  SearchAdvCubit get searchBloc => context.bloc<SearchAdvCubit>();
  ScrollController sc;
  QueryListParams qlp;
  QueryListParamsResult listResult;
//  List<Widget> taskList;
  Future getAllCustomerTransaction() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.get("cu/transaction");
      print("data ===> ${ data }");
      transactionListModel= TransactionListModel.fromJson(data);
      count = transactionListModel.count;
      limit = transactionListModel.limit;
      pages = transactionListModel.page;
      print("count ===> ${ count }");
      print("limit ===> ${ limit }");
      print("pages ===> ${ pages }");
      transactionLst.addAll(transactionListModel.rows);
      print("transactionLst.m ===> ${ transactionLst.map((e) => e.updated_at) }");
    } catch (e) {
      print("CatchError getAllDoctorFolder()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }
//  importlist(List<Widget> lists) {
//    taskList = lists;
//  }

//  createCard() {
//    dataList = [
//      TransActionCardModel(
//          day: "05",
//          year: "99",
//          month: "12",
//          describtion: transActionSuccesful,
//          hour: "03",
//          priceAmount: "30000",
//          second: "50",
//          colorStatus: Colors.green),
//      TransActionCardModel(
//          day: "12",
//          year: "99",
//          month: "03",
//          describtion: transActionFailed,
//          hour: "11",
//          priceAmount: "150000",
//          second: "48",
//          colorStatus: Colors.red),
//      TransActionCardModel(
//          day: "08",
//          year: "99",
//          month: "06",
//          describtion: transActionSuccesful,
//          hour: "13",
//          priceAmount: "50000",
//          second: "30",
//          colorStatus: Colors.red),
//      TransActionCardModel(
//          day: "17",
//          year: "99",
//          month: "02",
//          describtion: transActionFailed,
//          hour: "14",
//          priceAmount: "10000",
//          second: "18",
//          colorStatus: Colors.red),
//      TransActionCardModel(
//          day: "05",
//          year: "99",
//          month: "12",
//          describtion: transActionSuccesful,
//          hour: "03",
//          priceAmount: "30000",
//          second: "50",
//          colorStatus: Colors.green),
//      TransActionCardModel(
//          day: "25",
//          year: "99",
//          month: "10",
//          describtion: transActionFailed,
//          hour: "07",
//          priceAmount: "150000",
//          second: "20",
//          colorStatus: Colors.red),
//    ];
//    return SingleChildScrollView(
//        scrollDirection: Axis.horizontal,
//        child:Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: dataList.map((e) {
//              return TransactionCardComponent(
//                  isFirstItem: dataList.indexOf(e) < 1 ?true:false,
//                  hasDate: true,
//                  hasShadow: true,
////                      isFirstItem: dataList.indexOf(e) < 1 ?true:false,
//                  model: e,
//                  onTap: (key) {
//                    setState(() {
//
//                    });
//                  });
//            }).toList()));
//  }

  @override
  void initState() {
    transactionLst=[];
    transactionListModel=TransactionListModel();
    qlp = QueryListParams();
    listResult = QueryListParamsResult();
    sc = ScrollController();
    qlp.page = 0;
    searchBloc.setQlp(qlp);
    sc.addListener(() {
      if (sc.offset >= sc.position.maxScrollExtent - 100 &&
          !sc.position.outOfRange &&
          qlp.page < pages) {
        setState(() {
          qlp.page++;
          searchBloc.setQlp(qlp);
          getAllCustomerTransaction();
        });
      }
    });getAllCustomerTransaction();
    super.initState();
  }
  @override
  void dispose() {
    sc.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            width: double.infinity,
            height:Conf.p2h(100),
            color: Conf.blue,
          ),
          Positioned(
            width: Conf.p2w(100),
            top: Conf.p2h(4),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("تراکنش ها",
                    style: Conf.headline.copyWith(
                        color: Colors.white,
                        fontSize: Conf.p2t(25),
                        fontWeight: FontWeight.w500,
                        decoration: TextDecoration.none)),
              ],
            ),
          ),
          Positioned(
            top: Conf.p2h(4),
            left: Conf.p2w(6),
            child: Column(
              children: [
                MasBackBotton()
              ],
            ),
          ),
          Positioned(bottom: 0,top: Conf.p2w(3),left: 0,right: 0,child:      Container(
            margin: EdgeInsets.only(top: Conf.p2w(25)),
            decoration: BoxDecoration(
                color: Conf.lightColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))
            ),
            width: double.infinity,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30)),
              child:Container(child:
              SingleChildScrollView(
                controller: sc,
                  scrollDirection: Axis.vertical,
                  padding: EdgeInsets.only(bottom: Conf.xlSize),
                  child: Container(
                    child: transactionLst==[]?Container(child: Center(child: Text("تراکنشی موحود نمی باشد"),),)
                    :Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: transactionLst.map((e) {
                          return TransactionCardComponent(
                              hasShadow: true,
                              hasDate: true,
                              model: e,
                              sts: e.sts,
                              onTap: (key) {
//                                Navigator.push(
//                                    context,
//                                    PageTransition(
//                                        type: PageTransitionType.leftToRight,
//                                        duration: Duration(milliseconds: 500),
//                                        alignment: Alignment.centerLeft,
//                                        curve: Curves.easeOutCirc,
//                                        child: StatusReserveCoustomerPage()
//                                    ));
//                                        setState(() {});
                              });
                        }).toList()),
                  )),
            ),
          ), )

          ),loading==true?Conf.circularProgressIndicator():Container()],
      )),
    );
  }
}
