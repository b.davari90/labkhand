import 'dart:ui';

import 'package:counsalter5/CounsalterProfile.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'BottomNavigation.dart';
import 'MasBackBotton_ui.dart';
import 'DoctorModel.dart';
import 'CounsalterSearchPage.dart';
import 'DoctorCardComponent.dart';
import 'config.dart';

class ChatPage extends StatefulWidget {
  TextEditingController controller = TextEditingController();
  List<DoctorModel> dataList2;

  createCard2() {
    dataList2 = [
      DoctorModel(
          fname: "حسین ",
          lname: "کلایی",
          avatar: null,
          degree: 0,
//          degree: "0",
          specialty: "روانشناس کودک"
      ),
      DoctorModel(
          fname: "محمد ",
          lname: "محمدی",
          avatar: null,
          degree:0,
//          degree:"0",
          specialty: "روانشناس خانواده"
      ),
      DoctorModel(
          fname: "علی ",
          lname: "رضایی",
          avatar: null,
          degree:0,
//          degree: "0",
          specialty: "روانشناس بالینی"
      ),
      DoctorModel(
          fname: "حسین ",
          lname: "کلایی",
          avatar: null,
          degree:0,
//          degree: "0",
          specialty: "روانشناس کودک"
      ),
      DoctorModel(
          fname: "احمد ",
          lname: "ریئیسی",
          avatar: null,
//          degree: "0",
          degree:0,
          specialty: "روانشناس خانواده"
      ),
      DoctorModel(
          fname: "دانیال ",
          lname: "شهسواری",
          avatar: null,
//          degree:"0",
          degree:0,
          specialty: "روانشناس بالینی"
      ),
      DoctorModel(
          fname: "حسین ",
          lname: "کلایی",
          avatar: null,
//          degree: "0",
          degree:0,
          specialty: "روانشناس کودک"
      ),
      DoctorModel(
          fname: "محمد ",
          lname: "محمدی",
          avatar: null,
//          degree: "0",
          degree:0,
          specialty: "روانشناس خانواده"
      ),
    ];
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: const EdgeInsets.only(left: 25, top: 4, bottom: 10),
          child: Row(
            children: dataList2
                .map((item) =>
                DoctorCardComponent(
//                    model: item
                ))
                .toList(),
          ),
        ));
  }

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  void initState() {
    widget.createCard2();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Stack(
//          fit: StackFit.loose,
          children: <Widget>[
            SingleChildScrollView(

              child: Container(
                width: double.infinity,
                height: Conf.p2h(100),
                color: Conf.blue,
              ),
            ),
            Positioned(
              width: Conf.p2w(100),
              top: Conf.p2h(4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                      "پیام رسان",
//                      softWrap: true,
//                      overflow: TextOverflow.fade,
                      style: Conf.headline.copyWith(
                          color: Colors.white,
                          fontSize: Conf.p2t(30),
                          fontWeight: FontWeight.w900,
                          decoration: TextDecoration.none)
//                          TextStyle(
//                              color: Colors.white,
//                              fontSize: 29,
//                              fontWeight: FontWeight.w900,
//                              decoration: TextDecoration.none),
                  ),
                ],
              ),
            ),
            Positioned(
              top: Conf.p2h(4),
              left: Conf.p2w(6),
              child: MasBackBotton(),
            ),
            Positioned(
              top: Conf.p2h(12), //123,
              left: Conf.p2w(6.25),
//              top: Conf.p2h(14), //123,
//              left: Conf.p2w(6.25), //30,
              child: Padding(
                padding: Conf.smEdgeTop0,
                child: Container(
                  width: Conf.p2w(88),
                  height: Conf.p2h(6.5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(color: Colors.grey.withOpacity(0.4)),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: Conf.p2w(6.25), //30,
                        )
                      ]),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: Conf.smEdgeRight1 / 1.2,
                        child: Icon(
                          FeatherIcons.search,
                          size: Conf.p2w(3.75), //18,
                          color: Colors.grey.withOpacity(0.4),
                        ),
                      ),
                      Padding(
                        padding: Conf.smEdgeRightLeft / 1.5,
                        child: SizedBox(
                          width: Conf.p2w(60),
                          height: Conf.p2h(10),
                          child: TextField(
                            onTap: () {
                              showSearch(
                                context: context,
                                delegate: CounsalterSearchPage(),
                              );
                            },
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                                fontSize: Conf.p2t(15)),
                            controller: widget.controller,
                            decoration: InputDecoration(
                                hintStyle: TextStyle(
                                    color: Colors.grey.withOpacity(0.3)),
                                border: InputBorder.none,
                                hintText:
                                ''),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Column(
              children: [
                Expanded(
                  child: Container(

//                    height: Conf.p2h(50),
                    margin: EdgeInsets.only(top: Conf.p2h(22)),
                    decoration: BoxDecoration(
//                        color: Colors.yellow,
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(Conf.p2w(3.125)),
                            topRight: Radius.circular(Conf.p2w(3.125)))),
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: Conf.p2w(1.042)),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Padding(
                          padding: EdgeInsets.only(top: Conf.p2h(2)),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: widget.dataList2.map((e) =>
                                  DoctorCardComponent(
//                                    model: e,
                                    hasLightShadow: true,
                                    isOnline: false,
                                    hasShadow: true,
                                    hasUnSeenMsg: true,
                                    hasTime: true,
                                    hour: "08",
                                    minute: "10",
                                    msgNumber: "5",
                                  )).toList()),
                        )
//                          children:
//                          <Widget>[
//                            InkWell(
//                              onTap: () {
//                                Navigator.push(
//                                    context,
//                                    PageTransition(
//                                        type: PageTransitionType.leftToRight,
//                                        duration: Duration(milliseconds: 500),
//                                        alignment: Alignment.centerLeft,
//                                        curve: Curves.easeOutCirc,
//                                        child: CounsalterProfile()
//                                    ));
//                              },
//                              child: DoctorCardComponent(
//                                model: ,
////                                name: "حسین ",
////                                family: "کلایی",
////                                grade: "متخصص",
////                                isOnline: false,
////                                kind: "روانشناس کودک",
////                                img:null,
//////                                img:
//////                                "assets/images/prof1.jpg",
//////                                      "https://th.bing.com/th/id/OIP.MVPo7ij6L0D4_7tmS53kKAHaE7?pid=Api&rs=1",
//////                              isOnline: "آنلاین",
////                                hasShadow: false,
////                                hasUnSeenMsg: true,
////                                hasTime: true,
////                                hour: "08",
////                                minute: "10",
////                                msgNumber: "5",
//                              ),
//                            ),
//
//                          ],
                        ),
                      ),
                    ),
                  ),
//                ),
              ],
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigation(1),
      ),
    );
  }
}
