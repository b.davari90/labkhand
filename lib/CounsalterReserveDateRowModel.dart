class CounsalterReserveDateRowModel {
    String numberDate;
//    bool isActive;

    CounsalterReserveDateRowModel({this.numberDate});

    factory CounsalterReserveDateRowModel.fromJson(Map<String, dynamic> json) {
        return CounsalterReserveDateRowModel(
            numberDate: json['numberDate'],
//            isActive: json['isActive'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['numberDate'] = this.numberDate;
//        data['isActive'] = this.isActive;
        return data;
    }
}