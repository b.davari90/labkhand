class ContainerModel {
    String img;
    double rate;
    String title1;
    String title2;

    ContainerModel({this.img, this.rate, this.title1, this.title2});

    factory ContainerModel.fromJson(Map<String, dynamic> json) {
        return ContainerModel(
            img: json['img'], 
            rate: json['rate'], 
            title1: json['title1'], 
            title2: json['title2'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['img'] = this.img;
        data['rate'] = this.rate;
        data['title1'] = this.title1;
        data['title2'] = this.title2;
        return data;
    }
}