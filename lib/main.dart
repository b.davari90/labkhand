import 'package:counsalter5/DoctorPresentsModel.dart';
import 'package:counsalter5/ModelBloc/DoctorBlocDoctorInformationPage.dart';
import 'package:counsalter5/ModelBloc/DoctorCollectionModel.dart';
import 'package:counsalter5/ModelBloc/DocumentModel.dart';
import 'package:counsalter5/ModelBloc/CustomerChilderenModel.dart';
import 'package:flutter/material.dart';
import 'package:counsalter5/Doctor/DoctorTimeSliceModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/ModelBloc/CustomerVisitLists.dart';
import 'package:flutter/services.dart';
import 'SplashScreenPage.dart';
import 'CustomerModel.dart';
import 'DoctorModel.dart';
import 'Collections.dart';
import 'config.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc.dart';
import 'package:counsalter5/SearchDelegateCityTown/city_province_bloc2.dart';
import 'package:counsalter5/SearchDelegateCityTown/city.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
import 'package:counsalter5/Doctor/pagination/search_repository.dart';
import 'dart:io';
import 'package:counsalter5/ModelBloc/WalletModel.dart';
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
void main() {
  HttpOverrides.global = new MyHttpOverrides();

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Conf.primaryColor, // status bar color
    statusBarBrightness: Brightness.dark,//status bar brigtness
    statusBarIconBrightness:Brightness.dark , //status barIcon Brightness
    systemNavigationBarIconBrightness: Brightness.light, //navigation bar icon
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MultiBlocProvider(
      providers: [
        BlocProvider<CoustomerBloc>(create: (BuildContext context) => CoustomerBloc(CoustomerRepository())),
        BlocProvider<WalletBloc>(create: (BuildContext context) => WalletBloc(WalletRepository())),
        BlocProvider<SearchAdvCubit>(create: (BuildContext context) => SearchAdvCubit(SearchRepository())),
        BlocProvider<CustomerVisitListsBloc>(create: (BuildContext context) => CustomerVisitListsBloc(CustomerVisitListsRepository())),
        BlocProvider<DoctorTimeSliceBloc>(create: (BuildContext context) => DoctorTimeSliceBloc(DoctorTimeSliceRepository())),
        BlocProvider<CollectionsBloc>(create: (BuildContext context) => CollectionsBloc(CollectionsRepository())),
        BlocProvider<DoctorPresentsBloc>(create: (BuildContext context) => DoctorPresentsBloc(DoctorPresentsRepository())),
        BlocProvider<DocumentModelBloc>(create: (BuildContext context) => DocumentModelBloc(DocumentModelRepository())),
        BlocProvider<DoctorBlocDoctorInformationPage>(create: (BuildContext context) => DoctorBlocDoctorInformationPage(DoctorRepository())),
//        BlocProvider<VisitsModelBloc>(create: (BuildContext context) => VisitsModelBloc(VisitsModelRepository())),
        BlocProvider<DoctorBloc>(create: (BuildContext context) => DoctorBloc(DoctorRepository())),
        BlocProvider<CityProvinceCubit>(create: (BuildContext context) => CityProvinceCubit(CityCubitModel())),
        BlocProvider<CityProvinceCubit2>(create: (BuildContext context) => CityProvinceCubit2(CityCubitModel())),
        BlocProvider<CoustomerChilderenBloc>(create: (BuildContext context) => CoustomerChilderenBloc(CustomerChilderenRepository())),
        BlocProvider<DoctorCollectionModelCubit>(create: (BuildContext context) => DoctorCollectionModelCubit(DoctorCollectionModelRepository())),
      ],
      child: MaterialApp(
        theme: ThemeData(fontFamily: 'ybakh'),
        builder: (context, Widget child) {
          SystemUiOverlayStyle.light
              .copyWith(systemNavigationBarColor: Conf.blue);
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: Conf.blue, //or set color with: Color(0xFF0000FF)
          ));
          final mediaQueryData=MediaQuery.of(context);
          final constrainedTextScaleFactor= mediaQueryData.textScaleFactor.clamp(1.0,1.0);
          return MediaQuery(
            data: mediaQueryData.copyWith(textScaleFactor: constrainedTextScaleFactor),
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: child,
            ),
          );
        },
        debugShowCheckedModeBanner: false,
        home: SplashScreenPage(),
      ),
    );
  }
}
