import 'package:circular_check_box/circular_check_box.dart';
import 'package:counsalter5/HomePage.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'AboutUs.dart';
import 'CustomerModel.dart';
import 'config.dart';
import 'package:counsalter5/CustomerModel.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'htify.dart';

class 
RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormBuilderState>();
  CustomerModel customer;
  CoustomerBloc get bloc => context.bloc<CoustomerBloc>();
  SharedPreferences sharedPreferences;
  bool isChecked = true;
  bool loading = true;
  bool _obscureText = true;
  bool _passwordVisible = false;
  String _password;
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  TextStyle txtStyleOrange = TextStyle(
    color: Conf.orang,
    fontWeight: FontWeight.w700,
    fontSize: 15,
    decoration: TextDecoration.none,
  );
  Future createUser() async {
    setState(() {
      loading = false;
    });
    try {
      print("customer.fname ===> ${customer.fname}");
      print("customer.lname ===> ${customer.lname}");
      print("customer.mobile ===> ${customer.mobile}");
      print("customer.password ===> ${customer.password}");
      var data = await Htify.create('auth/login', {

          "mobile": bloc.state.user.mobile,
          "fname": bloc.state.user.fname,
          "lname": bloc.state.user.lname,
          "password": bloc.state.user.password,
          "provider": "register"

      });
      print("data ===> ${data}");
      sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString('token', data['access_token']);
      Conf.token = data['access_token'];

      Navigator.pushReplacement(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: HomePage()
          ));

      print("*****    bloc.state.user.fname ===> ${ bloc.state.user.fname }");
      print("*****    bloc.state.user.fname ===> ${ bloc.state.user.lname }");
    } catch (e) {
      print("e ===> ${e.toString()}");
      _showTopFlash(errorText: 'عدم توانایی در ارسال داده به سرور');
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
  @override
  void initState() {
    customer = CustomerModel();
    _passwordVisible = false;
    super.initState();
  }
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                width: double.infinity,
                color: Conf.blue,
              ),
              Positioned(
                top: Conf.p2h(4),
                width: Conf.p2w(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ثبت نام",
//                            softWrap: true,
//                          overflow: TextOverflow.fade,
                        style:Conf.body2.copyWith(
                            color: Colors.white.withOpacity(0.9),
                            fontSize: Conf.p2t(20),
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.none)),
                  ],
                ),
              ),
              Positioned(
                width: Conf.p2w(100),
                top: Conf.p2h(4),
                left: Conf.p2h(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
//                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    MasBackBotton()
                  ],
                ),
              ),
              Positioned(
                bottom: Conf.p2h(0),
                top: Conf.p2h(15),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child:BlocBuilder<CoustomerBloc, CoustomerRepository>(builder: (context, state){
                return Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: ClipRRect(
                      borderRadius:  BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      child: SingleChildScrollView(
                        child: FormBuilder(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: Padding(
                            padding: Conf.xlEdge,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ثبت نام",
                                  softWrap: true,
                                  overflow: TextOverflow.fade,
                                  style: Conf.body1.copyWith(
                                    color: Colors.black87.withOpacity(0.75),
                                    fontWeight: FontWeight.w700,
                                    fontSize: Conf.p2t(20),
                                    decoration: TextDecoration.none,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.p2h(0.7)),
                                  child: Text("اطلاعات خود را وارد نمائید",
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                      style: Conf.body1.copyWith(
                                        color: Colors.black45,
                                        fontWeight: FontWeight.w800,
                                        fontSize: Conf.p2t(19.5),
                                        decoration: TextDecoration.none,
                                      )),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: Conf.p2w(1),
//                                  bottom: Conf.p2w(2)
                                  ),
                                  child: Conf.textFieldFormBuilder(
                                      'نام',
                                      "fname",(){},
                                      true,
                                      Icons.person,null,
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "*"),FormBuilderValidators.minLength(3,errorText: "الزامی-حداقل سه حرف")
                                      ],
                                      TextInputType.text,
                                      false),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: Conf.p2w(1),
//                                  bottom: Conf.p2w(2)
                                  ),
                                  child: Conf.textFieldFormBuilder(
                                      'نام خانوادگی',
                                      "lname",
                                          (){},
                                      true,
                                      Icons.person,null,
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "*الزامی-حداقل سه حرف"),FormBuilderValidators.minLength(3,errorText: "حداقل 3 کارکتر")
                                      ],
                                      TextInputType.text,
                                      false),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: Conf.p2w(1),
//                                  bottom: Conf.p2w(2)
                                  ),
//                          padding: Conf.smEdgeBottomTop1,
                                  child: Conf.textFieldFormBuilder(
                                      'تلفن همراه',
                                      "mobile",
                                      (){},
                                      true,
                                      Icons.call,null,
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "*الزامی-عدد وارد کنید"),
                                        FormBuilderValidators.required(
                                            errorText: "*"),
                                        FormBuilderValidators.minLength(11,
                                            errorText:
                                                "شماره همراه خود باید 11 رقم باشد"),
                                        FormBuilderValidators.maxLength(11,
                                            errorText:
                                            "نمیتواند بیشتر از 11 رقم باشد")
                                      ],
                                      TextInputType.number,
                                      false),
                                ),
//                                Padding(
//                                  padding: EdgeInsets.only(top: Conf.p2w(1)
////                                  , bottom: Conf.p2w(2)
//                                      ),
//                                  child: Conf.textFieldFormBuilder(
//                                      'رمز عبور',
//                                      "password",(){
//                                    setState(() {
//                                      _passwordVisible = !_passwordVisible;
//                                    });
//                                  },
//                                      true,
//                                      Icons.vpn_key,_passwordVisible ? FeatherIcons.eye: FeatherIcons.eyeOff,
//                                      [
//                                        FormBuilderValidators.required(
//                                            errorText: "*الزامی"),
//                                      ],
//                                      TextInputType.text,
//                                      false,obescureText: !_passwordVisible),
//                                ),
                                FittedBox(
                                  fit: BoxFit.cover,
                                  child: Padding(
                                    padding: EdgeInsets.only(top: Conf.p2h(1)),
                                    child: Row(
                                      children: <Widget>[
                                        CircularCheckBox(
//                                        tristate: false,
                                            value: isChecked,
                                            activeColor: Conf.orang,
                                            onChanged: (bool newValue) {
                                              setState(() {
                                                isChecked = newValue;
                                              });
                                            }),
                                        Text(
                                            " شرایط خدمات و سیاست حفظ حریم خصوصی بهداشت و درمان را بپذیرید ",
                                            softWrap: true,
                                            overflow: TextOverflow.fade,
                                            style: Conf.body2.copyWith(
                                              color: Conf.orang,
                                              fontSize: Conf.p2t(14),
                                              fontWeight: FontWeight.w700,
                                              decoration: TextDecoration.none,
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width: Conf.p2w(100),
                                  height: Conf.p2w(15),
                                  child: RaisedButton(
                                    color: Conf.blue,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(13.0),
                                        side: BorderSide(
                                          color: Conf.blue,
                                        )),
                                    onPressed: () async {
                                      _formKey.currentState.save();
                                      print( "***********&&&****_formKey.currentState.value.save() ===> ${_formKey.currentState.value}");
                                      bool validate =_formKey.currentState.validate();
                                      if (validate) {
                                        var result = _formKey.currentState.value;
                                        customer.fname = result["fname"];
                                        customer.lname = result["lname"];
                                        customer.mobile = result["mobile"];
                                        customer.password = result["password"];
                                        print("9999 ===> ${ 9999 }");
                                        print(" bloc.state.user ===> ${bloc.state.user }");
                                        CustomerModel t  = bloc.state.user;
                                        print("0000 ===> ${ t}");
                                        t.fname =result["fname"];
                                        t.lname =result["lname"];
                                        t.mobile =result["mobile"];
                                        t.password =result["password"];
                                        print(" 55555===> ${t.mobile  }");

                                        bloc.setUser(t);
                                        await createUser();
                                      } else {
                                        print("validation failed");
                                        _showTopFlash();
                                      }
//                                    _formKey.currentState.save();
//                                    print("5566 ===> ${ 5566 }");
//                                    var a=_formKey.currentState;
//                                    print("_formKey.currentState.value ===> ${_formKey.currentState.value}");
//                                    if (a.validate()) {
//                                      customer.fname = a.value["fname"];
//                                      customer.lname = a.value["lname"];
//                                      customer.mobile = a.value["mobile"];
//                                      customer.password = a.value["password"];
//                                      print("88 ===> ${88}");
//                                      CustomerModel t  = bloc.state.user;
//                                      t.fname =a.value["fname"];
//                                      t.lname =a.value["lname"];
//                                      t.mobile =a.value["mobile"];
//                                      t.password =a.value["password"];
//                                      print("t.account_number ===> ${ t.fname }");
//                                      bloc.setUser(t);
//                                      await createUser();
//                                    } else {
//                                      print("validation failed");
//                                      _showTopFlash(errorText: 'شماره موبایل تکراری است...');
//                                    }
                                    },
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text("ثبت نام",
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                          style: Conf.body2.copyWith(
                                              color: Colors.white,
                                              fontSize: Conf.p2t(19.5),
                                              fontWeight: FontWeight.w600)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: Conf.xlSize),
                                  child: Container(
                                    width: double.infinity,
                                    height: Conf.p2w(15),
                                    child: RaisedButton(
                                      elevation: 0.25,
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          side: BorderSide(
                                            color: Colors.grey[300],
                                          )),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType.leftToRight,
                                                duration: Duration(milliseconds: 500),
                                                alignment: Alignment.centerLeft,
                                                curve: Curves.easeOutCirc,
                                                child: AboutUs()
                                            ));
                                      },
                                      child: FittedBox(
                                        fit: BoxFit.cover,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: Conf.p2w(6.25)),
                                              child: Image.asset(
                                                "assets/images/googlesymbol.png",
                                                width: Conf.p2w(4.167), // 20,
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: Conf.p2w(10.417)),
                                              child: Text("ورود با اکانت گوگل",
                                                  softWrap: true,
                                                  overflow: TextOverflow.fade,
                                                  style: Conf.body1.copyWith(
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.black45,
                                                    fontSize: Conf.p2t(15),
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  );}
                ),
              ),
              loading == false ? Conf.circularProgressIndicator() : Container()
            ],
          ),
        ));
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating,String errorText}) {
    showFlash(
      context: context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Padding(
            padding: EdgeInsets.only(right: Conf.p2w(2)),
            child: Icon(
              FeatherIcons.alertOctagon, color: Colors.white, size: 40,),
          ),
            title: Text('خطا', style: TextStyle(fontSize: Conf.p2t(14),
                fontWeight: FontWeight.w600,
                color: Colors.white),),
            message: Text(errorText,
              style: TextStyle(fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
