import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/NumericKeyboard.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'RegisterNumberLoginPage.dart';
import 'config.dart';

class ForgotPasswordPage2 extends StatefulWidget {
  @override
  _ForgotPasswordPage2State createState() => _ForgotPasswordPage2State();
}

class _ForgotPasswordPage2State extends State<ForgotPasswordPage2> {
  String text,numberPhone="";
  TextEditingController _controller1;
  TextStyle txttStyleBlack = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w900,
    fontSize: Conf.p2t(27),
    decoration: TextDecoration.none,
  );
  TextStyle txttStyleGray = TextStyle(
    color: Colors.black54,
    fontWeight: FontWeight.w700,
    fontSize: Conf.p2t(19),
    decoration: TextDecoration.none,
  );
  TextStyle txtStyleOrange = TextStyle(
    color: Conf.orang,
    fontWeight: FontWeight.w700,
    fontSize:  Conf.p2t(16),
    decoration: TextDecoration.none,
  );
  bool loading=true;
  DoctorBloc get blocDoctor=> context.bloc<DoctorBloc>();
  Future sendData() async {
    setState(() {
      loading = false;
    });
    try {
      var data = await Htify.create("auth/forget", {
          "mobile": blocDoctor.state.doctor.mobile,
      });
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: RegisterNumberLoginPage()
          ));
      DoctorModel d=DoctorModel();
     d.id =data["id"];
      blocDoctor.setId(d.id);
      print("response __id===> ${ d.id.toString() }");
    } catch (e) {
      print("e  sending data to server===> ${e.toString()}");
    } finally {
      setState(() {
      loading = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    text="";
    _controller1 = TextEditingController();
    _controller1.clear();
  }

  void dispose() {
    _controller1.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,bottom: true,
        child: Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            width: double.infinity,
            height:double.infinity,
            color: Conf.blue,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: Conf.xlSize*2).copyWith(top: Conf.xlSize*1.5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:Conf.smEdgeTop0,
                    child: Text(
                      "ادامه دادن", softWrap: true,
                      overflow: TextOverflow.fade,
                      style: Conf.body2.copyWith( color: Colors.white,
                          fontSize: Conf.p2t(15),
                          fontWeight: FontWeight.w900,
                          decoration: TextDecoration.none)
                    ),
                  ),
                  MasBackBotton()
//                  GestureDetector(
//                    onTap: (){Navigator.pop(context);},
//                    child: Container(
//                      width: Conf.p2w(8.3),
//                      height: Conf.p2h(4.5),
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(10),
//                          color: Conf.orang),
//                      child: Icon(
//                        Icons.navigate_next,
//                        size: Conf.p2t(23),
//                        color: Colors.white,
//                      ),
//                    ),
//                  )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: Conf.p2w(0),
            top:  Conf.p2w(6),
            left:  Conf.p2w(0),
            right:  Conf.p2w(0),
            child: Container(
              height: double.infinity,
              margin: Conf.smEdgeTop2*5.787,
//              margin: EdgeInsets.only(top: 150),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30))),
              child: Padding(
                padding:EdgeInsets.only(top: Conf.p2h(4),right: Conf.p2w(5)) ,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "رمز عبور خود را فراموش کرده اید؟", softWrap: true,
                        overflow: TextOverflow.fade,
                        style: Conf.subtitle.copyWith(
                          color: Colors.black87.withOpacity(0.8),
                          fontWeight: FontWeight.w600,
                          fontSize: Conf.p2t(20),
                          decoration: TextDecoration.none)
                      ),
                      Padding(
                        padding: Conf.smEdgeBottomTop2/3,
//                      padding: Conf.smEdgeBottom * 3.5,
                        child: Text(
                            "برای بازیابی رمز عبور شماره موبایل خود را وارد نمائید",
                            softWrap: true,
                            overflow: TextOverflow.fade,
                            style:Conf.subheading.copyWith(
                              color: Colors.black45,
                              fontWeight: FontWeight.w700,
                              fontSize: Conf.p2t(15),
                              decoration: TextDecoration.none,)
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: Conf.p2h(5)),
                        child: SizedBox(
                          width: Conf.p2w(90),
                          height: Conf.p2h(6.5),
                          child: TextFormField(
                            showCursor: true,
                            readOnly: true,
                            textAlign: TextAlign.right,
                            controller: _controller1,
                            decoration: InputDecoration(
                                border: new OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(13.0),
                                  ),
                                  borderSide: BorderSide.none,),
                              contentPadding: EdgeInsets.only(top: Conf.p2h(1.042),left: Conf.p2w(1.042)),
//                              hintText:" 55 - 80 - 800 - 0912+ " ,
                                filled: true,
                                hintStyle: new TextStyle(
                                    fontSize: Conf.p2t(20),
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w900),
                                prefixIcon: Padding(
                                  padding: Conf.smEdgeleftX*2,
                                  child:Padding(
                                    padding: Conf.smEdgeRight1,
                                    child: Icon(
                                        Icons.call,
                                        color: Colors.black87,
                                        size: Conf.p2t(25),
                                      ),
                                  ),
                                ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: Conf.p2w(0),
            left: Conf.p2w(0),
            right: Conf.p2w(0),
            height: Conf.p2h(45),
            child: Container(
              child: Padding(
                padding:EdgeInsets.symmetric(horizontal: Conf.xlSize*2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                                  Directionality(
                      textDirection: TextDirection.ltr,
                      child: Material(
                        color: Colors.transparent,type: MaterialType.button,
                        child: NumericKeyboard(
                          onKeyboardTap: _onKeyboardTap,
                          textColor: Colors.white,
                          rightButtonFn: () {
                            setState(() {
                              if (_controller1.text  != null && _controller1.text .length > 0) {
                                text = _controller1.text .substring(0, _controller1.text .length -1);
                                _controller1.text= text;

                              }return _controller1.text;
//                              if(_controller1.text==""){_controller1.clear();}
//                              if(_controller1.text.length<1){_controller1.text="";}
                            });
                          },
                          rightIcon: Icon(
                            Icons.backspace,
                            color: Colors.white,
                          ),
//                          leftButtonFn: () {
//                            print('left button clicked');
//                          },
//                        leftIcon: Icon(
//                          Icons.check,
//                          color: Colors.white,
//                        ),
                        ),
                      ),
                    ),

                  ],),
              ),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: Conf.p2w(1.042),//5,
                    blurRadius:Conf.p2w(1.459), //7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(42),
                    topRight: Radius.circular(42)),
                color: Conf.blue,
              ),
//              width: double.infinity,
//              height: Conf.p2h(40.7),
            ),
          ),
          Positioned(
            bottom: Conf.p2h(2.5),
            right: Conf.p2w(7),
            left: Conf.p2w(7),
            child:  SizedBox(
                width: Conf.p2w(88),
                height: Conf.p2w(16),
                child:
                MasRaisedButton(
                  margin: EdgeInsets.only(
                      right: Conf.p2w(0),
                      left: Conf.p2w(1),
                      top: Conf.p2w(2),bottom: Conf.p2w(2)),
                  borderSideColor: Conf.lightColor,text: "ادامه",color: Conf.blue,
                  textColor: Colors.white,
                  onPress:(){
                    blocDoctor.setMobile(_controller1.text);
                    print("_controller1.text.length ===> ${ _controller1.text.length }");
                    sendData();
                  } ,)
            ),
          )   ],
      ),
    ));
  }
  _onKeyboardTap(String value) {
    setState(() {
      if(_controller1.text.length>0 && _controller1.text.length<11){
       text= text + value;
       _controller1.text =text;}
      else {
        _controller1.clear();
        text = value;
        _controller1.text = text;
      }
  });
  }
}
