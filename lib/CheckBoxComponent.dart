import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

class CheckBoxComponent {
  bool _isChecked = true;
  String _currText = '';
  List<String> text = ["InduceSmile.com", "Flutter.io", "google.com"];

  @override
  Widget build(BuildContext context) {
    return  Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Text(_currText,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  )),
            ),
          ),
          Expanded(
              child: Container(
                height: 350.0,
                child: Column(
                  children: text
                      .map((t) => CheckboxListTile(
                    title: Text(t),
                    value: _isChecked,
//                    onChanged: (val) {
//                      setState(() {
//                        _isChecked = val;
//                        if (val == true) {
//                          _currText = t;
//                        }
//                      });
//                    },
                  ))
                      .toList(),
                ),
              )),
        ]);
  }
}