import 'dart:ui';
import 'package:counsalter5/CounsalterFish.dart';
import 'package:counsalter5/IncreaseMoneyPriceComponent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:counsalter5/htify.dart';
import 'raised_button_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'config.dart';
import 'package:counsalter5/ModelBloc/WalletModel.dart';
import 'package:flash/flash.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
class Increasemoney extends StatefulWidget {
  TextEditingController controllerPrice =  TextEditingController();
  String text = '',selectedTime,gerogarianDate,subject;
  bool isKeyboardOpen = false;
  Function onTap;
int price;
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(17),
      fontWeight: FontWeight.bold,
      color: Colors.black87);
  TextStyle numberStyle = TextStyle(
      fontSize: Conf.p2t(19),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  List priceArr = [];
  bool loading = false;
  double p=0.0;
  Increasemoney({this.isKeyboardOpen, this.onTap,this.subject,this.price,this.selectedTime,this.gerogarianDate});

  @override
  _IncreasemoneyState createState() => _IncreasemoneyState();
}

class _IncreasemoneyState extends State<Increasemoney> {
  int index;
  WalletBloc get walletBloc => context.bloc<WalletBloc>();
  Future getPriceList() async {
    try {
      setState(() {
        widget.loading = true;
      });
      var data = await Htify.get('cu/transaction/case');
      print(data);
      widget.priceArr = data;
      print("___priceArr ===> ${widget.priceArr.map((e) => e.toString())}");
    } catch (e) {
      print("CATCH ERROR getDoctorList===> ${e.toString()}");
    } finally {
      setState(() {
        widget.loading = false;
      });
    }
  }

  createCart() {
    print("createCart start.....===> ${createCart}");
    return Padding(
      padding: EdgeInsets.only(right: Conf.p2w(4), top: Conf.p2h(2)),
      child: Row(
        children: widget.priceArr
            .map((item) => IncreaseMoneyPriceComponent(
                  isGreyColor: false,
                  containerPrice: item,
                  isSelected:widget.priceArr.indexOf(item) == index ? true :false,
          onTap: (){
            setState(() {
              print("7777 ===> ${ 7777 }");
              index=widget.priceArr.indexOf(item);
              widget.controllerPrice.text=item.toString();
            });}))
            .toList(),
      ),
    );
  }
  int totalPrice(int pricee, controllerPrice){
    int p=pricee+controllerPrice;
    print("totalPrice ===> ${ p}");
    return p;
  }
  @override
  void dispose() {
    widget.controllerPrice.dispose();
    super.dispose();
  }
  @override
  void initState() {
    print("widget.selectedTime ===> ${ widget.selectedTime }");
    print("widget.gerogaridanDate ===> ${ widget.gerogarianDate }");
    print("widget.subject ===> ${ widget.subject }");
    print("widget.price===> ${ widget.price}");
    getPriceList();
    createCart();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child:  BlocBuilder<WalletBloc, WalletRepository>(
        builder: (context, state) {
      return Scaffold(
          body: Stack(
            children: [
              IntrinsicHeight(
                child: Container(
                  height: Conf.p2h(75), //
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding:
                          EdgeInsets.only(right: Conf.p2w(9), top: Conf.p2h(2)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "موجودی فعلی:   ",
                                style: widget.txtStyle,
                              ),
                              Text(
//                                  widget.price.toString(),
                               state.wallet.amount==0||state.wallet.amount==null?"0":state.wallet.amount.toString(),
                                style: widget.numberStyle),Padding(
                                  padding:EdgeInsets.only(right: Conf.p2w(1)),
                                  child: Text("ریال",style: widget.txtStyle),
                                ),
                              Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(0.75)),
                                child: Text(
                                  "  تومان",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: Conf.p2t(15),
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                        ),
                        createCart(),
                        Padding(
                          padding: EdgeInsets.only(top: Conf.p2h(4)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: Conf.p2w(50),
                                child: TextFormField(
                                  controller: widget.controllerPrice,
                                  showCursor: true,
                                  textDirection: TextDirection.ltr,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                      hintStyle: new TextStyle(
                                          height: 0.5,
                                          fontSize: Conf.p2t(14),
                                          color: Colors.black45.withOpacity(0.3),
                                          fontWeight: FontWeight.w500),
                                      hintText: 'مبلغ مورد نظر'),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: Conf.p2w(6), right: Conf.p2w(2)),
                                child: Text(
                                  "تومان",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
//                  width: Conf.p2w(85),
                            height: Conf.p2h(12),
                            margin: EdgeInsets.only(
                                top: Conf.p2h(20), bottom: Conf.p2h(4)),
                            child: MasRaisedButton(
                              margin: EdgeInsets.only(
                                  right: Conf.p2w(6),
                                  left: Conf.p2w(6),
                                  top: Conf.p2w(1.5),
                                  bottom: Conf.p2w(3)),
                              borderSideColor: Conf.orang,
                              text: "تایید",
                              color: Conf.orang,
                              textColor: Colors.white,
                              onPress: () {
                                if( widget.controllerPrice.text==""){_showTopFlash();}
                                walletBloc.setAmount(int.parse(widget.controllerPrice.text));
                                print("state.wallet.amount ===> ${ state.wallet.amount }");
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => (CounsalterFish(gerogarianDate: widget.gerogarianDate,
                                          price:totalPrice(state.wallet.amount,int.parse(widget.controllerPrice.text)),
                                          subject: widget.subject,selectedTime: widget.selectedTime,))));
                              },
                            )
                        ),
                      ],
                    ),
                  ),
//                height: Conf.p2h(13),
                  decoration: BoxDecoration(
                      color: Conf.lightColor,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 8,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(Conf.p2w(6.25)),
                          topLeft: Radius.circular(Conf.p2w(6.25)))),
                ),
              ),
              (widget.loading==true)?Conf.circularProgressIndicator():Container()
            ],
          ),
        );}
      ),
    );
  }
  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context:context,
      duration: const Duration(seconds:5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(icon: Icon(FeatherIcons.alertOctagon,color: Colors.red,size: 50,),
            title: Text('خطا',style: TextStyle(fontSize:Conf.p2t(16),fontWeight: FontWeight.w400,color: Colors.red),),
            message: Text('لطفامبلغ مورد نظر را وارد کنید...',style: TextStyle(fontSize:Conf.p2t(14),fontWeight: FontWeight.w400,color: Colors.black),),
            showProgressIndicator: false,
          ),
        );
      },
    );
  }
}
