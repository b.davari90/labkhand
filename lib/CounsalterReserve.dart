import 'dart:ui';
import 'package:counsalter5/CounsalterReserveDateRowComponent.dart';
import 'package:counsalter5/CounsalterReserveDateRowModel.dart';
import 'package:counsalter5/CoustomerPageForm.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shamsi_date/shamsi_date.dart';
import 'package:counsalter5/DoctorPresentsModel.dart';
import 'config.dart';
import 'package:persian_date/persian_date.dart';
import 'package:counsalter5/CustomerModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/config.dart';
import 'package:flash/flash.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/rendering.dart';
import 'raised_button_ui.dart';
import 'package:counsalter5/ModelBloc/DoctorPresentListModel.dart';

class CounsalterReserve extends StatefulWidget {
  int id;
  List<int> priceLst;
  List<int> typeLst;
  int price, type;

  CounsalterReserve(
      {this.id, this.priceLst, this.typeLst, this.price, this.type});

  @override
  _CounsalterReserveState createState() => _CounsalterReserveState();
}

class _CounsalterReserveState extends State<CounsalterReserve> {
  CustomerModel customerModel;
  int sumPriceList = 0;

  CoustomerBloc get coustomerBloc => context.bloc<CoustomerBloc>();
  TextStyle priceStyle =
      TextStyle(fontSize: Conf.p2t(22), fontWeight: FontWeight.w600);
  CounsalterReserveDateRowModel a = CounsalterReserveDateRowModel();
  DoctorPresentsModel doctorPresentModel = DoctorPresentsModel();
  TextStyle priceUnitStyle = TextStyle(
      fontSize: Conf.p2t(14),
      color: Colors.black87,
      fontWeight: FontWeight.w700);
  TextStyle txtStyle = TextStyle(
      fontSize: Conf.p2t(15),
      color: Colors.black87,
      fontWeight: FontWeight.w500);
  bool isSwitched = false;
  bool isChecked = false;
  bool loading = false, isChoose = false;
  List times;
  int pages, count, limit;
  String selectedTime;
  String noInformation = "متاسفانه اطلاعاتی ثبت نشده است";

  int index;
  List<DoctorPresentsModel> presentsList;
  DoctorPresentListModel doctorPresentListModel;
  List<DoctorPresentsModel> doctorPresets;
  List timesFree;
  String finalDate = "";
  String monthChoosen, dateChoosen, dayChoosen, yearChoosen = "1400";
  String finalDay;
  Jalali j1;
  List times_Serever = [], slots_Server = [];
  List types, times_free;
  List<int> idDate;
  String final_Date_Shamsi, date_Shamsi, final_Date_Shamsi_Month;

  DoctorPresentsBloc get doctorPresentBloc =>
      context.bloc<DoctorPresentsBloc>();
  DoctorModel s = DoctorModel();

  List dayOfMonthList() {
    if (indexMonth == 0 ||
        indexMonth == 1 ||
        indexMonth == 2 ||
        indexMonth == 3 ||
        indexMonth == 4 ||
        indexMonth == 5) {
      return firstSixMonth;
    }
    if (indexMonth == 6 ||
        indexMonth == 7 ||
        indexMonth == 8 ||
        indexMonth == 9 ||
        indexMonth == 10) {
      return secondSixMonth;
    } else {
      return esfandMonth;
    }
  }

  Future sendDate() async {
    dateChoosen = yearChoosen +
        convertStringMonthTOInteger(monthName).toString() +
        changeDayFormate(dayChoosen);
    print("__dateChoosen ===> ${dateChoosen}");
    DateTime dt = DateTime(
        int.parse(yearChoosen),
        int.parse(convertStringMonthTOInteger(monthName).toString()),
        int.parse(changeDayFormate(dayChoosen)));
    PersianDate persianDate = PersianDate(gregorian: dt.toString());
    finalDate = persianDate.jalaliToGregorian(dt.toString()).toString();

    Gregorian g1 = Gregorian(
        int.parse(finalDate.substring(0, 4)),
        //بدست اورردن روز شنبه از تاریخ میلادی
        int.parse(finalDate.substring(5, 7)),
        int.parse(finalDate.substring(8, 10)));
    j1 = g1.toJalali();
    print("format1(j1) ===> ${format1(j1)}");

    finalDate = finalDate.split(" ").first;
    print("finalDate.split(" ").first ===> ${finalDate.split(" ").first}");
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.create("cu/present/list",
          {"date": finalDate, "doctor_id": Conf.idLoginDoctor});

      doctorPresentListModel = DoctorPresentListModel.fromJson(data);
      print("doctorPresentListModel ===> ${data}");
//        count = doctorPresentListModel.count;
//        limit = doctorPresentListModel.limit;
//        pages = doctorPresentListModel.page;
      doctorPresets = [];
      times_free = [];
      doctorPresets.addAll(doctorPresentListModel.rows);
      times_free.addAll(doctorPresets.map((e) => e.times_free));
      print("times_free ===> ${times_free.map((e) => e)}");
      print("doctorPresets ===> ${doctorPresets.map((e) => e.times_free)}");
      print("doctorPresentListModel date===> ${doctorPresentListModel.date}");
    } catch (e) {
      print("CatchError sendData SelectCschaduleDoctor()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

//  Future sendData() async {
//    dateChoosen = yearChoosen +
//        convertStringMonthTOInteger(monthName).toString() +
//        changeDayFormate(dayChoosen);
//    print("__dateChoosen ===> ${dateChoosen}");
//    DateTime dt = new DateTime(
//        int.parse(yearChoosen),
//        int.parse(convertStringMonthTOInteger(monthName).toString()),
//        int.parse(changeDayFormate(dayChoosen)));
//    PersianDate persianDate = PersianDate(gregorian: dt.toString());
//    finalDate = persianDate.jalaliToGregorian(dt.toString()).toString();
//    finalDate = finalDate
//        .split(" ")
//        .first;
//    print("finalDate.split(" ").first ===> ${finalDate
//        .split(" ")
//        .first}");
//    try {
//      setState(() {
//        loading = true;
//      });
//
//      var data = await Htify.create('cu/present/list',{"date": finalDate,"doctor_id":Conf.idLoginDoctor});
//      setState(() {
//        timesFree =data["times_free"];
//        print("__timesFree ===> ${ timesFree }");
//      });
//    } catch (e) {
//      print("CATCH ERROR getTimes()===> ${e.toString()}");
//    } finally {
//      setState(() {
//        loading = false;
//      });
//    }
//  }
  int indexSelectdate;
  List selectMonth = [];
  List<String> monthList = [
    "فروردین",
    "اردیبهشت",
    "خرداد",
    "تیر",
    "مرداد",
    "شهریور",
    "مهر",
    "آبان",
    "اذر",
    "دی",
    "بهمن",
    "اسفند"
  ];
  String monthName = "";
  int indexMonth = 0;
  List daysList = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ];

  List firstSixMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ];
  List secondSixMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30
  ];
  List esfandMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29
  ];

  List dayShow() {
    if (indexMonth == 0 ||
        indexMonth == 1 ||
        indexMonth == 2 ||
        indexMonth == 3 ||
        indexMonth == 4 ||
        indexMonth == 5) {
      return firstSixMonth;
    }
    if (indexMonth == 6 ||
        indexMonth == 7 ||
        indexMonth == 8 ||
        indexMonth == 9 ||
        indexMonth == 10) {
      return secondSixMonth;
    } else {
      return esfandMonth;
    }
  }

  String format1(Date d) {
    final f = d.formatter;
    print("5000 ===> ${5000}");
    return '${f.wN} ${f.d} ${f.mN} ${f.yy}';
  }

  @override
  void initState() {
    Gregorian g = Gregorian(Gregorian.now().year, Gregorian.now().month, Gregorian.now().day);
    Jalali jj=g.toJalali();
    print("jj ===> ${ jj }");
    int m=jj.month;
    print("monthName initState===> ${ m }");
    print("widget.price ===> ${widget.price}");
    print("widget.type ===> ${widget.type}");
    print("Conf.idLoginDoctor ===> ${Conf.idLoginDoctor}");
    print("Conf.idLogin ===> ${Conf.idLogin}");
    print("Conf.token ===> ${Conf.token}");
    print("widget.priceLst ===> ${widget.priceLst}");
    print("widget.typeLst ===> ${widget.typeLst}");

    times_free = [];
    doctorPresets = [];
    types = [];
    idDate = [];
    doctorPresentListModel = DoctorPresentListModel();
    customerModel = CustomerModel();
    times = [];
    presentsList = [];
    monthName = monthList[m-1];
    selectMonth = firstSixMonth;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle titleStyle = Conf.title.copyWith(
        fontSize: Conf.p2t(25),
        fontWeight: FontWeight.w700,
        color: Colors.black);
    TextStyle subTitleStyle = Conf.title.copyWith(
        fontSize: Conf.p2t(14),
        fontWeight: FontWeight.w700,
        color: Colors.black87);
    return SafeArea(
      bottom: true,
      top: true,
      child: Stack(
        children: [
          Scaffold(
            body: BlocBuilder<DoctorBloc, DoctorRepository>(
                builder: (context, state) {
              return Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: Conf.p2w(5), top: Conf.p2h(4)),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: Conf.p2w(11),
                            height: Conf.p2w(11),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Conf.orang),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.navigate_next,
                                size: Conf.p2t(23),
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: Conf.p2w(5)),
                            child: Container(
//                        color: /d,
                              width: Conf.p2w(15),
                              height: Conf.p2w(15),
//                        height: Conf.p2w(9.55),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(43.0),
                                child: FadeInImage.assetNetwork(
                                  placeholder: "assets/images/prof1.jpg",
                                  image:
                                      "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: Conf.p2w(5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(children: [
                                  Text(
                                    state.doctor.fname ?? "",
//                                state.doctor.name??"",
                                    style: titleStyle,
                                  ),
                                  Text(" "),
                                  Text(
                                    state.doctor.lname ?? "",
                                    style: titleStyle,
                                  ),
                                ]),
                                Row(
                                  children: [
                                    Text(
                                      "متخصص",
                                      style: subTitleStyle,
                                    ),
                                    Text("  "),
                                    Text(
                                      state.doctor.specialty ?? "",
                                      style: subTitleStyle,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      BlocBuilder<DoctorBloc, DoctorRepository>(
                          builder: (context, state) {
                        return Padding(
                          padding: EdgeInsets.only(top: Conf.p2h(2)),
                          child: Container(
                            height: Conf.p2h(70),
                            decoration: BoxDecoration(
                                color: Colors.white10.withOpacity(0.7),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.3),
                                    spreadRadius: 2,
                                    blurRadius: 8,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(Conf.p2w(6.25)),
                                    topLeft: Radius.circular(Conf.p2w(6.25)))),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.navigate_before),
                                        onPressed: () {
                                          setState(() {
                                            indexMonth--;
                                            print(
                                                "before indexMonth ===> ${indexMonth}");
                                            if (indexMonth == -1) {
                                              indexMonth = 11;
                                            }
                                            monthName =
                                                monthList.elementAt(indexMonth);
                                          });
                                        }),
                                    Text(
                                      monthName,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: Conf.p2t(24)),
                                    ),
                                    IconButton(
                                        icon: Icon(Icons.navigate_next),
                                        onPressed: () {
                                          setState(() {
                                            indexMonth++;
                                            print(
                                                "after indexMonth ===> ${indexMonth}");
                                            if (indexMonth == 12) {
                                              indexMonth = 0;
                                            }
                                            monthName =
                                                monthList.elementAt(indexMonth);
                                          });
                                        })
                                  ],
                                ),
                                Flex(
                                  direction: Axis.vertical,
                                  children: [
                                    SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Padding(
                                          padding:
                                              EdgeInsets.only(top: Conf.p2h(1)),
                                          child: Row(
                                              children: dayShow()
                                                  .map((e) =>
                                                      CounsalterReserveDateRowComponent(
                                                          model: CounsalterReserveDateRowModel(
                                                              numberDate:
                                                                  e.toString()),
                                                          isSelectedRow: dayOfMonthList()
                                                                      .indexOf(
                                                                          e) ==
                                                                  indexSelectdate
                                                              ? true
                                                              : false,
                                                          onTap: () {
                                                            setState(() {
                                                              dayChoosen =
                                                                  e.toString();
                                                              print(
                                                                  "dayChoosen ===> ${dayChoosen}");
                                                              indexSelectdate =
                                                                  dayOfMonthList()
                                                                      .indexOf(
                                                                          e);
                                                              dayChoosen.length ==
                                                                      1
                                                                  ? changeDayFormate(
                                                                      dayChoosen)
                                                                  : dayChoosen
                                                                      .toString();
//                                                          getTimes();
                                                              sendDate();
                                                            });
                                                          }))
                                                  .toList()),
                                        )),
                                  ],
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: Conf.p2w(4), bottom: Conf.p2w(2)),
                                    child:
                                        doctorPresets == null ||
                                                doctorPresets.isEmpty ||
                                                doctorPresets == []
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: Conf.p2w(5)),
                                                child: Text(
                                                  noInformation,
                                                  style: txtStyle,
                                                ),
                                              )
                                            : SingleChildScrollView(
                                                scrollDirection: Axis.vertical,
                                                child:
//                                      Text("")
                                                    Padding(
                                                  padding: EdgeInsets.only(
                                                      top: Conf.p2w(4),
                                                      bottom: Conf.p2w(2)),
                                                  child: Wrap(
                                                    runAlignment: WrapAlignment
                                                        .spaceBetween,
                                                    alignment:
                                                        WrapAlignment.start,
                                                    crossAxisAlignment:
                                                        WrapCrossAlignment.end,
                                                    textDirection:
                                                        TextDirection.ltr,
                                                    runSpacing: 15,
                                                    spacing: Conf.p2w(5),
                                                    children:
                                                        times_free.map((e) {
                                                      return GestureDetector(
                                                          onTap: () {
                                                            setState(() {
                                                              index = times_free
                                                                  .indexOf(e);
                                                              if (isChoose ==
                                                                  false) {
                                                                isChoose = true;
                                                                selectedTime = e
                                                                    .toString()
                                                                    .substring(
                                                                        1, 6);
                                                                print(
                                                                    "selectedTime ===> ${selectedTime}");
                                                                print(
                                                                    "isChoose ===> ${isChoose}");
                                                              } else {
                                                                isChoose =
                                                                    false;
                                                                print(
                                                                    "isChoose ===> ${isChoose}");
                                                                selectedTime =
                                                                    "";
                                                                print(
                                                                    "selectedTime ===> ${selectedTime}");
                                                                print(
                                                                    "isChoose ===> ${isChoose}");
                                                              }
                                                            });
                                                          },
                                                          child: Container(
                                                              child: Center(
                                                                  child:
                                                                      doctorPresets == null ||
                                                                              doctorPresets ==
                                                                                  [] ||
                                                                              doctorPresets
                                                                                  .isEmpty
                                                                          ? Container(
                                                                              margin: EdgeInsets.only(top: Conf.p2w(10)),
                                                                              child: Text(
                                                                                noInformation,
                                                                                style: txtStyle,
                                                                              ),
                                                                            )
                                                                          : Text(
                                                                              e.toString().substring(1, 6),
                                                                              style: TextStyle(color: index == times_free.indexOf(e) && isChoose ? Colors.black87 : Colors.white, fontSize: Conf.p2t(20), fontWeight: FontWeight.bold),
                                                                            )),
                                                              margin: EdgeInsets.only(
                                                                  top: Conf.p2h(
                                                                      0.25),
                                                                  left: Conf.p2w(
                                                                      0.25),
                                                                  right: Conf.p2w(
                                                                      0.25)),
                                                              width:
                                                                  Conf.p2w(27),
                                                              height:
                                                                  Conf.p2h(8),
                                                              decoration:
                                                                  BoxDecoration(
                                                                      color: index == times_free.indexOf(e) && isChoose
                                                                          ? Colors
                                                                              .white
                                                                          : Conf
                                                                              .blue,
                                                                      border: Border.all(
                                                                          color: Conf
                                                                              .transparentColor),
                                                                      boxShadow: [
                                                                        BoxShadow(
                                                                            color:
                                                                                Colors.grey.withOpacity(0.5),
                                                                            blurRadius: 4)
                                                                      ],
                                                                      borderRadius: BorderRadius.all(Radius.circular(Conf.p2w(2.71))))));
                                                    }).toList(),
                                                  ),
                                                ),
                                              ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Switch(
                                        activeColor: Conf.blue,
                                        inactiveTrackColor:
                                            Colors.grey.withOpacity(0.1),
                                        inactiveThumbColor: Conf.blue,
                                        value: isSwitched,
                                        onChanged: (value) {
                                          setState(() {
                                            isSwitched = value;
                                            print(isSwitched);
                                          });
                                        }),
                                    Text(
                                      "مشاوره به صورت خصوصی انجام شود",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: Conf.p2t(15),
                                      ),
                                    )
                                  ],
                                ),
                                FittedBox(
                                  fit: BoxFit.cover,
                                  child: Container(
                                    margin: EdgeInsets.only(left: Conf.p2w(17)),
                                    child: Row(
                                      children: [
                                        Checkbox(
                                            value: isChecked,
                                            activeColor: Conf.orang,
                                            onChanged: (bool newValue) {
                                              setState(() {
                                                isChecked = newValue;
                                              });
                                            }),
                                        Text(
                                          "شرایط خدمات و سیاست حفظ حریم خصوصی بهداشت و درمان را بپذیرید",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              color: Conf.blue,
                                              fontSize: Conf.p2t(14)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(bottom: Conf.xxlSize),
                                  child:
                                      BlocBuilder<DoctorBloc, DoctorRepository>(
                                    builder: (context, state) {
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "قیمت نهایی   ",
                                            style: priceStyle,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: Conf.p2h(0.5)),
                                            child: Text(
                                              state.priceDrType.toString()??"0",
                                              style: TextStyle(
                                                fontSize: Conf.p2t(24),
                                                fontWeight: FontWeight.bold,
                                                color: Colors.indigo,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: Conf.p2h(1)),
                                            child: Text("  تومان",
                                                style: priceUnitStyle),
                                          )
                                        ],
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      })
                    ],
                  ));
            }),
            bottomSheet: Padding(
              padding: EdgeInsets.only(bottom: Conf.p2w(1.5)),
              child: FittedBox(
                fit: BoxFit.cover,
                child: Flex(
                  children: [
                    Container(
                      width: Conf.p2w(100),
                      height: Conf.p2h(9),
                      color: Colors.white,
                      child: Container(
//                    margin: EdgeInsets.only(bottom: Conf.p2h(1)),
                          width: Conf.p2w(100),
                          height: Conf.p2w(18),
//                      color: Colors.red,
                          padding: EdgeInsets.only(
                              right: Conf.p2w(2.0839), left: Conf.p2w(2.0839)),
                          child: MasRaisedButton(
                            margin: EdgeInsets.only(
                                right: Conf.p2w(0),
                                left: Conf.p2w(0.5),
                          top: Conf.p2w(0.75),
                                bottom: Conf.p2w(1)),
                            borderSideColor: Conf.blue,
                            text: "ثبت و ادامه",
                            color: Conf.blue,
                            textColor: Colors.white,
                            onPress: () {
                              print("isChecked ===> ${ isChecked }");
                              selectedTime == "" || index == null ||
                                  doctorPresets == null ||
                                  doctorPresets == [] ||
                                  doctorPresets.isEmpty || isChecked == false
                                  ? _showTopFlash(context)
                                  : Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.leftToRight,
                                      duration: Duration(milliseconds: 500),
                                      alignment: Alignment.centerLeft,
                                      curve: Curves.easeOutCirc,
                                      child: CoustomerPageForm(
                                        selectedTime: selectedTime,
                                        shamsiDate: format1(j1),
                                        id: Conf.idLogin,
                                        idDr: Conf.idLoginDoctor,
                                        price: widget.price,
                                        type: widget.type,
                                        gerogarianDate: finalDate,
                                        isPrivate: isSwitched,
                                        selectedDay: int.parse(dayChoosen),
                                        selectedMonth: monthName,
                                      )));
                            })                          ),
                    )
                  ],
                  direction: Axis.vertical,
                ),
              ),
            ),
          ),
          loading == true ? Conf.circularProgressIndicator() : Container()
        ],
      ),
    );
  }
}

String changeDayFormate(String str) {
  str = "0" + str;
  return str;
}

String format1(Date d) {
  final f = d.formatter;
  return '${f.wN} ${f.d} ${f.mN} ${f.yy}';
}

void _showTopFlash(BuildContext cnt, {FlashStyle style = FlashStyle.floating}) {
  showFlash(
    context: cnt,
    duration: const Duration(seconds: 5),
    persistent: true,
    builder: (_, controller) {
      return Flash(
        controller: controller,
        backgroundColor: Colors.red,
        brightness: Brightness.light,
        boxShadows: [BoxShadow(blurRadius: 4)],
        barrierBlur: 3.0,
        barrierColor: Colors.black38,
        barrierDismissible: true,
        style: style,
        position: FlashPosition.top,
        child: FlashBar(
          icon: Padding(
            padding: EdgeInsets.only(right: Conf.p2w(2)),
            child: Icon(
              FeatherIcons.alertOctagon,
              color: Colors.white,
              size: 40,
            ),
          ),
          title: Text(
            'خطا',
            style: TextStyle(
                fontSize: Conf.p2t(14),
                fontWeight: FontWeight.w600,
                color: Colors.white),
          ),
          message: Text(
            ' ساعت مورد نظر جهت مشاوره \n  پذیرفتن شرایط و خدمات حفظ حریم خصوصی',
            style: TextStyle(
                fontSize: Conf.p2t(12),
                fontWeight: FontWeight.w600,
                color: Colors.white),
          ),
          showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
        ),
      );
    },
  );
}

int convertStringMonthTOInteger(String m) {
  switch (m) {
    case "فروردین":
      return 1;
      break;
    case "اردیبهشت":
      return 2;
      break;
    case "خرداد":
      return 3;
      break;
    case "تیر":
      return 4;
      break;
    case "مرداد":
      return 5;
      break;
    case "شهریور":
      return 6;
      break;
    case "مهر":
      return 7;
      break;
    case "آبان":
      return 8;
      break;
    case "آذر":
      return 9;
      break;
    case "دی":
      return 10;
      break;
    case "بهمن":
      return 11;
      break;
    case "اسفند":
      return 12;
      break;
  }
}

Widget timeFun({String time, Function onTap, bool isActive}) {
  return GestureDetector(
    onTap: () {},
    child: Padding(
      padding: EdgeInsets.only(top: Conf.p2h(2)),
      child: Container(
          child: Center(
              child: Text(
            time,
            style: TextStyle(
                color: isActive ? Colors.black87 : Colors.white,
                fontSize: Conf.p2t(20),
                fontWeight: FontWeight.bold),
          )),
          margin: EdgeInsets.only(
              top: Conf.p2h(0.25),
              left: Conf.p2w(2.918),
              right: Conf.p2w(2.918)),
          width: Conf.p2w(27),
          height: Conf.p2h(5),
          decoration: BoxDecoration(
              color: isActive ? Colors.white : Conf.blue,
              border: Border.all(color: Conf.transparentColor),
//          border: Border.all(color: Colors.grey.withOpacity(0.1)),
              boxShadow: [
                BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 4)
              ],
              borderRadius: BorderRadius.all(Radius.circular(Conf.p2w(2.71))
//          borderRadius: BorderRadius.all(Radius.circular(13)
                  ))),
    ),
  );
}
