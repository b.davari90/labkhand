import 'RegisterNumberLoginPage.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:counsalter5/RegisterPageDoctor.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:page_transition/page_transition.dart';
import 'RegisterPage.dart';
import 'CustomerModel.dart';
import 'config.dart';
import 'htify.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _controller1, _controller2;
  CustomerModel userLogin;
  bool checkValue;
  bool _obscureText = true, loading = true;
  bool _passwordVisible = false;
  final _formKey = GlobalKey<FormBuilderState>();
  int id;

  void _toggle() {
    setState(() {
      _passwordVisible = !_passwordVisible;
    });
  }

  TextStyle txttStyleBlack = TextStyle(
    color: Colors.black87,
    fontWeight: FontWeight.w900,
    fontSize: 28,
    decoration: TextDecoration.none,
  );
  TextStyle txttStyleGray = TextStyle(
    color: Colors.black45,
    fontWeight: FontWeight.w900,
    fontSize: 18,
    decoration: TextDecoration.none,
  );
  TextStyle txtStyleOrange = TextStyle(
    color: Colors.orange,
    fontWeight: FontWeight.w700,
    fontSize: 15,
    decoration: TextDecoration.none,
  );

  Future setLogin() async {
    setState(() {
      loading = false;
    });
    try {
      print("userLogin.mobile ===> ${userLogin.mobile}");
      var data = await Htify.create(
          "auth/login", {"username": userLogin.mobile, "provider": "customer"});
      var x = data["id"];
      Conf.idLogin = x;
      print("Conf.idLogin ===> ${Conf.idLogin}");
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.leftToRight,
              duration: Duration(milliseconds: 500),
              alignment: Alignment.centerLeft,
              curve: Curves.easeOutCirc,
              child: RegisterNumberLoginPage()));
    } catch (e) {
      print("catch Error ===> ${e.toString()}");
      _showTopFlash();
    } finally {
      setState(() {
        loading = true;
      });
    }
  }

  @override
  void initState() {
    _controller1 = TextEditingController();
    _controller2 = TextEditingController();
    userLogin = CustomerModel();
    super.initState();
  }

  void dispose() {
    _controller1.dispose();
    _controller2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        bottom: true,
        child: Scaffold(
          body: Stack(
//            fit: StackFit.expand,
            children: <Widget>[
              Positioned(
                width: Conf.p2w(100),
                top: Conf.p2h(0),
                bottom: Conf.p2h(2),
                child: Container(
                  width: Conf.p2w(20),
                  color: Conf.blue,
                ),
              ),
              Positioned(
                top: Conf.p2h(4),
                width: Conf.p2w(100),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("ورود  | ثبت نام",
//                        softWrap: true,
//                        overflow: TextOverflow.fade,
                        style: Conf.body2.copyWith(
                            color: Colors.white.withOpacity(0.9),
                            fontSize: Conf.p2t(20),
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.none)),
                  ],
                ),
              ),
              Positioned(
                top: Conf.p2h(4),
                left: Conf.p2w(6),
                width: Conf.p2w(100),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [MasBackBotton()],
                ),
              ),
              Positioned(
                bottom: Conf.p2w(0),
                top: Conf.p2w(0),
                left: Conf.p2w(0),
                right: Conf.p2w(0),
                child: Container(
                  height: double.infinity,
                  margin: Conf.smEdgeTop2 * 5.787,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Padding(
                    padding: EdgeInsets.zero,
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      child: SingleChildScrollView(
                        padding: Conf.xlEdge,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerRight,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text(
                                      "خوش آمدید",
                                      style: Conf.body1.copyWith(
                                        color: Colors.black87.withOpacity(0.7),
                                        fontWeight: FontWeight.w700,
                                        fontSize: Conf.p2t(23),
                                        decoration: TextDecoration.none,
                                      ),
                                      softWrap: true,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(bottom: Conf.xlSize),
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text("اطلاعات خود را وارد نمائید",
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                          style: Conf.body1.copyWith(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.w600,
                                            fontSize: Conf.p2t(17),
                                            decoration: TextDecoration.none,
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            FormBuilder(
                              key: _formKey,
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              child: Column(
                                children: [
                                  Conf.textFieldFormBuilder(
                                      'تلفن همراه خود را وارد نمائید...',
                                      "mobile",
                                      () {},
                                      true,
                                      Icons.call,
                                      null,
                                      [
                                        FormBuilderValidators.required(
                                            errorText: "*الزامی"),
                                        FormBuilderValidators.numeric(
                                            errorText: "عدد وارد کنید"),
                                        FormBuilderValidators.minLength(11,
                                            errorText:
                                                "*تلفن همراه خود را به صورت 11 رقمی وارد کنید"),
                                        FormBuilderValidators.maxLength(11,
                                            errorText:
                                                "* همراه خود را به صورت 11 رقمی وارد کنید"),
                                      ],
                                      TextInputType.number,
                                      true,
                                      hasPadding: true),
//                                  Conf.textFieldFormBuilder(
//                                      "رمز عبور خود را وارد نمائید...",
//                                      "password", () {},
//                                      true,
//                                      Icons.vpn_key,
//                                      null,
//                                      [
//                                        FormBuilderValidators.required(
//                                            errorText: "*الزامی"),
//                                      ],
//                                      ],
//                                      TextInputType.text,
//                                      true,
//                                      hasPadding: true,onPoresedSuffixIcon:  _toggle,),
//                                  Padding(
//                                    padding: EdgeInsets.only(top: Conf.xlSize),
//                                    child:
//                                    FormBuilderTextField(
//                                      attribute: "password",
//                                      obscureText: !_passwordVisible,
//                                      keyboardType: TextInputType.text,
//                                      textAlign: TextAlign.right,
//                                      decoration: InputDecoration(
//                                        fillColor: Colors.grey.withOpacity(0.5),
//                                        enabledBorder: OutlineInputBorder(
//                                            borderRadius: BorderRadius.all(
//                                              Radius.circular(13),
//                                            ),
//                                            borderSide: BorderSide(
//                                                color: Colors.grey
//                                                    .withOpacity(0.4))),
//                                        border: new OutlineInputBorder(
//                                          borderRadius: const BorderRadius.all(
//                                            const Radius.circular(18),
//                                          ),
//                                          borderSide:
//                                          BorderSide(color: Conf.orang),
////                                            gapPadding: 10
////                                        borderSide: BorderSide.none,
//                                        ),
//                                        suffixIcon: Padding(
//                                            padding: EdgeInsets.only(
//                                                left: Conf.p2w(5)),
//                                            child: IconButton(
//                                              icon: Icon(_passwordVisible
//                                                  ? FeatherIcons.eye
//                                                  : FeatherIcons.eyeOff),
//                                              onPressed: _toggle,
//                                              color:
//                                              Colors.grey.withOpacity(0.6),
//                                              iconSize: Conf.p2t(18),
//                                            )),
//                                          labelText: 'رمز عبور خود را وارد نمائید...',labelStyle:  TextStyle(
//                                          height: 0.5,
//                                          fontSize: Conf.p2t(15),
//                                          color:
//                                          Colors.black45.withOpacity(0.4),
//                                          fontWeight: FontWeight.w900),
//                                        prefixIcon: Icon(
//                                          Icons.vpn_key,
//                                          color: Colors.black87,
//                                          size: Conf.p2t(23),
//                                        ),
//                                      ),
//                                      validators: [
//                                        FormBuilderValidators.required(
//                                            errorText: "*الزامی"),
////                                        FormBuilderValidators.minLength(5,
////                                            errorText:
////                                                "*حداقل رمز عبور  5 رقم میباشد"),
//                                      ],
////                                        validator:  FormBuilderValidators.compose([
////                                          FormBuilderValidators.required(context,errorText: "*الزامی"),
////                                          FormBuilderValidators.minLength(context,5,errorText: "*حداقل رمز عبور  5 رقم میباشد"),
////                                        ]),
//
////                                TextFormField(
////                                  keyboardType: TextInputType.text,
////                                  validator: (val) => val.length < 6
////                                      ? 'حداقل تعداد کارکترهای رمز عبور شش رقم می باشد'
////                                      : null,
////                                  onSaved: (val) => _password = val,
////                                  obscureText: true,
////                                  textAlign: TextAlign.right,
////                                  controller: _controller2,
////                                  decoration: InputDecoration(
////                                      border: new OutlineInputBorder(
////                                        borderRadius: const BorderRadius.all(
////                                          const Radius.circular(18),
////                                        ),
////                                        borderSide: BorderSide.none,
////                                      ),
////                                      filled: true,
////                                      hintStyle: new TextStyle(
////                                          height: 0.5,
////                                          fontSize: Conf.p2t(15),
////                                          color:
////                                          Colors.black45.withOpacity(0.4),
////                                          fontWeight: FontWeight.w900),
////                                      suffixIcon: Padding(
////                                          padding: Conf.smEdgeleft2 * 2,
////                                          child: IconButton(
////                                            icon: Icon(FeatherIcons.eyeOff),
////                                            onPressed: _toggle,
////                                            color: Colors.grey.withOpacity(0.6),
////                                            iconSize: Conf.p2t(18),
////                                          )
////                                        //FlatButton(
//////                                 onPressed: _toggle,
//////                                 child: new Text(_obscureText ? "Show" : "Hide"))
////
////                                      ),
////                                      prefixIcon: Icon(
////                                        Icons.vpn_key,
////                                        color: Colors.black87,
////                                        size: Conf.p2t(23),
////                                      ),
//////                          fillColor: Colors.white70,
////                                      hintText: 'رمز عبور خود را وارد نمائید'),
////                                ),
//                                    ),
//                                  )
                                ],
                              ),
                            ),

                            Padding(
                              padding: EdgeInsets.only(top: Conf.p2w(3)),
                              child: Container(
                                  width: double.infinity,
                                  height: Conf.p2w(15),
//                              padding: Conf.xxlEdge,
                                  child: RaisedButton(
                                    color: Conf.blue,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(13.0),
//                          side: BorderSide(color: Conf.blue)
                                    ),
                                    child: Text('ورود',
                                        style: Conf.subtitle.copyWith(
                                            fontSize: Conf.p2t(20),
                                            fontWeight: FontWeight.w700,
                                            color: Conf.lightColor)),
                                    onPressed: () async {
                                      print(
                                          "userLogin.mobile ===> ${userLogin.mobile} }");
                                      print(
                                          "userLogin.pasword ===> ${userLogin.password} }");
                                      _formKey.currentState.save();
                                      print(
                                          "_formKey.currentState.value ===> ${_formKey.currentState.value}");
                                      if (_formKey.currentState.validate()) {
                                        userLogin.mobile = _formKey
                                            .currentState.value["mobile"];
//                                        userLogin.password = _formKey.currentState.value["password"];
                                        await setLogin();
                                      } else {
                                        print("validation failed");
                                      }
                                    },
                                  )),
                            ),
//                            Padding(
//                              padding: EdgeInsets.only(top: Conf.p2w(1)),
//                              child: FittedBox(
//                                fit: BoxFit.cover,
//                                child: Padding(
//                                  padding: EdgeInsets.only(
//                                      right: Conf.p2w(27),
//                                      left: Conf.p2w(27),
//                                      top: Conf.p2h(2.5)),
//                                  child: GestureDetector(
//                                    onTap: () {
//                                      Navigator.push(context, PageTransition(
//                                          type: PageTransitionType.rightToLeft,
//                                          child: ForgotPasswordPage1()));
//                                    },
//                                    child: Text(
//                                        " رمز عبور خود را فراموش کرده ام",
////                            "                                                                  رمز عبور خود را فراموش کرده ام",
//                                        softWrap: true,
//                                        overflow: TextOverflow.fade,
//                                        style: Conf.subtitle.copyWith(
//                                          color: Conf.orang,
//                                          fontWeight: FontWeight.w700,
//                                          fontSize: Conf.p2t(15),
//                                          decoration: TextDecoration.none,
//                                        )
////                        txtStyleOrange
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ),
                            Padding(
                              padding: EdgeInsets.only(top: Conf.p2w(3.125)),
                              child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                        child: Divider(
                                            color: Conf.orang,
                                            height: Conf.p2w(12.705), //60,
                                            indent: Conf.p2w(3),
                                            endIndent: Conf.p2w(4))),
                                    FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text("ویا         ",
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                          style: Conf.body1.copyWith(
                                            color: Conf.orang,
                                            fontWeight: FontWeight.w900,
                                            fontSize: Conf.p2t(17),
                                            decoration: TextDecoration.none,
                                          )),
                                    ),
                                    Expanded(
                                        child: Divider(
                                            color: Conf.orang,
                                            height: Conf.p2w(12.705), //60,
                                            endIndent: Conf.p2w(8.5))),
                                  ]),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: Conf.p2w(0.40), bottom: Conf.p2w(0.418)),
//                          padding: Conf.smEdgeBottomTop1,
                              child: Padding(
                                padding: EdgeInsets.only(top: Conf.p2h(1)),
//                          padding: Conf.smEdgeTop1 * 2,
                                child: Container(
                                  width: double.infinity,
                                  height: Conf.p2w(15),
                                  child: RaisedButton(
                                    elevation: 0.5,
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        side: BorderSide(
                                          color: Colors.grey[300],
                                        )),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              child: RegisterPage()));
                                    },
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text("تا به حال ثبت نام نکرده ام",
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                          style: Conf.body1.copyWith(
                                            fontWeight: FontWeight.w800,
                                            color: Colors.black45,
                                            fontSize: Conf.p2t(15),
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                            ),
//                  Positioned(bottom: 20,left: 10,
//                    child:
                            Padding(
                              padding: EdgeInsets.only(top: Conf.xlSize),
                              child: Container(
                                width: double.infinity,
                                height: Conf.p2w(15),
                                child: RaisedButton(
                                  elevation: 0.5,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      side: BorderSide(
                                        color: Colors.grey[300],
                                      )),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                                PageTransitionType.rightToLeft,
                                            child: RegisterPageDoctor()));
                                  },
                                  child: FittedBox(
                                    fit: BoxFit.cover,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text("           ورود مشاور",
                                            softWrap: true,
                                            overflow: TextOverflow.fade,
                                            style: Conf.body1.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.black45,
                                              fontSize: Conf.p2t(15),
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
//                            Container(
//                              width: Conf.p2w(88),
//                              height: Conf.p2h(6.5),
//                              child: TextField(
//                                onTap: () {},
//                                controller: _controller4,
//                                textAlign: TextAlign.right,
//                                decoration: InputDecoration(
//                                    filled: true,
//                                    fillColor: Colors.white,
//                                    border: new OutlineInputBorder(
//                                      borderRadius: const BorderRadius.all(
//                                        const Radius.circular(13.0),
//                                      ),
//                                    ),
////                                filled: true,
//                                    hintStyle: Conf.body2.copyWith(
//                                        fontSize: Conf.p2t(15),
//                                        color: Colors.grey[500],
//                                        fontWeight: FontWeight.w900),
//                                    contentPadding: EdgeInsets.only(top: Conf.p2h(1.042)),
////                                new TextStyle(
////                                    fontSize: 14,
////                                    color: Colors.grey[500],
////                                    fontWeight: FontWeight.w900),
//                                    prefixIcon: Padding(
//                                        padding: EdgeInsets.only(right: Conf.p2w(28)),
//                                        child:
//                                            Image.asset(
//                                          "assets/images/googlesymbol.png",
//                                          width: Conf.p2w(4.167), // 20,
//                                        )),
//                                    hintText: '           ورود با اکانت گوگل'),
//                              ),
//                            ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              loading == false ? Conf.circularProgressIndicator() : Container()
            ],
          ),
        ));
  }

  void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 5),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.red,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              'خطا',
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
              'چنین شماره ای در سامانه ثبت نشده است...',
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
