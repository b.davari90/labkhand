import 'dart:ui';
import 'package:counsalter5/CounsalterAbout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:page_transition/page_transition.dart';
import 'config.dart';
import 'Collections.dart';

class DoctorCardComponent extends StatelessWidget {
  DoctorModel model;
  String msgNumber, hour, minute, birthdayDate;
  bool isOnline, hasShadow, hasUnSeenMsg, hasTime, hasLightShadow;
  Collections collection;
Function onTap;
  @override
  Widget build(BuildContext context) {
    print(Conf.p2h(10));
    return Padding(
        padding: EdgeInsets.only(),
        child: GestureDetector(
            onTap: () {
              onTap();
            },
            child: Container(
              padding: Conf.xlEdge,
              margin: EdgeInsets.only(
                right: Conf.p2w(5), //18,//
                left: Conf.p2w(5), //18,//Conf.p2h(2.1095)
                bottom: Conf.p2h(2.5), //7//
              ),
//                width:300,
              width: double.infinity,
//          height: Conf.p2w(20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey.withOpacity(0.2)),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(13),
                  boxShadow: [
                    BoxShadow(
                        color: hasShadow
                            ? hasLightShadow
                                ? Colors.grey.withOpacity(0.4)
                                : Colors.grey.withOpacity(0.3)
                            : Colors.transparent,
                        blurRadius: hasLightShadow ? 10 : 1,
                        spreadRadius: 0.4,
                        offset: Offset(-1, 2.7))
                  ]),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Stack(
                      overflow: Overflow.visible,
                      children: <Widget>[
                        Padding(
                          padding: Conf.smEdgeRight1 / 3.5,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Container(
                              color: Colors.grey.withOpacity(0.2),
                              width: Conf.p2w(14.5),
                              height: Conf.p2w(14.5),
                              child: Conf.image(model.avatar) ?? "",
                            ),
                          ),
                        ),
                        Positioned(
                          left: Conf.p2w(0.9), //4,//
                          top: Conf.p2w(12), //61,//
                          child: Container(
                            decoration: BoxDecoration(
                                color: isOnline
                                    ? Color.fromRGBO(1, 255, 0, 1.0)
                                    : Colors.transparent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50)),
                                border: isOnline
                                    ? Border.all(
                                        color: Colors.white,
                                        width: Conf.p2w(0.2345) //2//
                                        )
                                    : Border.all(color: Colors.transparent)),
                            width: Conf.p2w(2.5),
                            height: Conf.p2w(2.5),
                          ),
                        )
                      ],
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: Conf.p2h(1.2), right: Conf.p2w(1.5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
//                              color: Colors.red,
                              width: Conf.p2w(75),height: Conf.p2w(10),
//                              padding: EdgeInsets.only(right: Conf.p2w(4),left: Conf.p2w(4)),
                              child: Row(
                                children: <Widget>[
                                  Text(model.fname ?? "",
//                                    softWrap:true,
                                      overflow: TextOverflow.ellipsis,
                                      style: Conf.body1.copyWith(
                                          color: Colors.black,
                                          fontSize: Conf.p2t(16),
                                          fontWeight: FontWeight.w600)),
                                  Flexible(child: Text(" ")),
                                  Flexible(
                                    child: Text(model.lname ?? "",
//                                    softWrap: true,
                                        overflow: TextOverflow.ellipsis,
                                        style: Conf.body1.copyWith(
                                            color: Colors.black,
                                            fontSize: Conf.p2t(16),
                                            fontWeight: FontWeight.w600)),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Text("متخصص" + " ",
                                    softWrap: true,
                                    overflow: TextOverflow.fade,
                                    style: Conf.body2.copyWith(
                                        color:
                                            Colors.black87.withOpacity(0.5),
                                        fontSize: Conf.p2t(12),
                                        fontWeight: FontWeight.w700)
//                                txtStyleAboutDoctor
                                    ),
                                Text(model.specialty ?? "",
                                    softWrap: true,
                                    overflow: TextOverflow.fade,
                                    style: Conf.body2.copyWith(
                                        color:
                                            Colors.black87.withOpacity(0.5),
                                        fontSize: Conf.p2t(12),
                                        fontWeight: FontWeight.w700)
//                                txtStyleAboutDoctor
                                    )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Text(
                      hasTime
                          ? hour.toString() + ":" + minute.toString()
                          : minute,
                      style: TextStyle(fontSize: Conf.p2t(12)),
                    ),
                    IconButton(
                      padding: EdgeInsets.zero,
                      icon: hasUnSeenMsg
                          ? hasTime
                              ? CircleAvatar(
                                  backgroundColor: Conf.blue,
                                  maxRadius: 8,
                                  child: Center(
                                      child: Text(
                                    msgNumber.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: Conf.p2t(12)),
                                  )),
                                )
                              : CircleAvatar(
                                  backgroundColor: Colors.transparent,
                                  child: Text(""),
                                )
                          : Icon(Icons.navigate_next,
                              color: Conf.blue, size: Conf.p2w(9) //41 //
                              ),
                    ),
                  ]),
            )));
  }

  DoctorCardComponent(
      {this.model,
      this.msgNumber,
      this.hour,
        this.onTap,
      this.minute,
      this.birthdayDate,
      this.isOnline,
      this.hasShadow,
      this.hasUnSeenMsg,
      this.hasTime,
      this.hasLightShadow});
}
