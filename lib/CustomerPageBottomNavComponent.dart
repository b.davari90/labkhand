import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import "package:counsalter5/ModelBloc/CustomerChilderenModel.dart";
import 'config.dart';

class CustomerPageBottomNavComponent extends StatefulWidget {
  Function onTap;
  CustomerChilderenModel model;
  bool active=false;
  CustomerPageBottomNavComponent({this.model,this.active=false,this.onTap});
  @override
  _CustomerPageBottomNavComponentState createState() =>
      _CustomerPageBottomNavComponentState();
}
class _CustomerPageBottomNavComponentState
    extends State<CustomerPageBottomNavComponent> {
  @override
  Widget build(BuildContext context) {
    return
      GestureDetector(
          onTap: () {
            widget.onTap();
            setState(() {
              if (widget.onTap is Function) {
                widget.onTap();
                if (widget.active == false) {
                  print("widget.active===> ${ widget.active }");
                  widget.active = true;
                } else
                  widget.active = false;
                print("widget.active ===> ${ widget.active}");
              }
            });
          },
//          onTap: () {
//            setState(() {
//              if (widget.onTap is Function) {
//                widget.onTap();
//                print("widget.active ===> ${ widget.active }");
//                print("widget.onTap ===> ${ widget.onTap }");
//              }
//            }
//            );
//          },
        child: IntrinsicHeight(
          child: Container(
            margin: EdgeInsets.only(
                left: Conf.p2w(1), right: Conf.p2w(0.2), top: Conf.p2h(0.95)),
            width: Conf.p2w(24),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: Conf.p2w(12),
                    height: Conf.p2w(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                        border: widget.active==true?Border.all(color:Conf.orang):Border.all(color:Colors.transparent)),
//              borderRadius: widget.model.active?BorderRadius.all(Radius.circular(50)):BorderRadius.all(Radius.zero),
//                        border: widget.model.active?Border.all(width: 2,color: Conf.orang):Border.all(color:Colors.transparent)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(Conf.p2w(10.417)),
                      child: FadeInImage.assetNetwork(
                        placeholder: widget.model.avatar??"",
                        image:
                            "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
//                    color: Colors.red,
                    height: Conf.p2w(6),
                    child: Padding(
                      padding: EdgeInsets.only(top: Conf.p2h(0.418)),
                      child: Center(
                        child: FittedBox(
                          child: Text(widget.model.name,
                              overflow: TextOverflow.ellipsis,
//                            softWrap: true,
                              style: Conf.body2.copyWith(color: Colors.white)),
                        ),
                      ),
                    ),
                  ),
                ]),
          ),
        ));
  }
}
