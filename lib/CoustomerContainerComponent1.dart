import 'dart:ui';
import 'package:counsalter5/DoctorListPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'config.dart';
import 'package:counsalter5/ModelBloc/MyCounsalterListModel.dart';

class CoustomerContainerComponent1 extends StatelessWidget {
 MyCounsalterListModel model;
// String avatar,name,speciallity;
  TextStyle txtTitle2 = TextStyle(
      fontSize: Conf.p2t(12), color: Colors.black12.withOpacity(0.5));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: Conf.p2w(2.0839), right: Conf.p2w(2.0839)),
      child: GestureDetector(
        onTap: (){ Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    DoctorListPage()));},
        child: Container(
          margin: EdgeInsets.only(left: Conf.p2w(0.834) ),
          decoration: BoxDecoration(
            color: Conf.lightColor,
                border: Border.all(color:Colors.grey.withOpacity(0.2)),
              borderRadius: BorderRadius.all(Radius.circular(13)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  offset: Offset(-1,2.7), //10
                )
              ]),
          width: Conf.p2w(34),
          height: Conf.p2w(35),
          child: Padding(
            padding:EdgeInsets.only(bottom: Conf.mdSize),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding:EdgeInsets.only(top:Conf.p2w(2.918)),
                  child: FittedBox(
                    child: Container(
                      width: Conf.p2w(16),
                      height: Conf.p2w(16),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: FadeInImage.assetNetwork(
                          placeholder:
                          model.doctor.avatar??"",
                          image: "http://www.clker.com/cliparts/d/L/P/X/z/i/no-image-icon-md.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: Conf.p2h(1)),
                  child: Text(model.doctor.name??"",
                      softWrap: true,
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                          fontWeight: FontWeight.w800,
                          color: Colors.black87,
                          fontSize:Conf.p2t(14))),
                ),
                Padding(
                  padding:  EdgeInsets.only(top: Conf.p2h(0.5)),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    Text("متخصص",
                        softWrap: true,
                        overflow: TextOverflow.visible,
                        style:txtTitle2
//                      style:Conf.caption.copyWith( fontWeight: FontWeight.w600, color: Colors.black87)
                    ),Text(model.doctor.specialty??"",
                        softWrap: true,
                        overflow: TextOverflow.visible,
                        style:txtTitle2
//                      style:Conf.caption.copyWith( fontWeight: FontWeight.w600, color: Colors.black87)
                    ),],),
                ),
             ],
            ),
          ),
        ),
      ),
    );
  }

  CoustomerContainerComponent1({
    this.model,
  });
}
