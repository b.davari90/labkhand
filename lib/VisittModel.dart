//import 'TransactionModel.dart';
//import 'CustomerModel.dart';
//import 'package:flutter/cupertino.dart';
//import 'file:///D:/Android/flutter_project/flutter_counsalter4/lib/DoctorModel.dart';
//import 'file:///D:/Android/flutter_project/flutter_counsalter4/lib/DocumentModel.dart';
//
//class VisittModel {
//  String counsaltingKind, time, date,describtion,title , price;
//  IconData icon;
//  CustomerModel user;
//  DocumentModel document;
//  DoctorModel doctor;
//  TransactionModel transaction;
//
//  VisittModel({this.counsaltingKind,this.icon,this.document, this.transaction,this.describtion,this.title,this.user,this.doctor,this.date, this.price, this.time});
//
//  factory VisittModel.fromJson(Map<String, dynamic> json) {
//    try {
//      DoctorModel doctor;
//      if (json['doctor'] != null && json.containsKey('doctor')) {
//        doctor = DoctorModel.fromJson(json['doctor']);
//      }
//      DocumentModel document;
//      if (json['document'] != null && json.containsKey('document')) {
//        document = DocumentModel.fromJson(json['document']);
//      }
//      TransactionModel transaction;
//      if (json['transaction'] != null && json.containsKey('transaction')) {
//        transaction = TransactionModel.fromJson(json['transaction']);
//      }
//      return VisittModel(
//        counsaltingKind: json['counsaltingKind']??null,
//        date: json['date']??null,
//        price: json['price']??null,
//        time: json['time']??null,
//        user: json['user']??null,
//        describtion: json['describtion']??null,
//        title: json['title']??null,
//        icon: json['icon']??null,
//        doctor: json['doctor']??null,
//        document: json['document']??null,
//        transaction: json['transaction']??null,
//      );
//    } catch (e) {
//      throw 'Adv not valid model=> ${e}';
//    }
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['counsaltingKind'] = this.counsaltingKind;
//    data['date'] = this.date;
//    data['price'] = this.price;
//    data['time'] = this.time;
//    data['icon'] = this.icon;
//    data['user'] = this.user;
//    data['title'] = this.title;
//    data['doctor'] = this.doctor;
//    data['describtion'] = this.describtion;
//    data['document'] = this.document;
//    data['transaction'] = this.transaction;
//    return data;
//  }
//}