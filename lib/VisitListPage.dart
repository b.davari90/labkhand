import 'dart:ui';
import 'package:counsalter5/CustomerModel.dart';
import 'package:counsalter5/ModelBloc/DoctorVisitListModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:counsalter5/MasBackBotton_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'VisitListComponent.dart';
import 'package:badges/badges.dart';
import 'config.dart';
import 'package:counsalter5/ModelBloc/CustomerVisitLists.dart';
import 'package:counsalter5/htify.dart';
import 'package:counsalter5/ModelBloc/VisitModel.dart';
import 'Doctor/HomePageDoctor.dart';
import 'package:counsalter5/config.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params.dart';
import 'package:counsalter5/Doctor/pagination/query_list_params_result.dart';
import 'package:counsalter5/Doctor/pagination/search_adv_bloc.dart';
class VisitListPage extends StatefulWidget {
  @override
  _VisitListPageState createState() => _VisitListPageState();
}

class _VisitListPageState extends State<VisitListPage>
    with TickerProviderStateMixin {
  String awaiting = "منتظر ویزیت", reject = "رد شده", accept = "ویزیت شده";
  bool loading;
  int countAll,pagesAll,limitAll,pagesWaiting,limitWaiting,
      countDone,pagesDone,limitDone,countReject,pagesReject,limitReject
  ,countWaiting=0,stsWaiting,stsDone,stsReject,stsAll;
  TextStyle tabsTitleStyle = TextStyle(
      color: Colors.white, fontSize: Conf.p2t(17), fontWeight: FontWeight.bold);
  TextEditingController controller = TextEditingController();
  TabController _tabController;
  List<VisitModel> visitModelListAll,visitModelListDone,visitModelListReject,visitModelListWainting;
  DoctorVisitListModel doctorVisitListModel;
  SearchAdvCubit get searchBloc => context.bloc<SearchAdvCubit>();
  ScrollController scWaiting,scAll,scDone,scReject;
  QueryListParams qlpAll,qlpWait,qlpReject,qlpDone;
  QueryListParamsResult listResult;
  List<Widget> taskList;
  CustomerVisitListsModel customerVisitListModel=CustomerVisitListsModel();
  CustomerVisitListsBloc get coustomerVisitListBloc => context.bloc<CustomerVisitListsBloc>();

  Future getAllCustomerVisitList() async {
    try {
      setState(() {
        loading = false;
      });
      var data = await Htify.create("cu/visit/list",{"sts":"","page":qlpAll.page,"limit":"15"});
      doctorVisitListModel= DoctorVisitListModel.fromJson(data);
      countAll = doctorVisitListModel.count;
      limitAll = doctorVisitListModel.limit;
      pagesAll = (countAll/limitAll).ceil();
      stsAll = doctorVisitListModel.sts;
      print("pagesAll getAllCustomerVisitList===> ${ pagesAll }");
      visitModelListAll.addAll(doctorVisitListModel.rows);
      print("visitModelListAll.m ===> ${ visitModelListAll.map((e) => e) }");
    } catch (e) {
      print("CatchError getAllCustomerVisitList()===> ${e.toString()}");
    } finally {
      setState(() {
        loading = true;
      });
    }
  }
  Future getWaitingCustomerVisitList() async {
    try {
      setState(() {
//        loading = true;
      });
      var data = await Htify.create("cu/visit/list",{"sts":2,"limit":"15"});
      doctorVisitListModel= DoctorVisitListModel.fromJson(data);
      countWaiting = doctorVisitListModel.count;
      limitWaiting = doctorVisitListModel.limit;
      pagesWaiting = doctorVisitListModel.page;
      stsWaiting = doctorVisitListModel.sts;
      visitModelListWainting.addAll(doctorVisitListModel.rows);
      print("visitModelListWainting.m ===> ${ visitModelListWainting.map((e) => e) }");
    } catch (e) {
      print("CatchError getWaitingCustomerVisitList()===> ${e.toString()}");
    } finally {
      setState(() {
//        loading = false;
      });
    }
  }
  Future getRejectCustomerVisitList() async {
    try {
      setState(() {
//        loading = true;
      });
      var data = await Htify.create("cu/visit/list",{"sts":3,"limit":"15"});
      doctorVisitListModel= DoctorVisitListModel.fromJson(data);
      countReject = doctorVisitListModel.count;
      limitReject = doctorVisitListModel.limit;
      pagesReject = doctorVisitListModel.page;
      stsReject = doctorVisitListModel.sts;
      visitModelListReject.addAll(doctorVisitListModel.rows);
      print("visitModelListReject ===> ${ visitModelListReject.map((e) => e) }");
    } catch (e) {
      print("CatchError getRejectCustomerVisitList()===> ${e.toString()}");
    } finally {
      setState(() {
//        loading = false;
      });
    }
  }
  Future getDoneCustomerVisitList() async {
    try {
      setState(() {
//        loading = true;
      });
      var data = await Htify.create("cu/visit/list",{"sts":6,"limit":"15"});
      doctorVisitListModel= DoctorVisitListModel.fromJson(data);
      countDone = doctorVisitListModel.count;
      limitDone = doctorVisitListModel.limit;
      pagesDone = doctorVisitListModel.page;
      stsDone =doctorVisitListModel.sts;
      visitModelListDone.addAll(doctorVisitListModel.rows);
      print("doctorVisitListModel ===> ${ visitModelListDone.map((e) => e) }");
    } catch (e) {
      print("CatchError getDoneCustomerVisitList()===> ${e.toString()}");
    } finally {
      setState(() {
//        loading = false;
      });
    }
  }
  @override
  void initState() {
    _tabController = new TabController(length: 4, vsync: this);
    visitModelListAll=[];
    visitModelListDone=[];
    visitModelListWainting=[];
    visitModelListReject=[];
    doctorVisitListModel=DoctorVisitListModel();

    qlpAll = QueryListParams();
    qlpDone = QueryListParams();
    qlpReject = QueryListParams();
    qlpWait = QueryListParams();

    listResult = QueryListParamsResult();

    scAll = ScrollController();
    scDone = ScrollController();
    scReject = ScrollController();
    scWaiting = ScrollController();

    qlpAll.page = 0;
    qlpWait.page = 0;
    qlpReject.page = 0;
    qlpDone.page = 0;

    searchBloc.setQlp(qlpAll);
    searchBloc.setQlp(qlpDone);
    searchBloc.setQlp(qlpReject);
    searchBloc.setQlp(qlpWait);

    scAll.addListener(() {
      if (scAll.offset >= scAll.position.maxScrollExtent &&
          !scAll.position.outOfRange &&
          qlpAll.page < pagesAll) {
        setState(() {
          qlpAll.page++;
          searchBloc.setQlp(qlpAll);
          getAllCustomerVisitList();
        });
      }
    });
    scReject.addListener(() {
      if (scReject.offset >= scReject.position.maxScrollExtent - 100 &&
          !scReject.position.outOfRange &&
          qlpReject.page < pagesReject) {
        setState(() {
          qlpReject.page++;
          searchBloc.setQlp(qlpReject);
          getRejectCustomerVisitList();
        });
      }
    });
    scDone.addListener(() {
      if (scDone.offset >= scDone.position.maxScrollExtent - 100 &&
          !scDone.position.outOfRange &&
          qlpDone.page < pagesDone) {
        setState(() {
          qlpDone.page++;
          searchBloc.setQlp(qlpDone);
          getDoneCustomerVisitList();
        });
      }
    });
    scWaiting.addListener(() {
      if (scWaiting.offset >= scWaiting.position.maxScrollExtent - 100 &&
          !scWaiting.position.outOfRange &&
          qlpWait.page < pagesWaiting) {
        setState(() {
          qlpWait.page++;
          searchBloc.setQlp(qlpWait);
          getWaitingCustomerVisitList();
        });
      }
    });
    getAllCustomerVisitList();
    getDoneCustomerVisitList();
    getRejectCustomerVisitList();
    getWaitingCustomerVisitList();
    print("____widget.DR_id ===> ${Conf.idLoginDoctor}");
    print("____widget.id ===> ${Conf.idLogin}");
    print("Conf.token ===> ${ Conf.token }");
    super.initState();
  }
  @override
  void dispose() {
    _tabController.dispose();
    scReject.dispose();
    scDone.dispose();
    scAll.dispose();
    scWaiting.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child:
      Stack(
        children: [
          BlocBuilder<CoustomerBloc, CoustomerRepository>(
          builder: (context, state){
        return Scaffold(
              body: DefaultTabController(
                length: 4,
                child: SafeArea(
                  top: true,
                  bottom: true,
                  child: Scaffold(
                      body: Stack(
                        children: [
                          Container(
                            width: double.infinity,
                            height: Conf.p2h(100),
                            color: Conf.blue,
                          ),
                          Positioned(
                            width: Conf.p2w(100),
                            top: Conf.p2h(2.5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("لیست ویزیت ها",
                                    style: Conf.headline.copyWith(
                                        color: Colors.white,
                                        fontSize: Conf.p2t(25),
                                        fontWeight: FontWeight.w500,
                                        decoration: TextDecoration.none)),
                              ],
                            ),
                          ),
                          Positioned(
                            top: Conf.p2h(2.5),
                            left: Conf.p2w(6),
                            child: Column(
                              children: [
                                MasBackBotton()
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: Conf.p2w(15)),
                            decoration: BoxDecoration(
                              color: Conf.blue,
                            ),
                            width: double.infinity,
                            child: Column(
                              children: <Widget>[
                                TabBar(
                                  isScrollable: true,indicatorColor: Conf.orang,
                                  indicatorPadding:EdgeInsets.zero,indicatorWeight: Conf.p2w(1),
                                  labelPadding: EdgeInsets.symmetric(horizontal: Conf.xlSize+5),
                                  tabs: [
                                    Tab(
                                      child: Badge(badgeColor: Conf.orang,
                                        badgeContent: Text(countWaiting.toString()),
                                        position: BadgePosition(bottom: Conf.p2w(2.5),end: Conf.p2w(-2)),
                                        child: Text("منتظر تایید", style: tabsTitleStyle),
                                      ),
                                    ),Tab(
//                              text: "همه",
                                      child: Text("همه", style: tabsTitleStyle),
                                    ),
                                    Tab(
                                      child: Text("ویزیت شده", style: tabsTitleStyle),
                                    ),
                                    Tab(
                                      child: Text("رد شده", style: tabsTitleStyle),
                                    ),

                                  ],
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    color: Colors.white,
                                    child: TabBarView(
                                      children: <Widget>[
                                        SingleChildScrollView(
                                          controller: scWaiting,
                                          child:
                                          Column(
                                              children: visitModelListWainting==[]||visitModelListWainting.length==0?
                                              [Center(child: Padding(
                                                padding:EdgeInsets.only(top: Conf.p2h(30),bottom: Conf.p2h(30)),
                                                child: Text("اطلاعاتی موجود نمی باشد"),
                                              ))]:
                                              visitModelListWainting.map((e) {
                                                return VisitListComponent(
                                                    isFirstItem: visitModelListWainting.indexOf(e) < 1
                                                        ? true
                                                        : false,
                                                    hasShadow: true,
                                                    sts: e.sts,
                                                    icon: e.type,
                                                    date: e.dtj.split("-").first,
                                                    hasDate: true,
                                                    hasPicture: true,
                                                    isOnline: false,
                                                    model: e,
                                                    onTap: (key) {
                                                      Navigator.push(
                                                          context,
                                                          PageTransition(
                                                              type: PageTransitionType.leftToRight,
                                                              duration: Duration(milliseconds: 500),
                                                              alignment: Alignment.centerLeft,
                                                              curve: Curves.easeOutCirc,
                                                              child: HomePageDoctor()
                                                          ));
                                                    });
                                              }).toList()),
                                        ),
                                        Column(
                                          children: [
                                            Expanded(
                                              child: SingleChildScrollView(
                                                controller: scAll,
                                                child: Column(
                                                    children:  loading == true && visitModelListAll.isEmpty ?
                                                [Center(child: Padding(
                                                  padding:EdgeInsets.only(top: Conf.p2h(30),bottom: Conf.p2h(30)),
                                                  child: Text("اطلاعاتی موجود نمی باشد"),
                                                ))]:
                                                    [Column(
                                                      children:visitModelListAll.map((e) {
                                                        return VisitListComponent(
                                                            isFirstItem: visitModelListAll.indexOf(e) < 1
                                                                ? true
                                                                : false,
                                                            hasShadow: true,
                                                            date: e.dtj.split("-").first,
                                                            model: e,
                                                            icon: e.type,
                                                            hasDate: true,
                                                            sts: e.sts,
                                                            hasPicture: true,
                                                            isOnline: false,
                                                            onTap: (key) {
                                                            });
                                                      }).toList() ,
                                                    ),loading == false && visitModelListAll.isNotEmpty ? Align(
                                                      alignment: Alignment.topCenter,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(bottom: Conf.p2w(20)),
                                                        child: Conf.circularProgressIndicator(),
                                                      ),
                                                    ) :Container()]),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SingleChildScrollView(
                                          controller: scDone,
                                          child: Column(
                                              children: visitModelListDone==[]||visitModelListDone.length==0?
                                              [Center(child: Padding(
                                                  padding:EdgeInsets.only(top: Conf.p2h(30),bottom: Conf.p2h(30)),
                                                child: Text("اطلاعاتی موجود نمی باشد"),
                                              ))]:visitModelListDone.map((e) {
                                                return VisitListComponent(
                                                    isFirstItem: visitModelListDone.indexOf(e) < 1
                                                        ? true
                                                        : false,
                                                    hasDate: true,
                                                    hasShadow: true,
                                                    date: e.dtj.split("-").first,
                                                    hasPicture: true,
                                                    icon: e.type,
                                                    isOnline: false,
                                                    model: e,
                                                    sts: e.sts,
                                                    onTap: (key) {
                                                    });
                                              }).toList()),
                                        ),
                                        SingleChildScrollView(
                                          controller: scReject,
                                          child: Column(
                                              children: visitModelListReject==[]||visitModelListReject.length==0?
                                              [Center(child: Padding(
                                                padding:EdgeInsets.only(top: Conf.p2h(30),bottom: Conf.p2h(30)),
                                                child: Text("اطلاعاتی موجود نمی باشد",
                                                  style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,
                                                      color:Colors.black45),),
                                              ))]:visitModelListReject.map((e) {
                                                return VisitListComponent(
                                                    isFirstItem: visitModelListReject.indexOf(e) < 1
                                                        ? true
                                                        : false,
                                                    hasShadow: true,
                                                    date: e.dtj.split("-").first,
//                                          isNew: false,
                                                    model: e,
                                                    icon: e.type,
                                                    hasDate: true,
                                                    sts: e.sts,
                                                    hasPicture: true,
                                                    isOnline: false,
                                                    onTap: (key) {
//                                      Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft,
//                                          child: HomePageDoctor()));
//                                        setState(() {});
                                                    });
                                              }).toList()),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )),
                ),
              ),
            );
            })
      ,loading==false &&visitModelListAll.isEmpty ?Conf.circularProgressIndicator():Container()  ],
      ),
    );
//      ),
//    );
  }
}
