import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'config.dart';
import 'package:counsalter5/ModelBloc/TransactionModel.dart';
class TransactionCardComponent extends StatelessWidget {
  TransactionModel model;
  int sts;
  Function onTap;
  bool hasShadow, hasDate;
  bool isFirstItem;

  TransactionCardComponent({this.sts,this.model, this.onTap, this.isFirstItem=false,this.hasShadow, this.hasDate});
  TextStyle txtStyle=Conf.body1.copyWith(
    color: Colors.black,
    fontSize: Conf.p2t(16.5),
    fontWeight: FontWeight.w600);
  TextStyle priceStyle=Conf.body1.copyWith(
      color: Colors.black,
      fontSize: Conf.p2t(14),
      fontWeight: FontWeight.w600);
  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
        child: Padding(
          padding:isFirstItem?EdgeInsets.only(top: Conf.p2h(10)):EdgeInsets.only(top: Conf.p2h(2)),
          child: GestureDetector(
            onTap: (){
//              Navigator.push(
//                  context,
//                  PageTransition(
//                      type: PageTransitionType.leftToRight,
//                      duration: Duration(milliseconds: 500),
//                      alignment: Alignment.centerLeft,
//                      curve: Curves.easeOutCirc,
//                      child: StatusReserveCoustomerPage()
//                  ));
            },
            child: Container(
//            padding: Conf.xlEdge,
                margin: EdgeInsets.only(
                  right: Conf.p2w(5), //18,//
                  left: Conf.p2w(5), //18,//Conf.p2h(2.1095)
                  bottom: Conf.p2h(0.5), //7//
//                  top: Conf.p2h(2.5), //7//
                ),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(13),
                    boxShadow: [
                      BoxShadow(
                          color: hasShadow
                              ? Colors.grey.withOpacity(0.4)
                              : Colors.white,
                          blurRadius: 10,
                          spreadRadius: 0.1,
                          offset: Offset(-1, 1))
                    ]),
                child: Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: Conf.p2w(0.1),top: Conf.p2h(0.1)),
                        width: Conf.p2w(2),
                        decoration: BoxDecoration(
                            color:sts==1?Colors.red:sts==2?Conf.greencontainerColor:Colors.transparent,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(Conf.p2w(3.125)),
                                bottomRight: Radius.circular(Conf.p2w(3.125)))),
                      ),
                      Column(
                        crossAxisAlignment:CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: Conf.p2h(2),
                            ),
                            child: Container(width: Conf.p2w(82),
//                              color: Colors.yellowAccent,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.cover,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding:EdgeInsets.only(right: Conf.p2w(1)),
                                          child: Text(model.amount.toString(),
                                              style:priceStyle
                                          ),
                                        ),
                                        Text(" تومان ",style: priceStyle,),
                                        Text(""+model.sts_str=="کم شده"?"کاهش موجودی":model.sts_str=="اضافه شده"?"افزایش موجودی":"",
                                            softWrap: true,
                                            overflow: TextOverflow.fade,
                                            style:txtStyle
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: Conf.p2h(3)),
                                    child:
                                    hasDate ?Row(
                                      children: [
                                        Text(
                                           model.caj.split("-").last,
                                          style: TextStyle(
                                              fontSize: Conf.p2w(2),
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black45),
                                        ),Text(
                                         " - ",
                                          style: TextStyle(
                                              fontSize: Conf.p2w(2),
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black45),
                                        ),
                                        Text(
                                            model.caj.split("-").first,
                                          style: TextStyle(
                                              fontSize: Conf.p2w(2),
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black45),
                                        ),
                                      ],
                                    ):"",
                                  ),

                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: Conf.p2h(1),right: Conf.p2w(1.5),bottom:Conf.p2h(4),),
                            child: Row(
                              children: [
                                Text(model.sts_str=="کم شده"?" کاهش موجودی ":model.sts_str=="اضافه شده"?" افزایش موجودی ":"",
                                    softWrap: true,
                                    overflow: TextOverflow.visible,
                                    style: Conf.body1.copyWith(
                                        color: Colors.grey,
                                        fontSize: Conf.p2t(14),
                                        fontWeight: FontWeight.w600)),
                                Text(model.note,
                                    softWrap: true,
                                    overflow: TextOverflow.visible,
                                    style: Conf.body1.copyWith(
                                        color: Colors.grey,
                                        fontSize: Conf.p2t(14),
                                        fontWeight: FontWeight.w600)),
                              ],
                            ),
                          )
                        ],
                      ),

//
                   ])),
          ),
        ));
  }

}
