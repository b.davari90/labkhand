import 'package:counsalter5/PriceModel.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'CounsalterPriceROwComponent.dart';
import 'CounsalterReserve.dart';
import 'config.dart';
import 'raised_button_ui.dart';
import 'package:counsalter5/htify.dart';
import 'DoctorModel.dart';
import 'package:counsalter5/ModelBloc/DoctorPriceModel.dart';
import 'package:counsalter5/DoctorModel.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:counsalter5/raised_button_ui.dart';
import 'package:counsalter5/ModelBloc/LikeListModelList.dart';
import 'package:counsalter5/ModelBloc/LikeListModel.dart';

class CounsalterPrice extends StatefulWidget {
  int id;
  List<int> priceLst;
  List<int> typeLst;
  int price, type;

  @override
  _CounsalterPriceState createState() => _CounsalterPriceState();
}

class _CounsalterPriceState extends State<CounsalterPrice> {
  TextStyle txtStyletitle = Conf.subtitle.copyWith(
      fontSize: 28, fontWeight: FontWeight.w700, color: Colors.black87);
  TextStyle txtStyleSubtitle = Conf.subtitle.copyWith(
      fontSize: 17, fontWeight: FontWeight.w700, color: Colors.black54);
  TextStyle txtStyleRate = TextStyle(fontWeight: FontWeight.bold, fontSize: 18);

  List<PriceModel> dataList;
  List<Widget> taskList;
  int rowId, likeCnt;
  bool loading = true, loading1 = true;
  DoctorModel s = DoctorModel();
  List<DoctorModel> sList;
  List<DoctorPriceModel> doctorPrices;
  LikeListModelList likeListModelList;
  List<LikeListModel> row;
  List likeListInArrayCount;

  DoctorBloc get doctorBloc => context.bloc<DoctorBloc>();

  Future getData() async {
    try {
      setState(() {
        loading = true;
      });
      var data = await Htify.one('cu/doctor', Conf.idLoginDoctor);
      print("____widget.id ===> ${Conf.idLoginDoctor}");
      print("data getData()===> ${data}");
      s = DoctorModel.fromJson(data);

      List r = s.price;
      print("____r ===> ${r}");
      setState(() {
        print("105000");
        doctorPrices.addAll(s.price);
        print("doctorPrices.ma ===> ${doctorPrices.map((e) => e.price)}");
        print("doctorPrices.ma ===> ${doctorPrices.map((e) => e.type)}");
//        doctorPrices.addAll(r.map((e) => DoctorPriceModel.fromJson(e)).toList());
//        print("doctorPrices fee ===> ${ doctorPrices.map((e) => e.fee) }");
//        print("1200  ok");
      });
      doctorBloc.setDoctor(s);
    } catch (e) {
      print("e.toString() ===> ${e.toString()}");
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  Future setLike() async {
    setState(() {
      loading1 = true;
    });
    try {
      var data =
          await Htify.create("cu/pin", {"doctor_id": Conf.idLoginDoctor});
      print("setLike data ===> ${data}");
      getLikeListInArray();
    } catch (e) {
      print("CAtch ERROR setLike() ===> ${e.toString()}");
    } finally {
      setState(() {
        loading1 = false;
      });
    }
  }

  Future getLikeListInArray() async {
    try {
      setState(() {
        loading1 = true;
      });
      var data = await Htify.get('cu/pin/list');
      likeListInArrayCount = data;
      print("getLikeListInArray *** data ===> ${data}");
      print("likeListInArrayCount ===> ${likeListInArrayCount.map((e) => e)}");
    } catch (e) {
      print("CATCH ERROR getLikeListInArray===> ${e.toString()}");
    } finally {
      setState(() {
        loading1 = false;
      });
    }
  }

//  Future getLikeList() async {
//    try {
//      setState(() {
//       loading1 = true;
//      });
//      var data = await Htify.get('cu/pin');
//      print("getLikeList *** data ===> ${ data }");
//      likeListModelList=LikeListModelList.fromJson(data);
//      List x=likeListModelList.rows.map((item) => item).toList();
//      print("___x ===> ${ x.map((e) => e) }");
//      row=x.map((e) => LikeListModel.fromJson(e)).toList();
//      print("** ++++row ===> ${ row.map((e) => e.doctor.countLike) }");
//    } catch (e) {
//      print("CATCH ERROR getLikeList===> ${e.toString()}");
//    } finally {
//      setState(() {
//        loading1 = false;
//      });
//    }
//  }
  Future deleteLike() async {
    setState(() {
      loading1 = true;
    });
    try {
      var x = await Htify.delete("cu/pin", Conf.idLoginDoctor);
      print("deleteLike ===> ${x}");
      getLikeListInArray();
    } catch (e) {
      print("deleteLike CATCH ERROR  ===> ${e.toString()}");
    } finally {
      setState(() {
        loading1 = false;
      });
    }
  }

  int index;
  bool isSelectedLike, isSelectedRow;

  @override
  void initState() {
    print("Conf.token ===> ${Conf.token}");
    print("Conf.idLoginDoctor ===> ${Conf.idLoginDoctor}");
    print("Conf.idLogin ===> ${Conf.idLogin}");
    sList = [];
    row = [];
    widget.priceLst = [];
    widget.typeLst = [];
    isSelectedRow = false;
    doctorPrices = [];
    likeListModelList = LikeListModelList();
    if (likeListModelList.rows == null) {
      likeListModelList.rows = [];
    }
    likeListInArrayCount = [];
    getLikeListInArray();
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Stack(children: [
      Positioned(
        top: Conf.p2h(0),
        right: Conf.p2w(0),
        left: Conf.p2w(0),
        child: Container(
            width: Conf.p2w(100),
            height: Conf.p2h(35),
            child: Image.asset("assets/images/counsalterAbout.png" ??
                "https://th.bing.com/th/id/OIP.MVPo7ij6L0D4_7tmS53kKAHaE7?pid=Api&rs=1")),
      ),
      Positioned(
        top: Conf.p2h(4.167),
        left: Conf.p2w(4.167),
        child: Container(
          width: Conf.p2w(8.3),
          height: Conf.p2h(4.5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Conf.orang,
          ),
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: IconButton(
                icon: Icon(Icons.navigate_next,
                    size: Conf.p2w(5), //23,
                    color: Colors.white),
                onPressed: () {
                  Navigator.pop(context);
                }),
          ),
        ),
      ),
      Positioned(
        right: Conf.p2w(0),
        left: Conf.p2w(0),
        bottom: Conf.p2h(0),
        child: Container(
          height: Conf.p2h(65),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 8,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30))),
        ),
      ),
      Positioned(
        top: Conf.p2h(61),
        left: Conf.p2w(0),
        right: Conf.p2w(2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(right: Conf.p2w(4.167)),
              child: Row(
                children: [
                  Text(" مشاور", style: txtStyletitle),
                  Text(" حسین", style: txtStyletitle),
                  Text(" کلایی", style: txtStyletitle)
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: Conf.p2w(4.167)),
              child: Row(
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: Conf.p2w(1.042)),
                        child: Icon(
                          Icons.star,
                          color: Colors.yellow.shade600,
                          size: 25,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: Conf.p2h(1.042)),
                    child: Row(
                      children: [
                        Text(
                          "(",
                          style: txtStyleRate,
                        ),
                        Text(
                          "10 ",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          " نظر",
                          style: TextStyle(
                              fontSize: 13, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          " )",
                          style: txtStyleRate,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      Positioned(
        bottom: Conf.p2w(0),
        right: Conf.p2w(0),
        left: Conf.p2w(0),
        child: Container(
          height: Conf.p2h(51), //50
          decoration: BoxDecoration(
            color: Conf.blue,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 8,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                  padding: EdgeInsets.only(top: Conf.p2h(1)),
                  width: Conf.p2w(90),
                  child: Icon(
                    FeatherIcons.chevronDown,
                    color: Colors.white,
                    size: 24,
                  )),
            ),
            Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: doctorPrices
                    .map((e) => Padding(
                          padding: EdgeInsets.only(top: Conf.p2h(0.5)),
                          child: GestureDetector(
                            child: CounsalterPriceROwComponent(
                              onTap: () {
                                setState(() {
                                  widget.id = e.doctor_id;
                                  print("widget.id ===> ${widget.id}");
                                  index = doctorPrices.indexOf(e);
                                  if (isSelectedRow == false) {
                                    isSelectedRow = true;
                                    print(
                                        "isSelectedRow ===> ${isSelectedRow}");
                                    widget.type = e.type;
                                    widget.price = e.price;
                                    doctorBloc.setPriceDrType(widget.price);
                                    print("widget.price ===> ${widget.price}");
                                    print("widget.type ===> ${widget.type}");
                                  }
                                }
                                    ////////////////////////////////چند انتخابی
//                                    setState(() {
//                                      widget.id=e.doctor_id;
//                                      print("widget.id ===> ${ widget.id }");
//                                      index=doctorPrices.indexOf(e);
//                                      if(index==doctorPrices.indexOf(e)){
//                                        isSelectedRow=true;
//                                        print("isSelectedRow ===> ${ isSelectedRow }");
//                                        if(!widget.typeLst.contains(e.type)){
//                                         widget.priceLst.add(e.price);
//                                         widget.typeLst.add(e.type);
//                                         print("widget.priceLst add ===> ${ widget.priceLst.map((e) => e) }");
//                                         print("widget.typeLst add ===> ${ widget.typeLst.map((e) => e) }");
//                                         }else{
//                                        isSelectedRow=false;
//                                        if(widget.typeLst.contains(e.type)){
//                                        widget.priceLst.remove(e.price);
//                                          widget.typeLst.remove(e.type);
//                                        print("widget.priceLst remove ===> ${ widget.priceLst.map((e) => e) }");}
//                                        print("widget.typeLst remove ===> ${ widget.typeLst.map((e) => e) }");
//                                      print("isSelectedRow ===> ${ isSelectedRow }");}
//                                    }}
                                    /////////////////
                                    );
                              },
                              model: e,
                              isSelectedRow: isSelectedRow &&
                                  index == doctorPrices.indexOf(e),
                            ),
                          ),
                        ))
                    .toList())
          ]),
        ),
      ),
      Positioned(
        bottom: Conf.xlSize,
        right: Conf.xlSize,
        left: Conf.xlSize,
        child: Padding(
          padding: EdgeInsets.only(top: Conf.p2w(4)),
          child: Flex(
            direction: Axis.vertical,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: Conf.p2w(0.5)),
                    child: Row(
                      children: [
                        Text("( ",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Conf.p2t(12),
//                                    fontSize: 20
                            )),
                        Text((likeListInArrayCount.length).toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Conf.p2t(17),
//                                    fontSize: 20
                            )),
                        Text(" ) ",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Conf.p2t(12),
//                                    fontSize: 20
                            )),
                      ],
                    ),
                  ),
                  FittedBox(
                    fit: BoxFit.cover,
                    child: GestureDetector(
                      onTap: () {
                        if (!likeListInArrayCount.contains(Conf.idLoginDoctor)) {
                          setState(() {
                            print("likeListInArrayCount *Doesnt* contain doctor Id");
                            print("add....");
                            getLikeListInArray();
                              setLike();
//                              getLikeListInArray();
                            _showTopFlash(context,"","مشاور مورد نظر شما در لیست علاقه مندی قرار گرفت",Colors.lightGreen);
//                            }
                          });
                        } else {
                          print("likeListInArrayCount *contain* doctor Id");
                          setState(() {
                            print("delete...");
                              deleteLike();
                            _showTopFlash(context,"","مشاور مورد نظر شما از لیست علاقه مندی حذف شد",Colors.red);
//                            }
                          });
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(Conf.p2t(Conf.p2w(2.71))),
                          border: Border.all(
                              color: likeListInArrayCount!=[]&& likeListInArrayCount.contains(Conf.idLoginDoctor) ? Conf.orang : Colors.white,
                              width: 2),
                        ),
                        width: Conf.p2w(8),
                        height: Conf.p2h(4.5),
                        child: Icon(
                          likeListInArrayCount!=[]&& likeListInArrayCount.contains(Conf.idLoginDoctor) ? Icons.favorite : FeatherIcons.heart,
                          color: Colors.white,
                          size: Conf.p2h(3),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Flex(
                      direction: Axis.vertical,
                      children: [
                        Container(
                            width: Conf.p2w(77),
                            height: Conf.p2w(17),
                            padding: EdgeInsets.only(right: Conf.p2w(3)),
                            child: MasRaisedButton(
                              margin: EdgeInsets.only(
                                  right: Conf.p2w(0),
                                  left: Conf.p2w(0.5),
                                  top: Conf.p2w(1.5),
                                  bottom: Conf.p2w(3)),
                              borderSideColor: Conf.orang,
                              text: "تائید و ادامه رزرو",
                              color: Conf.orang,
                              textColor: Colors.white,
                              onPress: () {
                                isSelectedRow == false
                                    ? _showTopFlash(context,"خطا","نوع مشاوره خود را انتخاب کنید",Colors.red)
                                    : Navigator.push(
                                        context,
                                        PageTransition(
                                            type:
                                                PageTransitionType.leftToRight,
                                            duration:
                                                Duration(milliseconds: 500),
                                            alignment: Alignment.centerLeft,
                                            curve: Curves.easeOutCirc,
                                            child: CounsalterReserve(
                                                id: widget.id,
                                                type: widget.type)
//                                                child: CounsalterReserve(id:widget.rowId)
                                            ));
                              },
                            )
//                                    RaisedButton(
//                                        onPressed: () {
//                                          Navigator.push(
//                                              context,
//                                              PageTransition(
//                                                  type: PageTransitionType.leftToRight,
//                                                  duration: Duration(milliseconds: 500),
//                                                  alignment: Alignment.centerLeft,
//                                                  curve: Curves.easeOutCirc,
//                                                  child: CounsalterReserve()
//                                              ));
//                                        },
//                                        color: Conf.orang,
//                                        child: FittedBox(
//                                          fit: BoxFit.cover,
//                                          child: Text(
//                                            "تائید و ادامه رزرو",
//                                            style: TextStyle(
//                                                fontSize: Conf.p2t(20),
////                                  fontSize: 24,
//                                                color: Colors.white,
//                                                fontWeight: FontWeight.w500),
//                                          ),
//                                        ),
//                                        shape: new RoundedRectangleBorder(
//                                            borderRadius:
//                                            new BorderRadius.circular(13)))),
                            )
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
      loading == true && loading1 == true
          ? Conf.circularProgressIndicator()
          : Container()
    ])));
  }

  void _showTopFlash(BuildContext cnt,String title1,String title2,Color color,
      {FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: cnt,
      duration: const Duration(seconds:3),
      persistent: true,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: color,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            icon: Padding(
              padding: EdgeInsets.only(right: Conf.p2w(2)),
              child: Icon(
                FeatherIcons.alertOctagon,
                color: Colors.white,
                size: 40,
              ),
            ),
            title: Text(
              title1,
              style: TextStyle(
                  fontSize: Conf.p2t(14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            message: Text(
            title2,
              style: TextStyle(
                  fontSize: Conf.p2t(12),
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            showProgressIndicator: false,
//            primaryAction: FlatButton(
//              onPressed: () => controller.dismiss(),
//              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
//            ),
          ),
        );
      },
    );
  }
}
